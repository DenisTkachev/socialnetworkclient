//
//  UITests.swift
//  UITests
//
//  Created by Denis Tkachev on 19/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import XCTest

class UITests: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        setupSnapshot(app)
        app.launch()
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
//        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        
//        snapshot("01LoginScreen")
//        app.staticTexts["Войти"].tap()
//        sleep(1)
//        snapshot("01TabScreen")
//
//        let tableFirstCell = app.tables.cells.firstMatch
//        tableFirstCell.tap()
//        sleep(1)
//        snapshot("01UserPage")
//
//        let userPageTable = app.tables.firstMatch
//        userPageTable.swipeUp()
//        snapshot("02UserPage")
//        userPageTable.swipeUp()
//        snapshot("03UserPage")
//
//        app.tabBars.children(matching: .button).element(boundBy: 1).tap()
//        sleep(1)
//        snapshot("01TabMessage")
//
//        app.tabBars.children(matching: .button).element(boundBy: 2).tap()
//        sleep(1)
//        snapshot("01TabLikes")
//
//        app.tabBars.children(matching: .button).element(boundBy: 3).tap()
//        sleep(1)
//        snapshot("01TabReview")
//
//        app.tabBars.children(matching: .button).element(boundBy: 4).tap()
//        snapshot("01TabMenu")
        
//        let tablesQuery = tablesQuery2
//        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Написать в блог"]/*[[".cells.staticTexts[\"Написать в блог\"]",".staticTexts[\"Написать в блог\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
//        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Поиск"]/*[[".cells.staticTexts[\"Поиск\"]",".staticTexts[\"Поиск\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
//        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Выход"]/*[[".cells.staticTexts[\"Выход\"]",".staticTexts[\"Выход\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeDown()
//        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Мои лайки"]/*[[".cells.staticTexts[\"Мои лайки\"]",".staticTexts[\"Мои лайки\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeDown()
//        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Мария-Мария, 32"]/*[[".cells.staticTexts[\"Мария-Мария, 32\"]",".staticTexts[\"Мария-Мария, 32\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        tablesQuery/*@START_MENU_TOKEN@*/.buttons["Как видят мою страницу"]/*[[".cells.buttons[\"Как видят мою страницу\"]",".buttons[\"Как видят мою страницу\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
//        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .table).element.swipeUp()
//        tablesQuery2/*@START_MENU_TOKEN@*/.cells.containing(.button, identifier:"Все записи")/*[[".cells.containing(.staticText, identifier:\"Машка\")",".cells.containing(.staticText, identifier:\"2 Записи\")",".cells.containing(.staticText, identifier:\"Блог \")",".cells.containing(.staticText, identifier:\"Напишите в блог\")",".cells.containing(.button, identifier:\"30\")",".cells.containing(.button, identifier:\"1 комментарий\")",".cells.containing(.button, identifier:\"3 лайка\")",".cells.containing(.button, identifier:\"userPage blogMoreBtn\")",".cells.containing(.button, identifier:\"Все записи\")"],[[[-1,8],[-1,7],[-1,6],[-1,5],[-1,4],[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .textView).element.swipeUp()
//
//
//
        
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
