//
//  APIClient.swift
//  ToDoApp
//
//  Created by Ivan Akulov on 27/10/2018.
//  Copyright © 2018 Ivan Akulov. All rights reserved.
//

import Foundation
@testable import LinkYou

protocol URLSessionProtocol {
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

extension URLSession: URLSessionProtocol {}

class APIClient {
    lazy var urlSession: URLSessionProtocol = URLSession.shared
    
    func login(withName: String, password: String, completionHandler: @escaping (String?, Error?) -> Void) {
        
        //        let sut = RegistrationInteractor()
        //        sut.authorization(mode: 0, login: withName, password: password)
        
        guard let url = URL(string: "\(LinkYouConfig().setHostAPI())/auth/signin") else {
            fatalError()
        }
        
        urlSession.dataTask(with: url) { (data, response, error) in
            print(data)
            }.resume()
        
        
    }
}

