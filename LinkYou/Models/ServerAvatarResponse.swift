
struct ServerAvatarResponse : Codable {
	let data : AvatarData?
	let error : String?

	enum CodingKeys: String, CodingKey {

		case data = "data"
		case error = "error"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent(AvatarData.self, forKey: .data)
		error = try values.decodeIfPresent(String.self, forKey: .error)
	}

}

struct BoolServerRespone : Codable {
    let response : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case response = "done"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        response = try values.decodeIfPresent(Bool.self, forKey: .response)
    }
    
}
