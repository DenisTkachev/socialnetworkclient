


struct LocationLikes : Codable {
	let city_name : String?
	let country_name : String?

	enum CodingKeys: String, CodingKey {

		case city_name = "city_name"
		case country_name = "country_name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
		country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
	}

}
