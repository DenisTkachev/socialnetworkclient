


struct AvatarLikes : Codable {
	let small : String?
	let `default` : String?
	let origin : String?

	enum CodingKeys: String, CodingKey {

		case small = "small"
		case `default` = "default"
		case origin = "origin"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		small = try values.decodeIfPresent(String.self, forKey: .small)
		`default` = try values.decodeIfPresent(String.self, forKey: .default)
		origin = try values.decodeIfPresent(String.self, forKey: .origin)
	}

}
