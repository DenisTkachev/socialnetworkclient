


struct BirthdayLikes : Codable {
	let age : Int?

	enum CodingKeys: String, CodingKey {

		case age = "age"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		age = try values.decodeIfPresent(Int.self, forKey: .age)
	}

}
