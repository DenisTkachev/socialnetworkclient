
import Foundation

final class Guest : Codable {
	let user : User?
	let datetime : Date?

	enum CodingKeys: String, CodingKey {

		case user = "user"
		case datetime = "datetime"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user = try values.decodeIfPresent(User.self, forKey: .user)
        
        let dateString = try values.decode(String.self, forKey: .datetime)
        let formatter = DateFormatter.iso8601Full
        if let date = formatter.date(from: dateString) {
            datetime = date
        } else {
            throw DecodingError.dataCorruptedError(forKey: .datetime,
                                                   in: values,
                                                   debugDescription: "Date string does not match format expected by formatter.")
        }
	}
    
    init(user: User?, datetime: Date?) {
        self.user = user
        self.datetime = datetime
    }
    
}
