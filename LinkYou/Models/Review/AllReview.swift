import Foundation

struct AllReview : Codable {
	let id : Int?
	let user : User?
	let photo : ReviewPhoto?
	let comment : String?
	let datetime : Date?

    let type: String?
    let timestamp: Int?
    let photos_count: Int?
    let ublogs_count: Int?
    let last_auth: String?
    let gift: Gift?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user = "user"
		case photo = "photo"
		case comment = "comment"
		case datetime = "datetime"
        
        case type = "type"
        case timestamp = "timestamp"
        case photos_count = "photos_count"
        case ublogs_count = "ublogs_count"
        case last_auth = "last_auth"
        case gift = "gift"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user = try values.decodeIfPresent(User.self, forKey: .user)
		photo = try values.decodeIfPresent(ReviewPhoto.self, forKey: .photo)
		comment = try values.decodeIfPresent(String.self, forKey: .comment)
        gift = try values.decodeIfPresent(Gift.self, forKey: .gift)
        
        let dateString = try values.decode(String.self, forKey: .datetime)
        let formatter = DateFormatter.iso8601Full
        if let date = formatter.date(from: dateString) {
            datetime = date
        } else {
            throw DecodingError.dataCorruptedError(forKey: .datetime,
                                                   in: values,
                                                   debugDescription: "Date string does not match format expected by formatter.")
        }
        
        type = try values.decodeIfPresent(String.self, forKey: .type)
        timestamp = try values.decodeIfPresent(Int.self, forKey: .timestamp)
        photos_count = try values.decodeIfPresent(Int.self, forKey: .photos_count)
        ublogs_count = try values.decodeIfPresent(Int.self, forKey: .ublogs_count)
        last_auth = try values.decodeIfPresent(String.self, forKey: .last_auth)
	}

}
