


struct ReviewPhoto : Codable {
	let id : Int?
	let user_id : Int?
	let datetime : String?
	let description : String?
	let src : Src?
	let is_liked : Bool?
	let likes_count : Int?
	let comments_count : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case datetime = "datetime"
		case description = "description"
		case src = "src"
		case is_liked = "is_liked"
		case likes_count = "likes_count"
		case comments_count = "comments_count"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		src = try values.decodeIfPresent(Src.self, forKey: .src)
		is_liked = try values.decodeIfPresent(Bool.self, forKey: .is_liked)
		likes_count = try values.decodeIfPresent(Int.self, forKey: .likes_count)
		comments_count = try values.decodeIfPresent(Int.self, forKey: .comments_count)
	}

}
