

struct PhotoLike : Codable {
	let id : Int?
	let avatar : Avatar?
	let name : String?
	let role : Int?
	let is_top100 : Bool?
	let is_vip : Bool?
	let is_premium : Bool?
	let is_online : Bool?
	let birthday : Birthday?
	let job : Job?
	let location : Location?
	let likes : Likes?
	let photos_count : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case avatar = "avatar"
		case name = "name"
		case role = "role"
		case is_top100 = "is_top100"
		case is_vip = "is_vip"
		case is_premium = "is_premium"
		case is_online = "is_online"
		case birthday = "birthday"
		case job = "job"
		case location = "location"
		case likes = "likes"
		case photos_count = "photos_count"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		avatar = try values.decodeIfPresent(Avatar.self, forKey: .avatar)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		role = try values.decodeIfPresent(Int.self, forKey: .role)
		is_top100 = try values.decodeIfPresent(Bool.self, forKey: .is_top100)
		is_vip = try values.decodeIfPresent(Bool.self, forKey: .is_vip)
		is_premium = try values.decodeIfPresent(Bool.self, forKey: .is_premium)
		is_online = try values.decodeIfPresent(Bool.self, forKey: .is_online)
		birthday = try values.decodeIfPresent(Birthday.self, forKey: .birthday)
		job = try values.decodeIfPresent(Job.self, forKey: .job)
		location = try values.decodeIfPresent(Location.self, forKey: .location)
		likes = try values.decodeIfPresent(Likes.self, forKey: .likes)
		photos_count = try values.decodeIfPresent(Int.self, forKey: .photos_count)
	}

}
