

struct ErrorResponse : Codable {
	let name : String?
	let message : String?
	let code : Int?
	let status : Int?
	let type : String?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case message = "message"
		case code = "code"
		case status = "status"
		case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		status = try values.decodeIfPresent(Int.self, forKey: .status)
		type = try values.decodeIfPresent(String.self, forKey: .type)
	}

}
