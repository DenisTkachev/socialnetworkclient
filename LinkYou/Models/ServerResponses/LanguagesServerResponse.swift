

struct LanguagesServerResponse : Codable {
	let id : Id?
	let success : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "languages"
		case success = "success"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Id.self, forKey: .id)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
	}

}
