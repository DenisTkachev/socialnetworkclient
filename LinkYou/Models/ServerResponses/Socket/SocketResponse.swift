


struct SocketResponse : Codable {
    
	let cmd : String?
	let socketData : SocketDataMy?

	enum CodingKeys: String, CodingKey {

		case cmd = "cmd"
		case socketData = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		cmd = try values.decodeIfPresent(String.self, forKey: .cmd)
		socketData = try values.decodeIfPresent(SocketDataMy.self, forKey: .socketData)
	}

}
