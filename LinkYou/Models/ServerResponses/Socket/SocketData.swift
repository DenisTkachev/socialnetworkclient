


struct SocketDataMy : Codable {
	let user_id : Int?

	enum CodingKeys: String, CodingKey {

		case user_id = "user_id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
	}

}
