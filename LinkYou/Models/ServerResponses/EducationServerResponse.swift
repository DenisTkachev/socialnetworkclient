


struct EducationServerResponse: Codable {
    let id : Int?
    let success : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "education"
        case success = "success"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
    
}
