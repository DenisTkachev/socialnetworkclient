//
//  AvatarChangeResponse.swift
//  LinkYou
//
//  Created by Denis Tkachev on 31/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//




struct AvatarChangeResponse: Codable {
    let avatar : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case avatar = "avatar"

    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        avatar = try values.decodeIfPresent(Bool.self, forKey: .avatar)
    }
    
}
