

struct NotificationServerResponse : Codable {
	let done : Bool?
	let status : Bool?

	enum CodingKeys: String, CodingKey {

		case done = "done"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		done = try values.decodeIfPresent(Bool.self, forKey: .done)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
	}

}

struct ServerStandartResponse : Codable {
    let key : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case key = "avatar"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        key = try values.decodeIfPresent(Bool.self, forKey: .key)
    }
    
}

