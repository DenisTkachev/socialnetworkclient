//
//  LikeServerRespone.swift
//  LinkYou
//
//  Created by Denis Tkachev on 26.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//



struct LikeServerRespone : Codable {
    let is_liked : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case is_liked = "is_liked"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        is_liked = try values.decodeIfPresent(Bool.self, forKey: .is_liked)
    }
    
}
