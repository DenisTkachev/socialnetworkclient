

struct AvatarCurrentUser : Codable {
	let small : String?
	let origin : String?
	let `default` : String?

	enum CodingKeys: String, CodingKey {

		case small = "small"
		case origin = "origin"
		case `default` = "default"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		small = try values.decodeIfPresent(String.self, forKey: .small)
		origin = try values.decodeIfPresent(String.self, forKey: .origin)
		`default` = try values.decodeIfPresent(String.self, forKey: .default)
	}

}
