


struct UserNotifications : Codable {
    let message : Int?
    let view_profile : Int?
    let like_photos : Int?
    let like_profile : Int?
    let comment_photos : Int?
    let gifts : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case message = "message"
        case view_profile = "view_profile"
        case like_photos = "like_photos"
        case like_profile = "like_profile"
        case comment_photos = "comment_photos"
        case gifts = "gifts"
    }
    
    var dictionary: [String: Int?] {
        return ["message": message,
                "view_profile": view_profile,
                "like_photos": like_photos,
                "like_profile": like_profile,
                "comment_photos": comment_photos,
                "gifts": gifts
        ]
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(Int.self, forKey: .message)
        view_profile = try values.decodeIfPresent(Int.self, forKey: .view_profile)
        like_photos = try values.decodeIfPresent(Int.self, forKey: .like_photos)
        like_profile = try values.decodeIfPresent(Int.self, forKey: .like_profile)
        comment_photos = try values.decodeIfPresent(Int.self, forKey: .comment_photos)
        gifts = try values.decodeIfPresent(Int.self, forKey: .gifts)
    }
    
}
//
//extension UserNotifications: Equatable {
//    static func ==(lhs: UserNotifications, rhs: UserNotifications) -> Bool {
//        return lhs.message == rhs.message
//        && lhs.view_profile == rhs.view_profile
//        && lhs.like_photos == rhs.like_photos
//        && lhs.like_profile == rhs.like_profile
//        && lhs.comment_photos == rhs.comment_photos
//        && lhs.gifts == rhs.gifts
//    }
//}
