
struct LocationCurrentUser : Codable {
	let country_name : String?
	let country_id : Int?
	let city_name : String?
	let city_id : Int?

	enum CodingKeys: String, CodingKey {

		case country_name = "country_name"
		case country_id = "country_id"
		case city_name = "city_name"
		case city_id = "city_id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
		country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
		city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
		city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
	}

}
