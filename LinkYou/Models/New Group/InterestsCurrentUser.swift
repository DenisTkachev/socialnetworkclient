
struct InterestsCurrentUser : Codable {
	let id : Int?
	let interests : [String]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case interests = "interests"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		interests = try values.decodeIfPresent([String].self, forKey: .interests)
	}

}
