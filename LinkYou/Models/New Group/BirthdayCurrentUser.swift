

struct BirthdayCurrentUser : Codable {
	let date : String?
	let age : Int?
	let zodiac : Zodiac?

	enum CodingKeys: String, CodingKey {

		case date = "date"
		case age = "age"
		case zodiac = "zodiac"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		age = try values.decodeIfPresent(Int.self, forKey: .age)
		zodiac = try values.decodeIfPresent(Zodiac.self, forKey: .zodiac)
	}

}
