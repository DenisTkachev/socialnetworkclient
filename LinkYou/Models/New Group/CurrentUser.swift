
struct CurrentUser : Codable {
	let id : Int?
	let name : String?
	let gender : Gender?
	let goal : Goal?
	let looking_for : Looking_for?
	let age : Age?
	let is_premium : Bool?
	let last_auth : String?
	let birthday : Birthday?
	let avatar : Avatar?
	let is_online : Bool?
	let is_stealth : Bool?
    let endStealth: String?
	let is_invisible : Bool?
	let is_vip : Bool?
	let role : Int?
	let job : Job?
	let location : Location?
	var interests : Interests?
	let languages : [Languages]?
	let likes : Likes?
	let is_favorited : Bool?
	let is_blacklisted : Bool?
	let rating : Rating?
	let photos_count : Int?
	let ublogs_count : Int?
	let ublogs : Ublogs?
	let is_top100 : Bool?
    let email : String?
    let is_active: Bool?
    let gifts: Gifts?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case gender = "gender"
		case goal = "goal"
		case looking_for = "looking_for"
		case age = "age"
		case is_premium = "is_premium"
		case last_auth = "last_auth"
		case birthday = "birthday"
		case avatar = "avatar"
		case is_online = "is_online"
		case is_stealth = "is_stealth"
		case is_invisible = "is_invisible"
		case is_vip = "is_vip"
		case role = "role"
		case job = "job"
		case location = "location"
		case interests = "interests"
		case languages = "languages"
		case likes = "likes"
		case is_favorited = "is_favorited"
		case is_blacklisted = "is_blacklisted"
		case rating = "rating"
		case photos_count = "photos_count"
		case ublogs_count = "ublogs_count"
		case ublogs = "ublogs"
		case is_top100 = "is_top100"
        case email = "email"
        case is_active = "is_active"
        case endStealth = "end_stealth"
        case gifts = "gifts"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		gender = try values.decodeIfPresent(Gender.self, forKey: .gender)
		goal = try values.decodeIfPresent(Goal.self, forKey: .goal)
		looking_for = try values.decodeIfPresent(Looking_for.self, forKey: .looking_for)
		age = try values.decodeIfPresent(Age.self, forKey: .age)
		is_premium = try values.decodeIfPresent(Bool.self, forKey: .is_premium)
		last_auth = try values.decodeIfPresent(String.self, forKey: .last_auth)
		birthday = try values.decodeIfPresent(Birthday.self, forKey: .birthday)
		avatar = try values.decodeIfPresent(Avatar.self, forKey: .avatar)
		is_online = try values.decodeIfPresent(Bool.self, forKey: .is_online)
		is_stealth = try values.decodeIfPresent(Bool.self, forKey: .is_stealth)
		is_invisible = try values.decodeIfPresent(Bool.self, forKey: .is_invisible)
		is_vip = try values.decodeIfPresent(Bool.self, forKey: .is_vip)
		role = try values.decodeIfPresent(Int.self, forKey: .role)
		job = try values.decodeIfPresent(Job.self, forKey: .job)
		location = try values.decodeIfPresent(Location.self, forKey: .location)
		interests = try values.decodeIfPresent(Interests.self, forKey: .interests)
		languages = try values.decodeIfPresent([Languages].self, forKey: .languages)
		likes = try values.decodeIfPresent(Likes.self, forKey: .likes)
		is_favorited = try values.decodeIfPresent(Bool.self, forKey: .is_favorited)
		is_blacklisted = try values.decodeIfPresent(Bool.self, forKey: .is_blacklisted)
		rating = try values.decodeIfPresent(Rating.self, forKey: .rating)
		photos_count = try values.decodeIfPresent(Int.self, forKey: .photos_count)
		ublogs_count = try values.decodeIfPresent(Int.self, forKey: .ublogs_count)
		ublogs = try values.decodeIfPresent(Ublogs.self, forKey: .ublogs)
		is_top100 = try values.decodeIfPresent(Bool.self, forKey: .is_top100)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        is_active = try values.decodeIfPresent(Bool.self, forKey: .is_active)
        endStealth = try values.decodeIfPresent(String.self, forKey: .endStealth)
        gifts = try values.decodeIfPresent(Gifts.self, forKey: .gifts)
	}
}
