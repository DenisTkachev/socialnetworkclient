

struct LanguagesCurrentUser : Codable {
	let id : Int?
	let user_id : Int?
	let deleted : Bool?
	let last_update : String?
	let language : LanguageCurrentUser?
	let level : LevelCurrentUser?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case deleted = "deleted"
		case last_update = "last_update"
		case language = "language"
		case level = "level"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
		last_update = try values.decodeIfPresent(String.self, forKey: .last_update)
		language = try values.decodeIfPresent(LanguageCurrentUser.self, forKey: .language)
		level = try values.decodeIfPresent(LevelCurrentUser.self, forKey: .level)
	}

}
