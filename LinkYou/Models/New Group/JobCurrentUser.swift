

struct JobCurrentUser : Codable {
	let id : Int?
	let company_name : String?
	let pros : String?
	let cons : String?
	let achievements : String?
	let profession : String?
	let profession_id : Int?
	let occupation : String?
	let occupation_id : Int?
	let finance : Finance?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case company_name = "company_name"
		case pros = "pros"
		case cons = "cons"
		case achievements = "achievements"
		case profession = "profession"
		case profession_id = "profession_id"
		case occupation = "occupation"
		case occupation_id = "occupation_id"
		case finance = "finance"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		company_name = try values.decodeIfPresent(String.self, forKey: .company_name)
		pros = try values.decodeIfPresent(String.self, forKey: .pros)
		cons = try values.decodeIfPresent(String.self, forKey: .cons)
		achievements = try values.decodeIfPresent(String.self, forKey: .achievements)
		profession = try values.decodeIfPresent(String.self, forKey: .profession)
		profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
		occupation = try values.decodeIfPresent(String.self, forKey: .occupation)
		occupation_id = try values.decodeIfPresent(Int.self, forKey: .occupation_id)
		finance = try values.decodeIfPresent(Finance.self, forKey: .finance)
	}

}
