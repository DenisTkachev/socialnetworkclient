

struct LanguageCurrentUser : Codable {
	let id : Int?
	let sorting : Int?
	let name : String?
	let deleted : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case sorting = "sorting"
		case name = "name"
		case deleted = "deleted"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		sorting = try values.decodeIfPresent(Int.self, forKey: .sorting)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
	}

}
