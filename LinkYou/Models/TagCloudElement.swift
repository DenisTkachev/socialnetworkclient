//
//  TagsCloud.swift
//  LinkYou
//
//  Created by Denis Tkachev on 08.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class BgView: UIView {
    var text: String?
    weak var delegate: ButtonActionType? = nil
    
    @objc func pressButtonn(_: Any) {
        delegate?.deleteBtnPress(bgView: self)
    }
    
    func addButton() {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: self.bounds.maxX - 25, y: 3.0, width: 23.0, height: 23.0)
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
        button.setImage(UIImage(named: "cross"), for: .normal)
        button.tag = tag
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(BgView.pressButtonn(_:)), for: .touchUpInside)
        self.addSubview(button)
    }
}

struct TagCloudElement {
    var text: String = ""
    var icon: UIImage? = nil
    
    func createTagCloud(withArray data:[TagCloudElement], tagsCloudView: UIView, cellHeightConstraint: NSLayoutConstraint, deleteBtnisOn: Bool = false, deleteBtnDelegate: ButtonActionType? = nil) {
        for tempView in tagsCloudView.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        var xPos: CGFloat = 0.0
        var yPos: CGFloat = 16.0
        var tag: Int = 0
        for str in data  {
            var startstring = str.text
            var width = startstring.widthOfString(usingFont: UIFont.systemFont(ofSize: 12.0))
            if width > UIScreen.main.bounds.width - 20 {
                startstring = startstring.trunc(length: 40)
                width = startstring.widthOfString(usingFont: UIFont.systemFont(ofSize: 12.0))
            }
            
            if width + 17.0 + 38.5 > tagsCloudView.bounds.size.width { // add ... and cut string
                let substring = xPos - UIScreen.main.bounds.width + 39 + 17
                startstring = startstring.trunc(length: Int(abs(substring + 66)))
                width = startstring.widthOfString(usingFont: UIFont.systemFont(ofSize: 12.0))
            }
            let checkWholeWidth = CGFloat(xPos) + CGFloat(width)
            if checkWholeWidth > tagsCloudView.bounds.size.width - 30.0 {
                xPos = 0.0
                yPos = yPos + 29.0 + 10.0
            }
            
            let bgView = BgView(frame: CGRect(x: xPos, y: yPos, width: (deleteBtnisOn) ? width + 40 : width + 19.0, height: 29.0))
            bgView.delegate = deleteBtnDelegate
            bgView.layer.cornerRadius = 14.5
            bgView.layer.borderWidth = 1
            bgView.layer.borderColor = UIColor.paleGrey.cgColor
            bgView.backgroundColor = UIColor.white
            bgView.tag = tag
            bgView.text = text
            
            let textlable = UILabel(frame: CGRect(x: 10, y: 0.0, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont.systemFont(ofSize: 12.0)
            textlable.text = startstring
            textlable.textColor = UIColor.charcoalGrey
            textlable.isUserInteractionEnabled = false
            
            bgView.addSubview(textlable)
            if deleteBtnisOn {
                bgView.addButton()
            }
            xPos = CGFloat(xPos) + CGFloat(width) + CGFloat(28.0) + ((deleteBtnisOn) ? 23.0 : 0.0)
            tagsCloudView.addSubview(bgView)
            tag = tag  + 1
        }
        cellHeightConstraint.constant = 29 + yPos
    }
}

