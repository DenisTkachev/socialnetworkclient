//
//  UserEducation.swift
//  LinkYou
//
//  Created by Denis Tkachev on 05/02/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Foundation

struct UserEducation {
    
    typealias educationId = EditorRegistrationPresenter.ActionSheetType
    
    enum RowKey: String {
        case educationtype = "education[type_id]"
        case speciality = "education[speciality_name]"
        case institution = "education[institution_name]"
        case id = "education[id]"
    }
    
    var educationtype: String? // среднее, высшее           "education[type_id]"
    var speciality: String? //                              "education[speciality_name]"
    var institution: String? //                             "education[institution_name]"
    var id: String?                                    // "education[id]"
    
    var dictionaryForSave: [String:String] = [:]
    
    mutating func saveField(key: String, rowTag: String) -> Bool {
        switch key {
        case "education[type_id]":
            self.educationtype = rowTag
        case "education[speciality_name]":
            self.speciality = rowTag
        case "education[institution_name]":
            self.institution = rowTag
        case "education[id]":
            self.id = rowTag
        default: break
            
        }
        return canBeSaved()
    }
    
    private mutating func makeDictionaryParameter() { // создать обьект для отправки на сервер
        dictionaryForSave[RowKey.educationtype.rawValue] = insertTypeId(self.educationtype!)
        dictionaryForSave[RowKey.speciality.rawValue] = self.speciality
        dictionaryForSave[RowKey.institution.rawValue] = self.institution
        dictionaryForSave[RowKey.id.rawValue] = self.id
    }
    
    private mutating func canBeSaved() -> Bool {
        if educationtype != nil, speciality != nil, institution != nil {
            makeDictionaryParameter() // можно отправлять на сервер
            return true
        } else {
            return false
        }
    }
    
    private func insertTypeId(_ educationtype: String) -> String {
        switch educationtype {
        case educationId.unknown.rawValue:
            return "0"
        case educationId.mid.rawValue:
            return "1"
        case educationId.midSpec.rawValue:
            return "2"
        case educationId.halfHight.rawValue:
            return "3"
        case educationId.hight.rawValue:
            return "4"
        default:
            print("Ошибка конвертации типа образования")
            return ""
        }
    }
}




