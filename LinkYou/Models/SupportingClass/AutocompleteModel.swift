


struct AutocompleteModel : Codable {
	let id : Int?
	let title : String?
	let highlighted : String?
    let subtitle: String?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case title = "title"
		case highlighted = "highlighted"
        case subtitle = "subtitle"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		highlighted = try values.decodeIfPresent(String.self, forKey: .highlighted)
        subtitle = try values.decodeIfPresent(String.self, forKey: .subtitle)
	}

    init(id: Int, title: String, highlighted: String, subtitle: String) {
        self.id = id
        self.title = title
        self.highlighted = highlighted
        self.subtitle = subtitle
    }
}
