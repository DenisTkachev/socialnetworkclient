//
//  ClaimReport.swift
//  LinkYou
//
//  Created by Denis Tkachev on 14.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//


class ClaimReport  {
    
    enum TicketType: String {
        case photo = "photo"
        case user = "user"
        case general = "general"
    }
    
    let header: String
    let body: String
    var userId: Int?
    let ticketType: TicketType
    
    init(header: String, body: String, ticketType: TicketType, userId: Int?) {
        self.header = header
        self.body = body
        self.userId = userId
        self.ticketType = ticketType
    }
}

//REQUIRED
//title - string
//message - string
//
//OPTIONAL
//entity - ENUM (photo, user, general)
//entityId - int
