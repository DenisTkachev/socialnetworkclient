//
//  claimReport.swift
//  LinkYou
//
//  Created by Denis Tkachev on 14.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//


class ClaimReport  {
    
    enum <#name#> {
        case <#case#>
    }
    
    let header: String
    let body: String
    
    init(header: String, body: String) {
        self.header = header
        self.body = body
    }
}

//REQUIRED
//title - string
//message - string
//
//OPTIONAL
//entity - ENUM (photo, user, general)
//entityId - int
