//
//  SearchRequest.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation

class SearchRequest {
    // дефолтные значения для поиска
    var lookingFor: (Int, String) = (3, "Друзей") // Друзей
    var ageFrom = 18
    var ageTo = 75
    var city: (Int, String) = (0, "")
    var profession: (Int, String) = (0, "")
    var nationality: (Int, String) = (0, "")
    var religion: (Int, String) = (0, "")
    var language: (Int, String) = (4, "Русский")
    var page: Int = 0
    var profArea: (Int, String) = (0, "")
    
    func setValue(type: SearchViewController.TextFieldType, value: (Int, String), ageValue: (Int, Int)?) {
        switch type {
        case .age:
            if let ageValue = ageValue {
                ageFrom = ageValue.0
                ageTo = ageValue.1
            }
        case .cities: city = value
        case .languages: language = value
        case .nationality: nationality = value
        case .professions: profession = value
        case .religions: religion = value
        case .target: lookingFor = value
        case .profArea: profArea = value
        }
    }
    
    func getValue(cellType: SearchViewController.TextFieldType) -> (Int, String) {
        switch cellType {
        case .age:
            let ages = (0 ,"\(ageFrom), \(ageTo)")
            return ages
        case .cities: return city
        case .languages: return language
        case .nationality: return nationality
        case .professions: return profession
        case .religions: return religion
        case .target: return lookingFor
        case .profArea: return profArea
        }
    }
    
}


//
//?looking_for=1&age_from=25&age_to=30&city=580&profession=2,7&nationality=1&religion=2&language=8&page=2
