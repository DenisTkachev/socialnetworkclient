

struct Tags : Codable {
	let id : String?
	let tag : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case tag = "tag"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		tag = try values.decodeIfPresent(String.self, forKey: .tag)
	}

}
