


final class BlogPostDetail : Codable {
	let id : Int?
	let user : User?
	let text : String?
	let text_short : String?
	let tags : [Tags]?
	let photos : [Photos]?
	let video_link : String?
	let audio_link : String?
	let comments_count : Int?
	let views_count : Int?
	let likes_count : Int?
	let is_liked : Bool?
	let datetime : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user = "user"
		case text = "text"
		case text_short = "text_short"
		case tags = "tags"
		case photos = "photos"
		case video_link = "video_link"
		case audio_link = "audio_link"
		case comments_count = "comments_count"
		case views_count = "views_count"
		case likes_count = "likes_count"
		case is_liked = "is_liked"
		case datetime = "datetime"
	}

    init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user = try values.decodeIfPresent(User.self, forKey: .user)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		text_short = try values.decodeIfPresent(String.self, forKey: .text_short)
		tags = try values.decodeIfPresent([Tags].self, forKey: .tags)
		photos = try values.decodeIfPresent([Photos].self, forKey: .photos)
		video_link = try values.decodeIfPresent(String.self, forKey: .video_link)
		audio_link = try values.decodeIfPresent(String.self, forKey: .audio_link)
		comments_count = try values.decodeIfPresent(Int.self, forKey: .comments_count)
		views_count = try values.decodeIfPresent(Int.self, forKey: .views_count)
		likes_count = try values.decodeIfPresent(Int.self, forKey: .likes_count)
		is_liked = try values.decodeIfPresent(Bool.self, forKey: .is_liked)
		datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
	}
    
    init(last: Last, user: User) {
        self.user = user
        self.id = last.id

        self.text = last.text
        self.text_short = last.text_short
        self.tags = last.tags
        self.photos = last.photos
        self.video_link = last.video_link
        self.audio_link = last.audio_link
        self.comments_count = last.comments_count
        self.views_count = last.views_count
        self.likes_count = last.likes_count
        self.is_liked = last.is_liked
        self.datetime = last.last_update
    }

}
