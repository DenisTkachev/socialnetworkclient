

struct BlogPostComments : Codable {
	let id : Int?
	let user_id : Int?
	let user : User?
	let deleted : Int?
	let datetime : String?
	let comment : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case user = "user"
		case deleted = "deleted"
		case datetime = "datetime"
		case comment = "comment"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		user = try values.decodeIfPresent(User.self, forKey: .user)
		deleted = try values.decodeIfPresent(Int.self, forKey: .deleted)
		datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
		comment = try values.decodeIfPresent(String.self, forKey: .comment)
	}

}
