

struct Photos : Codable {
	let id : String?
	var src : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case src = "src"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		src = try values.decodeIfPresent(String.self, forKey: .src)
        
        src = checkURLFormat(src)
        
	}

    private func checkURLFormat(_ url: String?) -> String? {
        guard let url = url else { return nil }
        
        if url.contains("http") {
            return url
        } else {
            return "https:\(url)"
        }
    }
    
}
