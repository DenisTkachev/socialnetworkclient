
struct Payment : Codable {
    
	let premium : [DayPayment]?
	let mainPage : [DayPayment]?
	let interestingPage : [DayPayment]?
	let invisible : [DayPayment]?
	let fee : [DayPayment]?
	let searchRaise : [DayPayment]?
    let gift : [DayPayment]?
    
	enum CodingKeys: String, CodingKey {

		case premium = "premium"
		case mainPage = "main_page"
		case interestingPage = "interesting_page"
		case invisible = "invisible"
		case fee = "fee"
		case searchRaise = "search_raise"
        case gift = "gift"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		premium = try values.decodeIfPresent([DayPayment].self, forKey: .premium)
		mainPage = try values.decodeIfPresent([DayPayment].self, forKey: .mainPage)
		interestingPage = try values.decodeIfPresent([DayPayment].self, forKey: .interestingPage)
		invisible = try values.decodeIfPresent([DayPayment].self, forKey: .invisible)
		fee = try values.decodeIfPresent([DayPayment].self, forKey: .fee)
		searchRaise = try values.decodeIfPresent([DayPayment].self, forKey: .searchRaise)
        gift = try values.decodeIfPresent([DayPayment].self, forKey: .gift)
	}

}
