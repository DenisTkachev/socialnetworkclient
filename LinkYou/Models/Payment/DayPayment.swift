struct DayPayment : Codable {
	let entityId : Int?
	let name : String?
	let paymentTypeId : Int?
	let price : Int?
	let period : String?

	enum CodingKeys: String, CodingKey {

		case entityId = "entity_id"
		case name = "name"
		case paymentTypeId = "payment_type_id"
		case price = "price"
		case period = "period"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		entityId = try values.decodeIfPresent(Int.self, forKey: .entityId)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		paymentTypeId = try values.decodeIfPresent(Int.self, forKey: .paymentTypeId)
		price = try values.decodeIfPresent(Int.self, forKey: .price)
		period = try values.decodeIfPresent(String.self, forKey: .period)
	}

}
