
import Foundation
struct SearchRaise : Codable {
	let day1 : DayPayment?

	enum CodingKeys: String, CodingKey {

		case day1 = "1"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        day1 = try values.decodeIfPresent(DayPayment.self, forKey: .day1)
        day2 = try values.decodeIfPresent(DayPayment.self, forKey: .day2)
        day3 = try values.decodeIfPresent(DayPayment.self, forKey: .day3)
        day4 = try values.decodeIfPresent(DayPayment.self, forKey: .day4)
	}

}
