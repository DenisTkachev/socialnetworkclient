

struct InterestingUsers: Codable {
	let items: [ItemsInteresting]?
	let selfUserId: String?
	let avatar: String?
	let avatarSize: String?
	let cntUsers: Int?
	let lookingForCode: String?
	let userPaidData: [String]?

	enum CodingKeys: String, CodingKey {

		case items = "items"
		case selfUserId = "selfUserId"
		case avatar = "avatar"
		case avatarSize = "avatarSize"
		case cntUsers = "cntUsers"
		case lookingForCode = "lookingForCode"
		case userPaidData = "userPaidData"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		items = try values.decodeIfPresent([ItemsInteresting].self, forKey: .items)
		selfUserId = try values.decodeIfPresent(String.self, forKey: .selfUserId)
		avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
		avatarSize = try values.decodeIfPresent(String.self, forKey: .avatarSize)
		cntUsers = try values.decodeIfPresent(Int.self, forKey: .cntUsers)
		lookingForCode = try values.decodeIfPresent(String.self, forKey: .lookingForCode)
		userPaidData = try values.decodeIfPresent([String].self, forKey: .userPaidData)
	}

}
