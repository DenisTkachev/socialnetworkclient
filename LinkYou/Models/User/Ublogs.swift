

struct Ublogs : Codable {
	let count : Int?
    let last : Last?

	enum CodingKeys: String, CodingKey {

		case count = "count"
        case last = "last"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		count = try values.decodeIfPresent(Int.self, forKey: .count)
        last = try values.decodeIfPresent(Last.self, forKey: .last)
	}

}
