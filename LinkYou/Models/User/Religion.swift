

struct Religion : Codable {
	let id : Int?
	let name : String?
	let deleted : Bool?
	let status : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case deleted = "deleted"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
		status = try values.decodeIfPresent(String.self, forKey: .status)
	}

}
