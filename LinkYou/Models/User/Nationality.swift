

struct Nationality : Codable {
	let id : Int?
	let name : String?
	let sorting : Int?
	let deleted : Bool?
	let visibility : Visibility?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case sorting = "sorting"
		case deleted = "deleted"
		case visibility = "visibility"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		sorting = try values.decodeIfPresent(Int.self, forKey: .sorting)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
		visibility = try values.decodeIfPresent(Visibility.self, forKey: .visibility)
	}

}
