

struct Rating : Codable {
	let score : Double?
	let likes : Int?
	let views : Int?
	let code : String?

	enum CodingKeys: String, CodingKey {

		case score = "score"
		case likes = "likes"
		case views = "views"
		case code = "code"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		score = try values.decodeIfPresent(Double.self, forKey: .score)
		likes = try values.decodeIfPresent(Int.self, forKey: .likes)
		views = try values.decodeIfPresent(Int.self, forKey: .views)
		code = try values.decodeIfPresent(String.self, forKey: .code)
	}

}
