

struct Music : Codable {
	let id : Int?
	let user_id : Int?
	let link : String?
	let source : String?
	let datetime : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case link = "link"
		case source = "source"
		case datetime = "datetime"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		link = try values.decodeIfPresent(String.self, forKey: .link)
		source = try values.decodeIfPresent(String.self, forKey: .source)
		datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
	}

}
