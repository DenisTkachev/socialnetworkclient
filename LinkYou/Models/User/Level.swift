

struct Level : Codable {
    var id : Int?
    var name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
    init(name: String) {
        
        self.name = name
        
        switch name {
        case "Родной язык": self.id = 1
        case "Базовый уровень": self.id = 2
        case "Читаю литературу": self.id = 3
        case "Свободно владею": self.id = 4
            
        default:
            self.id = nil
        }
    }
}
