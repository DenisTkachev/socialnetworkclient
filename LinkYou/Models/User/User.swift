


struct User: Codable {
	var id: Int?
    var avatar: Avatar?
	var name: String?
	var gender: Gender?
	var nationality: Nationality?
	var religion: Religion?
	var goal: Goal?
	var looking_for: Looking_for?
	var children: Children?
	var relationship: Relationship?
	var smoking: Smoking?
	var alcohol: Alcohol?
	var orientation: Orientation?
	var age: Age?
	var height: Int?
	var weight: Int?
	var about: String?
	var is_premium: Bool?
	var end_premium: String?
	var last_auth: String?
	var birthday: Birthday?
	var is_online: Bool?
//    var is_stealth: Bool?
//    var is_invisible: Bool?
	var is_vip: Bool?
	var role: Int?
    var roleStatus: String?
	var job: Job?
	var location: Location?
	var education: [Education]?
	var interests: Interests?
	var languages: [Languages]?
	var music: [Music]?
	var books: [Books]?
	var pets: [Pets]?
	var likes: Likes?
	var is_favorited: Bool?
	var is_blacklisted: Bool?
	var rating: Rating?
	var gifts: Gifts?
	var photos_count: Int?
	var blogs_count: Int?
	var is_top100: Bool?
    var ublogs: Ublogs?
    var countRaising: Int?
    var email: String?
//    var endStealth: String?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case gender = "gender"
		case nationality = "nationality"
		case religion = "religion"
		case goal = "goal"
		case looking_for = "looking_for"
		case children = "children"
		case relationship = "relationship"
		case smoking = "smoking"
		case alcohol = "alcohol"
		case orientation = "orientation"
		case age = "age"
		case height = "height"
		case weight = "weight"
		case about = "about"
		case is_premium = "is_premium"
		case end_premium = "end_premium"
		case last_auth = "last_auth"
		case birthday = "birthday"
		case avatar = "avatar"
		case is_online = "is_online"
//        case is_stealth = "is_stealth"
//        case is_invisible = "is_invisible"
		case is_vip = "is_vip"
		case role = "role"
		case job = "job"
		case location = "location"
		case education = "education"
		case interests = "interests"
		case languages = "languages"
		case music = "music"
		case books = "books"
		case pets = "pets"
		case likes = "likes"
		case is_favorited = "is_favorited"
		case is_blacklisted = "is_blacklisted"
		case rating = "rating"
		case gifts = "gifts"
		case photos_count = "photos_count"
		case blogs_count = "blogs_count"
		case is_top100 = "is_top100"
        case ublogs = "ublogs"
        case countRaising = "count_raising"
        case roleStatus = "role_status"
        case email = "email"
//        case endStealth = "end_Stealth"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		gender = try values.decodeIfPresent(Gender.self, forKey: .gender)
		nationality = try values.decodeIfPresent(Nationality.self, forKey: .nationality)
		religion = try values.decodeIfPresent(Religion.self, forKey: .religion)
		goal = try values.decodeIfPresent(Goal.self, forKey: .goal)
		looking_for = try values.decodeIfPresent(Looking_for.self, forKey: .looking_for)
		children = try values.decodeIfPresent(Children.self, forKey: .children)
		relationship = try values.decodeIfPresent(Relationship.self, forKey: .relationship)
		smoking = try values.decodeIfPresent(Smoking.self, forKey: .smoking)
		alcohol = try values.decodeIfPresent(Alcohol.self, forKey: .alcohol)
		orientation = try values.decodeIfPresent(Orientation.self, forKey: .orientation)
		age = try values.decodeIfPresent(Age.self, forKey: .age)
		height = try values.decodeIfPresent(Int.self, forKey: .height)
		weight = try values.decodeIfPresent(Int.self, forKey: .weight)
		about = try values.decodeIfPresent(String.self, forKey: .about)
		is_premium = try values.decodeIfPresent(Bool.self, forKey: .is_premium)
		end_premium = try values.decodeIfPresent(String.self, forKey: .end_premium)
		last_auth = try values.decodeIfPresent(String.self, forKey: .last_auth)
		birthday = try values.decodeIfPresent(Birthday.self, forKey: .birthday)
		avatar = try values.decodeIfPresent(Avatar.self, forKey: .avatar)
		is_online = try values.decodeIfPresent(Bool.self, forKey: .is_online)
//        is_stealth = try values.decodeIfPresent(Bool.self, forKey: .is_stealth)
//        is_invisible = try values.decodeIfPresent(Bool.self, forKey: .is_invisible)
		is_vip = try values.decodeIfPresent(Bool.self, forKey: .is_vip)
		role = try values.decodeIfPresent(Int.self, forKey: .role)
		job = try values.decodeIfPresent(Job.self, forKey: .job)
		location = try values.decodeIfPresent(Location.self, forKey: .location)
		education = try values.decodeIfPresent([Education].self, forKey: .education)
		interests = try values.decodeIfPresent(Interests.self, forKey: .interests)
		languages = try values.decodeIfPresent([Languages].self, forKey: .languages)
		music = try values.decodeIfPresent([Music].self, forKey: .music)
		books = try values.decodeIfPresent([Books].self, forKey: .books)
		pets = try values.decodeIfPresent([Pets].self, forKey: .pets)
		likes = try values.decodeIfPresent(Likes.self, forKey: .likes)
		is_favorited = try values.decodeIfPresent(Bool.self, forKey: .is_favorited)
		is_blacklisted = try values.decodeIfPresent(Bool.self, forKey: .is_blacklisted)
		rating = try values.decodeIfPresent(Rating.self, forKey: .rating)
		gifts = try values.decodeIfPresent(Gifts.self, forKey: .gifts)
		photos_count = try values.decodeIfPresent(Int.self, forKey: .photos_count)
		blogs_count = try values.decodeIfPresent(Int.self, forKey: .blogs_count)
		is_top100 = try values.decodeIfPresent(Bool.self, forKey: .is_top100)
        ublogs = try values.decodeIfPresent(Ublogs.self, forKey: .ublogs)
        countRaising = try values.decodeIfPresent(Int.self, forKey: .countRaising)
        roleStatus = try values.decodeIfPresent(String.self, forKey: .roleStatus)
        email = try values.decodeIfPresent(String.self, forKey: .email)
//        endStealth = try values.decodeIfPresent(String.self, forKey: .endStealth)
	}

}

extension User {
    
    mutating func shortUpdate(user: ShortUser) {
        self.avatar = user.avatar
        self.name = user.name
        self.role = user.role
        self.is_top100 = user.is_top100
        self.is_vip = user.is_vip
        self.is_premium = user.is_premium
        self.gender = user.gender
        self.birthday = user.birthday
        self.is_online = user.is_online
        self.birthday = user.birthday
        self.job = user.job
        self.location = user.location
        self.likes = user.likes
        self.photos_count = user.photos_count
        self.blogs_count = user.ublogs_count
        self.last_auth = user.last_auth
        
    }
    
    enum RoleStatus: String {
        case edit = "edit"       //edit - только зарегистрировался, должен заполнить профиль
        case banned = "banned"     //banned - Забаненый юзер
        case disabled = "disabled"   //disabled - Другое. Пользоваться сайтом нельзя
        case fee = "fee"        //fee - Всё сделал но еще не оплатил членский взнос
        case moderation = "moderation" //moderation - Юзер находится на модерации
        case normal = "normal"    //normal - Обычный юзер
        case premium = "premium"   //premium - Премиум юзер
        case emailConfirm = "email_confirm" // ожидание подтверждения почты
        
        // подтвердить почту -> заполнить профиль -> оплатить взнос -> купить премиум?
        // email_confirm -> edit -> fee -> normal -> premium
        //Только при двух статусах normal и premium, пользователь может нормально работать с сайтом.
        //Для пользователя fee надо постоянно требовать бабки т.е. показывать окно с оплатой
        //Пользователя со статусом edit надо кидать на страницу редактирования
        //banned - показываем заглушку, мол вы забанены

    }
    
    // guest mode
    init() {
        self.id = 0
        self.name = "Гость"
        let avatar = Avatar.init(id: 0, user_id: 0, datetime: "", description: "", src: nil, isLiked: false, likesCount: 0)
        self.avatar = avatar
    }
    
}
