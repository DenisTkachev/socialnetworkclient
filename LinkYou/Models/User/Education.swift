 

 
struct Education : Codable {
	let id : Int?
	let user_id : Int?
	let speciality : Speciality?
	let active : Bool?
	let deleted : Bool?
	let last_update : String?
	let education_type : Education_type?
	let institution : Institution?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case speciality = "speciality"
		case active = "active"
		case deleted = "deleted"
		case last_update = "last_update"
		case education_type = "education_type"
		case institution = "institution"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		speciality = try values.decodeIfPresent(Speciality.self, forKey: .speciality)
		active = try values.decodeIfPresent(Bool.self, forKey: .active)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
		last_update = try values.decodeIfPresent(String.self, forKey: .last_update)
		education_type = try values.decodeIfPresent(Education_type.self, forKey: .education_type)
		institution = try values.decodeIfPresent(Institution.self, forKey: .institution)
	}

    init(id: Int?, user_id: Int, active: Bool, deleted: Bool, last_update: String?, speciality: Speciality?, education_type: Education_type?, institution: Institution?) {
        self.id = id
        self.user_id = user_id
        self.active = active
        self.deleted = deleted
        self.last_update = last_update
        self.speciality = speciality
        self.education_type = education_type
        self.institution = institution
    }
    
}
