
struct Pets : Codable {
	let id : Int?
	let user_id : Int?
	let name : String?
	let type : String?
	let description : String?
	let avatar : String?
	let deleted : Bool?
	let last_update : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case name = "name"
		case type = "type"
		case description = "description"
		case avatar = "avatar"
		case deleted = "deleted"
		case last_update = "last_update"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
		last_update = try values.decodeIfPresent(String.self, forKey: .last_update)
	}

    init(id: Int, user_id: Int, deleted: Bool, last_update: String, name: String, type: String, description: String, avatar: String) {
        self.id = id
        self.user_id = user_id
        self.deleted = deleted
        self.last_update = last_update
        self.name = name
        self.type = type
        self.description = description
        self.avatar = avatar
    }
}
