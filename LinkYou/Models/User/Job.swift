

struct Job: Codable {
	var id: Int?
	var company_name: String?
	var pros: String?
	var cons: String?
	var achievements: String?
	var profession: String?
	var profession_id: Int?
	var occupation: String?
	var occupation_id: Int?
	var finance: Finance?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case company_name = "company_name"
		case pros = "pros"
		case cons = "cons"
		case achievements = "achievements"
		case profession = "profession"
		case profession_id = "profession_id"
		case occupation = "occupation"
		case occupation_id = "occupation_id"
		case finance = "finance"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		company_name = try values.decodeIfPresent(String.self, forKey: .company_name)
		pros = try values.decodeIfPresent(String.self, forKey: .pros)
		cons = try values.decodeIfPresent(String.self, forKey: .cons)
		achievements = try values.decodeIfPresent(String.self, forKey: .achievements)
		profession = try values.decodeIfPresent(String.self, forKey: .profession)
		profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
		occupation = try values.decodeIfPresent(String.self, forKey: .occupation)
		occupation_id = try values.decodeIfPresent(Int.self, forKey: .occupation_id)
		finance = try values.decodeIfPresent(Finance.self, forKey: .finance)
	}
    
    init() {
        self.id = nil
        self.company_name = nil
        self.pros = nil
        self.cons = nil
        self.achievements = nil
        self.profession = nil
        self.profession_id = nil
        self.occupation = nil
        self.occupation_id = nil
        self.finance = Finance.init()
    }

}
