

struct Language : Codable {
	var id : Int?
	var sorting : Int?
	var name : String?
	var deleted : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case sorting = "sorting"
		case name = "name"
		case deleted = "deleted"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		sorting = try values.decodeIfPresent(Int.self, forKey: .sorting)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
	}

    init(id: Int, sorting: Int, name: String, deleted: Bool) {
        self.id = id
        self.sorting = sorting
        self.name = name
        self.deleted = deleted
    }
}
