


struct UserSession : Codable {
	let token : String?
	var user : User

	enum CodingKeys: String, CodingKey {

		case token = "token"
		case user = "user"
	}

	init?(from decoder: Decoder?) throws {
        let values = try decoder?.container(keyedBy: CodingKeys.self)
        token = (try values?.decodeIfPresent(String.self, forKey: .token))
        user = (try values?.decodeIfPresent(User.self, forKey: .user))!
	}

}
