

struct Languages : Codable {
	var id : Int?
	var user_id : Int?
	var deleted : Bool?
	var last_update : String?
	var language : Language?
	var level : Level?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case deleted = "deleted"
		case last_update = "last_update"
		case language = "language"
		case level = "level"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
		last_update = try values.decodeIfPresent(String.self, forKey: .last_update)
		language = try values.decodeIfPresent(Language.self, forKey: .language)
		level = try values.decodeIfPresent(Level.self, forKey: .level)
	}

    init(id: Int?, user_id: Int, deleted: Bool, last_update: String, language: Language, level: Level) {
        self.id = id
        self.user_id = user_id
        self.deleted = deleted
        self.last_update = last_update
        self.language = language
        self.level = level
    }
    
    init(user_id: Int) {
        self.id = nil
        self.user_id = user_id
        self.deleted = nil
        self.last_update = nil
        self.language = nil
        self.level = nil
    }
}
