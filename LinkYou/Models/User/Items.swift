

struct Items : Codable {
    let id : Int?
    let gift : Gift?
    let text : String?
    let is_private : Bool?
    let datetime : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case gift = "gift"
        case text = "text"
        case is_private = "is_private"
        case datetime = "datetime"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        gift = try values.decodeIfPresent(Gift.self, forKey: .gift)
        text = try values.decodeIfPresent(String.self, forKey: .text)
        is_private = try values.decodeIfPresent(Bool.self, forKey: .is_private)
        datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
    }
    
}
