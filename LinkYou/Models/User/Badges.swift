

struct Badges : Codable {
    
	let guests : Int
	let messages : Int
	let likes : Int
	let comments : Int
	let sympathies : Int

	enum CodingKeys: String, CodingKey {

		case guests = "guests"
		case messages = "messages"
		case likes = "likes"
		case comments = "comments"
		case sympathies = "sympathies"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		guests = try values.decode(Int.self, forKey: .guests)
		messages = try values.decode(Int.self, forKey: .messages)
		likes = try values.decode(Int.self, forKey: .likes)
		comments = try values.decode(Int.self, forKey: .comments)
		sympathies = try values.decode(Int.self, forKey: .sympathies)
	}

}
