
struct RemoveAccountResponse: Codable {
	let done : Bool?

	enum CodingKeys: String, CodingKey {

		case done = "done"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		done = try values.decodeIfPresent(Bool.self, forKey: .done)
	}

}
