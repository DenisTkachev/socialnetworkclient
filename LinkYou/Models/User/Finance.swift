

struct Finance : Codable {
	let id : Int?
	var name : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}
    
    init() {
        self.id = nil
        self.name = nil
    }
    
    init(name: String) {
        self.name = name
        switch name {
        case "Непостоянный доход": self.id = 2
        case "Постоянный небольшой доход": self.id = 3
        case  "Стабильный средний доход": self.id = 4
        case "Хорошо зарабатываю": self.id = 5
        default: self.id = 1
        }
    }

}
