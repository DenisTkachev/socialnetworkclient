

struct Books : Codable {
	let id : String?
	let user_id : Int?
	let name : String?
	let author : String?
	let deleted : Bool?
	let last_update : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case name = "name"
		case author = "author"
		case deleted = "deleted"
		case last_update = "last_update"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)?.toString()
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		author = try values.decodeIfPresent(String.self, forKey: .author)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
		last_update = try values.decodeIfPresent(String.self, forKey: .last_update)
	}

    init(id: String, user_id: Int, name: String, author: String, deleted: Bool, last_update: String) {
        self.id = id
        self.user_id = user_id
        self.name = name
        self.author = author
        self.deleted = deleted
        self.last_update = last_update
    }
    
    // nil constraction
    init() {
        self.id = nil
        self.user_id = nil
        self.name = nil
        self.author = nil
        self.deleted = nil
        self.last_update = nil
    }
}
