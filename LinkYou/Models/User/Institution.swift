

struct Institution : Codable {
	var id : Int?
	var user_id : Int?
	var name : String?
	var active : Bool?
	var deleted : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case name = "name"
		case active = "active"
		case deleted = "deleted"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		active = try values.decodeIfPresent(Bool.self, forKey: .active)
		deleted = try values.decodeIfPresent(Bool.self, forKey: .deleted)
	}
    
    init(id: Int?, user_id: Int, name: String, active: Bool, deleted: Bool) {
        self.id = id
        self.user_id = user_id
        self.name = name
        self.active = active
        self.deleted = deleted
    }

}
