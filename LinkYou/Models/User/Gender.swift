

struct Gender : Codable {
	let id : Int?
	let name : String?
	var code : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case code = "code"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
        if name == "" { // костыль - сервер по-умолчанию для новых юзеров возвращает мужской пол. Скидываем в nil
            code = nil
        } else {
            code = try values.decodeIfPresent(String.self, forKey: .code)
        }
	}

}
