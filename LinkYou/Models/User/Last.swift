

struct Last : Codable {
	let id : Int?
    //let user : User?
	let user_id : Int?
	let text : String?
	let text_short : String?
	let tags : [Tags]?
    let photos : [Photos]?
	let sort : Int?
	let video_link : String?
	let audio_link : String?
	let comments_count : Int?
	let views_count : Int?
	let likes_count : Int?
	let is_liked : Bool?
	let last_update : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
        //case user = "user"
		case user_id = "user_id"
		case text = "text"
		case text_short = "text_short"
		case tags = "tags"
        case photos = "photos"
		case sort = "sort"
		case video_link = "video_link"
		case audio_link = "audio_link"
		case comments_count = "comments_count"
		case views_count = "views_count"
		case likes_count = "likes_count"
		case is_liked = "is_liked"
		case last_update = "last_update"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self) // [Last]
		id = try values.decodeIfPresent(Int.self, forKey: .id)
        //user = try values.decodeIfPresent(User.self, forKey: .user)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		text_short = try values.decodeIfPresent(String.self, forKey: .text_short)
		tags = try values.decodeIfPresent([Tags].self, forKey: .tags)
        photos = try? values.decode([Photos].self, forKey: .photos)
		sort = try values.decodeIfPresent(Int.self, forKey: .sort)
		video_link = try values.decodeIfPresent(String.self, forKey: .video_link)
		audio_link = try values.decodeIfPresent(String.self, forKey: .audio_link)
		comments_count = try values.decodeIfPresent(Int.self, forKey: .comments_count)
		views_count = try values.decodeIfPresent(Int.self, forKey: .views_count)
		likes_count = try values.decodeIfPresent(Int.self, forKey: .likes_count)
		is_liked = try values.decodeIfPresent(Bool.self, forKey: .is_liked)
		last_update = try values.decodeIfPresent(String.self, forKey: .last_update)
	}

}
