

struct Children : Codable {
	let id : Int?
	var name : String?

    typealias actionSheetType = EditorRegistrationPresenter.ActionSheetType
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}

    init(name: String) {
        self.name = name
        
        switch name {
        case actionSheetType.unknown.rawValue: self.id = 0
        case actionSheetType.childrenYes.rawValue: self.id =  1
        case actionSheetType.no.rawValue: self.id = 2
        default:
            self.id = nil
        }
    }
    
}
