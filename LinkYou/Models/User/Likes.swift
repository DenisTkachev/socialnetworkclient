

struct Likes : Codable {
	var count : Int?
	var is_liked : Bool?

	enum CodingKeys: String, CodingKey {

		case count = "count"
		case is_liked = "is_liked"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		count = try values.decodeIfPresent(Int.self, forKey: .count)
		is_liked = try values.decodeIfPresent(Bool.self, forKey: .is_liked)
	}

}
