
import IGListKit
import Emoji

final class Message : Codable {
	let id: Int? // +
	let user: User? // +
	let unread_messages_count: Int? // +
	let last_message: Lastmessage? // +
    var read: Bool?
    var comment : String?
    let pictures : [String]?
    let datetime: String?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
        case user = "user"
		case last_message = "last_message"
        case unread_messages_count = "unread_messages_count"
        case read = "read"
        case comment = "comment_ios"
        case pictures = "pictures"
        case datetime = "datetime"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
        user = try values.decodeIfPresent(User.self, forKey: .user)
		last_message = try values.decodeIfPresent(Lastmessage.self, forKey: .last_message)
        unread_messages_count = try values.decodeIfPresent(Int.self, forKey: .unread_messages_count)
        read = try values.decodeIfPresent(Bool.self, forKey: .read)
        comment = try values.decodeIfPresent(String.self, forKey: .comment)
        pictures = try values.decodeIfPresent([String].self, forKey: .pictures)
        datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
        comment = comment?.emojiUnescapedString
	}

    init(user: User) {
        self.id = nil
        self.user = user
        self.last_message = nil
        self.unread_messages_count = 0
        self.read = false
        self.comment = ""
        self.pictures = []
        self.datetime = nil
    }
    
    func setRead() {
        self.read = true
    }
    
}

extension Message: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return id! as NSNumber
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? Message else { return false }
        return self.user!.id == object.user!.id
    }
}


extension String {
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF:   // Various (e.g. 🤖)
                return true
            default:
                continue
            }
        }
        return false
    }
}

