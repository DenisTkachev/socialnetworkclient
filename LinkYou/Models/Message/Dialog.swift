
import IGListKit

final class Dialog: Codable {
    
    let id: Int? // +
    let fromUser: User? // +
    let unread_messages_count: Int? // +
    let last_update: String? // +
    let last_message: Lastmessage? // +
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case fromUser = "from_user"
        case last_update = "last_update"
        case last_message = "last_message"
        case unread_messages_count = "unread_messages_count"

    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        fromUser = try values.decodeIfPresent(User.self, forKey: .fromUser)
        last_update = try values.decodeIfPresent(String.self, forKey: .last_update)
        last_message = try values.decodeIfPresent(Lastmessage.self, forKey: .last_message)
        unread_messages_count = try values.decodeIfPresent(Int.self, forKey: .unread_messages_count)
        
    }
    
    init(user: User) {
        self.id = nil
        self.fromUser = user
        self.last_update = nil
        self.last_message = nil
        self.unread_messages_count = 0
    }
}


extension Dialog: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return id! as NSNumber
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? Dialog else { return false }
        return self.id! == object.id!
    }
}
