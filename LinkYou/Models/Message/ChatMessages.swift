//
//  ChatMessages.swift
//  LinkYou
//
//  Created by Denis Tkachev on 20/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

class ChatMessages: Codable {
    
    let dialog: Dialog?
    let messages: [Message]?
    
    enum CodingKeys: String, CodingKey {
        
        case dialog = "dialog"
        case messages = "messages"
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dialog = try values.decodeIfPresent(Dialog.self, forKey: .dialog)
        messages = try values.decodeIfPresent([Message].self, forKey: .messages)
        
    }
    
}
