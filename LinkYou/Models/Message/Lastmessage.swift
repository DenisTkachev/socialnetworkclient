

struct Lastmessage : Codable {
	let id : Int?
	let dialog_id : Int?
	let user : User?
	let comment : String?
	let pictures : [String]?
	let read : Bool?
	let last_update : String?
    let datetime : String?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case dialog_id = "dialog_id"
		case user = "user"
		case comment = "comment"
		case pictures = "pictures"
		case read = "read"
		case last_update = "last_update"
        case datetime = "datetime"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		dialog_id = try values.decodeIfPresent(Int.self, forKey: .dialog_id)
		user = try values.decodeIfPresent(User.self, forKey: .user)
		comment = try values.decodeIfPresent(String.self, forKey: .comment)
		pictures = try values.decodeIfPresent([String].self, forKey: .pictures)
		read = try values.decodeIfPresent(Bool.self, forKey: .read)
		last_update = try values.decodeIfPresent(String.self, forKey: .last_update)
        datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
	}

}
