


struct UserPhotos : Codable {
	var id : Int?
	let user_id : Int?
	let datetime : String?
	let description : String?
	let src : Src?
    var isLiked: Bool?
    var likesCount: Int?
    var commentsCount: Int?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case datetime = "datetime"
		case description = "description"
		case src = "src"
        case isLiked = "is_liked"
        case likesCount = "likes_count"
        case commentsCount = "comments_count"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		src = try values.decodeIfPresent(Src.self, forKey: .src)
        isLiked = try values.decodeIfPresent(Bool.self, forKey: .isLiked)
        likesCount = try values.decodeIfPresent(Int.self, forKey: .likesCount)
        commentsCount = try values.decodeIfPresent(Int.self, forKey: .commentsCount)
	}

    init(id: Int, user_id: Int, datetime: String, description: String, src: Src?, isLiked: Bool, likesCount: Int, commentsCount: Int) {
        self.id = id
        self.user_id = user_id
        self.datetime = datetime
        self.description = description
        self.src = src
        self.isLiked = isLiked
        self.likesCount = likesCount
        self.commentsCount = commentsCount
    }
    
    init() {
        self.id = nil
        self.user_id = nil
        self.datetime = nil
        self.description = nil
        self.src = nil
        self.isLiked = nil
        self.likesCount = nil
        self.commentsCount = nil
    }
    
    static func makePhotoFromAvatarObj(avatar: Avatar?) -> UserPhotos {
        let photo = UserPhotos(id: avatar!.id!, user_id: avatar!.user_id!, datetime: avatar!.datetime!, description: avatar!.description!, src: avatar!.src, isLiked: avatar!.isLiked!, likesCount: avatar!.likesCount!, commentsCount: avatar!.commentsCount!)
        return photo
    }
}
