

struct Src : Codable {
	var small: String?
	var `default`: String?
	var origin: String?
    var square: String?
    
	enum CodingKeys: String, CodingKey {

		case small = "small"
		case `default` = "default"
		case origin = "origin"
        case square = "square"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		small = try values.decodeIfPresent(String.self, forKey: .small)
		`default` = try values.decodeIfPresent(String.self, forKey: .default)
		origin = try values.decodeIfPresent(String.self, forKey: .origin)
        square = try values.decodeIfPresent(String.self, forKey: .square)
        
        small = checkURLFormat(small)
        `default` = checkURLFormat(`default`)
        origin = checkURLFormat(origin)
        square = checkURLFormat(square)
	}

    private func checkURLFormat(_ url: String?) -> String? {
        guard let url = url else { return nil }
        
        if url.contains("http") {
            return url
        } else {
            return "https:\(url)"
        }
    }
}
