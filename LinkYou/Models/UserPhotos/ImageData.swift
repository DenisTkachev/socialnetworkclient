

struct ImageData : Codable {
	let id : String?
	let file_js_hash : [String]?
	let hostname : String?
	let src : String?
	let dir : String?
	let width : Int?
	let height : Int?
	let type : String?
	let name : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case file_js_hash = "file_js_hash"
		case hostname = "hostname"
		case src = "src"
		case dir = "dir"
		case width = "width"
		case height = "height"
		case type = "type"
		case name = "name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		file_js_hash = try values.decodeIfPresent([String].self, forKey: .file_js_hash)
		hostname = try values.decodeIfPresent(String.self, forKey: .hostname)
		src = try values.decodeIfPresent(String.self, forKey: .src)
		dir = try values.decodeIfPresent(String.self, forKey: .dir)
		width = try values.decodeIfPresent(Int.self, forKey: .width)
		height = try values.decodeIfPresent(Int.self, forKey: .height)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}

}
