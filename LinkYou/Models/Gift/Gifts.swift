
struct Gifts : Codable {
	let count : Int?
	let free_gifts : Int?
	let items : [GiftItems]?
	let got_free_gifts : Bool?

	enum CodingKeys: String, CodingKey {

		case count = "count"
		case free_gifts = "free_gifts"
		case items = "items"
		case got_free_gifts = "got_free_gifts"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		count = try values.decodeIfPresent(Int.self, forKey: .count)
		free_gifts = try values.decodeIfPresent(Int.self, forKey: .free_gifts)
		items = try values.decodeIfPresent([GiftItems].self, forKey: .items)
		got_free_gifts = try values.decodeIfPresent(Bool.self, forKey: .got_free_gifts)
	}

}
