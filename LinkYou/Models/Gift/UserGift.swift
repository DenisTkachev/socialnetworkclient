

struct UserGift : Codable {
	let id : Int?
	let user : User?
	let gift : Gift?
	let text : String?
	let is_private : Bool?
	let datetime : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user = "user"
		case gift = "gift"
		case text = "text"
		case is_private = "is_private"
		case datetime = "datetime"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user = try values.decodeIfPresent(User.self, forKey: .user)
		gift = try values.decodeIfPresent(Gift.self, forKey: .gift)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		is_private = try values.decodeIfPresent(Bool.self, forKey: .is_private)
		datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
	}

}
