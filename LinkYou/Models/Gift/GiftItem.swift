

final class GiftItem : Codable {
	let id : Int?
	let name : String?
	var cost : Int?
	let picture : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case cost = "cost"
		case picture = "picture"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		cost = try values.decodeIfPresent(Int.self, forKey: .cost)
		picture = try values.decodeIfPresent(String.self, forKey: .picture)
	}

}
