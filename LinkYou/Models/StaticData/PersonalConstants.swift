//
//  PersonalConstants.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

//import Foundation

struct PersonalConstants {
    
    let lookingFor = [AutocompleteModel(id: 2, title: "Женщину", highlighted: "", subtitle: ""),
                      AutocompleteModel(id: 1, title: "Мужчину", highlighted: "", subtitle: ""),
                      AutocompleteModel(id: 3, title: "Друзей", highlighted: "", subtitle: "")]
    
    let relationship = ["Не указано", "Нет", "В браке", "Есть отношения"]
    
    let orientation = ["Не указано","Гетеросексуальная","Гомосексуальная","Бисексуальная"]
    
    let children = ["Не указано","Есть","Нет"]
    
    let smoke = ["Не указано", "Не курю и не приемлю", "Не курю и отношусь нейтрально", "Курю", "Изредка курю", "Бросаю"]
    
    let alkho = ["Не указано", "Не пью", "Изредка могу поддержать компанию", "Люблю выпить"]
    
    let educationType = ["Не указано","Среднее","Среднее специальное","Неполное высшие","Высшие"]
    
    let languageLevel = ["Родной язык","Базовый уровень","Читаю литературу","Свободно владею"]
    
    let finance = ["Непостоянный доход","Постоянный небольшой доход","Стабильный средний доход","Хорошо зарабатываю"]
    
    static var allLanguages = [AutocompleteModel]()
    static var allReligions = [AutocompleteModel]()
    static var allNationality = [AutocompleteModel]()
    static var allProfessions = [AutocompleteModel]()
    static var allCities = [AutocompleteModel]()
    static var age = [AutocompleteModel]()
    static var profarea = [AutocompleteModel]()
    static var goals = [AutocompleteModel]()
    static var priceRates: Payment?
}
