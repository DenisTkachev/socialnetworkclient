


struct JobUsersDaily : Codable {
	let profession : String?
	let occupation : String?

	enum CodingKeys: String, CodingKey {

		case profession = "profession"
		case occupation = "occupation"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		profession = try values.decodeIfPresent(String.self, forKey: .profession)
		occupation = try values.decodeIfPresent(String.self, forKey: .occupation)
	}

}
