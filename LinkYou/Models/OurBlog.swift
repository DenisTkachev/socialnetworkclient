


struct OurBlog : Codable {
	let id : Int?
	let name : String?
	let code : String?
	let text : String?
	var text_short : String?
	let datetime : String?
	let comments_count : Int?
	let likes_count : Int?
	let is_liked : Bool?
	let images : [String]?
	let iframes : [String]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case code = "code"
		case text = "text"
		case text_short = "text_short"
		case datetime = "datetime"
		case comments_count = "comments_count"
		case likes_count = "likes_count"
		case is_liked = "is_liked"
		case images = "images"
		case iframes = "iframes"
	}

    init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		text_short = try values.decodeIfPresent(String.self, forKey: .text_short)
		datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
		comments_count = try values.decodeIfPresent(Int.self, forKey: .comments_count)
		likes_count = try values.decodeIfPresent(Int.self, forKey: .likes_count)
		is_liked = try values.decodeIfPresent(Bool.self, forKey: .is_liked)
		images = try values.decodeIfPresent([String].self, forKey: .images)
		iframes = try values.decodeIfPresent([String].self, forKey: .iframes)
	}

}
