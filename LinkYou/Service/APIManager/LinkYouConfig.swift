//
//  LinkYouConfig.swift
//  LinkYou
//
//  Created by Denis Tkachev on 02/07/2019.
//  Copyright © 2019 klen. All rights reserved.
//

class LinkYouConfig {
    
    private let hostAPI = "https://api.linkyou.ru/v2"
    private let hostTestAPI = "http://linkyou2.klendev.ru/v2"
    
    func setHostAPI() -> String {
        return hostAPI
    }
    
}
