//
//  NetworkRequest.swift
//  LinkYou
//
//  Created by Denis Tkachev on 17/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Moya

class NetworkRequest {
    
    func loadBadges(completion: @escaping (Badges)->()) {
        let authPlagin = AccessTokenPlugin { () -> String in
            DeviceCurrent.current.sessionToken
        }
        
        let usersProvider = MoyaProvider<GeneralService>(plugins: [authPlagin])
        usersProvider.request(.getCurrentBadges) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(Badges.self, from: response.data)
                    DispatchQueue.main.async {
                        return completion(serverResponse)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadShortUserData(id: Int, completion: @escaping (ShortUser)->()) {
        let authPlagin = AccessTokenPlugin { () -> String in
            DeviceCurrent.current.sessionToken
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        usersProvider.request(.userShortData(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(ShortUser.self, from: response.data)
                    DispatchQueue.main.async {
                        return completion(serverResponse)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func sendParametrsToServer(_ parameters: [String: String]) {
//        if !Reachability.isConnectedToNetwork() {
//            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
//            return
//        }
        let authPlagin = AccessTokenPlugin { () -> String in
            DeviceCurrent.current.sessionToken
        }
        
        let usersProvider = MoyaProvider<EditUserFieldsService>(plugins: [authPlagin])
        usersProvider.request(.delete(parameters)) { (result) in
            switch result {
            case .success(let response):
                var result = try? JSONDecoder().decode([String:Bool].self, from: response.data)
                if result?["success"] == true {
                    print("Сервер сохранил изменения!")
                } else {
                    print("Сервер не сохранил изменения!")
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
}


