//
//  PaymentService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

// POST http://linkyou2.klendev.ru/v2/payment/
/*
 Входящие параметры:
 payment_system_id - Это ТИП платежной системы. Допустимые значения 1 и 4. 1 это Fake Bank, 4 это Робокасса. Пока используем только 1.
 payment_type_id - Это тип платежа или по другому, что мы оплачиваем. Подарок, за регистрацию, премиум, поднятие анкеты и т.д.
 
 Возможные значения для payment_type_id:
 Подарок = 1;
 Премиум = 2;
 Поднятие анкеты = 3;
 Попасть на главную = 4;
 Попасть в блок "Интересные анкеты" = 5;
 Невидимость = 6;
 Членский взнос за регистрацию = 7;
 
 entity_id - Это ID продукта или ID сущности. Всё зависит от того, что мы оплачиваем. Параметр кастомный, используется не всегда. Возможные значения:
 Для подарков - это ID подарка
 
 Для премиума
 - 10 дней премиума = 1;
 - 30 премиума = 2;
 - 150 дней премиума = 3;
 - 1 год премиума = 4;
 
 Для остальных
 - 1 = Два дня на главной странице
 - 2 = 3 дня на главной
 - 3 = 4 дня на главной
 - 4 = Попасть на страницу "Интересные анкеты" на 2 дня
 - 5 = Попасть на страницу "Интересные анкеты" на 3 дня
 - 6 = Попасть на страницу "Интересные анкеты" на 4 дня
 - 7 = Невидимость на 10 дней
 - 8 = Невидимость на 30 дней
 - 9 = Невидимость на 150 дней
 - 10 = Невидимость на 1 год
 - 11 = Оплата за регистрацию
 */



import Moya

class PaymentServiceConstant {
    let paymentSystemId = "1" // тестовый
    //    let paymentSystemId = "4" // боевой робокасса
}


enum PaymentService {
    
    case getPaymentLink(paymentTypeId: Int, entityId: Int)
    case priceRate
    case checkPaymentStatus
}

extension PaymentService: TargetType {
    
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .getPaymentLink:
            return "/payment/"
        case .priceRate:
            return "/payment/prices"
        case .checkPaymentStatus:
            return "/payment/list"
        }
    }
    var method: Moya.Method {
        switch self {
        case .getPaymentLink, .checkPaymentStatus:
            return .post
        case .priceRate:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getPaymentLink(let param):
            return .requestParameters(parameters: ["payment_system_id": PaymentServiceConstant().paymentSystemId, "payment_type_id": param.paymentTypeId, "entity_id": param.entityId], encoding: URLEncoding.default)
        
        case .priceRate, .checkPaymentStatus:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

extension PaymentService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}


