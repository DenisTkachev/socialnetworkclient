//
//  GiftsService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 17.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum GiftsService {
    case getUserGifts(userId: Int)
    case getCategoriesOfGifts
    case recommendationGift(userId: Int)
    // post delete gift, send gift, new gift
    
    
}

extension GiftsService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .getUserGifts(let id):
            return "/gifts/\(id)/"
        case .getCategoriesOfGifts:
            return "/gifts/"
        case .recommendationGift(let id):
            return "/gifts/\(id)/recommended/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getUserGifts, .getCategoriesOfGifts, .recommendationGift:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getUserGifts, .getCategoriesOfGifts, .recommendationGift:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/json; charset=UTF-8"]
    }
}

extension GiftsService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}


