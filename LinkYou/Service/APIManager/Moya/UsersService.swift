//
//  UsersService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 02.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum UsersService {
    case login(login: String, pass: String)
    case registration(login: String, pass: String)
    case daily(page: Int)
    case userFullData(id: Int)
    case userLikersList(id: Int, page: Int)
    case userGuests(page: Int)
    case listsCities
    case favorites
    case blacklist
    case addUserToFav(id: Int)
    case deleteUserFromFav(id: Int)
    case top(page: Int)
    case forgotPassword(email: String)
    case matchedUsers
    case interestingUsers
    case toggleInvisableMode
    case removeCurrentAccount
    case currentUser
    case setAvatar(photoId: Int)
    case answers(page: Int)
    case comments(page: Int)
    case likes(page: Int)
    case sympathies(page: Int)
    case userShortData(id: Int)
}

enum XPaginationHeaders: String {
    case XPaginationTotal = "X-Pagination-Total"
    case XPaginationLimit = "X-Pagination-Limit"
    case XPaginationIsEnd = "X-Pagination-Is-End"
    case XPaginationCurrent = "X-Pagination-Current"
}

extension UsersService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .login:
            return "/auth/signin"
        case .registration:
            return "/auth/signup"
        case .daily:
            return "/users/daily"
        case .userFullData(let id):
            return "/user/\(id)"
        case .userLikersList(let param):
            return "/user/\(param.id)/likes"
        case .userGuests:
            return "/guests"
        case .listsCities:
            return "/lists/cities"
        case .favorites:
            return "/favorites"
        case .blacklist:
            return "/blacklist"
        case .addUserToFav:
            return "/favorites/add"
        case .deleteUserFromFav:
            return "/favorites/delete"
        case .top:
            return "/users/top/"
        case .forgotPassword:
            return "/auth/forgot"
        case .matchedUsers:
            return "/users/matched"
        case .interestingUsers:
            return "/users/interesting"
        case .toggleInvisableMode:
            return "/user/visible/switch"
        case .removeCurrentAccount:
            return "/delete/profile/"
        case .currentUser:
            return "/user/current"
        case .setAvatar:
            return "/user/update"
        case .answers:
            return "/user/answers"
        case .comments:
            return "/user/comments"
        case .likes:
            return "/user/likes"
        case .sympathies:
            return "/user/sympathies"
        case .userShortData(let id):
            return "/user/\(id)/short"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .daily, .userFullData, .userShortData, .userLikersList, .userGuests, .listsCities, .favorites, .blacklist, .top, .matchedUsers, .interestingUsers, .removeCurrentAccount, .currentUser, .answers, .comments, .likes, .sympathies:
            return .get
            
        case .addUserToFav, .deleteUserFromFav, .forgotPassword, .toggleInvisableMode, .setAvatar, .registration, .login:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .registration(let login, let pass), .login(let login, let pass):
            return .requestParameters(parameters: ["login" : login, "password" : pass], encoding: URLEncoding.default)
    case .userFullData, .userShortData, .listsCities, .favorites, .blacklist, .matchedUsers, .interestingUsers, .toggleInvisableMode, .removeCurrentAccount, .currentUser:
            return .requestPlain
        case .addUserToFav(let id):
            return .requestParameters(parameters: ["user_id": id], encoding: URLEncoding.default)
        case .deleteUserFromFav(let id):
            return .requestParameters(parameters: ["user_id": id], encoding: URLEncoding.default)
        case .top(let page), .userGuests(let page), .daily(let page):
            return .requestParameters(parameters: ["page": page], encoding: URLEncoding.queryString)
        case .forgotPassword(let email):
            return .requestParameters(parameters: ["email": email], encoding: URLEncoding.default)
        case .userLikersList(let param):
            return .requestParameters(parameters: ["page": param.page], encoding: URLEncoding.queryString)
        case .setAvatar(let id):
            return .requestParameters(parameters: ["avatar": id], encoding: URLEncoding.default)
        case .answers(let page), .likes(let page), .comments(let page), .sympathies(let page):
            return .requestParameters(parameters: ["page": page], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

extension UsersService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}
