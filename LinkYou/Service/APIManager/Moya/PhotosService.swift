//
//  PhotosService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 08.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum PhotosService {
    case userPhotos(id: Int, limit: Int)
    case userPhotoComments(id: Int)
    case userPhotosLikes(id: Int)
    case userPhotoLike(id: Int)
    case bindPhotoToGallery(url: String)
    case deletePhoto(id: Int)
    case addComment(id: Int, comment: String)
    case deletePhotoComment(commentId: Int, photoId: Int)
    case convertPhotoToAvatar(url: String)
    case updateAvatar(url: String)
}

extension PhotosService: TargetType {
    var baseURL: URL {
        switch self {
        case .convertPhotoToAvatar:
            return URL(string: "https://img.linkyou.ru/face.php")!
        default:
            return URL(string: LinkYouConfig().setHostAPI())!
        }
        
    }
    
    var path: String {
        switch self {
        case .userPhotos(let id):
            return "/photos/\(id.id)"
        case .userPhotoComments(let id):
            return "/photo/\(id)/comments"
        case .userPhotosLikes(let id):
            return "/photo/\(id)/likes"
        case .userPhotoLike(let id): // лайкаем фотографию
            return "/photo/\(id)/likes"
        case .bindPhotoToGallery:
            return "/photos/add"
        case .deletePhoto(let id):
            return "/photo/\(id)/delete"
        case .addComment(let id):
            return "/photo/\(id.id)/comment/add"
        case .deletePhotoComment(let commentId, let photoId):
            return "/photo/\(photoId)/comment/\(commentId)/delete"
        case .convertPhotoToAvatar:
            return ""
        case .updateAvatar:
            return "/user/update"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .userPhotos: return .get
        case .userPhotoComments: return .get
        case .userPhotosLikes: return .get
        case .userPhotoLike: return .post
        case .bindPhotoToGallery: return .post
        case .deletePhoto: return .post
        case .addComment, .deletePhotoComment: return .post
        case .convertPhotoToAvatar, .updateAvatar: return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .userPhotos(let limit): return .requestParameters(parameters: ["limit": limit.limit], encoding: URLEncoding.default)
        case .userPhotoComments: return .requestPlain
        case .userPhotosLikes: return .requestPlain
        case .userPhotoLike: return .requestPlain
        case .bindPhotoToGallery(let url):
            return .requestParameters(parameters: ["src": url], encoding: URLEncoding.default)
            
        case .updateAvatar(let url):
            return .requestParameters(parameters: ["avatar": url], encoding: URLEncoding.default)
            
        case .deletePhoto: return .requestPlain
        case .addComment(let comment):
            return .requestParameters(parameters: ["comment": comment.comment], encoding: URLEncoding.default)
        case .deletePhotoComment: return .requestPlain
        case .convertPhotoToAvatar(let url):
            if url.contains("https") {
                return .requestParameters(parameters: ["src": "\(url)"], encoding: URLEncoding.default)
            } else {
                return .requestParameters(parameters: ["src": "https:\(url)"], encoding: URLEncoding.default)
            }
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .userPhotos: return ["Content-Type" : "application/x-www-form-urlencoded"]
        case .userPhotoComments: return ["Content-Type" : "application/json; charset=UTF-8"]
        case .userPhotosLikes: return ["Content-Type" : "application/json; charset=UTF-8"]
        case .userPhotoLike: return ["Content-Type" : "application/x-www-form-urlencoded"]
        case .bindPhotoToGallery: return ["Content-Type" : "application/x-www-form-urlencoded"]
        case .deletePhoto: return ["Content-Type" : "application/json; charset=UTF-8"]
        case .addComment: return ["Content-Type" : "application/x-www-form-urlencoded"]
        case .deletePhotoComment: return ["Content-Type" : "application/x-www-form-urlencoded"]
        case .convertPhotoToAvatar, .updateAvatar: return ["Content-Type" : "application/x-www-form-urlencoded"]
        }
    }
}

extension PhotosService: AccessTokenAuthorizable {
        var authorizationType: AuthorizationType {
            return .bearer
        }
}
