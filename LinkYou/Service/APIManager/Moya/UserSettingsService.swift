//
//  UserSettingsService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 31/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum UserSettingsService {
    case setEmail(email: String)
    case setNewPassword(current: String, password: String, passwordConfirm: String)
    case getUserNitifications
    case switchNotification(code: String)
    case switchEnablingAccount
}

extension UserSettingsService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .setEmail:
            return "/auth/email"
        case .setNewPassword:
            return "/auth/password"
        case .getUserNitifications:
            return "/user/notifications"
        case .switchNotification:
            return "/user/notification/switch"
        case .switchEnablingAccount:
            return "/user/active/switch"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .setEmail, .setNewPassword, .switchNotification, .switchEnablingAccount:
            return .post
        case .getUserNitifications:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .setEmail(let email):
            return .requestParameters(parameters: ["email": email], encoding: URLEncoding.default)
            
        case .setNewPassword(let current, let password, let passwordConfirm):
            return .requestParameters(parameters: ["current": current, "password": password, "password_confirm": passwordConfirm], encoding: URLEncoding.default)
        case .getUserNitifications, .switchEnablingAccount:
            return .requestPlain
        case .switchNotification(let code):
            return .requestParameters(parameters: ["code": code], encoding: URLEncoding.default)
        }
        
    }
    
    var headers: [String : String]? {
        switch self {
        case .getUserNitifications:
            return ["Content-Type" : "application/json; charset=UTF-8"]
        default:
            return ["Content-Type" : "application/x-www-form-urlencoded"]
        }
    }
}

extension UserSettingsService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}
