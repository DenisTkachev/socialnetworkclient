//
//  MessageService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 06.10.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum MessageService {
    case getDialogs
    case getMessages(id: Int, page: Int)
    case sendMessage(recipientId: Int, content: String, pictures: [String]) // картинки массив https://img.linkyou.ru/ со слов Андрея. wtf?
    case deleteDialog(id: Int)
    case deleteMessage(id: Int)
    case getOneMessage(id: Int)
    case messageIsReading(id: Int)
}

extension MessageService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .getDialogs:
            return "/dialogs/"
        case .getMessages(let param):
            return "/dialog/\(param.id)"
        case .sendMessage:
            return "/message/send"
        case .deleteDialog(let id):
            return "/dialog/\(id)/delete"
        case .deleteMessage:
            return "/message/delete"
        case .getOneMessage(let id):
            return "/message/\(id)"
        case .messageIsReading(let id):
            return "/dialog/\(id)/read"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getDialogs, .getOneMessage, .getMessages:
            return .get
        case .deleteDialog, .deleteMessage, .sendMessage, .messageIsReading:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getMessages(let param):
            return .requestParameters(parameters: ["page": param.page], encoding: URLEncoding.default)
        case .sendMessage(let data):
            return .requestParameters(parameters: ["user_id": data.recipientId, "content": data.content, "pictures": data.pictures.first ?? ""], encoding: URLEncoding.default)
        case .deleteMessage(let data):
            return .requestParameters(parameters: ["id": data], encoding: URLEncoding.default)
        case .getOneMessage, .messageIsReading, .deleteDialog, .getDialogs:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .sendMessage, .deleteMessage:
            return ["Content-Type" : "application/x-www-form-urlencoded"]
        default:
            return ["Content-Type" : "application/json; charset=UTF-8"]
        }
        
    }
}

extension MessageService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}



