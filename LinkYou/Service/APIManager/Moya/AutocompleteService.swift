//
//  AutocompleteService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum AutocompleteService {
    case getLanguages
    case getReligions
    case getNationality
    case getProfessions
    case getCities
    case age
    case getProfarea
    case getGoals
}

extension AutocompleteService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .getLanguages:
            return "/autocomplete/languages"
        case .getReligions:
            return "/autocomplete/religions"
        case .getNationality:
            return "/autocomplete/nationality"
        case .getProfessions:
            return "/autocomplete/professions"
        case .getCities:
            return "/autocomplete/cities"
        case .age:
            return "/autocomplete/age"
        case .getProfarea:
            return "/autocomplete/profarea"
        case .getGoals:
            return "/autocomplete/goals"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getLanguages, .getReligions, .getNationality, .getProfessions, .getCities, .age, .getProfarea, .getGoals:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getLanguages, .getReligions, .getNationality, .getProfessions, .getCities, .age, .getProfarea, .getGoals:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/json; charset=UTF-8"]
    }
}

extension AutocompleteService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}


