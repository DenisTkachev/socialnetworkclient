//
//  UploadPhotosService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 31.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum UploadPhotosService {
    case uploadUserPhoto(data: Data)

}

extension UploadPhotosService: TargetType {
    var baseURL: URL {
        return URL(string: "https://img.linkyou.ru")!
    }
    
    var path: String {
        switch self {
        case .uploadUserPhoto:
            return "/index.php"
            

        }
    }
    
    var method: Moya.Method {
        switch self {
        case .uploadUserPhoto: return .post

        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .uploadUserPhoto(let data):
            
            let imageSize: Int = data.count
            print("size of image in MB: ", Double(imageSize) / 1024.0 / 1024.0)
            
            return .uploadMultipart([MultipartFormData(provider: .data(data),
                                                       name: "images",
                                                       fileName: "images.jpeg",
                                                       mimeType: "image/jpeg")])
        }
    }

    var headers: [String : String]? {
        switch self {
        case .uploadUserPhoto: return ["Content-Type" : "application/json"]

        }
    }
}

extension UploadPhotosService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}
