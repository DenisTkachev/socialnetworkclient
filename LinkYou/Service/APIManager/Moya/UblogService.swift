//
//  UblogService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 02.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum UblogService {
    case listBlogs(userId: Int)
    case postDetail(postId: Int)
    case postComments(postId: Int)
    case savePost(text: String, videoLink: String, audioLink: String, photos: [String:String])
    case deletePost(postId: Int)
    case popularBlog
    case addCommentToPost(postId: Int, text: String)
    case deleteComment(postId: Int, commentId: Int)
    case getArticle(articleId: Int)
    case setPostLike(postId: Int)
    // Company blog
    case ourBlog
}

extension UblogService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .listBlogs(let userId):
            return "/ublogs/\(userId))"
            
        case .postDetail(let postId):
            return "/ublog/\(postId)"
            
        case .postComments(let postId):
            return "/ublog/\(postId)/comments"
            
        case .savePost:
            return "/ublogs/add"
            
        case .deletePost(let postId):
            return "/ublog/\(postId)/delete"
            
        case .ourBlog:
            return "/articles/"
            
        case .popularBlog:
            return "/ublogs/popular/"
            
        case .addCommentToPost(let data):
            return "/ublog/\(data.postId)/comment/add"
            
        case .deleteComment(let data):
            return "/ublog/\(data.postId)/comment/\(data.commentId)/delete"
            
        case .getArticle(let articleId):
            return "/article/\(articleId)"
            
        case .setPostLike(let postId):
            return "/post/\(postId)/like"
        }

    }
    
    var method: Moya.Method {
        switch self {
        case .listBlogs, .getArticle, .postDetail, .postComments, .ourBlog, .popularBlog: return .get
        case .savePost, .deletePost, .addCommentToPost, .deleteComment, .setPostLike: return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .listBlogs, .postDetail, .postComments, .deletePost, .popularBlog, .deleteComment, .getArticle, .setPostLike:
            return .requestPlain
        case .savePost(let text, let videoLink, let audioLink, let photos):
            var parameters: [String : String] = ["text": text, "video_link": videoLink, "audio_link": audioLink]
            parameters.merge(photos, uniquingKeysWith: { (first, _) in first })
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case .addCommentToPost(let data):
            return .requestParameters(parameters: ["comment": data.text], encoding: URLEncoding.default)
        case .ourBlog:
            return .requestParameters(parameters: ["mobile": "1"], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

extension UblogService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}
