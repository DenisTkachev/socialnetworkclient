//
//  ClaimTicketService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 15.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//


import Moya

enum ClaimTicketService {
    case sendTicket(id: Int?, title: String, message: String, entity: String) // repeat for user disliked
    case addUserToBL(id: Int)
    case deleteUserFromBl(id: Int)

    
}

extension ClaimTicketService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }

    var path: String {
        switch self {
        case .sendTicket:
            return "/tickets/support"
        case .addUserToBL:
            return "/blacklist/add"
        case .deleteUserFromBl:
            return "/blacklist/delete"

            
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .sendTicket:
            return .post
        case .addUserToBL, .deleteUserFromBl:
            return .post


        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .sendTicket(let id, let title, let message, let entity):
            return .requestParameters(parameters: ["entityId" : id ?? "", "title" : title, "message": message, "entity": entity], encoding: URLEncoding.default)
        case .addUserToBL(let id):
                        return .requestParameters(parameters: ["user_id": id], encoding: URLEncoding.default)
        case .deleteUserFromBl(let id):
                        return .requestParameters(parameters: ["user_id": id], encoding: URLEncoding.default)
        

        }
    }

    var headers: [String: String]? {
        return ["Content-type": "application/x-www-form-urlencoded"]
    }
}

extension ClaimTicketService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}
