//
//  LikesService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 14.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum LikesService {
    case userLike(id: Int) // repeat for user disliked
}

extension LikesService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .userLike(let id):
            return "/user/\(id)/like"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .userLike:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .userLike:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/json; charset=UTF-8"]
    }
}

extension LikesService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}


