//
//  LinkYouLikeCardsService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Moya

enum LinkYouLikeCardsService {
    case getList
    case disLike(userID: Int)
    case like(userID: Int)
}

extension LinkYouLikeCardsService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .getList:
            return "/likeyou/list"
        case .disLike:
            return "/likeyou/skip"
        case .like:
            return "/likeyou/like"
        }
    }
    var method: Moya.Method {
        switch self {
        case .getList:
            return .get
        case .disLike, .like:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getList:
            return .requestPlain
        case .like(let userID):
            return .requestParameters(parameters: ["user_id": userID], encoding: URLEncoding.default)
        case .disLike(let userID):
            return .requestParameters(parameters: ["user_id": userID], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

extension LinkYouLikeCardsService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}

