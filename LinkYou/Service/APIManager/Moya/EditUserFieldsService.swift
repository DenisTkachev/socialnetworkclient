//
//  EditUserFieldsService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 30/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Moya

enum EditUserFieldsService {
    case set(_ parameters: [String:String])
    case delete(_ parameters: [String:String])
}

extension EditUserFieldsService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .set:
            return "/user/update"
        case .delete:
            return "/user/delete"
        }
    }
        var method: Moya.Method {
            switch self {
            case .set, .delete:
                return .post
            }
        }
        
        var sampleData: Data {
            return Data()
        }
        
        var task: Task {
            switch self {
            case .set(let data), .delete(let data):
                return .requestParameters(parameters: data, encoding: URLEncoding.default)
            }
        }
        
        var headers: [String : String]? {
            return ["Content-Type" : "application/x-www-form-urlencoded"]
        }
}

extension EditUserFieldsService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}
