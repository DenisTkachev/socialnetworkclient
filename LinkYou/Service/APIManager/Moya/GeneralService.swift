//
//  GeneralService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//


import Moya

enum GeneralService {
    case searchUsers(request: SearchRequest, page: Int)
    case searchListCount(request: SearchRequest, page: Int)
    case getCurrentBadges
}

extension GeneralService: TargetType {
    var baseURL: URL {
        return URL(string: LinkYouConfig().setHostAPI())!
    }
    
    var path: String {
        switch self {
        case .searchUsers:
            return "users/search"
        case .getCurrentBadges:
            return "badges"
        case .searchListCount:
            return "users/search/count"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .searchUsers, .getCurrentBadges, .searchListCount:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .searchUsers(let request, let page), .searchListCount(let request, let page):
            return .requestParameters(parameters: ["looking_for": request.lookingFor.0,
                                                   "age_from": request.ageFrom,
                                                   "age_to": request.ageTo,
                                                   "city": request.city.0,
                                                   "profession": request.profession.0,
                                                   "nationality": request.nationality.0,
                                                   "religion": request.religion.0,
                                                   "language": request.language, "page": page,
                                                   "prof_are": request.profArea.0], encoding: URLEncoding.default)
        
        case .getCurrentBadges:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

extension GeneralService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}
