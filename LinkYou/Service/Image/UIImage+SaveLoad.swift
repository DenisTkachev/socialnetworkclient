//
//  UIImage+SaveLoad.swift
//  LinkYou
//
//  Created by Denis Tkachev on 31.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UIImage {
    
    func saveImage() -> Bool {
        guard let data = self.jpegData(compressionQuality: 1) ?? self.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("screenShot.png")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
}
