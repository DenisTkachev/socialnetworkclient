//
//  SocketConnection.swift
//  LinkYou
//
//  Created by Denis Tkachev on 15/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Foundation
import SocketIO


open class SocketConnection {
    
    open static let `default` = SocketConnection()
    private let manager: SocketManager
    private var socket: SocketIOClient
    
    private init() {
        
        manager = SocketManager(socketURL: URL(string: "http://socket.linkyou.klendev.ru:8080")!, config: [.log(true),.connectParams(["token":DeviceCurrent.current.sessionToken]),.reconnects(true)])
        socket = manager.socket(forNamespace: "/ios")
    }
}



//    let manager = SocketManager(socketURL: URL(string: "http://socket.linkyou.ru:8080")!, config: [.log(true), .compress]) БОЕВОЙ

//let manager = SocketManager(socketURL: URL(string: "http://socket.linkyou.klendev.ru:8080")!, config: [.log(true), .compress]) // тестовый
