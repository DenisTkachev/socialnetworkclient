//
//  SocketConnection.swift
//  LinkYou
//
//  Created by Denis Tkachev on 15/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Foundation
import SocketIO

fileprivate enum CmdType: String {
    case userOnline = "user_online"
    case visitNew = "visit_new"
    case likeProfile = "like_profile"
    case messageNew = "message_new"
    case likePhoto = "like_photo"
    case userGift = "user_gift"
    case likeUblog = "like_ublog"
    case userWriting = "user_writing"
    case messageIsRead = "message_read"
}

fileprivate enum DataType: String {
    case chatDialogId = "dialogId"
    case chatMessageId = "messageId"
    case chatUserId = "userId"
    
    case user_Id = "user_id"
    case photo_id = "photo_id"
    case post_id = "post_id"
}


open class SocketConnection {
    
    public static let `default` = SocketConnection()
    private let manager: SocketManager
    private var socket: SocketIOClient
    #if DEBUG
    private let serverUrl = "http://socket.linkyou.klendev.ru:8080" // тестовый
    #else
    private let serverUrl = "http://socket.linkyou.ru:8080" // боевой
    #endif
    
    private init() {
        manager = SocketManager(socketURL: URL(string: serverUrl)!, config:
            [.log(true),.connectParams(["token":DeviceCurrent.current.sessionToken]),.reconnects(true)])
        socket = manager.socket(forNamespace: "/ios")
    }
    
    func connect() {
        manager.connect()
        manager.defaultSocket.once(clientEvent: .connect) { (data, act) in
            self.handshake()
        }
    }
    
    private func handshake() {
        manager.defaultSocket.emit("handshake", DeviceCurrent.current.sessionToken)
    }
    
    func sendPacket(dialogueId: String) {
        manager.defaultSocket.emit("dialog_id:\(dialogueId)", DeviceCurrent.current.sessionToken)
    }
    
    func addHandlers() {
        manager.defaultSocket.onAny { (event) in
          //  print(event)
        }
        
        manager.defaultSocket.on("packet") { data, ack in
            if let packet = data[0] as? [String: Any] {
                if let cmd = packet["cmd"] as? String, let data = packet["data"] as? [String:Any] {
                self.prepareNotification(cmd: cmd, data: data)
                }
            }
        }
    }
    
    private func prepareNotification(cmd: String, data: [String: Any]) {
        
        switch cmd {
        case CmdType.userOnline.rawValue:
            print(cmd)
            sendNotification(notificationName: .socketUserOnline, data: data)
            
        case CmdType.likePhoto.rawValue:
            print(cmd)
            
        case CmdType.likeProfile.rawValue:
            print(cmd)
        case CmdType.likeUblog.rawValue:
            print(cmd)
        case CmdType.messageNew.rawValue:
            print(cmd)
            
            let tabBarController = UIApplication.shared.keyWindow!.rootViewController?.presentedViewController as? UITabBarController
            AppBadgesManager().updateTabbarBadges(vc: tabBarController)
            
            sendNotification(notificationName: .socketMessageNew, data: data)
        case CmdType.userGift.rawValue:
            print(cmd)
        case CmdType.visitNew.rawValue:
            print(cmd)
        case CmdType.userWriting.rawValue:
            print(cmd)
         
        case CmdType.messageIsRead.rawValue:
            sendNotification(notificationName: .socketMessageIsRead, data: data)
        default:
            print("Socket command not found")
            break
        }
    }
    
    
    
    private func sendNotification(notificationName: Notification.Name, data: [String: Any]) {
        print("сокет нотификация => \(notificationName.rawValue) c \(data)")
        NotificationCenter.default.post(name: notificationName, object: nil, userInfo: data)
    }
    
    
    //    manager.defaultSocket.onAny {
    //    print("Got event: \($0.event), with items: \($0.items!)")
    //    if let response = $0.items as? SocketResponse, let socketData = response.socketData {
    //    }
    //    }
}

/*
Основной пакет данных, который присылает Бэк - это "packet".
Вот его структура:

packet
- cmd
- data

cmd - это условное название команды
data - это ассоциативный массив входящих данных

Далее я опишу какие бывают cmd и какие при этом входящие данные.

user_online - Пользователь зашел онлайн. Если этот пользователь есть на текущей странице то можно изменить его статус.
data
user_id - ID этого пользоваетля

visit_new - На вашу страницу кто-то зашел поглазеть
data
user_id - ID этого пользоваетля

like_profile - Вашу анкету лайкнули
data
user_id - ID этого пользоваетля

message_new - Прилетело новое сообщение
data
dialogId - ID диалога
messageId - ID сообщения
userId - ID пользователя

like_photo - Вашу анкету лайкнули
data
photo_id - ID фотки

user_gift - Новый подарок, без входных данных.

like_ublog - Лайкнули блок
data
post_id - ID поста, который лайкнули

like_portfolio - Лайк портфолио (метод устарел)



Так же есть возможность отправлять команды другим пользователям
В библиотеке socket.io есть метод отправки данных emit, можно при помощи этого метода отправить пакет.
Название пакета "redirect", вот его структура входных данных
{
    'packet': {
        'cmd': packet_name,
        'data': data
    },
    'users': [to_user_id] // Массив ID пользователей
}

Таким образом пользователи которые есть в массиве получат пакет "packet_name".
*/
