//
//  NotificationManager.swift
//  LinkYou
//
//  Created by Denis Tkachev on 16/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Foundation
import UIKit

protocol NotificationManagerDelegate {
//    private func executeNotification()
}

class NotificationManager { // его можно вынести в контейнер ассембли как сервис и уже во вью дидлоад делать инит с нужным вью!
    
    var vc: UIViewController!
    
    required init(vc: UIViewController) {
        self.vc = vc
        self.setupNotification()
    }
    
    func setupNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(executeNotification(notification:)), name: .socketMessageNew, object: nil)
    }
    
    @objc private func executeNotification(notification: NSNotification) {
        
        switch notification.name {
        case NSNotification.Name.socketMessageNew:
            DeviceCurrent.pushNotification.pushLocalNotification(notificationData: notification)
            
            if let badge = vc.tabBarController?.children[1] {
                DispatchQueue.main.async {
                    NetworkRequest().loadBadges(completion: { (badges) in
                        badge.tabBarItem.badgeValue = badges.messages.toString()
                    })
                }
            }
            
        default:
            break
        }
    }
    
}
