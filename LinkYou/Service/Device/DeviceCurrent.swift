//
//  DeviceCurrent.swift
//  LinkYou
//
//  Created by Denis Tkachev on 20.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//
import UIKit

protocol DeviceProtocol {
    var isActiveUser: Bool { get }
    var sessionToken: String { get set }
    func setGuestMode(is: Bool)
    func guestModeStatus() -> Bool
    func runGuestMode()
    func showGuestModeAlert()
}

class DeviceCurrent: DeviceProtocol  {
    
    static var current: DeviceProtocol = DeviceCurrent()
    static var pushNotification = PushNotificationService()
    static var badgesManager = AppBadgesManager()
    private var isGuestMode = false
    
    var sessionToken: String {
        get {
            return UserDefaults.standard.string(forKey: Keys.sessionToken) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.sessionToken)
        }
    }
    
    var isActiveUser: Bool {
        guard let token = UserDefaults.standard.string(forKey: Keys.sessionToken) else { return false }
        return (!token.isEmpty)
    }
    
    var appVersion: String {
        return String(UIApplication.version())
    }
    
    var batteryLevel: Int {
        return Int(UIDevice.current.batteryLevel)
    }
    
    static func deleteToken() {
        self.current.sessionToken = ""
    }
    
    func guestModeStatus() -> Bool {
        return self.isGuestMode
    }
    
    func setGuestMode(is: Bool) {
        self.isGuestMode = `is`
    }
    
    func runGuestMode() {
        let tabBarController = UIApplication.shared.keyWindow!.rootViewController?.presentedViewController as? UITabBarController
        if self.isGuestMode, tabBarController != nil {
            tabBarController?.children[1].tabBarItem.isEnabled = false
            tabBarController?.children[3].tabBarItem.isEnabled = false
        } else {
            tabBarController?.children[1].tabBarItem.isEnabled = true
            tabBarController?.children[3].tabBarItem.isEnabled = true
        }
    }
    
    func showGuestModeAlert() {
        let msg = "В гостевом режиме отключены некоторые возможности приложения."
        let alertController = UIAlertController(title: "Гостевой режим", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        let topVC = AppDelegate().getTopMostViewController()
        topVC?.present(alertController, animated: true, completion: nil)
    }
    
    static func checkRequiredFields() -> String? {
        guard let superUser = SuperUser.shared.superUser else { return "" }
        guard let name = superUser.name, !name.isEmpty else { return "Заполните поле имя" }
        guard let location = superUser.location?.city_name, !location.isEmpty else { return "Заполните поле город" }
        guard let profession = superUser.job?.profession, !profession.isEmpty else { return "Заполните поле профессия" }
        guard let occupation = superUser.job?.occupation, !occupation.isEmpty else { return "Заполните поле отрасль" }
        guard let lookingFor = superUser.looking_for?.name, !lookingFor.isEmpty else { return "Заполните поле я ищу" }
        guard let age = superUser.age, age.from ?? 0 > 0, age.to ?? 0 > 0 else { return "Заполните поле возраст" }
        guard let goal = superUser.goal?.name, !goal.isEmpty else { return "Заполните поле цель знакомства" }
        guard let date = superUser.birthday?.date, date.count > 0 else { return "Заполните поле дата рождения" }
        guard let avatarUrl = superUser.avatar?.src?.square, !avatarUrl.contains("avatar-default") else { return "Загрузите аватар" }
        guard let photosCount = superUser.photos_count, photosCount >= 3 else { return "Загрузите не менее 3-х фотографий в галерею" }
        
        return nil
    }
}
