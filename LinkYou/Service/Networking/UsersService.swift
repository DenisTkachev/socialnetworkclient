//
//  UsersService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 02.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation
import Moya

enum UsersService {
    case daily
    case userLikes(id: Int)
}

extension UsersService: TargetType {
    var baseURL: URL {
        return URL(string: "http://linkyou2.klendev.ru/v2")!
    }
    
    var path: String {
        switch self {
        case .daily:
            return "/users/daily"
        case .userLikes(let id):
            return "/user/\(id)"
        }
        
    }
    
    var method: Moya.Method {
        switch self {
        case .daily:
            return .get
        case .userLikes(_):
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .daily:
            return .requestPlain
        case .userLikes(let id):
            return .requestParameters(parameters: ["id" : id], encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

extension UsersService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }

}
