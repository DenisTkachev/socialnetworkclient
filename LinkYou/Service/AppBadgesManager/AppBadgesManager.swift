//
//  AppBadgesManager.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

open class AppBadgesManager {
    
//    init() {
//        NetworkRequest().loadBadges { (badges) in
//            AppBadgesManager.tabBarBadges.setTabbarBadges(badges: badges)
//        }
//    }
    
    public static let manager = AppBadgesManager()
//    private static var tabBarBadges = TabbarBadges()
    
    private var currentAppBadges = 0 {
        didSet {
            self.updateNumberOfBadges(value: currentAppBadges)
        }
    }
    // есть ли смысл хранить значения бейджей локально?
//    private struct TabbarBadges {
//        var guest = 0
//        var messages = 0
//        var likes = 0
//        var comments = 0
//        var sympathies = 0
//
//        mutating func setTabbarBadges(badges: Badges) {
//            self.guest = badges.guests
//            self.messages = badges.messages
//            self.likes = badges.likes
//            self.comments = badges.comments
//            self.sympathies = badges.sympathies
//        }
//    }
    
    func updateTabbarBadges(vc: UITabBarController?) {
        NetworkRequest().loadBadges { (badges) in
//            AppBadgesManager.tabBarBadges.setTabbarBadges(badges: badges)
            
//            if let tabComments = vc?.children[0].tabBarItem {
//                if badges.comments == 0 {
//                    tabComments.badgeValue = nil
//                } else {
//                    tabComments.badgeValue = badges.comments.toString()
//                }
//            }
            
            print(badges)
            
            if let tabMessages = vc?.children[1].tabBarItem { // сообщения
                if badges.messages == 0 {
                    tabMessages.badgeValue = nil
                } else {
                    tabMessages.badgeValue = badges.messages.toString()
                }
            }
            
            if let tabLikes = vc?.children[3].tabBarItem { // лайки
                if badges.likes == 0 {
                    tabLikes.badgeValue = nil
                } else {
                    tabLikes.badgeValue = badges.likes.toString()
                }
            }
            
//            if let tabGuest = vc?.children[4].tabBarItem { // гости
//                if badges.guests == 0 {
//                    tabGuest.badgeValue = nil
//                } else {
//                    tabGuest.badgeValue = badges.guests.toString()
//                }
//            }

            
//            if let tabSympathies = vc?.children[4].tabBarItem {
//                if badges.sympathies == 0 {
//                    tabSympathies.badgeValue = nil
//                } else {
//                    tabSympathies.badgeValue = badges.sympathies.toString()
//                }
//            }
        }
    }
    
    func setAppBadge(value: Int) {
        currentAppBadges = value
    }
    
    func currentNumberOfBadges() -> Int {
        return currentAppBadges
    }
    
    private func updateNumberOfBadges(value: Int) {
        UIApplication.shared.applicationIconBadgeNumber = value
    }
}
