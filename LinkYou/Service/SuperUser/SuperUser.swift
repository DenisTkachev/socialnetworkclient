//
//  SuperUser.swift
//  LinkYou
//
//  Created by Denis Tkachev on 21.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation
import Moya

///Access class function in a single line: SuperUser.shared.requestForUser()
class SuperUser {
    
    static let shared = SuperUser()
    
    var superUser: User?
    var currentUser: CurrentUser?
    
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    enum UserContentType {
        case interests
        case about
        case job
        case book
        case education
    }
    
    private init(){}
    
    func save(user: User) {
        if self.superUser?.id == user.id {
            self.superUser = user
        }
    }
    
    func shortUpdate(completion: @escaping (Bool)->()) {
        guard let id = self.superUser?.id else { return completion(false) }
        NetworkRequest().loadShortUserData(id: id) { (user) in
            self.superUser?.shortUpdate(user: user)
            completion(true)
        }
    }
    
    private func returnUserProperty<T>()-> T {
        return "T" as! T
    }
    
    func saveCurrentUser(currentUser: CurrentUser) {
        self.currentUser = currentUser
    }
    
    private func updateUserData() {
        guard let selfId = SuperUser.shared.superUser?.id else { return }
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        usersProvider.request(.userFullData(id: selfId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                    SuperUser.shared.save(user: userFullData)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func saveUserToServer<T>(with data: T, element: UserContentType) {
        switch element {
        case .interests:
            self.addNewInterestingTag(tags: data as! [TagCloudElement])
        case .about:
            self.collectFieldAboutMe()
        case .job:
            self.collectFieldJob()
        case .book:
            self.collectFieldBook()
        case .education:
            self.collectFieldEducation()
        }
    }
    
    
    // ===============================
    // преобразования в формат string:string
    private func addNewInterestingTag(tags: [TagCloudElement]) { // интересы
        var array: [String] = []
        let _ = tags.forEach({ (element) in
            array.append(element.text)
        })
        let stringWithComma = array.joined(separator: ",")
        
        saveUserField(["interests[interests]": stringWithComma])
    }
    
    private func collectFieldEducation() {
        var parametrs : [String:String] = [:]
        guard let educations = superUser?.education else { return }
        for edu in educations {
            parametrs.updateValue(edu.education_type?.id?.toString() ?? "", forKey: "education[type_id]")
            parametrs.updateValue(edu.institution?.name ?? "", forKey: "education[institution_name]")
            parametrs.updateValue(edu.speciality?.name ?? "", forKey: "education[speciality_name]")
            parametrs.updateValue(edu.id?.toString() ?? "", forKey: "education[id]")
        }
        saveUserField(parametrs)
        parametrs.removeAll()
        guard let languages = superUser?.languages else { return }
        for lang in languages {
            parametrs.updateValue(lang.id?.toString() ?? "", forKey: "languages[id]")
            parametrs.updateValue(lang.language?.id?.toString() ?? "", forKey: "languages[language_id]")
            parametrs.updateValue(lang.level?.id?.toString() ?? "", forKey: "languages[level_id]")
        }
        saveUserField(parametrs)
    }
    
    private func collectFieldAboutMe() {
        var parametrs : [String:String] = [:]
        guard let superUser = superUser else { return }
        parametrs.updateValue(superUser.about ?? "", forKey: "about")
        parametrs.updateValue(superUser.relationship?.id?.toString() ?? "", forKey: "relationship[id]")
        parametrs.updateValue(superUser.orientation?.id?.toString() ?? "", forKey: "orientation[id]")
        parametrs.updateValue(superUser.children?.id?.toString() ?? "", forKey: "children[id]")
        parametrs.updateValue(superUser.smoking?.id?.toString() ?? "", forKey: "smoking[id]")
        parametrs.updateValue(superUser.alcohol?.id?.toString() ?? "", forKey: "alcohol[id]")
        parametrs.updateValue(superUser.height?.toString() ?? "", forKey: "height")
        parametrs.updateValue(superUser.weight?.toString() ?? "", forKey: "weight")
        
        saveUserField(parametrs)
        
    }
    
    private func collectFieldJob() {
        var parametrs : [String:String] = [:]
        guard let job = superUser?.job else { return }
        
        parametrs.updateValue(job.id?.toString() ?? "", forKey: "career[id]")
        parametrs.updateValue(job.company_name ?? "", forKey: "career[companyName]")
        parametrs.updateValue(job.pros ?? "", forKey: "career[pros]")
        parametrs.updateValue(job.cons ?? "", forKey: "career[cons]")
        parametrs.updateValue(job.achievements ?? "", forKey: "career[achievements]")
//        parametrs.updateValue(job.profession ?? "", forKey: "relationship[id]")
        parametrs.updateValue(job.profession_id?.toString() ?? "", forKey: "career[professionId]")
//        parametrs.updateValue(job.occupation ?? "", forKey: "relationship[id]")
        parametrs.updateValue(job.occupation_id?.toString() ?? "", forKey: "career[profAreaId]")
        parametrs.updateValue(job.finance?.id?.toString() ?? "", forKey: "career[financeId]")
        
        saveUserField(parametrs)

    }
    
    private func collectFieldBook() {
        var parametrs : [String:String] = [:]
        guard let books = superUser?.books else { return }
        for book in books {
            parametrs.updateValue(book.id ?? "", forKey: "book[id]")
            parametrs.updateValue(book.author ?? "", forKey: "book[book_author]")
            parametrs.updateValue(book.name ?? "", forKey: "book[book_name]")
        }
        
        saveUserField(parametrs)
    }
    
    
    
    // ===============================
    // saving Собрать все поля и отправить на сервер в формате [String: String]
    
    private func saveUserField(_ parameters: [String: String]) {
        if !Reachability.isConnectedToNetwork() {
            print("Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<EditUserFieldsService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.set(parameters)) { (result) in
            switch result {
            case .success(let response):
                var result = try? JSONDecoder().decode([String:Bool].self, from: response.data)
                let educationResult = try? JSONDecoder().decode(EducationServerResponse.self, from: response.data)
                let languagesResult = try? JSONDecoder().decode(LanguagesServerResponse.self, from: response.data)
                
                if result?["success"] == true {
                    self.updateUserData()
                } else if educationResult?.success == true{
                    self.updateUserData()
                } else if languagesResult?.success == true {
                    self.updateUserData()
                } else {
                    print("Сервер не сохранил изменения!")
                    //self.output.showError()
                }
                
            case let .failure(error):
                print(error)
                //self.output.showError()
            }
        }
    }
    
    
    
}

