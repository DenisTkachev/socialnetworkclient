//
//  PushNotificationService.swift
//  LinkYou
//
//  Created by Denis Tkachev on 21/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Foundation
import UserNotifications

class PushNotificationService: NSObject, UNUserNotificationCenterDelegate {
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    func notificationRequest() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
    }
    
    func pushLocalNotification(notificationData: NSNotification) {
        
        if AppDelegate().getTopMostViewController() is DialogsViewController {
        } else {
            guard let messageId = notificationData.userInfo?["messageId"] as? Int, let userId = notificationData.userInfo?["userId"] as? Int else { return }
            
            let content = UNMutableNotificationContent()
            let pushCategory = "FromSocket"
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let identifier = messageId.toString() // должен быь ункальным чтобы не перезаписывались и шли как отдельные нотификации
            DeviceCurrent.badgesManager.setAppBadge(value: 1)
            
            NetworkRequest().loadShortUserData(id: userId) { (user) in
                content.title = "LinkYou"
                content.body = "\(user.name!) новое сообщение"
                content.sound = UNNotificationSound.default
                content.badge = DeviceCurrent.badgesManager.currentNumberOfBadges() as NSNumber
                content.categoryIdentifier = pushCategory
                let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                self.notificationCenter.add(request) { (error) in
                    if let error = error {
                        print("Error \(error.localizedDescription)")
                    }
                }
            }
        }
        

//        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
//        let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
//        let category = UNNotificationCategory(identifier: userActions,
//                                              actions: [snoozeAction, deleteAction],
//                                              intentIdentifiers: [],
//                                              options: [])
//
//        notificationCenter.setNotificationCategories([category])
    }
 
    // получать уведомления когда приложение на переднем плане
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        completionHandler([.alert,.sound])
//        completionHandler([])
    }
    
    
    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case "action1":
            print("Action First Tapped")
        case "action2":
            print("Action Second Tapped")
        default:
            break
        }
        completionHandler()
    }
}
