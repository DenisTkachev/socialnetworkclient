//
//  SettingsScreenSettingsScreenViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol SettingsScreenViewInput: class {
    /// - author: Denis Tkachev
    func setupInitialState(_ userNotifications: [(String, String, String)])
    func showAlert(title: String, msg: String)
    func setEmailLabel(_ email: String)
    func showHUD()
    func hideHUD()
    func dismisView()
}
