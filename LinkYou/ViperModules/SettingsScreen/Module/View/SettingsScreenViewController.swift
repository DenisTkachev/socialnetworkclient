//
//  SettingsScreenSettingsScreenViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Eureka
import PKHUD

protocol SettingsPressActionButtonType: class {
    func pressSaveEmailBtn(newEmail: String)
    func pressDeleteBtn()
    func pressDisableForm()
    func pressSavePasswordBtn(current: String, password: String, passwordConfirm: String)
    var currentEmail: String { get set }
}

final class SettingsScreenViewController: FormViewController {
    
    // MARK: -
    // MARK: Properties
    var output: SettingsScreenViewOutput!
    //let checkBox = ["Личные сообщения", "Просмотры анкеты", "Лайки ваших фотографий", "Лайки вашей страницы", "Комментарии к вашим фотографиям", "Подарки"]
    
    var currentEmail = ""
    var currentEnabledSwitch = false
    
    var notifications = [(String, String, String)]()
    
    enum FieldType {
        case email
        case pass
        case phone
        case disableForm
    }
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Настройка аккаунта"
        self.navigationController?.navigationBar.tintColor = .white
        tableView.separatorStyle = .none
        output.viewIsReady()
        self.tabBarController?.tabBar.backgroundColor = .white
        self.hideKeyboardWhenTappedAround()
    }
    
    func setupCell() {
        form +++ Section("Основное")
            <<< SettingsEmailRow("email") {
                $0.cellProvider = CellProvider<SettingsEmailTableViewCell>(nibName: "SettingsEmailTableViewCell", bundle: Bundle.main)
                $0.cell.delegateAction = self
                $0.cell.setEmailLabel(currentEmail)
            }
            
            <<< SettingsPassRecoveryRow("pass") {
                $0.cellProvider = CellProvider<SettingsPassRecoveryTableViewCell>(nibName: "SettingsPassRecoveryTableViewCell", bundle: Bundle.main)
                $0.cell.delegateAction = self
            }
            <<< SettingsPhoneRow("phone") {
                $0.cellProvider = CellProvider<SettingsPhoneTableViewCell>(nibName: "SettingsPhoneTableViewCell", bundle: Bundle.main)
                $0.cell.delegateAction = self
                $0.cell.isEnabledSwitch.setOn(currentEnabledSwitch, animated: false)
        }
        
        form +++ SelectableSection<ImageCheckRow<String>>("Оповещения на e-mail", selectionType: .multipleSelection)
        form.last!.tag = "emailSection"
        form.last! <<< SpacerRow()
        for oneCheckBox in notifications {
            
            let title = oneCheckBox.2
            let value = (oneCheckBox.1 == "1") ? "1" : nil
            let tag = oneCheckBox.0
            
            form.last! <<< ImageCheckRow<String>(title){ lrow in
                lrow.cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                lrow.cell.textLabel?.textColor = UIColor.charcoalGrey
                lrow.cell.detailTextLabel?.textColor = UIColor.charcoalGrey
                lrow.cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 14)
                lrow.title = title
                lrow.selectableValue = tag
                lrow.value = value
                lrow.tag = tag
            }
        }
        
        form.last! <<< SpacerRow()
        
        form +++ Section("Удаление аккаунта")
            <<< RemoveAccount("deleteAccount") {
                $0.cellProvider = CellProvider<RemoveAccountTableViewCell>(nibName: "RemoveAccountTableViewCell", bundle: Bundle.main)
                $0.cell.delegateAction = self
        }
        
    }
    
    override func valueHasBeenChanged(for row: BaseRow, oldValue: Any?, newValue: Any?) {
        if row.section?.tag == "emailSection" {
            self.output.switchNotificationStatus(code: row.tag ?? "")
//            print("Mutiple Selection:\((row.section as! SelectableSection<ImageCheckRow<String>>).selectedRows().map({$0.baseValue}))")
        }
    }
    
    func showHUD() {
        HUD.show(.progress, onView: view)
    }
    
    func hideHUD() {
        HUD.hide()
    }
    
}

// MARK: -
// MARK: SettingsScreenViewInput
extension SettingsScreenViewController: SettingsScreenViewInput {
    
    func setEmailLabel(_ email: String) {
        currentEmail = email
        tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        hideHUD()
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
        }
    }
    
    func setupInitialState(_ userNotifications: [(String, String, String)]) {
        self.notifications = userNotifications
        guard let email = SuperUser.shared.currentUser?.email, let isEnabledSwitch = SuperUser.shared.currentUser?.is_active else { return }
        self.currentEmail = email
        self.currentEnabledSwitch = !isEnabledSwitch
        setupCell()
    }
    
    func dismisView() {
        HUD.show(.label("Удаляю данные..."))
        dismiss(animated: true) {
            HUD.hide()
        }
    }
}

extension SettingsScreenViewController: SettingsPressActionButtonType {
    func pressSaveEmailBtn(newEmail: String) {
        print("send save request: \(newEmail)")
        showHUD()
        output.changeEmail(newEmail)
        
        view.endEditing(true)
        view.resignFirstResponder()
    }
    
    func pressDisableForm() {
        output.pressDisableForm()
    }
    
    func pressSavePasswordBtn(current: String, password: String, passwordConfirm: String) {
        if password == passwordConfirm, !current.isEmpty, !password.isEmpty, !passwordConfirm.isEmpty { // TODO: проверить с текущим паролем
            output.changePassword(current, password, passwordConfirm)
            showHUD()
            view.endEditing(true)
            view.resignFirstResponder()
        } else if current.isEmpty, password.isEmpty, passwordConfirm.isEmpty {
            showAlert(title: "Ошибка", msg: "Все поля должны быть заполнены")
        } else {
            showAlert(title: "Ошибка", msg: "Поля \"Новый пароль\" и \"Подтвердить пароль\" не соответствуют.")
        }
    }
    
    func pressDeleteBtn() {
        let alertController = UIAlertController(title: "Внимание!", message: "Вы точно уверены, что хотите удалить свой аккаунт?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .destructive) { (alert) in
            self.output.deleteAccount()
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true)
    }
}

