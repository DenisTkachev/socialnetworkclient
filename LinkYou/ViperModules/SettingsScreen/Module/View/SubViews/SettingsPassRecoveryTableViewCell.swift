//
//  SettingsPassRecoveryTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 05.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class SettingsPassRecoveryTableViewCell: Cell<String>, CellType, UITextFieldDelegate {
    
    
    @IBOutlet weak var currentPassTextField: TextFieldCustom!
    @IBOutlet weak var newPassTextField: TextFieldCustom!
    @IBOutlet weak var approveNewPassTextField: TextFieldCustom!
    
    @IBOutlet weak var currentValidStatusImage: UIImageView!
    @IBOutlet weak var newPassValidStatusImage: UIImageView!
    @IBOutlet weak var approveValidStatusImage: UIImageView!
    
    weak var delegateAction: SettingsPressActionButtonType!
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
        
        //        currentPassTextField.layer.cornerRadius = currentPassTextField.frame.height / 2
        currentPassTextField.clipsToBounds = true
        currentPassTextField.layer.borderWidth = 1
        currentPassTextField.layer.borderColor = UIColor.paleGrey.cgColor
        currentPassTextField.addTarget(self, action: #selector(SettingsEmailTableViewCell.textFieldDidChange(_:)), for: .allEvents)
        currentPassTextField.tag = 0
        
        //        newPassTextField.layer.cornerRadius = newPassTextField.frame.height / 2
        newPassTextField.clipsToBounds = true
        newPassTextField.layer.borderWidth = 1
        newPassTextField.layer.borderColor = UIColor.paleGrey.cgColor
        newPassTextField.addTarget(self, action: #selector(SettingsEmailTableViewCell.textFieldDidChange(_:)), for: .allEvents)
        newPassTextField.tag = 1
        
        //        approveNewPassTextField.layer.cornerRadius = approveNewPassTextField.frame.height / 2
        approveNewPassTextField.clipsToBounds = true
        approveNewPassTextField.layer.borderWidth = 1
        approveNewPassTextField.layer.borderColor = UIColor.paleGrey.cgColor
        approveNewPassTextField.addTarget(self, action: #selector(SettingsEmailTableViewCell.textFieldDidChange(_:)), for: .allEvents)
        approveNewPassTextField.tag = 2
    }
    
    public override func update() {
        super.update()
        currentPassTextField.text = nil
        newPassTextField.text = nil
        approveNewPassTextField.text = nil
    }
    
    
    @IBAction func pressSaveBtn(_ sender: RoundButtonCustom) {
        delegateAction.pressSavePasswordBtn(current: currentPassTextField.text ?? "", password: newPassTextField.text ?? "", passwordConfirm: approveNewPassTextField.text ?? "")
    }
    
    @objc public func textFieldDidChange(_ textField: UITextField) {
        row.value = textField.text
        //        update()
        
        guard let text = textField.text else { return }
        switch textField.tag {
        case 0: // currentPassTextField
            if text.count > 2 {
                currentValidStatusImage.image = nil
            } else {
                currentValidStatusImage.image = #imageLiteral(resourceName: "cross")
            }
        case 1: // newPassTextField
            
            if text.count > 2 {
                newPassValidStatusImage.image = nil
            } else if newPassTextField.text == approveNewPassTextField.text, text.count > 2 {
                newPassValidStatusImage.image = nil
            } else {
                newPassValidStatusImage.image = #imageLiteral(resourceName: "cross")
            }
        
        case 2: // approveNewPassTextField
            if newPassTextField.text == approveNewPassTextField.text, text.count > 2 {
                approveValidStatusImage.image = nil
            } else {
                approveValidStatusImage.image = #imageLiteral(resourceName: "cross")
            }
            
        default:
            approveValidStatusImage.image = nil
            currentValidStatusImage.image = nil
            newPassValidStatusImage.image = nil
        }
    }
}

// The custom Row also has the cell: CustomCell and its correspond value
public final class SettingsPassRecoveryRow: Row<SettingsPassRecoveryTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        // We set the cellProvider to load the .xib corresponding to our cell
        cellProvider = CellProvider<SettingsPassRecoveryTableViewCell>(nibName: "SettingsPassRecoveryTableViewCell")
    }
}
