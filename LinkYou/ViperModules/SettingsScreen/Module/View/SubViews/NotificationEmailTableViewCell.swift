//
//  DisableFormTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 05.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public final class ImageCheckRow<T: Equatable>: Row<ImageCheckCell<T>>, SelectableRowType, RowType {
    public var selectableValue: T?
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
        
    }
    weak var delegateAction: SettingsPressActionButtonType!
}

public class ImageCheckCell<T: Equatable> : Cell<T>, CellType {
    
    required public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Image for selected state
    lazy public var trueImage: UIImage = {
        return UIImage(named: "settingsNotiSelected")!
    }()
    
    /// Image for unselected state
    lazy public var falseImage: UIImage = {
        return UIImage(named: "settingsNotiUnselected")!
    }()
    
    public override func update() {
        super.update()
        checkImageView?.image = row.value != nil ? trueImage : falseImage
        checkImageView?.sizeToFit()
    }
    
    /// Image view to render images. If `accessoryType` is set to `checkmark`
    /// will create a new `UIImageView` and set it as `accessoryView`.
    /// Otherwise returns `self.imageView`.
    open var checkImageView: UIImageView? {
        guard accessoryType == .checkmark else {
            return self.imageView
        }
        
        guard let accessoryView = accessoryView else {
            let imageView = UIImageView()
            self.accessoryView = imageView
            return imageView
        }
        
        return accessoryView as? UIImageView
    }
    
    public override func setup() {
        super.setup()
        accessoryType = .none
    }
    
    public override func didSelect() {
        row.reload()
        row.select()
        row.deselect()
    }
    
}




//public class NotificationEmailTableViewCell: Cell<String>, CellType {
//
//
//    weak var delegateAction: SettingsPressActionButtonType!
//
//    public override func setup() {
//        super.setup()
//        selectionStyle = .none
//
//
//    }
//
//    public override func update() {
//        super.update()
//
//    }
//
//}
//
//// The custom Row also has the cell: CustomCell and its correspond value
//public final class NotificationEmailFormRow: Row<NotificationEmailTableViewCell>, RowType {
//    required public init(tag: String?) {
//        super.init(tag: tag)
//        // We set the cellProvider to load the .xib corresponding to our cell
//        cellProvider = CellProvider<NotificationEmailTableViewCell>(nibName: "NotificationEmailTableViewCell")
//    }
//}
