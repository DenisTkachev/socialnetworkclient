//
//  SettingsPhoneTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 05.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class SettingsPhoneTableViewCell: Cell<String>, CellType {
    
    weak var delegateAction: SettingsPressActionButtonType!
    @IBOutlet weak var isEnabledSwitch: UISwitch!
    
    public override func setup() {
        super.setup()
        selectionStyle = .none

    }
    
    public override func update() {
        super.update()
        
    }
    
    @IBAction func pressSwitch(_ sender: UISwitch) {
        delegateAction.pressDisableForm()
    }
}

// The custom Row also has the cell: CustomCell and its correspond value
public final class SettingsPhoneRow: Row<SettingsPhoneTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        // We set the cellProvider to load the .xib corresponding to our cell
        cellProvider = CellProvider<SettingsPhoneTableViewCell>(nibName: "SettingsPhoneTableViewCell")
    }
}
