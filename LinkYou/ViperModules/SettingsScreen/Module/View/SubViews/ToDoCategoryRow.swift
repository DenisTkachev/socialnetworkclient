
import Eureka

class SpacerCell: PushSelectorCell<String> {
  
    var customHeight:CGFloat = 10.0
    
  lazy var label: UILabel = {
    let lbl = UILabel()
    lbl.textAlignment = .center
    return lbl
  }()
  
  override func setup() {
    height = { self.customHeight }
    row.title = nil
    super.setup()
    selectionStyle = .none
    isUserInteractionEnabled = false
    
    contentView.addSubview(label)
    label.translatesAutoresizingMaskIntoConstraints = false
    let margin: CGFloat = 10.0
    label.heightAnchor.constraint(equalTo: contentView.heightAnchor, constant: -(margin * 2)).isActive = true
    label.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -(margin * 2)).isActive = true
    label.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
    label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
  }
  
  override func update() {
    row.title = nil
    label.text = row.value
  }
}

final class SpacerRow: _PushRow<SpacerCell>, RowType { }
