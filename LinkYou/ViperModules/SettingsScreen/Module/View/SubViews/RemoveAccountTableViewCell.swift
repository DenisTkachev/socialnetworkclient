//
//  RemoveAccountTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 05.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class RemoveAccountTableViewCell: Cell<String>, CellType {
    
    weak var delegateAction: SettingsPressActionButtonType!
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
        
    }
    
    public override func update() {
        super.update()
        
    }
    
    @IBAction func pressRemoveBtn(_ sender: RoundButtonCustom) {
        delegateAction.pressDeleteBtn()
    }
    
    
}

// The custom Row also has the cell: CustomCell and its correspond value
public final class RemoveAccount: Row<RemoveAccountTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        // We set the cellProvider to load the .xib corresponding to our cell
        cellProvider = CellProvider<RemoveAccountTableViewCell>(nibName: "RemoveAccountTableViewCell")
    }
}
