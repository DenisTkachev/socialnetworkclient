//
//  SettingsEmailTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class SettingsEmailTableViewCell: Cell<String>, CellType, UITextFieldDelegate {
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var validationStatusIcon: UIImageView!
    
    
    weak var delegateAction: SettingsPressActionButtonType!
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
        
        emailText.layer.cornerRadius = emailText.frame.height / 2
        emailText.clipsToBounds = true
        emailText.layer.borderWidth = 1
        emailText.layer.borderColor = UIColor.paleGrey.cgColor
        emailText.addTarget(self, action: #selector(SettingsEmailTableViewCell.textFieldDidChange(_:)), for: .allEvents)
    }
    
    public override func update() {
        super.update()
        userEmailLabel.text = delegateAction.currentEmail
        emailText.text = nil
    }
    
    @IBAction func pressSave(_ sender: RoundButtonCustom) {
        delegateAction.pressSaveEmailBtn(newEmail: emailText.text ?? "")
    }
    
    func setEmailLabel(_ email: String) {
        userEmailLabel.text = email
    }
    
    @objc public func textFieldDidChange(_ textField: UITextField) {
        row.value = textField.text
//        update()
        
        
        
        if let text = textField.text, text.isValidEmailFormat() {
            validationStatusIcon.image = nil
        } else if let text = textField.text, text.count > 0 {
            validationStatusIcon.image = #imageLiteral(resourceName: "cross")
        } else {
            validationStatusIcon.image = nil
        }
    }
}

// The custom Row also has the cell: CustomCell and its correspond value
public final class SettingsEmailRow: Row<SettingsEmailTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        // We set the cellProvider to load the .xib corresponding to our cell
        cellProvider = CellProvider<SettingsEmailTableViewCell>(nibName: "SettingsEmailTableViewCell")
    }
}
