//
//  SettingsScreenSettingsScreenViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol SettingsScreenViewOutput: class {
    /// - author: Denis Tkachev
    func viewIsReady()
    func changeEmail(_ newEmail: String)
    func changePassword(_ current: String, _ password: String, _ passwordConfirm: String)
    func switchNotificationStatus(code: String)
    func deleteAccount()
    func pressDisableForm()
}
