//
//  SettingsScreenSettingsScreenAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class SettingsScreenAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(SettingsScreenInteractor.self) { (r, presenter: SettingsScreenPresenter) in
			let interactor = SettingsScreenInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(SettingsScreenRouter.self) { (r, viewController: SettingsScreenViewController) in
			let router = SettingsScreenRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(SettingsScreenPresenter.self) { (r, viewController: SettingsScreenViewController) in
			let presenter = SettingsScreenPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(SettingsScreenInteractor.self, argument: presenter)
			presenter.router = r.resolve(SettingsScreenRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(SettingsScreenViewController.self) { r, viewController in
			viewController.output = r.resolve(SettingsScreenPresenter.self, argument: viewController)
		}
	}

}
