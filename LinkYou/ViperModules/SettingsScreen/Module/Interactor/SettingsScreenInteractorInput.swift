//
//  SettingsScreenSettingsScreenInteractorInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol SettingsScreenInteractorInput: class {
    func saveNewEmail(_ newEmail: String)
    func isValidCurrentPassword(current: String, password: String, passwordConfirm: String)
    func loadUserNotifications()
    func switchNotification(_ code: String)
    func removeCurrentAccount()
    func switchEnablingAccount()
}
