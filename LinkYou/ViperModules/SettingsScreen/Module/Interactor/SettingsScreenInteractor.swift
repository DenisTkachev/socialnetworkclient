//
//  SettingsScreenSettingsScreenInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class SettingsScreenInteractor: SettingsScreenInteractorInput {
    
    let dispatchGroup = DispatchGroup()
    var responseNotification: UserNotifications?
    var currentUser: CurrentUser?
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    func switchEnablingAccount() {
        let usersProvider = MoyaProvider<UserSettingsService>(plugins: [authPlagin])
        usersProvider.request(.switchEnablingAccount) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if response["done"] == true {
                        
                    } else {
                        // ошибка сохранения статуса
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func removeCurrentAccount() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        usersProvider.request(.removeCurrentAccount) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(RemoveAccountResponse.self, from: response.data)
                    if response.done == true {
                        DeviceCurrent().sessionToken = ""
                        self.output.showStartScreen()
                    } else {
                        self.output.showAlert(title: "Ошибка удаления анкеты", msg: "Что-то пошло не так. Поробуйте позже.")
                    }
                } catch {
                    print(error)
                    self.output.showAlert(title: "Ошибка удаления анкеты", msg: "Что-то пошло не так. Поробуйте позже.")
                }
            case let .failure(error):
                print(error)
                self.output.showAlert(title: "Ошибка удаления анкеты", msg: "Что-то пошло не так. Поробуйте позже.")
            }
        }
    }
    
    
    
    func switchNotification(_ code: String) {
        
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UserSettingsService>(plugins: [authPlagin])
        usersProvider.request(.switchNotification(code: code)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(NotificationServerResponse.self, from: response.data)
                    if response.done == true {
                        
                    } else {
                        // ошибка сохранения статуса
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func saveNewEmail(_ newEmail: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UserSettingsService>(plugins: [authPlagin])
        usersProvider.request(.setEmail(email: newEmail)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(BoolServerRespone.self, from: response.data)
                    if response.response ?? false {
                        self.output.showAlert(title: "", msg: "Запрос о смене Email успешно отправлен. Проверьте \(newEmail) и подтвердите смену ящика, перейдя по ссылке из письма.")
                        self.output.updateEmail(newEmail)
                    } else {
                        self.output.showAlert(title: "Ошибка", msg: "Запрос на смену Email не был отправлен. Попробуйте позже.")
                    }
                    print(response)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    private func saveNewPassword(current: String, password: String, passwordConfirm: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
        let usersProvider = MoyaProvider<UserSettingsService>(plugins: [authPlagin,debugPlagin])
        
        usersProvider.request(.setNewPassword(current: current, password: password, passwordConfirm: passwordConfirm)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(BoolServerRespone.self, from: response.data)
                    if response.response ?? false {
                        self.output.showAlert(title: "", msg: "Новый пароль успешно сохранён")
                    } else {
                        self.output.showAlert(title: "Ошибка", msg: "Пароль не сохранён")
                    }
                    print(response)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func isValidCurrentPassword(current: String, password: String, passwordConfirm: String) {
        // валидацию проводит сервер у себя
        self.saveNewPassword(current: current, password: password, passwordConfirm: passwordConfirm)
    }
    
    func loadUserNotifications() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UserSettingsService>(plugins: [authPlagin])
        dispatchGroup.enter()
        usersProvider.request(.getUserNitifications) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(UserNotifications.self, from: response.data)
                    self.responseNotification = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        let userProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        dispatchGroup.enter()
        userProvider.request(.currentUser) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(CurrentUser.self, from: response.data)
                    SuperUser.shared.saveCurrentUser(currentUser: response)
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            guard let responseNotification = self.responseNotification else { return }
            self.output.gettedUserNotification(userNotifications: responseNotification)
        }
        
    }
    
    weak var output: SettingsScreenInteractorOutput!
    
}
