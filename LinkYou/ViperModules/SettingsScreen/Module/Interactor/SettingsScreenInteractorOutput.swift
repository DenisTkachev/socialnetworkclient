//
//  SettingsScreenSettingsScreenInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol SettingsScreenInteractorOutput: class {
    func showAlert(title: String, msg: String)
    func updateEmail(_ email: String)
    func gettedUserNotification(userNotifications: UserNotifications)
    func showStartScreen()
}
