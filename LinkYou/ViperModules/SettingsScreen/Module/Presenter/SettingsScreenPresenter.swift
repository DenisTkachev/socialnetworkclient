//
//  SettingsScreenSettingsScreenPresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class SettingsScreenPresenter: SettingsScreenViewOutput {
    
    func deleteAccount() {
        interactor.removeCurrentAccount()
    }
    
    func switchNotificationStatus(code: String) {
        interactor.switchNotification(code)
    }
    
    func changePassword(_ current: String, _ password: String, _ passwordConfirm: String) {
        interactor.isValidCurrentPassword(current: current, password: password, passwordConfirm: passwordConfirm)
    }
    
    func changeEmail(_ newEmail: String) {
        interactor.saveNewEmail(newEmail)
    }
    
    // MARK: -
    // MARK: Properties

    weak var view: SettingsScreenViewInput!
    var interactor: SettingsScreenInteractorInput!
    var router: SettingsScreenRouterInput!

    // MARK: -
    // MARK: SettingsScreenViewOutput
    func viewIsReady() {
        interactor.loadUserNotifications()
    }

    private func prepareCheckBoxes(_ userNotifications: UserNotifications) -> [(String, String, String)]{
        var result = [(String, String, String)]()
        let checkBox = ["message":"Личные сообщения", "view_profile":"Просмотры анкеты", "like_photos":"Лайки ваших фотографий", "like_profile":"Лайки вашей страницы", "comment_photos":"Комментарии к вашим фотографиям", "gifts":"Подарки"]
        let dictionary = userNotifications.dictionary
        
        if checkBox.count == dictionary.count {
            for (key, value) in dictionary {
                let foo = value ?? 0
                result.append((key, String(foo), checkBox[key]!))
            }
        }
        return result
    }
    
    func pressDisableForm() {
        interactor.switchEnablingAccount()
    }
}

// MARK: -
// MARK: SettingsScreenInteractorOutput
extension SettingsScreenPresenter: SettingsScreenInteractorOutput {
    func gettedUserNotification(userNotifications: UserNotifications) {
        view.setupInitialState(prepareCheckBoxes(userNotifications))
    }
    
    func updateEmail(_ email: String) {
        view.setEmailLabel(email)
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func showStartScreen() {
        view.dismisView()
    }

}

// MARK: -
// MARK: SettingsScreenModuleInput
extension SettingsScreenPresenter: SettingsScreenModuleInput {
    

}
