//
//  PaymentPagePaymentPageViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import UIKit
import WebKit
import PKHUD

protocol PaymentPageVCType: class {
    func showPaymentView(paymentTypeId: Int, entityId: Int)
}

final class PaymentPageViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {

	// MARK: -
	// MARK: Properties
	var output: PaymentPageViewOutput!
    var webView: WKWebView!
    var request: URLRequest?
    
	// MARK: -
	// MARK: Life cycle
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
        webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
    }
    
	override func viewDidLoad() {
		super.viewDidLoad()
		
        HUD.show(.progress, onView: view)
        
        title = "Оплата"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
	}
    
    private func loadPage() {
        guard let request = request, webView != nil else { return }
        webView.load(request)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadPage()
    }
    
    func loadUrl(url: URL) {
        request = URLRequest(url: url)
        guard let scheme = url.scheme, let host = url.host else { return }
        request?.setValue("\(scheme)://\(host)", forHTTPHeaderField: "Referer")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) { // не всегда срабатывет
        HUD.hide()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url) {
            print("### URL:", self.webView.url!)
            if let urlString = webView.url?.absoluteString, let scheme = webView.url?.scheme, let host = webView.url?.host, urlString.contains("\(scheme)://\(host)/payment/success-test") {
                // ### URL: http://linkyou.klendev.ru/payment/success-test?id=38061&token=a088cc0fec06337b27e1b0c4e2159c59
                output.checkCompletePayment()
                self.showHud(show: true, dismiss: true)
                
            } else { // финальный урл не совпал
                
            }
        }
    }
    
    func showHud(show: Bool, dismiss: Bool) {
        if show {
            HUD.show(.progress, onView: view)
        } else {
            HUD.hide()
        }
        
        if dismiss {
            // можно скрыть webView
            let _ = InformationPopUpViewController.init(type: .paymentSuccess, delegate: self)
            
            }
        }
    }
    
    // проверить оплатил ли в итоге наш юзер что-то, для этого делаем запрос к серверу
    //        PaymentService.checkPaymentStatus
    // если оплатил, то включить таббар и поменять статус юзера с обновлением его экранов
    //

extension PaymentPageViewController: InformationPopUpType {
    
    func pressBtn() {
        if let giftVC = self.navigationController?.children.dropLast().last as? GiftsGalleryViewController { // только для страницы с подарками
            giftVC.giftWasPayed()
            self.navigationController?.popViewController(animated: false)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: -
// MARK: PaymentPageViewInput
extension PaymentPageViewController: PaymentPageViewInput {

    func setupInitialState() {
        if let urlString = output.url, let url = URL(string: urlString) {
            loadUrl(url: url)
        }
	}

}
