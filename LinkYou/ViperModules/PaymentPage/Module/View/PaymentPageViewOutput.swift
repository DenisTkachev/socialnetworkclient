//
//  PaymentPagePaymentPageViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

protocol PaymentPageViewOutput: class {
    /// - author: Denis Tkachev
    func viewIsReady()
    var url: String? { get set }
    
    func checkCompletePayment()
}
