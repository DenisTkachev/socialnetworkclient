//
//  PaymentPagePaymentPageViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//
import Foundation

protocol PaymentPageViewInput: class {
    /// - author: Denis Tkachev
    func setupInitialState()
    func loadUrl(url: URL)
    func showHud(show: Bool, dismiss: Bool)
}
