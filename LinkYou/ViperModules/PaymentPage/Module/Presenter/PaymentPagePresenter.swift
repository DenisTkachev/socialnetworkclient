//
//  PaymentPagePaymentPagePresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

final class PaymentPagePresenter: PaymentPageViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: PaymentPageViewInput!
    var interactor: PaymentPageInteractorInput!
    var router: PaymentPageRouterInput!
    var url: String?
    
    // MARK: -
    // MARK: PaymentPageViewOutput
    func viewIsReady() {
        
    }
    
    func checkCompletePayment() {
        interactor.checkPaymentStatus()
    }

}

// MARK: -
// MARK: PaymentPageInteractorOutput
extension PaymentPagePresenter: PaymentPageInteractorOutput {
    
    func showHUD(show: Bool) {
        view.showHud(show: show, dismiss: false)
    }
    
    func urlGeted(url: String) {
        self.url = url
        view.setupInitialState()
    }
}

// MARK: -
// MARK: PaymentPageModuleInput
extension PaymentPagePresenter: PaymentPageModuleInput {
    func getPaymentUrl(paymentTypeId: Int, entityId: Int) {
        interactor.getPaymentURL(paymentTypeId: paymentTypeId, entityId: entityId)
    }
    


}
