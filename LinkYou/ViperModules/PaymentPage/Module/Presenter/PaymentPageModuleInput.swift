//
//  PaymentPagePaymentPageModuleInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

protocol PaymentPageModuleInput: class {
    func getPaymentUrl(paymentTypeId: Int, entityId: Int)
}
