//
//  PaymentPagePaymentPageAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class PaymentPageAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(PaymentPageInteractor.self) { (r, presenter: PaymentPagePresenter) in
			let interactor = PaymentPageInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(PaymentPageRouter.self) { (r, viewController: PaymentPageViewController) in
			let router = PaymentPageRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(PaymentPagePresenter.self) { (r, viewController: PaymentPageViewController) in
			let presenter = PaymentPagePresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(PaymentPageInteractor.self, argument: presenter)
			presenter.router = r.resolve(PaymentPageRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(PaymentPageViewController.self) { r, viewController in
			viewController.output = r.resolve(PaymentPagePresenter.self, argument: viewController)
		}
	}

}
