//
//  PaymentPagePaymentPageInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Moya

final class PaymentPageInteractor: PaymentPageInteractorInput {
    
    weak var output: PaymentPageInteractorOutput!
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    let dispatchGroup = DispatchGroup()
    
    
    func checkPaymentStatus() {
        if !Reachability.isConnectedToNetwork() {
            //            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        dispatchGroup.enter()
        let usersPayProvider = MoyaProvider<PaymentService>(plugins: [authPlagin, debugPlagin])
        usersPayProvider.request(.checkPaymentStatus) { (result) in
            switch result {
            case .success(let response):
                do {
                    print(response)
                    // приходит массив ["main_page","main_page","main_page","main_page","main_page","main_page","main_page"]
                    self.dispatchGroup.leave() //TODO: Допиливать информирование юзера и обработку ошибок. пока сыро, тестируем
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                    print("Ошибка оплаты")
                }
            case let .failure(error):
                print(error)
                print("Ошибка оплаты")
                self.dispatchGroup.leave()
            }
        }
    
        guard let selfId = SuperUser.shared.superUser?.id else { return }
        dispatchGroup.enter()
        let usersServiceProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        usersServiceProvider.request(.userFullData(id: selfId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                    SuperUser.shared.save(user: userFullData)
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        dispatchGroup.enter()
        usersServiceProvider.request(.currentUser) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(CurrentUser.self, from: response.data)
                    SuperUser.shared.saveCurrentUser(currentUser: response)
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            // применен статус оплаты, обновлен юзер
            self.output.showHUD(show: false)
        }
    }
    
    func getPaymentURL(paymentTypeId: Int, entityId: Int) {
        if !Reachability.isConnectedToNetwork() {
//            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let usersProvider = MoyaProvider<PaymentService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.getPaymentLink(paymentTypeId: paymentTypeId, entityId: entityId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    print(response)
                    let paymentUrl = try JSONDecoder().decode([String:String].self, from: response.data)
                    if let paymentUrl = paymentUrl["url"] {
                        self.output.urlGeted(url: paymentUrl)
                    }
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
        
    }
    
}
