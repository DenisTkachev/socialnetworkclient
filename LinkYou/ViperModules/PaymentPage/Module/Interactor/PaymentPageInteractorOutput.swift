//
//  PaymentPagePaymentPageInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Foundation

protocol PaymentPageInteractorOutput: class {
    func urlGeted(url: String)
    func showHUD(show: Bool)
}
