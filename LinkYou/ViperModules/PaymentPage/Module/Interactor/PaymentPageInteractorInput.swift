//
//  PaymentPagePaymentPageInteractorInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Foundation

protocol PaymentPageInteractorInput: class {
    func getPaymentURL(paymentTypeId: Int, entityId: Int)
    func checkPaymentStatus()
}
