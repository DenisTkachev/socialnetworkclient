//
//  PayOptionTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 06/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class PayOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var backGroundRoundView: UIView!
    @IBOutlet weak var roundPointView: UIView!
    @IBOutlet weak var verticalLineView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backGroundRoundView.clipsToBounds = true
        roundPointView.clipsToBounds = true
        verticalLineView.clipsToBounds = true
        
        backGroundRoundView.layer.cornerRadius = backGroundRoundView.bounds.width / 2
        roundPointView.layer.cornerRadius = roundPointView.bounds.width / 2
        
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        markRoundPoint(select: selected)
    }
    
    func markRoundPoint(select: Bool) {
        
        if select {
            roundPointView.backgroundColor = UIColor.azure
            priceLabel.textColor = UIColor.azure
        } else {
            roundPointView.backgroundColor = UIColor.paleGrey
            priceLabel.textColor = UIColor.palePurple
        }
        
    }
    
    func setTopCorner() {
        verticalLineView.roundCorners(corners: [.topLeft, .topRight], radius: 6.0)
    }
    
    func setBottomCorner() {
        verticalLineView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 6.0)
    }
    
}
