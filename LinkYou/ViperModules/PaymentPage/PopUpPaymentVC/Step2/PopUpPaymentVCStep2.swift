//
//  PopUpPaymentVCStep2.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class PopUpPaymentVCStep2: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var priceTableView: UITableView!
    
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var bottomNoticeLabel: UILabel!
    //    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var closeViewBtn: UIButton!
    
    var prices: Payment?
    var payments = [DayPayment]()
    var tizerType: TizerType!
    var selectedValue: DayPayment?
    var giftID: Int?
    
    init(tizerType: TizerType, giftID: Int? = nil) {
        super.init(nibName: "PopUpPaymentVCStep2", bundle: nil)
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
        self.prices = PersonalConstants.priceRates
        self.tizerType = tizerType
        self.giftID = giftID
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        priceTableView.register(UINib(nibName: "PayOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "PayOptionTableViewCell")
        preparePriceList(tizerType: tizerType)
        setupView()
        guard let paymentType = tizerType else { return }
        pageTitleLabel.text = TizerModelHelper().returnTizerTitle(tizerType: paymentType)
        
        if tizerType == TizerType.registrationTax {
            closeViewBtn.isHidden = true
        }
    }
    
    private func setupView() {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 3
        let attributedString = NSMutableAttributedString(string: "Переходя к оплате вы соглашаетесь\nс правилами оказания услуги", attributes: [
            .font: UIFont.systemFont(ofSize: 12, weight: .regular),
            .foregroundColor: UIColor(red: 134.0 / 255.0, green: 134.0 / 255.0, blue: 148.0 / 255.0, alpha: 1.0)
            ])
        attributedString.addAttribute(.font, value:UIFont.systemFont(ofSize: 12, weight: .semibold), range: NSRange(location: 36, length: 25))
        
        bottomNoticeLabel.attributedText = attributedString
        let tap = UITapGestureRecognizer(target: self, action: #selector(PopUpPaymentVCStep2.showNoticeWeb(_:)))
        bottomNoticeLabel.addGestureRecognizer(tap)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    private func preparePriceList(tizerType: TizerType) {
        guard let prices = prices else { return }
        
        switch tizerType {
            
        case .toMainPage:
            guard let mainPage = prices.mainPage else { print("Нет опций оплаты!"); break }
            mainPage.forEach { (payOption) in
                payments.append(payOption)
            }
        case .premium:
            guard let premium = prices.premium else { print("Нет опций оплаты!"); break }
            premium.forEach { (payOption) in
                payments.append(payOption)
            }
            
        case .interestingDatingForm:
            guard let premium = prices.interestingPage else { print("Нет опций оплаты!"); break }
            premium.forEach { (payOption) in
                payments.append(payOption)
            }
            
        case .invisible:
            guard let premium = prices.invisible else { print("Нет опций оплаты!"); break }
            premium.forEach { (payOption) in
                payments.append(payOption)
            }
            
        case .upDatingForm:
            guard let upDatingForm = prices.searchRaise else { print("Нет опций оплаты!"); break }
            upDatingForm.forEach { (payOption) in
                payments.append(payOption)
            }
            
        case .registrationTax:
            guard let upDatingForm = prices.fee else { print("Нет опций оплаты!"); break }
            upDatingForm.forEach { (payOption) in
                payments.append(payOption)
            }
            
        case .gift:
            guard let upDatingForm = prices.gift else { print("Нет опций оплаты!"); break }
            upDatingForm.forEach { (payOption) in
                payments.append(payOption)
            }
        default: break
        }
    }
    
    @objc func showNoticeWeb(_ : UITapGestureRecognizer) {
        guard let url = URL(string: "https://linkyou.ru/rules") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func pressPaymentBtn(_ sender: UIButton) {
        guard let parentView = self.presentingViewController as? PopUpPaymentVCStep1,
            //let tabBarController = UIApplication.shared.keyWindow!.rootViewController?.presentedViewController as? UITabBarController,
            var entityId = selectedValue?.entityId, let paymentTypeId = selectedValue?.paymentTypeId  else { return }
        if giftID != nil { // подарок выбран, его id
            entityId = giftID!
        }
        parentView.showPaymentPageView(paymentTypeId: paymentTypeId, entityId: entityId)
    }
}

extension PopUpPaymentVCStep2: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedValue = payments[indexPath.row]
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return payments.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = priceTableView.dequeueReusableCell(withIdentifier: "PayOptionTableViewCell", for: indexPath) as! PayOptionTableViewCell
        
        if indexPath == [0,0] {
            cell.setTopCorner()
            priceTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            selectedValue = payments[indexPath.row]
        }
        
        if indexPath.row + 1 == payments.count {
            cell.setBottomCorner()
        }
        
        cell.titleLabel.text = payments[indexPath.row].name
        cell.priceLabel.text = " - \(payments[indexPath.row].price!) руб."
        
        return cell
    }
    
    
}
