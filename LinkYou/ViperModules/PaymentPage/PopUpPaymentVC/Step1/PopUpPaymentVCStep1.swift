//
//  PopUpPaymentVCStep1.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class PopUpPaymentVCStep1: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var closeViewBtn: UIButton!
    
    var paymentType: TizerType?
    var giftID: Int?
    
    init(tizerType: TizerType, giftID: Int? = nil) {
        super.init(nibName: "PopUpPaymentVCStep1", bundle: nil)
        self.giftID = giftID
        self.paymentType = tizerType
        self.modalPresentationStyle = .overCurrentContext
        let topView = AppDelegate().getTopMostViewController()
        topView!.present(self, animated: true, completion: nil) // можно через него делать презент и убрать
        if tizerType == TizerType.registrationTax, let tabBarController = UIApplication.shared.keyWindow!.rootViewController?.presentedViewController as? UITabBarController {
            closeViewBtn.isHidden = true
            tabBarController.children.forEach { (tab) in
                tab.tabBarItem.isEnabled = false
            }
        }
        prepareDataForView(tizerType: tizerType)
        
    }
    
    override func viewDidLoad() {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 3
        guard let paymentType = paymentType else { return }
        pageTitleLabel.text = TizerModelHelper().returnTizerTitle(tizerType: paymentType)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dismiss(animated: false, completion: nil)
    }
    
    private func prepareDataForView(tizerType: TizerType) {
        let tizers = TizerModelHelper().returnTizer(type: tizerType)
        mainStackView.addArrangedSubview(makeStack(tizers))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pressPayBtn(_ sender: UIButton) {
        if DeviceCurrent.current.guestModeStatus() { // гостевой режим, шаг 2 не показывать
            DeviceCurrent.current.showGuestModeAlert()
        } else {
            let step2 = PopUpPaymentVCStep2(tizerType: paymentType!, giftID: giftID)
            self.present(step2, animated: true, completion: nil)
        }
    }
}

extension PopUpPaymentVCStep1: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}


extension PopUpPaymentVCStep1 {
    
    private func makeStack(_ tizers: TizerModelType) -> UIStackView {
        
        mainStackView.arrangedSubviews.forEach( { $0.removeFromSuperview()} )
        var array = [[UIView]]()
        let stackView = UIStackView()
        
        for tizer in tizers.tizersArray {
            let tizerImageView = UIImageView(image: tizer.image)
            tizerImageView.clipsToBounds = true
            tizerImageView.contentMode = .scaleAspectFill
            
            let tizerLabel = UILabel()
            tizerLabel.text = tizer.text
            tizerLabel.numberOfLines = 3
            tizerLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            tizerLabel.textColor = UIColor.charcoalGrey
            tizerLabel.textAlignment = .center
            tizerLabel.widthAnchor.constraint(equalToConstant: tizerImageView.bounds.width * 2).isActive = true
            
            array.append([tizerImageView, tizerLabel])
            
        }
        
        array.forEach { (tizer) in
            let childStack = UIStackView(arrangedSubviews: tizer)
            childStack.axis = .vertical
            childStack.distribution = .equalCentering
            childStack.alignment = .center
            childStack.distribution = .equalSpacing
            childStack.spacing = 16
            stackView.addArrangedSubview(childStack)
        }
        
        stackView.axis = .vertical
        stackView.distribution = .equalCentering
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.spacing = 26
        
        return stackView
    }
    
    func showPaymentPageView(paymentTypeId: Int, entityId: Int) {
        
        if let parentView = self.presentingViewController?.children.first as? PaymentPageVCType {
            self.presentingViewController?.dismiss(animated: false) {
                parentView.showPaymentView(paymentTypeId: paymentTypeId, entityId: entityId)
            }
            // из чата
        } else if let parentView = self.presentingViewController as? DialogExtensionMenuViewController {
            parentView.dismiss(animated: true, completion: {
                parentView.showPaymentView(paymentTypeId: paymentTypeId, entityId: entityId)
            })
        }
        
    }
    
}
