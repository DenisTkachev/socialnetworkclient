//
//  PaymentURLType.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

/*
 Входящие параметры:
 payment_system_id - Это ТИП платежной системы. Допустимые значения 1 и 4. 1 это Fake Bank, 4 это Робокасса. Пока используем только 1. Значение по умолчанию 5
 payment_type_id - Это тип платежа или по другому, что мы оплачиваем. Подарок, за регистрацию, премиум, поднятие анкеты и т.д.
 
 Возможные значения для payment_type_id:
 Подарок = 1;
 Премиум = 2;
 Поднятие анкеты = 3;
 Попасть на главную = 4;
 Попасть в блок "Интересные анкеты" = 5;
 Невидимость = 6;
 Членский взнос за регистрацию = 7;
 
 entity_id - Это ID продукта или ID сущности. Всё зависит от того, что мы оплачиваем. Параметр кастомный, используется не всегда. Возможные значения:
 Для подарков - это ID подарка
 
 Для премиума
 - 10 дней премиума = 1;
 - 30 премиума = 2;
 - 150 дней премиума = 3;
 - 1 год премиума = 4;
 
 Для остальных
 - 1 = Два дня на главной странице
 - 2 = 3 дня на главной
 - 3 = 4 дня на главной
 - 4 = Попасть на страницу "Интересные анкеты" на 2 дня
 - 5 = Попасть на страницу "Интересные анкеты" на 3 дня
 - 6 = Попасть на страницу "Интересные анкеты" на 4 дня
 - 7 = Невидимость на 10 дней
 - 8 = Невидимость на 30 дней
 - 9 = Невидимость на 150 дней
 - 10 = Невидимость на 1 год
 - 11 = Оплата за регистрацию
 */

import Foundation

class PaymentURLType {

    let shared = PaymentURLType()
    enum PaymentSystemId: Int {
        case fakeBank = 1
        case robokassa = 4
    }
    
    enum PaymentTypeId: Int {
        case gift = 1                       // Подарок = 1;
        case premium = 2                    // Премиум = 2;
        case upDatingForm = 3              // Поднятие анкеты = 3;
        case toMainPage = 4                //        Попасть на главную = 4;
        case interestingDatingForm = 5      //        Попасть в блок "Интересные анкеты" = 5;
        case invisible = 6                   //        Невидимость = 6;
        case registrationTax = 7            //        Членский взнос за регистрацию = 7;
        
    }
    
    enum PremiumLimit: Int {
        case day1 = 1
        case day30 = 2
        case day150 = 3
        case day365 = 4
    }
    
    enum OtherType: Int {
        case twoDaysAtMainPage = 1 //Два дня на главной странице
        case threeDaysAtMainPage = 2 //3 дня на главной
        case fourDaysAtMainPage = 3 //4 дня на главной
        case interestingDatingForm2 = 4 //Попасть на страницу "Интересные анкеты" на 2 дня
        case interestingDatingForm3 = 5 //Попасть на страницу "Интересные анкеты" на 3 дня
        case interestingDatingForm4 = 6 //Попасть на страницу "Интересные анкеты" на 4 дня
        case invisible10d = 7 //Невидимость на 10 дней
        case invisible30d = 8 //Невидимость на 30 дней
        case invisible150d = 9 //Невидимость на 150 дней
        case invisible365d = 10 //Невидимость на 1 год
        case registrationTax = 11 //Оплата за регистрацию
    }
    
}
