//
//  TizerModelHelper.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

//import Foundation
import UIKit

typealias TizerType = PaymentURLType.PaymentTypeId

protocol TizerModelType {
    var tizersArray: [StandartTizer] { get set }
}

class TizerModelHelper {
    func returnTizer(type: TizerType) -> TizerModelType {
        return TizerGenerator.init(type: type)
    }
    
    func returnTizerTitle(tizerType: TizerType) -> String {
        switch tizerType {
        case .toMainPage:
            return "Попасть на главную"
        case .interestingDatingForm:
            return "Интересные анкеты"
        case .upDatingForm:
            return "Поднять анкету в поиске"
        case .premium:
            return "Премиум аккаунт дает Вам максимальные \nпреимущества на LinkYou:"
        case .registrationTax:
            return "Регистрационный сбор"
        case .invisible:
            return "Анонимный режим"
        case .gift:
            return "Сделать подарок"

        }
    }
}


class TizerGenerator: TizerModelType {
    var tizersArray = [StandartTizer]()
    init(type: TizerType) {
        switch type {
        case .toMainPage:
            for element in toMainPage {
                let object = StandartTizer(image: element.0, text: element.1)
                self.tizersArray.append(object)
            }
        case .premium:
            for element in premium {
                let object = StandartTizer(image: element.0, text: element.1)
                self.tizersArray.append(object)
            }
            
        case .interestingDatingForm:
            for element in interestingDatingForm {
                let object = StandartTizer(image: element.0, text: element.1)
                self.tizersArray.append(object)
            }
            
        case .upDatingForm:
            for element in upDatingForm {
                let object = StandartTizer(image: element.0, text: element.1)
                self.tizersArray.append(object)
            }
            
        case .registrationTax:
            for element in registrationTax {
                let object = StandartTizer(image: element.0, text: element.1)
                self.tizersArray.append(object)
            }
            
        case .invisible:
            for element in invisible {
                let object = StandartTizer(image: element.0, text: element.1)
                self.tizersArray.append(object)
            }
            
        default:
            for element in _default {
                let object = StandartTizer(image: element.0, text: element.1)
                self.tizersArray.append(object)
            }
            
        }
    }
    
    
    
    // data for teasers collection
    let toMainPage = [(#imageLiteral(resourceName: "toMainPageTizer1"),"Это ваша возможность показать себя всей аудитории LinkYou"),
                      (#imageLiteral(resourceName: "toMainPageTizer2"),"Вашу анкету увидет максимальное количество участников")]
    
    let interestingDatingForm = [(#imageLiteral(resourceName: "toMainPageTizer1"),"Вы в блоке «Интересные анкеты» \nТак ваша анкета будет видна на всех страницах LinkYou")]
    
    let upDatingForm = [(#imageLiteral(resourceName: "upDatingFormTizer1"),"Поднимите анкету в поиске \nТе, кто ищет человека похожего на вас, \nувидят вас в результатах поиска")]
    
    let premium = [(#imageLiteral(resourceName: "premiumTizer1"),"Поиск по национальности \nи вероисповеданию"),
                   (#imageLiteral(resourceName: "premiumTizer2"),"5 поднятий анкеты в результатах поиска"),
                   (#imageLiteral(resourceName: "premiumTizer3"),"Режим невидимости на сайте"),
                   (#imageLiteral(resourceName: "premiumTizer4"),"5 бесплатных подарков")]
    
    
    let registrationTax = [(#imageLiteral(resourceName: "upDatingFormTizer1"),"Ваша жертва в нашу честь!")]
    
    let invisible = [(#imageLiteral(resourceName: "premiumTizer3"),"Ваша жертва в нашу честь!")]
    
    let _default = [(#imageLiteral(resourceName: "toMainPageTizer1").toGrayTone!,"Нет текста и картинки для этого типа платежа"),
                   (#imageLiteral(resourceName: "toMainPageTizer2").toGrayTone!,"Нет текста и картинки для этого типа платежа")]
    
    
}

struct StandartTizer {
    let image: UIImage
    let text: String
}
