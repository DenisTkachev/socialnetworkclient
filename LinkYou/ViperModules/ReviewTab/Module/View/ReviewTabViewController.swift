//
//  ReviewTabReviewTabViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

protocol SelectedReviewCellType: class {
    func prepareForShowUserPage(user: AllReview)
}

protocol CollectionCellViewType {
    func setupCell(cellData: AllReview)
}

final class ReviewTabViewController: UIViewController, SelectedReviewCellType {
    
    // MARK: -
    // MARK: Properties
    var output: ReviewTabViewOutput!
    typealias screenType = ReviewTabPresenter.MenuRow
    let noDataView = NoDataView()
    
    @IBOutlet weak var reviewCollectionView: UICollectionView!
    @IBOutlet var slideMenuView: UIView!
    @IBOutlet weak var menuTableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.lightGray
        
        return refreshControl
    }()
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD.show(.progress, onView: reviewCollectionView.superview)
        
        output.viewIsReady()
        reviewCollectionView.dataSource = self
        reviewCollectionView.delegate = self
        
        setupNavigatioBar()
        setupTitleView()
        setupSlideMenu()
        setupTableView()
        
        reviewCollectionView.register(UINib(nibName: "ReviewImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ReviewImageCollectionViewCell")
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            reviewCollectionView.refreshControl = refreshControl
        } else {
            reviewCollectionView.addSubview(refreshControl)
        }
        
        setupNoDataView()
    }
    
    private func setupNoDataView() {
        noDataView.isHidden = true
        noDataView.frame = self.view.frame
        noDataView.delegate = self
        noDataView.setText(title: "", body: "", buttonText: nil)
        
        self.view.addSubview(noDataView)
    }
    
    private func setupTableView() {
        menuTableView.backgroundColor = UIColor.paleGrey
        menuTableView.separatorStyle = .none
        menuTableView.register(UINib(nibName: "ReviewMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewMenuTableViewCell")
    }
    
    private func setupNavigatioBar() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    private func setupSlideMenu() {
        let menuHeight = CGFloat(output.menuItems.count * 44)
        slideMenuView.frame = CGRect(x: 0, y: -menuHeight, width: self.view.frame.width, height: menuHeight)
        slideMenuView.layer.zPosition = 777
        self.view.addSubview(slideMenuView)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = reviewCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemWidth = view.bounds.width
            let itemHeight = CGFloat(100)
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
            layout.invalidateLayout()
        }
    }
    
    func prepareForShowUserPage(user: AllReview) {
        output.showUserPage(user: user)
    }
    
    @objc private func handleRefresh(_ sender: Any) {
        output.viewIsReady()
    }
    
    private func setupTitleView() {
        let titleView = HeaderTitleView(frame: CGRect(x: 0, y: 0, width: 300, height: 30))
        titleView.setTitle(text: output.menuItems.first!.rawValue)
        navigationItem.titleView = titleView
        navigationItem.titleView!.isUserInteractionEnabled = true
        navigationItem.titleView!.addTapGestureRecognizer(action: {
            titleView.pressToTitle(completion: { (showMenu) in
                self.showMenu(showMenu)
            })
        })
    }
    
    private func showMenu(_ isShow: Bool) {
        UIView.animate(withDuration: 0.3) {
            if isShow {
                self.noDataView.isUserInteractionEnabled = false
                self.slideMenuView.frame.origin.y = self.reviewCollectionView.frame.minY
            } else {
                self.noDataView.isUserInteractionEnabled = false
                self.slideMenuView.frame.origin.y = -self.slideMenuView.frame.height
            }
        }
    }
    
    private func setTitle(title: String) {
        (navigationItem.titleView as? HeaderTitleView)?.updateTitle(text: title)
        showMenu(false)
    }
    
}

// MARK: -
// MARK: ReviewTabViewInput
extension ReviewTabViewController: ReviewTabViewInput {
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
            self.refreshControl.endRefreshing()
        }
    }
    
    func presentData(data: [AllReview]) {
        reviewCollectionView.reloadData()
        HUD.hide()
        refreshControl.endRefreshing()
    }
}

extension ReviewTabViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        let total =
//        if total == 0 {
//            output.noDataForView()
//        } else {
//            noDataView.setHidden(condition: false)
//        }
        return output.mainData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = reviewCollectionView.dequeueReusableCell(withReuseIdentifier: "ReviewImageCollectionViewCell", for: indexPath) as! ReviewImageCollectionViewCell
        cell.setupCell(cellData: output.mainData[indexPath.row])
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let menu = navigationItem.titleView as? HeaderTitleView {
            if menu.arrowState == false {
                (navigationItem.titleView as! HeaderTitleView).pressToTitle { (showMenu) in
                    self.showMenu(showMenu)
                }
            }
        }
        output.showUserPage(user: output.mainData[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == output.mainData.count - 1 {
            output.requestNextPage()
        }
    }
    
}

extension ReviewTabViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewMenuTableViewCell", for: indexPath) as! ReviewMenuTableViewCell
        let menuItem = output.menuItems[indexPath.row].rawValue
        cell.setupCell(name: menuItem, count: "", selected: (output.selectedMenuCurrent.rawValue == menuItem))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        setTitle(title: output.menuItems[indexPath.row].rawValue)
        output.pressMenu(indexPath.row)
        HUD.show(.progress, onView: reviewCollectionView.superview)
        tableView.reloadData()
    }
    
}

extension ReviewTabViewController: NoDataViewDelegate {
    
    func buttonWasPressed() {
        self.tabBarController!.selectedIndex = 2
    }
    
    func showNoDataView(condition: Bool, text: (String,String,String?)) {
        noDataView.setText(title: text.0, body: text.1, buttonText: text.2)
        noDataView.setHidden(condition: !condition)
        HUD.hide()
    }
    
}
