//
//  ReviewImageCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 21/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit
import SwiftDate

class ReviewImageCollectionViewCell: UICollectionViewCell, CollectionCellViewType {
    
    @IBOutlet weak var userPic: MyAvatarView!
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messagePictureView: UIImageView!
    
    //    let whitePic = UIImage.init(color: UIColor.white, size: CGSize(width: 70, height: 70))
    
    override func prepareForReuse() {
        userPic.setStartSettings(hidden: true)
        messagePictureView.image = nil
        messagePictureView.subviews.forEach({ $0.removeFromSuperview() })
        messageLabel.text = nil
        nameUserLabel.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userPic.userAvatar.clipsToBounds = true
        userPic.userAvatar.layer.cornerRadius = userPic.frame.height / 2
        
        messagePictureView.clipsToBounds = true
        messagePictureView.layer.cornerRadius = 2
    }
    
    func setupCell(cellData: AllReview) {
        guard let user = cellData.user, let name = user.name, let age = user.birthday?.age else { return }
        nameUserLabel.text = name + ", " + age.toString()
        
        if let messagePictureURL = cellData.photo?.src?.small {
            messagePictureView.sd_setImage(with: URL(string: messagePictureURL), completed: nil)
        } else if let gift = cellData.gift, let url = gift.picture {
            messagePictureView.sd_setImage(with: URL(string: url), completed: nil)
        } else {
            messagePictureView.addSubview(makeBlankImage(date: cellData.datetime))
        }
        
        if let type = cellData.type {
            messageLabel.text = messageIs(type)
        } else if let comment = cellData.comment {
            messageLabel.text = comment
        } else {
            messageLabel.text = "У вас взаимная симпатия"
        }

        if let avatarStr = user.avatar?.src?.square {
            userPic.userAvatar.sd_setImage(with: URL(string: avatarStr)) { (image, error, cache, url) in
                self.userPic.userAvatar.toRoundedImage()
                self.userPic.addTunning()
                
                if let premium = user.is_premium {
                    self.userPic.enablePremiumStatus(on: premium)
                }
                if let online = user.is_online {
                    self.userPic.enableOnlineStatus(on: online)
                }
                if let vip = user.is_vip {
                    self.userPic.enableGoldRing(on: vip)
                }
            }
        } else {
            userPic.userAvatar.image = #imageLiteral(resourceName: "no-photo-user")
        }

    }
    
    private func makeBlankImage(date: Date?) -> UIImageView {
        var datetime = ""
        if let unwrapDate = date {
            if unwrapDate.isToday {
                datetime = unwrapDate.toFormat("HH:mm")
            } else {
                datetime = unwrapDate.toFormat("dd MMM, yyyy")
            }
        }
        
        let blankImage = UIImageView()
        blankImage.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        let imageUIView = UIImageView()
        let image = #imageLiteral(resourceName: "menu-heart")
        imageUIView.image = image
        imageUIView.contentMode = .scaleAspectFit
        imageUIView.frame = CGRect(x: 0, y: blankImage.center.y - 24, width: 20, height: 20)
        imageUIView.center.x = blankImage.center.x
        
        let label = UILabel.init(frame: blankImage.frame)
        label.text = datetime
        label.numberOfLines = 0
        label.textColor = UIColor.steelTwo
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.frame = CGRect(x: 0, y: imageUIView.frame.minY + 3, width: 70, height: 60)
        label.textAlignment = .center
        
        blankImage.addSubview(label)
        blankImage.addSubview(imageUIView)
        blankImage.backgroundColor = .white
        return blankImage
    }
    
    private func messageIs(_ type: String) -> String {
        switch type {
        case "profile":
            return "лайкнул вашу страницу"
        case "photo":
            return "лайкнул ваше фото"
        case "gift":
            return "сделал вам подарок"
        default:
            return ""
        }
        
        
    }
    
}
