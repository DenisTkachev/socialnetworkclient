//
//  ReviewMenuTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class ReviewMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var nameItem: UILabel!
    @IBOutlet weak var itemCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemCount.layer.cornerRadius = 16
        itemCount.layer.masksToBounds = true
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        nameItem.isHighlighted = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(name: String, count: String, selected: Bool) {
        nameItem.text = name
        nameItem.isHighlighted = selected
        
        if count == "0" {
            itemCount.isHidden = true
        } else {
            itemCount.text = count
//            itemCount.isHidden = false
            itemCount.isHidden = true // нет данных по лайкам с сервера, скрывем пока функционал
        }
    }
    
}
