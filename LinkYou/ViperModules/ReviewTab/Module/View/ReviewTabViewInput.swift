//
//  ReviewTabReviewTabViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol ReviewTabViewInput: class {
    /// - author: Denis Tkachev
    func presentData(data: [AllReview])
    func showAlert(title: String, msg: String)
    func showNoDataView(condition: Bool, text: (String,String,String?))
}
