//
//  HeaderTitleView.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class HeaderTitleView: UIView {
    
    var arrowState = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .clear
        addSubview(imageView)
        addSubview(headerTitle)
        setupLayout()
    }
    
    
    lazy var headerTitle: UILabel = {
        let headerTitle = UILabel()
        headerTitle.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        headerTitle.textColor = .white
        headerTitle.text = ""
        headerTitle.textAlignment = .center
        headerTitle.translatesAutoresizingMaskIntoConstraints = false
        return headerTitle
    }()
    
    lazy var imageView: UIImageView = {
        let contentView = UIImageView()
        contentView.image = UIImage(named: "titleArrowDown")
        contentView.contentMode = .scaleAspectFit
        contentView.translatesAutoresizingMaskIntoConstraints = false
        return contentView
    }()
    
    private func setupLayout() {
        
        NSLayoutConstraint.activate([
            headerTitle.topAnchor.constraint(equalTo: self.topAnchor),
            headerTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            headerTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            headerTitle.trailingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: -5),
            
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])
    }
    
    func pressToTitle(completion: (_ showMenu: Bool) -> ()) {
        
        if arrowState {
            // показать меню
            completion(true)
        } else {
            // скрыть меню
            completion(false)
        }
        
        self.imageView.flip(open: arrowState)
        arrowState.toggle()
    }
    
    func updateTitle(text: String) {
        self.headerTitle.text = text
        self.imageView.flip(open: arrowState)
        arrowState.toggle()
    }
    
    func setTitle(text: String) {
        self.headerTitle.text = text
    }
    
}
