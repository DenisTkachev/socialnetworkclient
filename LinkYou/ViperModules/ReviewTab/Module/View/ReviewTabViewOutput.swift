//
//  ReviewTabReviewTabViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol ReviewTabViewOutput: class {
    /// - author: Denis Tkachev
    func viewIsReady()
    func showUserPage(user: AllReview)
    func requestNextPage()
    func noDataForView(condition: Bool)
    
    // SlideMenu
    var menuItems: [ReviewTabPresenter.MenuRow] { get set }
    var mainData: [AllReview] { get set }
    func pressMenu(_ indexRow: Int)
    var selectedMenuCurrent: ReviewTabPresenter.MenuRow { get set }
}
