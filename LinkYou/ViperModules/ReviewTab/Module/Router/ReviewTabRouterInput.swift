//
//  ReviewTabReviewTabRouterInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol ReviewTabRouterInput: class {
    func presentUserPage(user: Guest, allUsers: [Guest])
}
