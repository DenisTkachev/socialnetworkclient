//
//  ReviewTabReviewTabPresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import SwiftDate

final class ReviewTabPresenter: ReviewTabViewOutput {

    enum MenuRow: String, CaseIterable {
        case whoLikeMe = "Кто лайкнул меня"
        case comments = "Комментарии"
        case like = "Кого лайкнул я"
        case sympathy = "Взаимные симпатии"
    }
    
    var selectedMenuCurrent = MenuRow.allCases.first!
    
    func pressMenu(_ indexRow: Int) {
        selectedMenuCurrent = menuItems[indexRow]
        resetPagination()
        interactor.loadData(page: paginationCurrent, ofType: selectedMenuCurrent)
    }
    
    // MARK: -
    // MARK: Properties

    weak var view: ReviewTabViewInput!
    var interactor: ReviewTabInteractorInput!
    var router: ReviewTabRouterInput!

    // pagination
    var paginationCurrent = 1
    var paginationIsEnd = 0
    var paginationLimit = 0
    var paginationTotal = 0
    var mainData = [AllReview]()
    
    // slideMenu
    var menuItems: [MenuRow] = MenuRow.allCases
    
    // MARK: -
    // MARK: ReviewTabViewOutput
    func viewIsReady() {
        interactor.loadData(page: paginationCurrent, ofType: menuItems.first!)
    }

    func showUserPage(user: AllReview) {
        let convertedModel = Guest.init(user: user.user, datetime: user.datetime)
        let convertedAllUsers = mainData.compactMap { (user) -> Guest in
            Guest.init(user: user.user, datetime: user.datetime)
        }
        router.presentUserPage(user: convertedModel, allUsers: convertedAllUsers)
    }
    
    func requestNextPage() {
        if mainData.count != paginationTotal, paginationIsEnd != 1  {
            paginationCurrent += 1
            interactor.loadData(page: paginationCurrent, ofType: selectedMenuCurrent)
        }
    }
    
    func noDataForView(condition: Bool) {
        switch selectedMenuCurrent {
        case .comments: view.showNoDataView(condition: condition, text: ("Нет комментариев","Вы еще никому не оставили комментарий, воспользуйтесь поиском или посмотрите анкеты дня, чтобы найти интересные анкеты.","Поиск"))
        case .like:     view.showNoDataView(condition: condition, text: ("Нет лайков","Вы еще никому не ставили лайк, воспользуйтесь поиском или посмотрите анкеты дня, чтобы найти интересные анкеты.","Поиск"))
        case .sympathy: view.showNoDataView(condition: condition, text: ("Нет симпатий","Поставьте лайк понравившейся анкете и получить взаимный лайк.","Поиск"))
        case .whoLikeMe: view.showNoDataView(condition: condition, text: ("Нет лайков","Вам еще никто не поставил лайк, воспользуйтесь поиском или посмотрите анкеты других участников.","Поиск"))

        }
    }
}

// MARK: -
// MARK: ReviewTabInteractorOutput
extension ReviewTabPresenter: ReviewTabInteractorOutput {

    func returnData(data: [AllReview], headers: [AnyHashable : Any]) {
        setHeaders(HttpHeaders.prepareHeaders(headers))
        self.mainData += data
        view.presentData(data: self.mainData)
        noDataForView(condition: !(mainData.count > 0))
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }

}

// MARK: -
// MARK: ReviewTabModuleInput
extension ReviewTabPresenter: ReviewTabModuleInput {

}

extension ReviewTabPresenter {
    
    private func resetPagination() {
        paginationCurrent = 1
        paginationIsEnd = 0
        paginationLimit = 0
        paginationTotal = 0
        mainData.removeAll()
    }
    
    fileprivate func setHeaders(_ headers: [XPaginationHeaders : Int]) {
        guard let current = headers[XPaginationHeaders.XPaginationCurrent],
            let isEnd = headers[XPaginationHeaders.XPaginationIsEnd], let limit = headers[XPaginationHeaders.XPaginationLimit],
            let total = headers[XPaginationHeaders.XPaginationTotal] else { return }
        paginationCurrent = current
        paginationIsEnd = isEnd
        paginationLimit = limit
        paginationTotal = total
    }
    
}





