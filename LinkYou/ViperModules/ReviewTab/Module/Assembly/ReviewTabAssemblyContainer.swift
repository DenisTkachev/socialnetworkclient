//
//  ReviewTabReviewTabAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class ReviewTabAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(ReviewTabInteractor.self) { (r, presenter: ReviewTabPresenter) in
			let interactor = ReviewTabInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(ReviewTabRouter.self) { (r, viewController: ReviewTabViewController) in
			let router = ReviewTabRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(ReviewTabPresenter.self) { (r, viewController: ReviewTabViewController) in
			let presenter = ReviewTabPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(ReviewTabInteractor.self, argument: presenter)
			presenter.router = r.resolve(ReviewTabRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(ReviewTabViewController.self) { r, viewController in
			viewController.output = r.resolve(ReviewTabPresenter.self, argument: viewController)
		}
	}

}
