//
//  ReviewTabReviewTabInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol ReviewTabInteractorOutput: class {
    func returnData(data: [AllReview], headers: [AnyHashable : Any])
    func showAlert(title: String, msg: String)
}
