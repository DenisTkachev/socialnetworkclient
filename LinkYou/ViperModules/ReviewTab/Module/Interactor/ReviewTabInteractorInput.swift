//
//  ReviewTabReviewTabInteractorInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol ReviewTabInteractorInput: class {
    func loadData(page: Int, ofType: ReviewTabPresenter.MenuRow)
}
