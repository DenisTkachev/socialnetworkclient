//
//  ReviewTabReviewTabInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class ReviewTabInteractor: ReviewTabInteractorInput {
    
    weak var output: ReviewTabInteractorOutput!
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    func loadData(page: Int, ofType: ReviewTabPresenter.MenuRow) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        var selectedRequest: UsersService?
        
        switch ofType {
        case .comments:
            selectedRequest = UsersService.comments(page: page)
        case .like:
            selectedRequest = UsersService.likes(page: page)
        case .whoLikeMe:
            selectedRequest = UsersService.answers(page: page)
        case .sympathy:
            selectedRequest = UsersService.sympathies(page: page)
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [debugPlagin, authPlagin])
        guard let request = selectedRequest else { return }
        
        usersProvider.request(request) { (result) in
            switch result {
            case .success(let response):
                do {
                    print("=>> \(ofType.rawValue)")
                    let userGuests = try JSONDecoder().decode([AllReview].self, from: response.data)
                    if let httpResponse = response.response {
                        let headers = httpResponse.allHeaderFields
                        self.output.returnData(data: userGuests, headers: headers)
                    }
                        } catch {
                            print(error)
                        }
            case let .failure(error):
                print(error)
            }
        }
    }
}
