//
//  SearchSearchInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol SearchInteractorOutput: class {
    func showAlert(title: String, msg: String)
    func returnAutocompleteData(languages: [AutocompleteModel], religions: [AutocompleteModel], nationality: [AutocompleteModel], professions: [AutocompleteModel], cities: [AutocompleteModel], age: [AutocompleteModel])
    func returnResult(users: [User], headers: [AnyHashable : Any])
    func totalSearchResults(_ counter: String)
}
