//
//  SearchSearchInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol SearchInteractorInput: class {
//    func getAutocomleteDictionaries()
    func sendSearchRequest(request: SearchRequest, page: Int)
    func getSearchCounter(request: SearchRequest)
}
