//
//  SearchSearchInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

//case getReligions
//case getNationality
//case getProfessions
//case getCities

final class SearchInteractor: SearchInteractorInput {
    
    weak var output: SearchInteractorOutput!
    
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    
//    var languages = PersonalConstants.allLanguages
//    var religions = PersonalConstants.allReligions
//    var nationality = PersonalConstants.allNationality
//    var professions = PersonalConstants.allProfessions
//    var cities = PersonalConstants.allCities
//    var age = PersonalConstants.age
//    var profArea = PersonalConstants.profarea
    
//    func getAutocomleteDictionaries() {
//       self.output.returnAutocompleteData(languages: self.languages, religions: self.religions, nationality: self.nationality, professions: self.professions, cities: self.cities, age: self.age)
//    }
    
    // MARK: Send Request
    
    func sendSearchRequest(request: SearchRequest, page: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<GeneralService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.searchUsers(request: request, page: page)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let findingPeople = try JSONDecoder().decode([User].self, from: response.data)
                        if let httpResponse = response.response {
//                            print("***\(findingPeople.count)")
                            let headers = httpResponse.allHeaderFields
                            self.output.returnResult(users: findingPeople, headers: headers)
                        }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getSearchCounter(request: SearchRequest) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<GeneralService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.searchListCount(request: request, page: 0)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let string = String(data: response.data, encoding: String.Encoding.utf8)
                    if let counter = string {
                        self.output.totalSearchResults(counter)
                    } else {
                        print("Ошибка парсинга ответа сервера")
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
}
