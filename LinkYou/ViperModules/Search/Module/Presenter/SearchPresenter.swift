//
//  SearchSearchPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class SearchPresenter: SearchViewOutput {
    
    // MARK: -
    // MARK: Properties
    
    weak var view: SearchViewInput!
    var interactor: SearchInteractorInput!
    var router: SearchRouterInput!
    
    var searchRequest = SearchRequest()
    typealias FiledType = SearchViewController.TextFieldType
    
    // pagination
    var paginationCurrent = 1
    var paginationIsEnd = 0
    var paginationLimit = 0
    var paginationTotal = 0
    var users = [User]()
    
    // MARK: -
    // MARK: SearchViewOutput
//    func viewIsReady() {
        // получить справочники
//        interactor.getAutocomleteDictionaries()
//    }
    
    func prepareSearchRequestObject(fieldType: SearchViewController.TextFieldType, value: (Int?,String?), ageValue: (Int, Int)?) {
        if let value = value as? (Int,String) {
            searchRequest.setValue(type: fieldType, value: value, ageValue: ageValue)
        }
    }
    
    func searchBtnPressed() {
            users.removeAll()
            paginationCurrent = 1
            view.showLoader()
            interactor.sendSearchRequest(request: searchRequest, page: paginationCurrent)
            interactor.getSearchCounter(request: searchRequest)
    }
    
    func showUserPage(user: User, allUser: [User]) {
        router.presentUserPage(user: user, allUsers: allUser)
    }
    
    func searchBtnPressedWithPagination() {
        if paginationIsEnd == 1 {
            view.isSpinner(on: false)
        }
        if users.count != paginationTotal, paginationIsEnd != 1 {
            view.isSpinner(on: true)
            paginationCurrent += 1
            interactor.sendSearchRequest(request: searchRequest, page: paginationCurrent)
        }
    }
    
//    private func isAllRequiredFieldsFilled() -> Bool {
//        if self.searchRequest.lookingFor == nil {
//            return false
//        } else {
//            return true
//        }
//
//    }
}

// MARK: -
// MARK: SearchInteractorOutput
extension SearchPresenter: SearchInteractorOutput {
    func returnResult(users: [User], headers: [AnyHashable : Any]) {
        setHeaders(HttpHeaders.prepareHeaders(headers))
//        if paginationIsEnd == 1 {
//            view.isSpinner(on: false)
//        }
        self.users += users
        view.setSearchResult(users: self.users, scrollToTop: paginationCurrent)
    }
    
    func returnAutocompleteData(languages: [AutocompleteModel], religions: [AutocompleteModel], nationality: [AutocompleteModel], professions: [AutocompleteModel], cities: [AutocompleteModel], age: [AutocompleteModel]) {
        
        view.setupInitialState(languages: languages, religions: religions, nationality: nationality, professions: professions, cities: cities, age: age, profArea: PersonalConstants.profarea)
        // TODO: можно сохранить локально и обновлять раз за сессию
        // теперь словари подгружаются на mainScreen и лежат в статике PersonalConstants
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func totalSearchResults(_ counter: String) {
        view.setSearchBtnTitle(foundedUsers: counter)
    }
    
}

// MARK: -
// MARK: SearchModuleInput
extension SearchPresenter: SearchModuleInput {
    
    
    
    
}


extension SearchPresenter {
    
    func setHeaders(_ headers: [XPaginationHeaders : Int]) {
        guard let current = headers[XPaginationHeaders.XPaginationCurrent],
            let isEnd = headers[XPaginationHeaders.XPaginationIsEnd], let limit = headers[XPaginationHeaders.XPaginationLimit],
            let total = headers[XPaginationHeaders.XPaginationTotal] else { return }
        paginationCurrent = current
        paginationIsEnd = isEnd
        paginationLimit = limit
        paginationTotal = total
    }
    
}
