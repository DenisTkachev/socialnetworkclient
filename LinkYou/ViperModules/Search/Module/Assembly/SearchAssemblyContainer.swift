//
//  SearchSearchAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class SearchAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(SearchInteractor.self) { (r, presenter: SearchPresenter) in
			let interactor = SearchInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(SearchRouter.self) { (r, viewController: SearchViewController) in
			let router = SearchRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(SearchPresenter.self) { (r, viewController: SearchViewController) in
			let presenter = SearchPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(SearchInteractor.self, argument: presenter)
			presenter.router = r.resolve(SearchRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(SearchViewController.self) { r, viewController in
			viewController.output = r.resolve(SearchPresenter.self, argument: viewController)
		}
	}

}
