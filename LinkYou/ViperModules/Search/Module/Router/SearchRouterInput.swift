//
//  SearchSearchRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol SearchRouterInput: class {
    func presentUserPage(user: User, allUsers: [User])
}
