//
//  SearchSearchRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class SearchRouter: SearchRouterInput {
    weak var transitionHandler: TransitionHandler!
    
    enum StorybordsID: String {
        case userPage = "UserpageSB"
    }
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func presentUserPage(user: User, allUsers: [User]) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .userPage), to: UserpageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(with: user, allUser: allUsers)
        }
    }
}
