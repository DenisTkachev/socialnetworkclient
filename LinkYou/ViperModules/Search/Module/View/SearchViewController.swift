//
//  SearchSearchViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

protocol SearchButtonActionHandlerType: class, UITextFieldDelegate {
    func buttonPressed()
    func setPickerData(_ type: SearchViewController.TextFieldType)
    func showHidePicker()
    func saveFromPopVc(id: (Int?,String?), type: SearchViewController.TextFieldType)
}

final class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SearchButtonActionHandlerType {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var slideUpView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var slideSearchBtn: RoundButtonCustom!
    
    // MARK: -
    // MARK: Properties
    var output: SearchViewOutput!
    var searchResults: [User]?
    
    lazy var languages: [AutocompleteModel] = { return PersonalConstants.allLanguages }()
    lazy var religions: [AutocompleteModel] =  { return PersonalConstants.allReligions }()
    lazy var nationality: [AutocompleteModel] = { return PersonalConstants.allNationality }()
    lazy var professions: [AutocompleteModel] = { return PersonalConstants.allProfessions }()
    lazy var cities: [AutocompleteModel] = { return PersonalConstants.allCities }()
        lazy var age: [AutocompleteModel] = { return PersonalConstants.age }()
    lazy var profArea: [AutocompleteModel] = { return PersonalConstants.profarea }()
    
//    var languages = [AutocompleteModel]()
//    var religions = [AutocompleteModel]()
//    var nationality = [AutocompleteModel]()
//    var professions = [AutocompleteModel]()
//    var cities = [AutocompleteModel]()
//    var age = [AutocompleteModel]()
//    var profArea = [AutocompleteModel]()
    
    var currentPickerData = [AutocompleteModel]()
    var isShowPicker = false
    let rowHeaders = [("Я ищу", TextFieldType.target), ("В возрасте", TextFieldType.age), ("В городе", TextFieldType.cities), ("Проф. область", TextFieldType.profArea), ("Профессия", TextFieldType.professions), ("Национальность", TextFieldType.nationality), ("Религия", TextFieldType.religions), ("Знание языка", TextFieldType.languages)]
    var selectedRow: IndexPath = [0,0]
    var selectedValueForRow = [TextFieldType : String]()
//    var defaultSearch : [TextFieldType : String] = [TextFieldType.target: "Друзей", TextFieldType.age: "18,75"]
    
    enum TextFieldType: Int {
        case age
        case target
        case cities
        case nationality
        case religions
        case languages
        case professions
        case profArea
    }
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        output.viewIsReady()
        tableView.register(UINib(nibName: "SearchHeaderTVCell", bundle: nil), forCellReuseIdentifier: "SearchHeaderTVCell")
        tableView.register(UINib(nibName: "SearchAgeTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchAgeTableViewCell")
        tableView.register(UINib(nibName: "UsersMatchedTableViewCell", bundle: nil), forCellReuseIdentifier: "UsersMatchedTableViewCell")
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        slideUpViewSetup()
        
    }
    
    func slideUpViewSetup() {
        slideUpView.frame = CGRect(x: 0, y: self.view.frame.maxY, width: UIScreen.main.bounds.width, height: 200)
        slideUpView.layer.cornerRadius = 10
        slideUpView.clipsToBounds = true
    }
    
    @IBAction func pressCancelBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if searchResults == nil, section == 0 {
            return 70
        } else if searchResults == nil, section == 1 {
            return 0
        } else if searchResults != nil, section == 1 {
            return 70
        } else if searchResults != nil, section == 0 {
            return 0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return searchResults?.count ?? 0
        } else {
            return rowHeaders.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 { // header
            let standartCell = tableView.dequeueReusableCell(withIdentifier: "SearchHeaderTVCell") as! SearchHeaderTVCell
            let ageCell = tableView.dequeueReusableCell(withIdentifier: "SearchAgeTableViewCell") as! SearchAgeTableViewCell
            
            standartCell.delegate = self
            standartCell.cellType = rowHeaders[indexPath.row].1
            standartCell.setupCell(pickerView: pickerView, title: rowHeaders[indexPath.row].0, placeHolder: output.searchRequest)
            
            switch indexPath.row {
                case 1:
                    ageCell.delegate = self
                    ageCell.cellType = rowHeaders[indexPath.row].1
                    ageCell.setupCell(pickerView: pickerView, title: rowHeaders[indexPath.row].0, placeHolder: output.searchRequest)
                    return ageCell
                default: return standartCell
            }
        } else {
            let resultCell = tableView.dequeueReusableCell(withIdentifier: "UsersMatchedTableViewCell") as! UsersMatchedTableViewCell
            if let searchResults = searchResults, !searchResults.isEmpty {
                resultCell.setupCell(data: searchResults[indexPath.row])
            }
            return resultCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath
        if let selectedCell = tableView.cellForRow(at: indexPath) as? SearchHeaderTVCell, selectedCell.cellType != nil {
            
            switch selectedCell.cellType! {
            case .cities:
                showPopOver(tableViewRow: cities, mapObject: selectedCell)
            case .professions:
                showPopOver(tableViewRow: professions, mapObject: selectedCell)
            case .profArea:
                showPopOver(tableViewRow: profArea, mapObject: selectedCell)
            default:
                setPickerData(selectedCell.cellType!)
                isShowPicker = true
                showHidePicker()
            }
        } else if let selectedCell = tableView.cellForRow(at: indexPath) as? SearchAgeTableViewCell, selectedCell.cellType != nil {
            setPickerData(selectedCell.cellType!)
            isShowPicker = true
            showHidePicker()
            addTitleToPicker()
        } else {
            guard let searchResults = searchResults else { return }
            output.showUserPage(user: searchResults[indexPath.row], allUser: searchResults)
        }
    }
    
    private func showPopOver(tableViewRow: [AutocompleteModel], mapObject: UIView) {
        DispatchQueue.main.async { [unowned self] in
            let popVC = AutocompleteController.init(mapObject: mapObject, rows: tableViewRow, delegate: nil)
            self.present(popVC, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let searchResults = searchResults else { return }
        if indexPath.row == searchResults.count - 1 {
            output.searchBtnPressedWithPagination()
        }
    }

    @IBAction func pressSearchBtn(_ sender: RoundButtonCustom) {
        searchResults = nil
        buttonPressed()
    }
    
    func buttonPressed() {
        output.searchBtnPressed()
        slideSearchBtn.isEnabled = false
    }
    
    func saveFromPopVc(id: (Int?, String?), type: SearchViewController.TextFieldType) {
        output.prepareSearchRequestObject(fieldType: type, value: id, ageValue: nil)
    }
    
    private func rotateSelectorArrow(open: Bool) {
        if let cell = self.tableView.cellForRow(at: self.selectedRow) as? SearchHeaderTVCell {
            cell.selectorImageView.rotate180WithAnimation(open: open)
        }
        if let cell = self.tableView.cellForRow(at: self.selectedRow) as? SearchAgeTableViewCell {
            cell.selectorFromImageView.rotate180WithAnimation(open: open)
            cell.selectorToImageView.rotate180WithAnimation(open: open)
        }
    }
    
    private func tableViewContentOffset(on: Bool) {
        if on, selectedRow.row > 3 {
            tableView.setContentOffset(CGPoint(x: 0.0, y: self.slideUpView.bounds.height), animated: false)
        } else {
            tableView.setContentOffset(CGPoint(x: 0.0, y: 0), animated: false)
        }
    }
    
    func showHidePicker() {
        if isShowPicker { // add
            pickerView.reloadAllComponents()
    
            if pickerView.numberOfComponents == 2 {
                    let ageFrom = currentPickerData.firstIndex(where: { $0.id == output.searchRequest.ageFrom })
                    let ageTo = currentPickerData.lastIndex(where: { $0.id == output.searchRequest.ageTo })
                    pickerView.selectRow(ageFrom ?? 0, inComponent: 0, animated: true)
                    pickerView.selectRow(ageTo ?? 0, inComponent: 1, animated: true)
            }
            
            tableView.isUserInteractionEnabled = false
            self.rotateSelectorArrow(open: true)
            self.view.addSubview(slideUpView)

            UIView.animate(withDuration: 0.3) {
//                self.slideUpView.frame.origin.y = self.view.frame.maxY - self.slideUpView.layer.bounds.height - 49
                let tabBarHeight = self.tabBarController?.tabBar.bounds.maxY ?? 0
                let navigationBar = self.navigationController?.navigationBar.bounds.height ?? 0
                self.slideUpView.frame.origin.y = UIScreen.main.bounds.height - self.slideUpView.bounds.height - tabBarHeight - navigationBar
                self.tableViewContentOffset(on: true)
            }
        } else { // delete
            tableView.isUserInteractionEnabled = true
            self.rotateSelectorArrow(open: false)
            UIView.animate(withDuration: 0.3, animations: {
                self.slideUpView.frame.origin.y = self.view.frame.maxY
                self.tableViewContentOffset(on: false)
                
            }) { (_) in
                self.slideUpView.removeFromSuperview()
                self.isShowPicker.toggle()
            }
        }
    }
    @IBAction func pickerSelectValueBtn(_ sender: UIButton) {
        removeTitleFromPicker()
        isShowPicker = false
        showHidePicker()
        self.pickerView(self.pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
    }
    
    @IBAction func pickerCancelBtn(_ sender: UIButton) {
        removeTitleFromPicker()
        isShowPicker = false
        showHidePicker()
    }
    
    private func addTitleToPicker() { 
        let labelWidth = pickerView.frame.width / CGFloat(2)
        let labelTexts = ["От","До"]
        for index in 0..<2 {
            let label: UILabel = UILabel.init(frame: CGRect(x: pickerView.frame.origin.x + labelWidth * CGFloat(index), y: -4, width: labelWidth, height: 20))
            label.text = labelTexts[index]
            label.textAlignment = .center
            pickerView.addSubview(label)
        }
    }
    
    private func removeTitleFromPicker() {
        let labels = self.pickerView.subviews
        labels.forEach { (view) in
            if view is UILabel {
                view.removeFromSuperview()
            }
        }
    }
    
    func setPickerData(_ type: SearchViewController.TextFieldType) {
        switch type {
        case .target:
            currentPickerData = PersonalConstants().lookingFor
        case .age:
            currentPickerData = age
        case .cities:
            currentPickerData = cities
        case .nationality:
            currentPickerData = nationality
        case .religions:
            currentPickerData = religions
        case .languages:
            currentPickerData = languages
        case .professions:
            currentPickerData = professions
        case .profArea:
            currentPickerData = profArea
        }
    }
}

// MARK: -
// MARK: SearchViewInput
extension SearchViewController: SearchViewInput {
    
    func shakeView() {
        guard let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? SearchHeaderTVCell else { return }
        cell.shake()
    }
    
    func isSpinner(on: Bool) {
    }
    
    func showLoader() {
        HUD.show(.progress, onView: tableView)
    }
    
    func setSearchResult(users: [User], scrollToTop: Int) {
        HUD.hide()
        searchResults = users
        slideSearchBtn.isEnabled = true
        if users.count > 0 {
            let resultSection = IndexSet(arrayLiteral: 1)
            tableView.reloadSections(resultSection, with: .bottom)
            if scrollToTop == 2 {
                tableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .top, animated: true)
            }
        } else {
            tableView.reloadData()
            showAlert(title: "Анкет не найдено", msg: "Попробуйте изменить условия поиска")
            slideSearchBtn.setTitle("Поиск", for: .normal)
        }
    }
    
    func setSearchBtnTitle(foundedUsers: String) {
        let btnTitle = String(format: NSLocalizedString("NumberOfForm", comment: ""))
        slideSearchBtn.setTitle("Найдено \(foundedUsers) " + btnTitle, for: .normal)
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
        }
    }
    
    func setupInitialState(languages: [AutocompleteModel], religions: [AutocompleteModel], nationality: [AutocompleteModel], professions: [AutocompleteModel], cities: [AutocompleteModel], age: [AutocompleteModel], profArea: [AutocompleteModel]) {
        
//        self.languages = languages
//        self.religions = religions
//        self.nationality = nationality
//        self.professions = professions
//        self.cities = cities
//        self.age = age
//        self.profArea = profArea
        HUD.hide()
    }
}

extension SearchViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if selectedRow == [0,1] {
            return 2 // возраст
        } else {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currentPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currentPickerData[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.numberOfComponents == 2 {
            if pickerView.selectedRow(inComponent: 0) >= pickerView.selectedRow(inComponent: 1) {
                pickerView.selectRow(pickerView.selectedRow(inComponent: 0), inComponent: 1, animated: true)
            }
        }
        
        if let selectedCell = tableView.cellForRow(at: selectedRow) as? SearchHeaderTVCell {
            selectedValueForRow[selectedCell.cellType!] = currentPickerData[row].title
            selectedCell.targetTextField.text = currentPickerData[row].title // все поля кроме возраста
            output.prepareSearchRequestObject(fieldType: selectedCell.cellType!, value: (currentPickerData[row].id, currentPickerData[row].title), ageValue: nil)
        } else {
            guard let selectedCell = tableView.cellForRow(at: selectedRow) as? SearchAgeTableViewCell else { return } // возраст
            let ageFrom = pickerView.selectedRow(inComponent: 0)
            let ageTo = pickerView.selectedRow(inComponent: 1)

            selectedCell.ageFromTextFiled.text = age[ageFrom].title!
            selectedCell.ageToTextField.text = age[ageTo].title!
            selectedValueForRow[selectedCell.cellType!] = "\(age[ageFrom].title!),\(age[ageTo].title!)"
            output.prepareSearchRequestObject(fieldType: selectedCell.cellType!, value: (currentPickerData[row].id, "\(age[ageFrom].title!),\(age[ageTo].title!)"), ageValue: (age[ageFrom].id!, age[ageTo].id!))
        }
    }
}
