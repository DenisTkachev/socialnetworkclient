//
//  SearchAgeTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 23/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class SearchAgeTableViewCell: UITableViewCell {

    typealias FiledType = SearchViewController.TextFieldType
    weak var delegate: SearchButtonActionHandlerType!
    @IBOutlet weak var ageFromTextFiled: TextFieldCustom!
    @IBOutlet weak var ageToTextField: TextFieldCustom!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectorFromImageView: UIImageView!
    @IBOutlet weak var selectorToImageView: UIImageView!
    
    var cellType: SearchViewController.TextFieldType?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    func setupCell(pickerView: UIPickerView, title: String, placeHolder: SearchRequest ) {
        titleLabel.text = title
        if let cellType = cellType { // костыльный обработчик подстановки цифр в строку с возрастом от и до
            let age = placeHolder.getValue(cellType: cellType).1
            let temp = age.components(separatedBy: ",")
            guard let from = temp.first, let to = temp.last else {
                ageFromTextFiled.text = "18"
                ageToTextField.text = "75"
                return
            }
            ageFromTextFiled.text = from
            ageToTextField.text = to
        } else {
            ageFromTextFiled.text = "18"
            ageToTextField.text = "75"
        }
    }
    
}
