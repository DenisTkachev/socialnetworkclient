//
//  SearchHeaderTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 05.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class SearchHeaderTVCell: UITableViewCell, AutocompleteControllerType {

    typealias FiledType = SearchViewController.TextFieldType
    
    @IBOutlet weak var targetTextField: TextFieldCustom!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectorImageView: UIImageView!
    @IBOutlet weak var premiumStar: UIImageView!
    
    weak var delegate: SearchButtonActionHandlerType!
    
    var languages = [AutocompleteModel]()
    var religions = [AutocompleteModel]()
    var nationality = [AutocompleteModel]()
    var professions = [AutocompleteModel]()
    var cities = [AutocompleteModel]()
    
    var cellType: SearchViewController.TextFieldType?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        selectorImageView.addTapGestureRecognizer {
//            self.targetTextField.text = nil
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
    }
    
    @IBAction func pressSearchBtn(_ sender: RoundButtonCustom) {
        delegate.buttonPressed()
    }
    
    func setupCell(pickerView: UIPickerView, title: String, placeHolder: SearchRequest) {
        titleLabel.text = title
        targetTextField.inputView = pickerView
        
        if let cellType = cellType {
            targetTextField.text = placeHolder.getValue(cellType: cellType).1
        } else {
            targetTextField.text = "Не выбрано"
        }
        
        guard let user = SuperUser.shared.superUser, let isPremium = user.is_premium else { return }
        if self.cellType == SearchViewController.TextFieldType.nationality || self.cellType == SearchViewController.TextFieldType.religions {
            premiumStar.isHidden = isPremium
            self.isUserInteractionEnabled = isPremium
            targetTextField.backgroundColor = (isPremium ? UIColor.white : UIColor.paleGreyTwo)
        }
        else {
            premiumStar.isHidden = true
            self.isUserInteractionEnabled = true
            targetTextField.backgroundColor = UIColor.white
        }
    }
    
    func returnSelectedValue(value: String, id: Int) {
        targetTextField.text = value
        delegate.saveFromPopVc(id: (id,value), type: cellType!)
    }
}
