//
//  SearchSearchViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol SearchViewOutput: class {
    /// - author: KlenMarket
//    func viewIsReady()
    func prepareSearchRequestObject(fieldType: SearchViewController.TextFieldType, value: (Int?, String?), ageValue: (Int, Int)?)
    func searchBtnPressed()
    func showUserPage(user: User, allUser: [User])
    func searchBtnPressedWithPagination()
    var searchRequest: SearchRequest { get set }
}
