//
//  SearchSearchViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 01/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol SearchViewInput: class {
    /// - author: KlenMarket
    func setupInitialState(languages: [AutocompleteModel], religions: [AutocompleteModel], nationality: [AutocompleteModel], professions: [AutocompleteModel], cities: [AutocompleteModel], age: [AutocompleteModel], profArea: [AutocompleteModel])
    func showAlert(title: String, msg: String)
    func setSearchResult(users: [User], scrollToTop: Int)
    func isSpinner(on: Bool)
    func shakeView()
    func showLoader()
    func setSearchBtnTitle(foundedUsers: String)
}
