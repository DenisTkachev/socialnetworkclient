//
//  PersonalPagePersonalPageRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class PersonalPageRouter: PersonalPageRouterInput {
    
    enum StorybordsID: String {
        case myBlog = "MyBlogSB"
        case newPost = "MyBlogAddPostSB"
        case photoAlbum = "PhotoAlbumSB"
        case userPage = "UserpageSB"
    }
    
  weak var transitionHandler: TransitionHandler!

    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func presentUserBlog(_ user: User) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .myBlog), to: MyBlogModuleInput.self)
            // Set transition case.
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(user)
        }
        // .perform()
    }
    
    func addNewPost() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .newPost), to: MyBlogAddPostModuleInput.self)
            // Set transition case.
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func showPhotoAlbum(_ photos: [UserPhotos], _ user: User) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .photoAlbum), to: PhotoAlbumModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(photos, user: user)
        }
    }
    
    func showUserPage(_ user: User) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .userPage), to: UserpageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .present))
            .then({ (moduleInput) -> Any? in
                moduleInput.configure(with: user, allUser: [])
                moduleInput.setOnlyViewMode()
                return nil
            })
    }
    
    func showBlogPost(_ post: BlogPostDetail) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .newPost), to: MyBlogAddPostModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.setPostData(post: post)
        }
    }

}
