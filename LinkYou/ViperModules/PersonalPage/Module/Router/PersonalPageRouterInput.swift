//
//  PersonalPagePersonalPageRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol PersonalPageRouterInput: class {
    func presentUserBlog(_ user: User)
    func addNewPost()
    func showPhotoAlbum(_ photos: [UserPhotos], _ user: User)
    func showUserPage(_ user: User)
    func showBlogPost(_ postId: BlogPostDetail)
}
