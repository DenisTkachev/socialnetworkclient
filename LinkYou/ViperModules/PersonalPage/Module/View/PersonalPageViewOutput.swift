//
//  PersonalPagePersonalPageViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

protocol PersonalPageViewOutput: class {
    /// - author: KlenMarket
    func viewIsReady()
    func refreshTags(_ user: User)
    func refrashEducation(_ user: User)
    func uploadImage(image: UIImage)
    func loadUserPhotos(userId: Int)
    func requestCountersGallery(_ idPhoto: Int)
    func pressDeletePhoto(photoId: Int)
    func showUserBlog(_ user: User)
    func addNewPost()
    func refrashBlog()
    func sendPhotoLike(_ photoID: Int)
    func sendUpdateAvatar(_ imageId: Int)
    func showPhotoAlbum(photos: [UserPhotos])
    func showUserPage(user: User)
    func deletePost(_ postId: Int)
    func editBlogPost(_ post: BlogPostDetail)
}
