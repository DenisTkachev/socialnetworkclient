//
//  EditInterestingPersonalFormViewController.swift
//  LinkYvar//
//  Created by Denis Tkachev on 27.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

class EditInterestingPersonalFormViewController: FormViewController {
    
    private var user: User!
    weak var updateDelegate: PersonalPageViewController!
    
    convenience init(superUser: User, delegate: PersonalPageViewController) {
        self.init()
        self.user = superUser
        self.updateDelegate = delegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NameRow.defaultCellUpdate = { cell, row in
            cell.textField.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            cell.textField.textColor = UIColor.charcoalGrey
            cell.textLabel?.textColor = UIColor.charcoalGrey
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            cell.textField.keyboardType = .default
        }
        
        form +++
            MultivaluedSection(multivaluedOptions: [.Insert, .Delete],
                               header: "Интересы",
                               footer: "") {
                                $0.tag = "textfields"
                                
                                $0.addButtonProvider = { section in
                                    return ButtonRow(){
                                        $0.title = "Добавить"
                                        }.cellUpdate { cell, row in
                                            cell.textLabel?.textAlignment = .left
                                    }
                                }
                                
                                $0.multivaluedRowToInsertAt = { index in
                                    return NameRow() {
                                        $0.placeholder = "Новый тэг" // when add new row
                                    }
                                }
                                
                                guard let tagsCloud = user.interests?.interests else { return }
                                for tag in tagsCloud {
                                    $0 <<< NameRow() {
                                        //$0.placeholder = tag
                                        $0.value = tag
                                    }
                                }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            let valuesDictionary = form.values()
            saveUserRow(values: valuesDictionary)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? { return "Удалить" }
    
    func saveUserRow(values: [String : Any?]) {
        guard var user = user else { return }
        guard var interesArray = user.interests?.interests else { return }
        guard let cloudTag = values["textfields"] as? [String] else { return }
        interesArray.removeAll()
        for tag in cloudTag {
            let row = String(describing: tag)
            interesArray.append(row)
        }
        user.interests?.interests = interesArray
        SuperUser.shared.save(user: user)
        updateDelegate.updateTags(user: user)
    }
    
}








