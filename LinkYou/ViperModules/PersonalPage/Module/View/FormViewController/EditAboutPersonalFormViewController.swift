//
//  EditAboutPersonalFormViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

class EditAboutPersonalFormViewController: FormViewController {
    
    fileprivate enum RowType: String {
        case smoke = "smoke"
        case relationship = "relationship"
        case orientation = "orientation"
        case children = "children"
        case alkho = "alkho"
        case height = "height"
        case weight = "weight"
        case about = "about"
    }
    
    private var user: User!
    weak var updateDelegate: PersonalPageViewController!
    
    convenience init(superUser: User, delegate: PersonalPageViewController) {
        self.init()
        self.user = superUser
        self.updateDelegate = delegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Обо мне"
        
        navigationAccessoryView.isHidden = true

        IntRow.defaultCellUpdate = { cell, row in
            cell.textField.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            cell.textField.textColor = UIColor.charcoalGrey
            cell.textLabel?.textColor = UIColor.charcoalGrey
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        }
        
        PickerInlineRow<String>.defaultCellUpdate = { cell, row in
            
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            cell.detailTextLabel?.textColor = UIColor.charcoalGrey
            cell.textLabel?.textColor = UIColor.charcoalGrey
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        }
        
        
        form +++ Section("")
            <<< TextAreaRow(RowType.about.rawValue) { row in
                row.title = "О себе"
                row.placeholder = "Напишите что-нибудь о себе"
                row.value = user.about
                }.cellSetup({ (cell, row) -> () in
                    cell.textView.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                    cell.textView.textColor = UIColor.charcoalGrey
                })
            
            <<< IntRow(RowType.height.rawValue) { row in
                row.title = "Рост, см."
                row.placeholder = ""
                row.value = user.height
                }
            
            <<< IntRow(RowType.weight.rawValue) { row in
                row.title = "Вес, кг."
                row.placeholder = ""
                row.value = user.weight
                
            }
            <<< PickerInlineRow<String>(RowType.relationship.rawValue) {
                $0.title = "Состоите ли в отношениях"
                $0.options = PersonalConstants().relationship
                $0.value = user.relationship?.name
            }
            <<< PickerInlineRow<String>(RowType.orientation.rawValue) {
                $0.title = "Ориентация"
                $0.options = PersonalConstants().orientation
                $0.value = user.orientation?.name
            }
            <<< PickerInlineRow<String>(RowType.children.rawValue) {
                $0.title = "Дети"
                $0.options = PersonalConstants().children
                $0.value = user.children?.name
            }
            <<< PickerInlineRow<String>(RowType.smoke.rawValue) {
                $0.title = "Отношение к курению"
                $0.options = PersonalConstants().smoke
                $0.value = user.smoking?.name
            }
            <<< PickerInlineRow<String>(RowType.alkho.rawValue) {
                $0.title = "Отношение к алкоголю"
                $0.options = PersonalConstants().alkho
                $0.value = user.alcohol?.name
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? { return "Удалить" }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            let valuesDictionary = form.values()
            saveUserRow(values: valuesDictionary)
        }
    }
    
    func saveUserRow(values: [String : Any?]) {
        guard var user = user else { return }
        
        if let relationship = values[RowType.relationship.rawValue] as? String {
            user.relationship = Relationship.init(name: relationship)
        }
        if let orientation = values[RowType.orientation.rawValue] as? String {
            user.orientation = Orientation.init(name: orientation)
        }
        if let children = values[RowType.children.rawValue] as? String {
            user.children = Children(name: children)
        }
        if let smoke = values[RowType.smoke.rawValue] as? String {
            user.smoking = Smoking(name: smoke)
        }
        if let alkho = values[RowType.alkho.rawValue] as? String {
            user.alcohol = Alcohol(name: alkho)
        }
        
        if let height = values[RowType.height.rawValue] as? Int {
            user.height = height
        }
        if let weight = values[RowType.weight.rawValue] as? Int {
            user.weight = weight
        }
        if let about = values[RowType.about.rawValue] as? String {
            user.about = about
        }
        SuperUser.shared.save(user: user)
        updateDelegate.updateView(user: user, element: .about)
    }
}






