//
//  EditInterestingPersonalViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 27.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

class EditInterestingPersonalViewController: FormViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        form +++
            MultivaluedSection(multivaluedOptions: [.Reorder, .Insert, .Delete],
                               header: "Multivalued TextField",
                               footer: ".Insert multivaluedOption adds the 'Add New Tag' button row as last cell.") {
                                $0.tag = "textfields"
                                $0.addButtonProvider = { section in
                                    return ButtonRow(){
                                        $0.title = "Add New Tag"
                                        }.cellUpdate { cell, row in
                                            cell.textLabel?.textAlignment = .left
                                    }
                                }
                                $0.multivaluedRowToInsertAt = { index in
                                    return NameRow() {
                                        $0.placeholder = "Tag Name"
                                    }
                                }
                                $0 <<< NameRow() {
                                    $0.placeholder = "Tag Name"
                                }
        }
        
    }
    

}
