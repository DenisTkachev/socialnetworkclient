//
//  EditEducationPersonalFormViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 27.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

// TODO: ПЕРЕПИСАТЬ весь класс. Работает криво!
import Eureka

class EditEducationPersonalFormViewController: FormViewController {
    
    private var user: User!
    private var languages: [Languages]!
    private var education: [Education]!
    weak var updateDelegate: PersonalPageViewController!
    
    var rowIndex = 0 // Unical tag for new row
    
    convenience init(superUser: User, delegate: PersonalPageViewController) {
        self.init()
        self.user = superUser
        self.updateDelegate = delegate
        if user.languages != nil {
            self.languages = user.languages!
        } else {
            self.languages = [Languages]()
        }
        
        if user.education != nil {
            self.education = user.education
        } else {
            self.education = [Education]()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rowIndex = user.education?.count ?? 0
        LabelRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .white
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 12)
            cell.textLabel?.textAlignment = .right
            
        }
        if let educations = user.education {
            for (index, element) in educations.enumerated() {
                form +++ Section("Образование") {
                    $0.tag = String(index)
                    
                    $0 <<< PickerInlineRow<String>("\(index)") {
                        $0.title = "Образование"
                        $0.options = PersonalConstants().educationType
                        $0.value = element.education_type?.name
                        //                        $0.tag = "education_type \(index)"
                        let deleteAction = SwipeAction(
                            style: .destructive,
                            title: "Удалить",
                            handler: { (action, row, completionHandler) in
                                guard let sectionIndexPath = row.indexPath?.section else { return }
                                self.form.remove(at: sectionIndexPath)
                                self.user.education?.remove(at: sectionIndexPath)
                                completionHandler?(true)
                        })
                        deleteAction.image = UIImage(named: "icon-trash")
                        
                        $0.trailingSwipe.actions = [deleteAction]
                        $0.trailingSwipe.performsFirstActionWithFullSwipe = true
                        
                    }
                    
                    $0 <<< TextRow() {
                        $0.title = "Название учебного заведения"
                        $0.value = element.institution?.name
                        //                        $0.tag = "institution \(index)"
                        //$0.hidden = Condition.init(booleanLiteral: self.isExpand)
                    }
                    
                    $0 <<< TextRow() {
                        $0.title = "Специальность"
                        $0.value = element.speciality?.name
                        //                        $0.tag = "speciality \(index)"
                        //$0.hidden = Condition.init(booleanLiteral: self.isExpand)
                    }
                }
            }
        }
        
        form +++ ButtonRow() {
            $0.title = "Добавить учебное заведение"
            $0.tag = "ButtonRow"
            $0.onCellSelection({ (cell, row) in
                self.insertEducationSection()
            })
        }
        
        form +++ SelectableSection<ListCheckRow<String>>("Знания языков", selectionType: .multipleSelection)
        form.last!.tag = "languageSectionTag"
        let languages = PersonalConstants.allLanguages.compactMap { (lang) -> (String, String) in
            return (lang.title!, String(lang.id!))
        }
        for lang in languages {
            form.last! <<< ListCheckRow<String>(lang.0){ listRow in
                listRow.title = lang.0
                listRow.selectableValue = lang.1// уровень владения
                listRow.tag = lang.1 // сюда id языка он уникален
                listRow.select()
                
                self.languages.forEach({ (myLang) in
                    if myLang.language!.id!.toString() == lang.1 {
                        listRow.value = myLang.level?.name!
                        print(myLang.id)
                    }
                })
                
            }
        }
        
        tableView.tableFooterView = UIView()
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView == self.tableView else { return }
        let row = form[indexPath]
        // row.baseCell.cellBecomeFirstResponder() may be cause InlineRow collapsed then section count will be changed. Use orignal indexPath will out of  section's bounds.
        if !row.baseCell.cellCanBecomeFirstResponder() || !row.baseCell.cellBecomeFirstResponder() {
            self.tableView?.endEditing(true)
        }
        row.didSelect()
        
        if row is ListCheckRow<String> {
            self.showActionSheet(indexPath)
        }
        
    }
    
    private func showActionSheet(_ indexPath: IndexPath) {
        
        let actionSheet = UIAlertController(title: "Уровень владения", message: nil, preferredStyle: .actionSheet)
        
        let selfLang = UIAlertAction(title: "Родной язык", style: .default) { (action) in
            setValue(action.title!)
        }
        
        let baseLavel = UIAlertAction(title: "Базовый уровень", style: .default) { (action) in
            setValue(action.title!)
        }
        
        let midLevel = UIAlertAction(title: "Читаю литературу", style: .default) { (action) in
            setValue(action.title!)
        }
        let heightLevel = UIAlertAction(title: "Свободно владею", style: .default) { (action) in
            setValue(action.title!)
        }
        let delete = UIAlertAction(title: "Не владею", style: .cancel) { (action) in
            setValue("")
            // удалить язык
        }
        
        actionSheet.addAction(selfLang)
        actionSheet.addAction(baseLavel)
        actionSheet.addAction(midLevel)
        actionSheet.addAction(heightLevel)
        actionSheet.addAction(delete)
        
        self.present(actionSheet, animated: true, completion: nil)
        
        func setValue(_ value: String) {
            let row = form[indexPath]
            
            if let _row = row as? ListCheckRow<String>, !value.isEmpty {
                _row.value = value
            } else if let _row = row as? ListCheckRow<String>, value.isEmpty {
                // delete
            }
        }
        
    }
    
    func insertEducationSection() {
        
        let row1 = PickerInlineRow<String>() {
            if let _ = user.education {
                $0.title = "Образование"
            } else {
                $0.title = "Образование"
            }
            
            $0.options = PersonalConstants().educationType
            $0.value = "Не указано"
            $0.tag = "education_type \(rowIndex)"
            let deleteAction = SwipeAction(
                style: .destructive,
                title: "Удалить",
                handler: { (action, row, completionHandler) in
                    guard let sectionIndexPath = row.indexPath?.section else { return }
                    self.form.remove(at: sectionIndexPath)
                    self.user.education?.remove(at: sectionIndexPath)
                    
                    //                    let row1:TextRow = self.form.rowBy(tag: selfTag)!
                    //                    row1.hidden = Condition.init(booleanLiteral: true)
                    //                    row1.evaluateHidden()
                    
                    completionHandler?(true)
            })
            deleteAction.image = UIImage(named: "icon-trash")
            
            $0.trailingSwipe.actions = [deleteAction]
            $0.trailingSwipe.performsFirstActionWithFullSwipe = true
            
        }
        
        let row2 = TextRow() {
            
            $0.title = "Название учебного заведения"
            //            $0.tag = "institution \(rowIndex + 1)"
        }
        let row3 = TextRow() {
            
            $0.title = "Специальность"
            //            $0.tag = "speciality \(rowIndex + 1)"
        }
        
        let section = Section("Образование")
        section.append(row1)
        section.append(row2)
        section.append(row3)
        section.tag = String(education.count + 1)
        form.insert(section, at: form.allSections.count - 2)
        
        let newEducationSection = Education.init(id: nil, user_id: user.id!, active: true, deleted: false, last_update: Date().toString(), speciality: nil, education_type: nil, institution: nil)
        user.education?.append(newEducationSection)
        
        rowIndex += 1
    }
    
    
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? { return "Удалить" }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            let valuesDictionary = form.values()
            saveUserRow(values: valuesDictionary)
        }
    }
    
    func saveUserRow(values: [String : Any?]) {
        guard var user = user else { return }
        
        var educationType: Education_type?
        var institution: Institution?
        var speciality: Speciality?
        var education = [Education]()
        
        let langSection: [(String?, String?, String)]? = (form.sectionBy(tag: "languageSectionTag")?.compactMap({ (row) -> (String?, String?, String) in
            let new = row as! ListCheckRow<String>
            return (new.title, new.value, new.tag!)
        }))
        
        if var langSection = langSection {
            langSection = langSection.filter { (row) -> Bool in
                return row.1 != nil // c уровнем владения
            }
            
            // удалить все языки с сервера
            if let currentLang = user.languages {
                currentLang.forEach { (lang) in
                    if let id = lang.id {
                        let parametrs = ["language[id]":id.toString(), "method":"language"]
                        NetworkRequest().sendParametrsToServer(parametrs)
                    }
                }
                user.languages?.removeAll()
            }
            
            for lang in langSection {
                    let language = Languages.init(id: nil, user_id: user.id!, deleted: false, last_update: Date().toString(), language:
                        Language.init(id: Int(lang.2)!, sorting: 0, name: lang.0!, deleted: false), level:
                        Level.init(name: lang.1!))
                        languages.append(language)
            }
            user.languages = languages
        }
        
        
        for section in form.allSections {
            if let title = section.header?.title, title.contains("Образование") {
                for cell in section {
                    if let cellTitle = cell.title, cell.baseValue != nil {
                        if cellTitle.contains("Образование") {
                            educationType = Education_type.init(name: cell.baseValue as! String)
                        }
                        
                        if cellTitle.contains("Название учебного заведения") {
                            institution = Institution.init(id: nil, user_id: user.id!, name: cell.baseValue as! String, active: true, deleted: false)
                        }
                        
                        if cellTitle.contains("Специальность") {
                            speciality = Speciality.init(id: nil, name: cell.baseValue as! String)
                        }
                    }
                }
                if speciality != nil, educationType != nil, institution != nil {
                    let eduTotal = Education.init(id: 0, user_id: user.id!, active: true, deleted: false, last_update: Date().toString(), speciality: speciality, education_type: educationType, institution: institution)
                    print("Образование добавлено в массив")
                    education.append(eduTotal)
                }
            }
        }
        
        user.education = education
        
        
        SuperUser.shared.save(user: user)
        updateDelegate.updateEducation(user: user)
        
        //TODO: отправить на сервер c верными ID!
    }
}
