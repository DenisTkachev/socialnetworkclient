//
//  CustomBookCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 30.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class CustomBookCell: Cell<String>, CellType {
    
    @IBOutlet weak var authorText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    var id = ""
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
    }
    
    public override func update() {
        super.update()

    }
    
}

// The custom Row also has the cell: CustomCell and its correspond value
public final class CustomBookRow: Row<CustomBookCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        // We set the cellProvider to load the .xib corresponding to our cell
        cellProvider = CellProvider<CustomBookCell>(nibName: "CustomBookCell")
    }
}
