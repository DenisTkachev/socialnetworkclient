//
//  EditCareerPersonalFormViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 30.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

class EditCareerPersonalFormViewController: FormViewController {
    
    fileprivate enum RowType: String {
        case profession = "profession"
        case occupation = "occupation"
        case pros = "pros"
        case cons = "cons"
        case finance = "finance"
    }
    
    private var user: User!
    private var job: Job!
    
    var rowIs: SelectedRow!
    
    enum SelectedRow {
        case none
        case prof
        case profArea
    }
    
    weak var updateDelegate: PersonalPageViewController!
    
    convenience init(superUser: User, delegate: PersonalPageViewController) {
        self.init()
        self.user = superUser
        self.updateDelegate = delegate
        self.rowIs = .none
        
        if let job = superUser.job {
            self.job = job
        } else {
            self.job = Job()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Карьера"
        navigationAccessoryView.isHidden = true
        
        TextRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .white
            cell.textLabel?.textColor = UIColor.charcoalGrey
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            cell.textField.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            cell.textField.textColor = UIColor.charcoalGrey
        }
        
        let defaultCellUpdate: (BaseCell, BaseRow) -> Void =  { cell, row in
            cell.contentView.backgroundColor = .white
            cell.textLabel?.textColor = UIColor.charcoalGrey
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            cell.detailTextLabel?.textColor = UIColor.charcoalGrey
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        }
        ActionSheetRow<String>.defaultCellUpdate = defaultCellUpdate
        
        setupRows()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            let valuesDictionary = form.values()
            saveUserRow(values: valuesDictionary)
        }
    }
    
    func setupRows() {
        form
            +++ Section("")
            <<< TextRow(RowType.profession.rawValue){ row in
                row.title = "Моя должность/профессия"
                row.placeholder = "Указать должность/профессию"
                row.value = user.job?.profession
                row.cellSetup({ (cell, row) in
                    cell.textField.isUserInteractionEnabled = false
                })
                }.onCellSelection({ (cell, row) in
                    DispatchQueue.main.async { [unowned self] in
                        let popVC = AutocompleteController.init(mapObject: cell, rows: PersonalConstants.allProfessions, delegate: nil)
                        popVC.defaultDelegate = self
                        self.present(popVC, animated: true, completion: { self.rowIs = .prof })
                    }
                }).onChange({ ( row ) in
                    row.value = self.user.job?.profession
                })
            
            <<< TextRow(RowType.occupation.rawValue){ row in
                row.title = "Профессиональная область"
                row.placeholder = "Указать проф. область"
                row.value = user.job?.occupation
                row.cellSetup({ (cell, row) in
                    cell.textField.isUserInteractionEnabled = false
                })
                }.onCellSelection({ (cell, row) in
                    DispatchQueue.main.async { [unowned self] in
                        let popVC = AutocompleteController.init(mapObject: cell, rows: PersonalConstants.profarea, delegate: nil)
                        popVC.defaultDelegate = self
                        self.present(popVC, animated: true, completion: { self.rowIs = .profArea })
                    }
                }).onChange({ ( row ) in
                    row.value = self.user.job?.occupation
                })
            
            <<< TextRow(RowType.pros.rawValue){ row in
                row.title = "Плюсы в моей работе"
                row.placeholder = "Несколько слов"
                row.value = user.job?.pros
            }
            
            <<< TextRow(RowType.cons.rawValue){ row in
                row.title = "Минусы в работе"
                row.placeholder = "Несколько слов"
                row.value = user.job?.cons
            }
            
            <<< ActionSheetRow<String>(RowType.finance.rawValue) {
                $0.title = "Материальное положение"
                $0.options = PersonalConstants().finance
                $0.value = user.job?.finance?.name
                $0.cell.detailTextLabel?.text = user.job?.finance?.name
        }

    }
    
    func saveUserRow(values: [String : Any?]) {
        
        if let pros = values[RowType.pros.rawValue] as? String {
            job.pros = pros
        }
        if let cons = values[RowType.cons.rawValue] as? String {
            job.cons = cons
        }
        if let finance = values[RowType.finance.rawValue] as? String {
            let financeObj = Finance(name: finance)
            job.finance = financeObj
        }
        user.job = job
        SuperUser.shared.save(user: user)
        updateDelegate.updateView(user: user, element: .job)
        
    }
    
}

extension EditCareerPersonalFormViewController: AutocompleteControllerType {
    func returnSelectedValue(value: String, id: Int) {
        
        switch rowIs.self! {
        case .prof:
            self.job.profession_id = id
            self.job.profession = value
        case .profArea:
            self.job.occupation_id = id
            self.job.occupation = value
        case .none: break
        }
    }
}
