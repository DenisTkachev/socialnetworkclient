//
//  EditFavBookPersonalFormViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 30.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

class EditFavBookPersonalFormViewController: FormViewController {
    
    fileprivate enum RowType: String {
        case profession = "profession"
        case occupation = "occupation"
        case pros = "pros"
        case cons = "cons"
        case finance = "finance"
    }
    
    private var user: User!
    private var books: [Books]!
    
    weak var updateDelegate: PersonalPageViewController!
    
    convenience init(superUser: User, delegate: PersonalPageViewController) {
        self.init()
        self.user = superUser
        self.updateDelegate = delegate
        if superUser.books != nil {
            self.books = superUser.books
        } else {
            self.books = [Books.init()]
            self.books.removeAll()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Любимые книги"
        navigationAccessoryView.isHidden = true
        
        LabelRow.defaultCellUpdate = { cell, row in
            //            cell.textField.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            //            cell.textField.textColor = UIColor.charcoalGrey
            cell.textLabel?.textColor = UIColor.charcoalGrey
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            cell.detailTextLabel?.textColor = UIColor.charcoalGrey
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        }
        
        setupRows()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            let valuesDictionary = form.allRows.dropLast()
            saveUserRow(values: valuesDictionary)
        }
    }
    
    func setupRows() {
        
        form +++
            MultivaluedSection(multivaluedOptions: [.Insert, .Delete],
                               header: "",
                               footer: "") {
                                $0.tag = "textfields"
                                
                                $0.addButtonProvider = { section in
                                    return ButtonRow(){
                                        $0.title = "Добавить"
                                        }.cellUpdate { cell, row in
                                            cell.textLabel?.textAlignment = .left
                                    }
                                }
                                
                                $0.multivaluedRowToInsertAt = { index in
                                    
                                    return CustomBookRow() {
                                        $0.cellProvider = CellProvider<CustomBookCell>(nibName: "CustomBookCell", bundle: Bundle.main)
                                        $0.cell.nameText.text = ""
                                        $0.cell.authorText.text = ""
                                    }
                                }
                                
                                guard let books = user.books else { return }
                                for book in books {
                                    $0 <<< CustomBookRow() {
                                        $0.cellProvider = CellProvider<CustomBookCell>(nibName: "CustomBookCell", bundle: Bundle.main)
                                        $0.cell.nameText.text = book.name
                                        $0.cell.authorText.text = book.author
                                        $0.cell.id = book.id ?? ""
                                    }
                                }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) { 
            let row = form[indexPath]
            if let book = row as? CustomBookRow {
                let parametrs = ["book[id]":book.cell.id, "method":"book"]
                NetworkRequest().sendParametrsToServer(parametrs)
                
                let section = row.section!
                if let _ = row.baseCell.findFirstResponder() {
                    tableView.endEditing(true)
                }
                section.remove(at: indexPath.row)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? { return "Удалить" }
    
    func saveUserRow(values: ArraySlice<BaseRow>) {
        guard var user = user else { return }
        books.removeAll()
        
        for row in values {
            if let bookCell = row.baseCell as? CustomBookCell, let name = bookCell.nameText.text, let author = bookCell.authorText.text {
                if name.count > 0, author.count > 0 {
                    let bookItem = Books.init(id: bookCell.id, user_id: user.id!, name: name, author: author, deleted: false, last_update: Date().toString())
                    books.append(bookItem)
                }
            }
        }
        
        user.books = books
        SuperUser.shared.save(user: user)
        updateDelegate.updateView(user: user, element: .book)
        
        //TODO: отправить на сервер c верными ID!
        //обновить данные на предыдущем view
    }
}
