//
//  PersonalPagePersonalPageViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import Photos
import PKHUD

protocol PersonalCellActionType: class {
    func editDataCell(cell: UITableViewCell)
    func updateView(user: User, element: SuperUser.UserContentType)
    func addPhoto()
    func pressButton(data: Any, actionType: UserpageViewController.ActionBtnType)
    func showPhotoAlbum()
    func pressPayBtn(type: TizerType)
}

final class PersonalPageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PhotoGalleryDelegateType {
    
    @IBOutlet weak var tableView: UITableView!
    // MARK: -
    // MARK: Properties
    var output: PersonalPageViewOutput!
    var user: User!
    var tableViewCell: [Void]!
    var tagsArray = [TagCloudElement()]
    var userPhotos = [UserPhotos]()
    var tagsInterestings = [TagCloudElement()]
    weak var photoGalleryController: PhotoGalleryViewController?
    
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    //var expandedIndexPaths: [IndexPath] = []
    
    enum CellType: String {
        case headerPersonalPageTableViewCell = "HeaderPersonalPageTableViewCell"
        case personalPhotoTableViewCell = "PersonalPhotoTableViewCell"
        case personalRatingTableViewCell = "PersonalRatingTableViewCell"
        case personalBlogTableViewCell = "PersonalBlogTableViewCell"
        case personalAboutMeTableViewCell = "PersonalAboutMeTableViewCell"
        case personalPageInterestsTableViewCell = "PersonalPageInterestsTableViewCell"
        case personalEducationTableViewCell = "PersonalEducationTableViewCell"
        case personalCareerTableViewCell = "PersonalCareerTableViewCell"
        case personalFavBookTableViewCell = "PersonalFavBookTableViewCell"
        case personalPetsTableViewCell = "PersonalPetsTableViewCell"
    }
    
    private enum EventSheetType {
        case delete
        case edit
    }
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        tableViewCell = [
            tableView.register(UINib(nibName: CellType.headerPersonalPageTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.headerPersonalPageTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalPhotoTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalPhotoTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalRatingTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalRatingTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalBlogTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalBlogTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalAboutMeTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalAboutMeTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalPageInterestsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalPageInterestsTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalEducationTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalEducationTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalCareerTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalCareerTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalFavBookTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalFavBookTableViewCell.rawValue),
            tableView.register(UINib(nibName: CellType.personalPetsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellType.personalPetsTableViewCell.rawValue)
        ]
        
        output.viewIsReady()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        setupNotification()
    }
    
    func setupNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateBlogCell(notification:)), name: .savedBlogPost, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBlogCell(notification:)), name: .deletedBlogPost, object: nil)
    }
    
    //    deinit {
    //        NotificationCenter.default.removeObserver(self)
    //    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.tintColor = UIColor.white
        title = "Моя страница"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath { // for collection view
        case [9,0]: if user?.pets != nil { return 165 } else { return 0 }
        case [3,0]: if user.ublogs?.count == 0 { return 150 } else { return UITableView.automaticDimension }
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 12
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewCell.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderPersonalPageTableViewCell", for: indexPath) as! HeaderPersonalPageTableViewCell
            cell.setupCell(user: user)
            cell.createTagCloud(withArray: tagsArray)
            cell.delegate = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalPhotoTableViewCell", for: indexPath) as! PersonalPhotoTableViewCell
            cell.setupImageToScrollView(imageArray: userPhotos)
            cell.delegate = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalRatingTableViewCell", for: indexPath) as! PersonalRatingTableViewCell
            cell.setupCell(data: user)
            return cell
        case 3:
           let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalBlogTableViewCell", for: indexPath) as! PersonalBlogTableViewCell
            cell.setupCell(blogData: user)
            cell.delegate = self
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalAboutMeTableViewCell", for: indexPath) as! PersonalAboutMeTableViewCell
            cell.setupCell(aboutMe: user)
            cell.editDelegate = self
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalPageInterestsTableViewCell", for: indexPath) as! PersonalPageInterestsTableViewCell
            cell.createTagCloud(withArray: tagsInterestings)
            cell.editDelegate = self
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalEducationTableViewCell", for: indexPath) as! PersonalEducationTableViewCell
            cell.editDelegate = self
            cell.setupCell(data: user)
            return cell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalCareerTableViewCell", for: indexPath) as! PersonalCareerTableViewCell
            cell.editDelegate = self
            cell.setupCell(data: user)
            return cell
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalFavBookTableViewCell", for: indexPath) as! PersonalFavBookTableViewCell
            cell.editDelegate = self
            cell.setupCell(data: user)
            return cell
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalPetsTableViewCell", for: indexPath) as! PersonalPetsTableViewCell
            cell.setupCell(data: user)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    func updateUserPhotos(userId: Int) {
        output.loadUserPhotos(userId: userId)
    }
    
    func requestCountersGallery(_ idPhoto: Int) {
        return output.requestCountersGallery(idPhoto)
    }
    
    func pressButton(data: Any, actionType: UserpageViewController.ActionBtnType) {
        if let photoId = data as? Int, actionType == .sendPhotoLike {
            output.sendPhotoLike(photoId)
        }
        
        if actionType == .zoomMainPhoto {
            let avatarPhoto = UserPhotos.init(id: 0, user_id: user.id!, datetime: "", description: "", src: user!.avatar!.src!, isLiked: false, likesCount: 0, commentsCount: 0)
            
            let photoVC = PhotoGalleryViewController.init(userPhotos: [avatarPhoto], selectedIndex: 0, user: user!)
            
            photoVC.delegateParent = self
            present(photoVC, animated: true, completion: nil)
        }
        
        if actionType == .showPhotoGallery {
            if let indexPath = data as? IndexPath {
                let photoGalleryController = PhotoGalleryViewController(userPhotos: userPhotos, selectedIndex: indexPath.row, user: user)
                photoGalleryController.delegateParent = self
                self.present(photoGalleryController, animated: true)
            }
        }
        
        if actionType == .pressMoreBtn {
            if let photoId = data as? Int {
                self.output.pressDeletePhoto(photoId: photoId)
            }
        }
        
        if actionType == .setAvatar {
            let photoGalleryController = PhotoGalleryViewController(userPhotos: userPhotos, selectedIndex: 0, user: user, enabledBottomToolBarElements: false)
            photoGalleryController.delegateParent = self
            self.present(photoGalleryController, animated: true)
        }
        
        if actionType == .sendUpdateAvatar, let id = data as? Int  {
            output.sendUpdateAvatar(id)
            self.showLoader(isShow: true)
        }
        
        if actionType == .showUserPage {
            output.showUserPage(user: user)
        }
        
    }
    
    func showPhotoComments(view: PhotoCommentView) {
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    @objc func updateBlogCell(notification: NSNotification) {
        output.refrashBlog() // post was added or deleted
    }
}

// MARK: -
// MARK: PersonalPageViewInput
extension PersonalPageViewController: PersonalPageViewInput {
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
        }
    }
    
    func showLoader(isShow: Bool = true) {
        if isShow {
            HUD.show(.progress)
        } else {
            HUD.hide()
        }
    }
    
    func removePhotoFromGallery(_ id: Int) {
        photoGalleryController?.deletePhoto(id)
    }
    
    func setCounters(likes: [PhotoLike], comments: [PhotoComment]) {
        photoGalleryController?.setCounters(likes: likes, comments: comments)
    }
    
    func reloadSectionWithNewData(user: User, section: PersonalPageViewController.CellType) {
        self.user = user
        switch section {
        case .personalEducationTableViewCell:
            tableView.reloadSections(IndexSet(integer: 6), with: .fade) // 6 образование
        case .headerPersonalPageTableViewCell:
            tableView.reloadSections(IndexSet(integer: 0), with: .fade) // 6 образование
        default:
            break
        }
        showLoader(isShow: false)
    }
    
    func setUserData(_ user: User, cloudTags: [TagCloudElement], tagsInterestings: [TagCloudElement]) {
        self.user = user
        tagsArray = cloudTags
        self.tagsInterestings = tagsInterestings
        reloadTableView()
    }
    
    func setUserPhoto(userPhotos: [UserPhotos]) {
        self.userPhotos = userPhotos
        self.user.photos_count = userPhotos.count
        tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        tableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .none)
        HUD.hide()
    }
    
    private func reloadTableView() {
        tableView.reloadData()
        HUD.hide()
    }
    
}

extension PersonalPageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            imagePicker.navigationBar.isTranslucent = false
            imagePicker.navigationBar.barTintColor = .cerulean
            imagePicker.navigationBar.tintColor = .white
            imagePicker.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        checkPermission()
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        output.uploadImage(image: image)
        dismiss(animated:true, completion: nil)
        HUD.show(.progress)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            print("User do not have access to photo album.")
        case .denied:
            print("User has denied the permission.")
        }
    }
}

extension PersonalPageViewController: PersonalCellActionType {
    
    func pressPayBtn(type: TizerType) {
        let _ = PopUpPaymentVCStep1.init(tizerType: type)
    }
    
    func editDataCell(cell: UITableViewCell) {
        
        guard let _user = SuperUser.shared.superUser else { return }
        
        if cell is PersonalPageInterestsTableViewCell { // +
            let detailVc = EditInterestingPersonalFormViewController(superUser: _user, delegate: self)
            pushEditViewController(vc: detailVc)
        }

        if cell is PersonalAboutMeTableViewCell { // +
            let detailVc = EditAboutPersonalFormViewController(superUser: _user, delegate: self)
            pushEditViewController(vc: detailVc)
        }

        if cell is PersonalEducationTableViewCell {
            let detailVc = EditEducationPersonalFormViewController(superUser: _user, delegate: self)
            pushEditViewController(vc: detailVc)
        }

        if cell is PersonalCareerTableViewCell {
            let detailVc = EditCareerPersonalFormViewController(superUser: _user, delegate: self)
            pushEditViewController(vc: detailVc)
        }

        if cell is PersonalFavBookTableViewCell {
            let detailVc = EditFavBookPersonalFormViewController(superUser: _user, delegate: self)
            pushEditViewController(vc: detailVc)
        }
    }
    
    func addPhoto() {
        showGalleryActionSheet()
    }
    
    func pushEditViewController(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateView(user: User, element: SuperUser.UserContentType) {
        self.user = user
        reloadTableView()
        SuperUser.shared.saveUserToServer(with: user, element: element)
    }
    
    func updateTags(user: User) {
        self.user = user // обновить облако тэгов
        output.refreshTags(user)
    }
    
    func updateEducation(user: User) {
        self.user.education = user.education
        self.user.languages = user.languages
        SuperUser.shared.saveUserToServer(with: user, element: .education)
        output.refrashEducation(user)
    }
    
    func showPhotoAlbum() {
        output.showPhotoAlbum(photos: userPhotos)
    }
}

extension PersonalPageViewController: CellButtonActionHandlerType {
    
    func showGalleryActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Камера", style: .default) { action in
            self.openCamera()
        }
        let photoGallery = UIAlertAction(title: "Галлерея", style: .default) { action in
            self.openPhotoLibrary()
        }
        actionSheet.addAction(photoGallery)
        actionSheet.addAction(camera)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func showBlogActionSheet(option: String) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        if self.user.ublogs != nil {
            let edit = UIAlertAction(title: "Отредактировать", style: .default) { action in
                self.actionSheetHandler(type: .edit)
            }
            let delete = UIAlertAction(title: "Удалить", style: .destructive) { action in
                self.actionSheetHandler(type: .delete)
            }
            actionSheet.addAction(edit)
            actionSheet.addAction(delete)
        }

        //        if option == "full" {
        //            actionSheet.addAction(blackList)
        //        }
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func actionSheetHandler(type: EventSheetType) {
        guard let postLast = self.user.ublogs?.last, let lastId = self.user.ublogs?.last?.id else { return }
        switch type {
        case .edit:
            print("edit")
            let post = BlogPostDetail.init(last: postLast, user: self.user!)
            output.editBlogPost(post)
        case .delete:
            print("delete")
            output.deletePost(lastId)
        }
    }
    
    func buttonPressed(cell: UITableViewCell, actionType: UserpageViewController.ActionBtnType) {
        switch cell {
            
        case is PersonalBlogTableViewCell:
            if actionType == UserpageViewController.ActionBtnType.pressMoreBtn {
                showBlogActionSheet(option: "short")
            }
            if actionType == UserpageViewController.ActionBtnType.showMoreBlog {
                output.showUserBlog(user)
            }
            if actionType == UserpageViewController.ActionBtnType.addNewPost {
                output.addNewPost()
            }
            
        default:
            break
        }
    }
    
}

fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
