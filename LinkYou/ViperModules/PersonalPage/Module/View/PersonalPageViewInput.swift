//
//  PersonalPagePersonalPageViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol PersonalPageViewInput: class {
  /// - author: KlenMarket
    func setUserData(_ user: User, cloudTags: [TagCloudElement], tagsInterestings: [TagCloudElement])
    func setUserPhoto(userPhotos: [UserPhotos])
    func reloadSectionWithNewData(user: User, section: PersonalPageViewController.CellType)
    func setCounters(likes: [PhotoLike], comments: [PhotoComment])
    func removePhotoFromGallery(_ id: Int)
    func showLoader(isShow: Bool)
    func showAlert(title: String, msg: String)
}
