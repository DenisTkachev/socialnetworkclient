//
//  PersonalCareerTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PersonalCareerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var oblastLabel: UILabel!
    @IBOutlet weak var plusLabel: UILabel!
    @IBOutlet weak var minusLabel: UILabel!
    @IBOutlet weak var incomeLabel: UILabel!
    
    weak var editDelegate: PersonalCellActionType!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    @IBAction func pressEditBtn(_ sender: UIButton) {
        editDelegate.editDataCell(cell: self)
    }
    
    func setupCell(data: User) {
        guard let job = data.job else { return }
        
        if let profession = job.profession {
            positionLabel.text = profession
        } else {
            positionLabel.text = ""
        }
        
        if let occupation = job.occupation {
            oblastLabel.text = occupation
        } else {
            oblastLabel.text = ""
        }
        
        if let finance = job.finance?.name {
            incomeLabel.text = finance
        } else {
            incomeLabel.text = ""
        }
        
        if let plus = job.pros {
            plusLabel.text = plus
        } else {
            plusLabel.text = ""
        }
        
        if let minus = job.cons {
            minusLabel.text = minus
        } else {
            minusLabel.text = ""
        }
    }
}
