//
//  PersonalEducationTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PersonalEducationTableViewCell: UITableViewCell {

    @IBOutlet weak var masterStack: UIStackView!
    @IBOutlet weak var typeStack: UIStackView!
    @IBOutlet weak var nameStack: UIStackView!
    @IBOutlet weak var specialityStack: UIStackView!
    @IBOutlet weak var languageStack: UIStackView!
    
    @IBOutlet weak var educationTypeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    
    weak var editDelegate: PersonalCellActionType!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        masterStack.subviews.forEach( { $0.removeFromSuperview() })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        masterStack.subviews.forEach( { $0.removeFromSuperview() })
        masterStack.spacing = 0
        masterStack.distribution = .fillEqually
        masterStack.alignment = .fill
    }
    
    @IBAction func editEducationBtn(_ sender: UIButton) {
        editDelegate.editDataCell(cell: self)
    }
    
    func setupCell(data: User) {

        if let educations = data.education {
            for education in educations {
                if !education.education_type!.name!.isEmpty {
                    let row = makeStackRow(left: "Образование", right: education.education_type!.name!)
                    masterStack.addArrangedSubview(row)
                }
                if !education.institution!.name!.isEmpty {
                    let row = makeStackRow(left: "Название учебного заведения", right: education.institution!.name!)
                    masterStack.addArrangedSubview(row)
                }
                if !education.speciality!.name!.isEmpty {
                    let row = makeStackRow(left: "Специальность", right: education.speciality!.name!)
                    masterStack.addArrangedSubview(row)
                }
                
                if education.id != educations.last?.id {
                    let separatorRow = makeStackRow(left: " ", right: " ")
                    masterStack.addArrangedSubview(separatorRow)
                }
            }
        }
        
        if let languages = data.languages, languages.count > 0  {
            var summaryArray = [String]()
            for lang in languages {
                summaryArray.append(lang.language!.name!)
            }
            let string = summaryArray.joined(separator: ", ")
            let row = makeStackRow(left: "Языки:", right: string)
            masterStack.addArrangedSubview(row)
        }

    }
    
    private func makeStackRow(left: String, right: String) -> UIStackView {
        let labelLeft = UILabel()
        labelLeft.widthAnchor.constraint(equalToConstant: 200).isActive = true
//        labelLeft.heightAnchor.constraint(equalToConstant: 30).isActive = true
        labelLeft.numberOfLines = 1
        labelLeft.text = left
        labelLeft.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        labelLeft.textColor = UIColor.charcoalGrey
        labelLeft.textAlignment = .left
        labelLeft.translatesAutoresizingMaskIntoConstraints = false
        labelLeft.heightAnchor.constraint(equalToConstant: 28).isActive = true
        
        let labelRight = UILabel()
        labelRight.numberOfLines = 4
        labelRight.text = right
        labelRight.font = UIFont.systemFont(ofSize: 12)
        labelRight.textColor = UIColor.charcoalGrey
        labelRight.textAlignment = .right
        labelRight.translatesAutoresizingMaskIntoConstraints = false
        labelRight.heightAnchor.constraint(equalToConstant: 28).isActive = true
        labelRight.lineBreakMode = .byWordWrapping
        let labelArray = [labelLeft, labelRight]
        
        let stackView = UIStackView(arrangedSubviews: labelArray)
        
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        return stackView
    }
}
