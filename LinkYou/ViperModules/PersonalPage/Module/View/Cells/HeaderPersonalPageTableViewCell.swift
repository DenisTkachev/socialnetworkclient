//
//  HeaderPersonalPageTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 21.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class HeaderPersonalPageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userPhotoView: UIImageView!
    @IBOutlet weak var nameAgeLabel: UILabel!
    @IBOutlet weak var professionLabel: UILabel!
    @IBOutlet weak var myFormPositionLabel: UILabel!
    
    @IBOutlet weak var myLikesBtn: UIButton!
    @IBOutlet weak var myPhotoBtn: UIButton!
    @IBOutlet weak var myLocationBtn: UIButton!
    
    @IBOutlet weak var tagCloudView: UIView!
    @IBOutlet weak var tagCloudHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var lineGradientView: LineGradientView!
    
    let placeHolder = UIImage.init(color: UIColor.white, size: CGSize(width: Double(UIScreen.main.bounds.width), height: 380))
    weak var delegate: PersonalCellActionType!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        myFormPositionLabel.layer.cornerRadius = myFormPositionLabel.bounds.height / 2
        myFormPositionLabel.clipsToBounds = true
        lineGradientView.isHidden = true
        userPhotoView.addTapGestureRecognizer(action: {
            self.delegate.pressButton(data: "", actionType: UserpageViewController.ActionBtnType.zoomMainPhoto)
        })
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    func setupCell(user: User) {
        guard let avatar = user.avatar?.src?.square, let name = user.name, let age = user.birthday?.age, let proffesion = user.job?.profession, let likesCount = user.likes?.count, let photoCount = user.photos_count, let location = user.location?.city_name else { return }
        
        let url = URL(string: avatar)
        userPhotoView.sd_setImage(with: url, placeholderImage: placeHolder) { (image, error, cache, url) in
            let resizedImage = image?.cropToBounds(image: image!, width: Double(UIScreen.main.bounds.width), height: Double(self.userPhotoView.bounds.width))
            self.userPhotoView.image = resizedImage!
            self.userPhotoView.backgroundColor = .black
            self.lineGradientView.isHidden = false
        }
        
        nameAgeLabel.text = "\(name), \(age)"
        professionLabel.text = proffesion
        myLikesBtn.setTitle(String(format: NSLocalizedString("NumberOfLikes", comment: ""), likesCount), for: .normal)
        myPhotoBtn.setTitle("\(photoCount) фото", for: .normal)
        myLocationBtn.setTitle("\(location)", for: .normal)
        
    }
    
    @IBAction func howViewMyPageBtn(_ sender: UIButton) {
        delegate.pressButton(data: "", actionType: .showUserPage)
    }
    
    @IBAction func changeMainPhotoBtn(_ sender: UIButton) {
        delegate.pressButton(data: "", actionType: UserpageViewController.ActionBtnType.setAvatar)
    }
    
    @IBAction func upFormBtn(_ sender: RoundButtonCustom) {
        delegate.pressPayBtn(type: .upDatingForm)
    }
    
    @IBAction func toMainPageBtn(_ sender: RoundButtonCustom) {
        delegate.pressPayBtn(type: .toMainPage)
    }
    
    @IBAction func favoriteBtn(_ sender: RoundButtonCustom) {
        delegate.pressPayBtn(type: .premium)
    }
}

extension HeaderPersonalPageTableViewCell {
    // prepare keyWord icons
    func setupKeyWordIcon(keyWord: String, bgView: UIView) -> UIButton {
        var icon = UIImage()
        switch keyWord {
        case "Овен": icon = #imageLiteral(resourceName: "zodiac-aries")
        case "Телец" : icon = #imageLiteral(resourceName: "zodiac-taurus")
        case "Близнецы" : icon = #imageLiteral(resourceName: "zodiac-gemini")
        case "Рак" : icon = #imageLiteral(resourceName: "zodiac-cancer")
        case "Лев" : icon = #imageLiteral(resourceName: "zodiac-leo")
        case "Дева" : icon = #imageLiteral(resourceName: "zodiac-virgo")
        case "Весы" : icon = #imageLiteral(resourceName: "zodiac-vesi")
        case "Скорпион" : icon = #imageLiteral(resourceName: "zodiac-scorpio")
        case "Стрелец" : icon = #imageLiteral(resourceName: "zodiac-sagittarius")
        case "Козерог" : icon = #imageLiteral(resourceName: "zodiac-capricorn")
        case "Водолей" : icon = #imageLiteral(resourceName: "zodiac-aquarius")
        case "Рыбы" : icon = #imageLiteral(resourceName: "zodiac-pisces")
            
        default: icon = #imageLiteral(resourceName: "userPage-target")
        }
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: bgView.bounds.origin.x + 8, y: 3.0, width: 23.0, height: 23.0)
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = CGFloat(button.frame.size.width) / CGFloat(2.0)
        button.setImage(icon, for: .normal)
        button.tag = tag
        //button.addTarget(self, action: #selector(removeTag(_:)), for: .touchUpInside)
        return button
    }
    // tagCloud
    func createTagCloud(withArray data: [TagCloudElement]) {
        
        for tempView in tagCloudView.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        var xPos: CGFloat = 0.0
        var yPos: CGFloat = 0.0
        var tag: Int = 1
        for str in data  {
            let startstring = str.text
            let width = startstring.widthOfString(usingFont: UIFont.systemFont(ofSize: 12.0))
            let checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )
            if checkWholeWidth > UIScreen.main.bounds.size.width - 8.0 {
                xPos = 0.0
                yPos = yPos + 29.0 + 8.0
            }
            let bgView = UIView(frame: CGRect(x: xPos, y: yPos, width: width + 27 , height: 29.0))
            
            bgView.layer.cornerRadius = 14.5
            bgView.layer.borderWidth = 1
            bgView.layer.borderColor = UIColor.paleGrey.cgColor
            bgView.backgroundColor = UIColor.white
            bgView.tag = tag
            
            var rect = CGRect(x: 15, y: 0.0, width: width, height: bgView.frame.size.height)
            if bgView.tag < 3 { // Знак зодика или цель
                
                bgView.addSubview(setupKeyWordIcon(keyWord: startstring, bgView: bgView))
                rect = CGRect(x: 35, y: 0.0, width: width, height: bgView.frame.size.height)
                bgView.frame = CGRect(x: xPos, y: yPos, width:width + 10.0 + 38.5 , height: 29.0)
            }
            
            let textlable = UILabel(frame: rect)
            
            textlable.font = UIFont.systemFont(ofSize: 12.0)
            textlable.text = startstring
            textlable.textColor = UIColor.charcoalGrey
            bgView.addSubview(textlable)
            
            
            xPos = CGFloat(xPos) + CGFloat(width) + CGFloat(10.0) + CGFloat(50.0)
            tagCloudView.addSubview(bgView)
            tag = tag  + 1
        }
        tagCloudHeightConstr.constant = 29 + yPos
    }
}
