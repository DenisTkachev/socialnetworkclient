//
//  PersonalPageInterestsTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PersonalPageInterestsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tagsCloudView: UIView!
    @IBOutlet weak var cellHeightConstraint: NSLayoutConstraint!
    
    weak var editDelegate: PersonalCellActionType!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    @IBAction func editCellBtn(_ sender: UIButton) {
        editDelegate.editDataCell(cell: self)
    }
}

extension PersonalPageInterestsTableViewCell {
    // tagCloud
    func createTagCloud(withArray data: [TagCloudElement]) {
        if data.count > 0 {
            data.first!.createTagCloud(withArray: data, tagsCloudView: tagsCloudView, cellHeightConstraint: cellHeightConstraint)
        }
    }
}
