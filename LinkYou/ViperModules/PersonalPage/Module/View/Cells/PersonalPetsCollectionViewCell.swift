//
//  PersonalPetsCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PersonalPetsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var typePetLabel: UILabel!
    @IBOutlet weak var namePetLabel: UILabel!
    @IBOutlet weak var leftFotoPet: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
