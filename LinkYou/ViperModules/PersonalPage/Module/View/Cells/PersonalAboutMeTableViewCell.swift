//
//  PersonalAboutMeTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PersonalAboutMeTableViewCell: UITableViewCell {

    weak var editDelegate: PersonalCellActionType!
    
    @IBOutlet weak var mainTextView: UITextView!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var relationshipLabel: UILabel!
    @IBOutlet weak var orientationLabel: UILabel!
    @IBOutlet weak var childrenLabel: UILabel!
    @IBOutlet weak var smokeLabel: UILabel!
    @IBOutlet weak var alcoholLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var middleConstraint: NSLayoutConstraint!
    // stackView
    @IBOutlet weak var heightStack: UIStackView!
    @IBOutlet weak var relationShipStack: UIStackView!
    @IBOutlet weak var orientationStack: UIStackView!
    @IBOutlet weak var childrensStack: UIStackView!
    @IBOutlet weak var smokeStack: UIStackView!
    @IBOutlet weak var alcoStack: UIStackView!
    @IBOutlet weak var weightStack: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
    func setupCell(aboutMe: User) {
        
        if let mainText = aboutMe.about, mainText.count > 0 {
            mainTextView.isHidden = false
            mainTextView.text = mainText
            middleConstraint.constant = 16
        } else {
            mainTextView.isHidden = true
            middleConstraint.constant = -20
        }
        if let height = aboutMe.height {
            heightLabel.text = height.toString()
        }
        if let relationShip = aboutMe.relationship?.name {
            relationshipLabel.text = relationShip
        }
        if let orientation = aboutMe.orientation?.name {
            orientationLabel.text = orientation
        }
        if let children = aboutMe.children?.name {
            childrenLabel.text = children
        }
        if let smoke = aboutMe.smoking?.name {
            smokeLabel.text = smoke
        }
        if let alcohol = aboutMe.alcohol?.name {
            alcoholLabel.text = alcohol
        }
        if let weight = aboutMe.weight {
            weightLabel.text = weight.toString()
        }
    }
    @IBAction func editAboutInfo(_ sender: UIButton) {
        editDelegate.editDataCell(cell: self)
    }
}
