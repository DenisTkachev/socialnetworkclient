//
//  PersonalPhotoTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class PersonalPhotoTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var photoCountLabel: UILabel!
    
    weak var delegate: PersonalCellActionType!
    var imageArray: [UserPhotos] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(UINib(nibName: "PersonalPhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PersonalPhotoCollectionViewCell")
        photoCountLabel.addTapGestureRecognizer {
            self.delegate.showPhotoAlbum()
        }
    }
    
    override func prepareForReuse() {
        imageArray.removeAll()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
}

extension PersonalPhotoTableViewCell {
    
    func setupImageToScrollView(imageArray: [UserPhotos]) {
        photoCountLabel.text = String(format: NSLocalizedString("NumberOfPhotos", comment: ""), imageArray.count)
        self.imageArray = imageArray
        collectionView.reloadData()
    }
    
}

extension PersonalPhotoTableViewCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonalPhotoCollectionViewCell", for: indexPath) as! PersonalPhotoCollectionViewCell
        
        if indexPath == [0,0] {
            cell.userPhoto.image = #imageLiteral(resourceName: "pesonalAddPhotoPlaceHolder")
            return cell
        } else {
            if let src = imageArray[indexPath.row - 1].src, let srcDefault = src.default {
                cell.userPhoto.sd_setImage(with: URL(string: srcDefault), placeholderImage: nil, completed: { (image, error, cache, url) in
                    let resizedImage = image?.cropToBounds(image: image!, width: 106, height: 106)
                    cell.userPhoto.image = resizedImage
                })
            }
        }

        return cell
    }
}

extension PersonalPhotoTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath == [0,0] {
            delegate.addPhoto()
        } else {
            let correctIndex = IndexPath(row: indexPath.row - 1, section: indexPath.section) // minus element image "pesonalAddPhotoPlaceHolder"
            delegate.pressButton(data: correctIndex, actionType: .showPhotoGallery)
        }
        
    }
}


