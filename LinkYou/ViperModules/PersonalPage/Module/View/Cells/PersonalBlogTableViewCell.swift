//
//  PersonalBlogTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PersonalBlogTableViewCell: UITableViewCell {

    weak var delegate: CellButtonActionHandlerType!
    
    @IBOutlet weak var writeInBlogLabel: UILabel!
    var userId = 0
    
    @IBOutlet weak var countOfPost: UILabel!
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var textPostTextView: UITextView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postLikesBtn: UIButton!
    @IBOutlet weak var postViewBtn: UIButton!
    @IBOutlet weak var postCommentBtn: UIButton!
    @IBOutlet weak var morePostBtn: UIButton!
//    @IBOutlet weak var imgHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var textHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var writeInBlogIcon: UIImageView!
    @IBOutlet weak var topDivLineView: UIView!
    @IBOutlet weak var bottomDivLineView: UIView!
    @IBOutlet weak var allPostBtn: RoundButtonCustom!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        removeDemoData()
        
        writeInBlogLabel.clipsToBounds = true
        writeInBlogLabel.layer.borderWidth = 1
        writeInBlogLabel.layer.borderColor = UIColor.paleGrey.cgColor
        writeInBlogLabel.layer.cornerRadius = writeInBlogLabel.bounds.height / 2
        
//        morePostBtn.addTapGestureRecognizer {
//            self.delegate.buttonPressed(cell: self, actionType: .pressMoreBtn)
//        }
        
        writeInBlogLabel.addTapGestureRecognizer {
            self.delegate.buttonPressed(cell: self, actionType: .addNewPost)
        }
        
        postCommentBtn.addTapGestureRecognizer {
            self.delegate.buttonPressed(cell: self, actionType: .showMoreBlog)
        }
        
        textPostTextView.addTapGestureRecognizer {
            self.delegate.buttonPressed(cell: self, actionType: .showMoreBlog)
        }
        
        postImageView.addTapGestureRecognizer {
            self.delegate.buttonPressed(cell: self, actionType: .showMoreBlog)
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    private func removeDemoData() {
//        countOfPost: UILabel!
        userAvatarImageView.image = nil
        userNameLabel.text = nil
        postDateLabel.text = nil
        textPostTextView.text = nil
        postImageView.image = nil
//        imgHeightConstr.constant = 0
        textHeightConstr.constant = 0
    }
    
    func isHidButton(hide: Bool) {
        postLikesBtn.isHidden = hide
        postViewBtn.isHidden = hide
        postCommentBtn.isHidden = hide
        morePostBtn.isHidden = hide
        userAvatarImageView.isHidden = hide
        topDivLineView.isHidden = hide
        bottomDivLineView.isHidden = hide
        
        self.layoutIfNeeded()
    }
    
    func setupCell(blogData: User) {
        isHidButton(hide: true)
        guard let blogs = blogData.ublogs else { return } // TODO: Проверить отображение с записями блога (когда ноль и больше 5)
        if let count = blogs.count, count > 0 {
            allPostBtn.isEnabled = true
            isHidButton(hide: false)
            countOfPost.text = String(format: NSLocalizedString("NumberOfBlogs", comment: ""), count)
            guard let last = blogs.last else { return }
            userNameLabel.text = blogData.name ?? ""
            postDateLabel.text = last.last_update
            if let urlString = blogData.avatar?.src?.square {
                userAvatarImageView.sd_setImage(with: URL(string: urlString), placeholderImage: nil)
                userAvatarImageView.layer.cornerRadius = userAvatarImageView.bounds.height / 2
                userAvatarImageView.clipsToBounds = true
            }
            
            textPostTextView.text = last.text_short
            if last.text_short?.count == 0 {
                textHeightConstr.constant = 0
            } else {
                textHeightConstr.constant = textPostTextView.contentSize.height
            }
            
            postLikesBtn.setTitle(String(format: NSLocalizedString("NumberOfLikes", comment: ""), last.likes_count ?? 0), for: .normal)
            postViewBtn.setTitle("\(last.views_count ?? 0)", for: .normal)
            postCommentBtn.setTitle(String(format: NSLocalizedString("NumberOfComments", comment: ""), last.comments_count ?? 0), for: .normal)
            
            if last.is_liked ?? false {
                postLikesBtn.setImage(#imageLiteral(resourceName: "heart-select-userPage"), for: .normal)
            } else {
                postLikesBtn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
            }
            
            if let urlString = blogData.ublogs?.last?.photos?.last?.src {
//                imgHeightConstr.constant = 219
                postImageView.sd_setImage(with: URL(string: urlString), placeholderImage: nil)
            } else {
//                imgHeightConstr.constant = 0
            }
        } else {
            countOfPost.text = "0 Записей"
            allPostBtn.isEnabled = false
            return
        }
    }
    
    @IBAction func showAllPostBtn(_ sender: RoundButtonCustom) {
        delegate.buttonPressed(cell: self, actionType: .showMoreBlog)
    }
    
    @IBAction func pressActionSheet(_ sender: UIButton) {
        delegate.buttonPressed(cell: self, actionType: .pressMoreBtn)
    }
    //    @objc func tapAction(recognizer: UITapGestureRecognizer) {
//        delegate.buttonPressed(cell: self, actionType: .addNewPost)
//    }
}
