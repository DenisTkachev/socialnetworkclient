//
//  PersonalPagePersonalPagePresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

final class PersonalPagePresenter: PersonalPageViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: PersonalPageViewInput!
    var interactor: PersonalPageInteractorInput!
    var router: PersonalPageRouterInput!
    
    var superUser: User!
    
    // MARK: -
    // MARK: PersonalPageViewOutput
    func viewIsReady() {
        guard let user = superUser else { return }
        view.showLoader(isShow: true)
        interactor.loadUserData(userID: user.id!)
        view.setUserData(user, cloudTags: prepareCloudTags(data: user), tagsInterestings: prepareInterestingsTags(data: user))
        let countOfPhoto = superUser.photos_count ?? 0
        interactor.loadUserPhotos(userID: user.id!, photoLimit: countOfPhoto)
    }

    func refreshTags(_ user: User) {
        self.superUser = user
        let cloudTags = prepareCloudTags(data: user)
        let tagsInterestings = prepareInterestingsTags(data: user)
        view.setUserData(user, cloudTags: cloudTags, tagsInterestings: tagsInterestings)
        SuperUser.shared.saveUserToServer(with: tagsInterestings, element: .interests)
    }
    
    func refrashEducation(_ user: User) {
        self.superUser = user
        view.reloadSectionWithNewData(user: user, section: PersonalPageViewController.CellType.personalEducationTableViewCell)
    }
    
    func refrashBlog() {
        interactor.loadUserData(userID: superUser.id!)
    }
    
    func uploadImage(image: UIImage) {
        if let data = image.jpegData(compressionQuality: 0.5) {
            interactor.uploadImageData(imageData: data)
        } else {
            // convert picture error
        }
    }
    
    func loadUserPhotos(userId: Int) {
        let countOfPhoto = superUser.photos_count ?? 0
        interactor.loadUserPhotos(userID: userId, photoLimit: countOfPhoto)
    }
    
    func requestCountersGallery(_ idPhoto: Int) {
        return interactor.loadCounterForPhoto(idPhoto)
    }
    
    func pressDeletePhoto(photoId: Int) {
        interactor.deletePhoto(id: photoId)
    }
    
    func showUserBlog(_ user: User) {
        router.presentUserBlog(user)
    }
    
    func addNewPost() {
        router.addNewPost()
    }
    
    func sendUpdateAvatar(_ imageId: Int) {
        interactor.setNewAvatar(imageId)
    }
    
    func showPhotoAlbum(photos: [UserPhotos]) {
        router.showPhotoAlbum(photos, superUser)
    }
    
    func showUserPage(user: User) {
        router.showUserPage(user)
    }
    
    func deletePost(_ postId: Int) {
        interactor.deletePost(postId)
    }
    
    func editBlogPost(_ post: BlogPostDetail) {
        router.showBlogPost(post)
    }
}

// MARK: -
// MARK: PersonalPageInteractorOutput
extension PersonalPagePresenter: PersonalPageInteractorOutput {
    
    func postWasDeleted() {
        interactor.loadUserData(userID: superUser.id!)
    }
    
    func showLoadingIndicator(isShow: Bool) {
        view.showLoader(isShow: isShow)
    }
    
    func updateHeader() {
        if let selfUser = SuperUser.shared.superUser {
            view.reloadSectionWithNewData(user: selfUser, section: PersonalPageViewController.CellType.headerPersonalPageTableViewCell)
        }
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func photoWasDeleted(id: Int) {
        view.removePhotoFromGallery(id)
    }
    
    func likesAndCommentGeted(_ likes: [PhotoLike], _ comments: [PhotoComment]) {
        view.setCounters(likes: likes, comments: comments)
    }
    
    func userDataGeted(data: User) {
        self.superUser = data
        view.setUserData(data, cloudTags: prepareCloudTags(data: data), tagsInterestings: prepareInterestingsTags(data: data))
        let countOfPhoto = superUser.photos_count ?? 0
        interactor.loadUserPhotos(userID: data.id!, photoLimit: countOfPhoto)
    }
    
    func userPhotosUpload(_ userId: Int) {
        interactor.loadUserData(userID: userId)
    }
    
    func userPhotoUrlGeted(data: [UserPhotos]) {
        self.view.setUserPhoto(userPhotos: data)
    }
    
    func sendPhotoLike(_ photoID: Int) {
        interactor.addPhotoLike(photoID)
    }

}

// MARK: -
// MARK: PersonalPageModuleInput
extension PersonalPagePresenter: PersonalPageModuleInput {

}

extension PersonalPagePresenter {
    
    func prepareCloudTags(data: User) -> [TagCloudElement] {
        var cloudTags = [TagCloudElement]()
        if let zodiac = data.birthday?.zodiac?.name {
            cloudTags.append(TagCloudElement(text: zodiac, icon: nil))
        }
        if let goal = data.goal?.name {
            cloudTags.append(TagCloudElement(text: "Цель: \(goal)", icon: nil))
        }
        if let lookingFor = data.looking_for?.name {
            cloudTags.append(TagCloudElement(text: lookingFor, icon: nil))
        }
        if let nationality = data.nationality {
            if nationality.visibility?.id == 1 { // "Показывать мою национальность всем"
                cloudTags.append(TagCloudElement(text: "Национальность: \(nationality.name!)", icon: nil))
            }
        }
        if let religion = data.religion?.name {
            cloudTags.append(TagCloudElement(text: "Религия: \(religion)", icon: nil))
        }
        return cloudTags
    }
    
    func prepareInterestingsTags(data: User) -> [TagCloudElement] {
        var cloudTags = [TagCloudElement]()
        guard let interestings = data.interests?.interests else { return cloudTags }
        for element in interestings {
            if !element.isEmpty {
                cloudTags.append(TagCloudElement(text: element, icon: nil))
            }
        }
        return cloudTags
    }
    
}
