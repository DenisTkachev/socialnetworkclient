//
//  PersonalPagePersonalPageInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol PersonalPageInteractorInput: class {
    
    func loadUserPhotos(userID: Int, photoLimit: Int)
    func uploadImageData(imageData: Data)
    func loadUserData(userID: Int)
    func loadCounterForPhoto(_ idPhoto: Int)
    func deletePhoto(id: Int)
    func addPhotoLike(_ photoId: Int)
    func setNewAvatar(_ imageId: Int)
    func updateUserData()
    func deletePost(_ postId: Int)
}
