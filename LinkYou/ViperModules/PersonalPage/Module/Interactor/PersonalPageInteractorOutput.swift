//
//  PersonalPagePersonalPageInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol PersonalPageInteractorOutput: class {
    func userPhotoUrlGeted(data: [UserPhotos])
    func userPhotosUpload(_ userId: Int)
    func userDataGeted(data: User)
    func likesAndCommentGeted(_ likes: [PhotoLike], _ comments: [PhotoComment])
    func photoWasDeleted(id: Int)
    func showAlert(title: String, msg: String)
    func showLoadingIndicator(isShow: Bool)
    func updateHeader()
    func postWasDeleted()
}
