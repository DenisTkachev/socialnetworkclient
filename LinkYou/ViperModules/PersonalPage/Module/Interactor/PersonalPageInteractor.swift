//
//  PersonalPagePersonalPageInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Moya

final class PersonalPageInteractor: PersonalPageInteractorInput {
    
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    weak var output: PersonalPageInteractorOutput!
    let dispatchGroup = DispatchGroup()
    
    // SEND image data and send image to user gallery
    func uploadImageData(imageData: Data) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let photosProvider = MoyaProvider<UploadPhotosService>(plugins: [debugPlagin, authPlagin])
        // send data image and get url
        photosProvider.request(.uploadUserPhoto(data: imageData)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userPhotos = try JSONDecoder().decode(ImageDataResponse.self, from: response.data)
                    if let photos = userPhotos.data {
                        for photo in photos {
                            if let url = photo.src {
                                self.addPhotoToGallery(url: url)
                            }
                        }
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addPhotoLike(_ photoId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.userPhotoLike(id: photoId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    self.loadCounterForPhoto(photoId)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addPhotoToGallery(url: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let photosProvider = MoyaProvider<PhotosService>(plugins: [debugPlagin, authPlagin])
        // send data image and get url
        photosProvider.request(.bindPhotoToGallery(url: url)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode([UserPhotos].self, from: response.data)
                    if let selfId = SuperUser.shared.superUser?.id {
                        // self.loadUserPhotos(userID: selfId) // reload photos
                        self.output.userPhotosUpload(selfId)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    // USER PHOTOS Request
    func loadUserPhotos(userID: Int, photoLimit: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let photosProvider = MoyaProvider<PhotosService>(plugins: [authPlagin,debugPlagin])
        
        photosProvider.request(.userPhotos(id: userID, limit: photoLimit)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userPhotosUrl = try JSONDecoder().decode([UserPhotos].self, from: response.data)
                    self.output.userPhotoUrlGeted(data: userPhotosUrl)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadUserData(userID: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        // FULL DATA Request
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.userFullData(id: userID)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                    SuperUser.shared.save(user: userFullData)
                    self.output.userDataGeted(data: userFullData)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadCounterForPhoto(_ idPhoto: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        //request first
        var likes: [PhotoLike] = []
        var comments: [PhotoComment] = []
        dispatchGroup.enter()
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.userPhotoComments(id: idPhoto)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([PhotoComment].self, from: response.data)
                    comments = serverResponse
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // request second
        dispatchGroup.enter()
        usersProvider.request(.userPhotosLikes(id: idPhoto)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([PhotoLike].self, from: response.data)
                    likes = serverResponse
                    self.dispatchGroup.leave()
                } catch let errorC { // TODO: Переписать обработку ошибки сервера, тут не верно! см. RegistrationInteractor
                    if let errorLynkYou = errorC as? ErrorResponse {
                        do {
                            let bodyError = try JSONSerialization.data(withJSONObject: errorLynkYou, options: [])
                            print(bodyError)
                            self.dispatchGroup.leave()
                        } catch {
                            print(error)
                            self.dispatchGroup.leave()
                        }
                    }
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        dispatchGroup.notify(queue: .main) {
            self.output.likesAndCommentGeted(likes, comments)
        }
    }
    
    func deletePhoto(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let photosProvider = MoyaProvider<PhotosService>(plugins: [authPlagin])
        
        photosProvider.request(.deletePhoto(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if response["done"] == true { // photo was deleted
                        self.output.photoWasDeleted(id: id)
                    }
                } catch {
                    print(error)
                    print("Ошибка удаления фотографии")
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func setNewAvatar(_ imageId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let photosProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        photosProvider.request(.setAvatar(photoId: imageId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if response["avatar"] == true {
                        self.updateUserData()
                    } else {
                        print("ошибка обновления аватара")
                        self.output.showAlert(title: "Ошибка", msg: "Не удалось обновить аватар")
                    }
                } catch {
                    print(error)
                    print("Ошибка установки аватарки")
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func updateUserData() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        guard let selfId = SuperUser.shared.superUser?.id else { return }
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        usersProvider.request(.userFullData(id: selfId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                    SuperUser.shared.save(user: userFullData)
                    self.output.updateHeader()
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func deletePost(_ postId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        usersProvider.request(.deletePost(postId: postId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(BlogPostDetail.self, from: response.data)
                    print(serverResponse)
                    self.output.postWasDeleted()
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
