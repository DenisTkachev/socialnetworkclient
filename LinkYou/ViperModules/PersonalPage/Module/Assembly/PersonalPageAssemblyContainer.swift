//
//  PersonalPagePersonalPageAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 21/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class PersonalPageAssemblyContainer: Assembly {
    
	func assemble(container: Container) {
		container.register(PersonalPageInteractor.self) { (r, presenter: PersonalPagePresenter) in
			let interactor = PersonalPageInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(PersonalPageRouter.self) { (r, viewController: PersonalPageViewController) in
			let router = PersonalPageRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(PersonalPagePresenter.self) { (r, viewController: PersonalPageViewController) in
			let presenter = PersonalPagePresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(PersonalPageInteractor.self, argument: presenter)
			presenter.router = r.resolve(PersonalPageRouter.self, argument: viewController)
            presenter.superUser = SuperUser.shared.superUser
			return presenter
		}

		container.storyboardInitCompleted(PersonalPageViewController.self) { r, viewController in
			viewController.output = r.resolve(PersonalPagePresenter.self, argument: viewController)
		}
	}

}
