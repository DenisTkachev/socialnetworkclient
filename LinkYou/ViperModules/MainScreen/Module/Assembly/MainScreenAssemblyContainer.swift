//
//  MainScreenMainScreenAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class MainScreenAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(MainScreenInteractor.self) { (r, presenter: MainScreenPresenter) in
			let interactor = MainScreenInteractor()
			interactor.output = presenter
			return interactor
		}
        
		container.register(MainScreenRouter.self) { (r, viewController: MainScreenViewController) in
            let router = MainScreenRouter()
            router.transitionHandler = viewController
			return router
		}
        
		container.register(MainScreenPresenter.self) { (r, viewController: MainScreenViewController) in
			let presenter = MainScreenPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(MainScreenInteractor.self, argument: presenter)
			presenter.router = r.resolve(MainScreenRouter.self, argument: viewController)
            presenter.notificationService = NotificationManager.init(vc: viewController)
            presenter.userStatusCoordinator = container.resolve(UserStatusCoordinatorInteractorInput.self)!
			return presenter
		}

		container.storyboardInitCompleted(MainScreenViewController.self) { r, viewController in
			viewController.output = r.resolve(MainScreenPresenter.self, argument: viewController)

		}
	}

}
