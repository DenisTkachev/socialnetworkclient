//
//  MainScreenMainScreenInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import Foundation

protocol MainScreenInteractorInput: class {
    func loadData(page: Int)
    func getAutocomleteDictionaries()
    func startSocket()
}
