//
//  MainScreenMainScreenInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import Foundation

protocol MainScreenInteractorOutput: class {
    func returnData(_ usersDaily: [UsersDaily], headers: [AnyHashable : Any])
    func showAlert(title: String, msg: String)
    func returnAutocompleteData(languages: [AutocompleteModel], religions: [AutocompleteModel], nationality: [AutocompleteModel], professions: [AutocompleteModel], cities: [AutocompleteModel], age: [AutocompleteModel], profarea: [AutocompleteModel], goals: [AutocompleteModel])
}
