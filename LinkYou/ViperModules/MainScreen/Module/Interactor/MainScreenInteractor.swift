//
//  MainScreenMainScreenInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//
import Moya

final class MainScreenInteractor: MainScreenInteractorInput {
    
    weak var output: MainScreenInteractorOutput!
    
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    
    let dispatchGroup = DispatchGroup()
    var languages = [AutocompleteModel]()
    var religions = [AutocompleteModel]()
    var nationality = [AutocompleteModel]()
    var professions = [AutocompleteModel]()
    var cities = [AutocompleteModel]()
    var age = [AutocompleteModel]()
    var profarea = [AutocompleteModel]()
    var goals = [AutocompleteModel]()
    
    let manager = SocketConnection.default  // singleTone
    
}

extension MainScreenInteractor {
    
    func startSocket() {
        manager.connect() // активация сокета
        manager.addHandlers()
    }
    
    func loadData(page: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }

        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.daily(page: page)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let usersDaily = try JSONDecoder().decode([UsersDaily].self, from: response.data)
                    if let httpResponse = response.response {
                        let headers = httpResponse.allHeaderFields
                        self.output.returnData(usersDaily, headers: headers)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
        
        usersProvider.request(.currentUser) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(CurrentUser.self, from: response.data)
                    SuperUser.shared.saveCurrentUser(currentUser: response)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadCitiesLists() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }

        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.listsCities) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode([CitiesList].self, from: response.data)
                    // self.output.listsCitiesGetted(listsCities)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getAutocomleteDictionaries() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<AutocompleteService>(plugins: [authPlagin])
        // languages
        dispatchGroup.enter()
        usersProvider.request(.getLanguages) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.languages = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        //religions
        dispatchGroup.enter()
        usersProvider.request(.getReligions) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.religions = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // nationality
        dispatchGroup.enter()
        usersProvider.request(.getNationality) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.nationality = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // professions
        dispatchGroup.enter()
        usersProvider.request(.getProfessions) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.professions = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // profArea
        dispatchGroup.enter()
        usersProvider.request(.getProfarea) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.profarea = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // goals
        dispatchGroup.enter()
        usersProvider.request(.getGoals) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.goals = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // cities
        dispatchGroup.enter()
        usersProvider.request(.getCities) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.cities = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        // age
        dispatchGroup.enter()
        usersProvider.request(.age) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.age = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            self.output.returnAutocompleteData(languages: self.languages, religions: self.religions, nationality: self.nationality, professions: self.professions, cities: self.cities, age: self.age, profarea: self.profarea, goals: self.goals)
        }
        
        // get price rates
        
        
        let paymentProvider = MoyaProvider<PaymentService>(plugins: [authPlagin, debugPlagin])
        
        paymentProvider.request(.priceRate) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(Payment.self, from: response.data)
                    PersonalConstants.priceRates = response
                    print(response)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
        
    }
    
    
}

