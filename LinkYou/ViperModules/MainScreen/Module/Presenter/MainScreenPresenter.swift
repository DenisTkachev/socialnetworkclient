//
//  MainScreenMainScreenPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

final class MainScreenPresenter: MainScreenViewOutput {

    // MARK: -
    // MARK: Properties
    
    weak var view: MainScreenViewInput!
    weak var userStatusCoordinator: UserStatusCoordinatorInteractorInput!
    var interactor: MainScreenInteractorInput!
    var router: MainScreenRouterInput!
    var notificationService: NotificationManager!
    
    var mainData: [UsersDaily]?
    var headers = [XPaginationHeaders:Int]()
    var isCityFilterActive = false
    var cityFilterPredicate = "Все города"
    // MARK: -
    // MARK: MainScreenViewOutput
    func viewIsReady(page: Int) {
        DeviceCurrent.current.runGuestMode()
        interactor.loadData(page: page)
//        view.isHideTableView(show: true)
        interactor.getAutocomleteDictionaries()
        interactor.startSocket()
    }

    func checkUserStatus() {
        if (self.userStatusCoordinator.checkUserStatus() == User.RoleStatus.normal) || (self.userStatusCoordinator.checkUserStatus() == User.RoleStatus.premium) {
            print("статус: Оке")
        } else if self.userStatusCoordinator.checkUserStatus() == User.RoleStatus.fee {
            print("статус: fee")
            view.showPaymentPage() // сбор за регистрацию
        } else if self.userStatusCoordinator.checkUserStatus() == User.RoleStatus.edit {
            print("статус: edit")
        } else if self.userStatusCoordinator.checkUserStatus() == User.RoleStatus.moderation {
            print("статус: moderation")
        } else if self.userStatusCoordinator.checkUserStatus() == User.RoleStatus.banned {
            print("статус: banned")
        } else if self.userStatusCoordinator.checkUserStatus() == User.RoleStatus.disabled {
            print("статус: disabled")
        } else if self.userStatusCoordinator.checkUserStatus() == User.RoleStatus.emailConfirm {
            print("статус: email_confirm")
//          router.presentPaymentPage(paymentTypeId: 7, entityId: 11) // Оплата за регистрацию
            let _ = InformationPopUpViewController(type: .emailConfirm, delegate: nil)
            
//            view.showPaymentPage() // для теста
        }
    }
    
    func showUserPage(user: UsersDaily, userOfDay: [UsersDaily]) {
        if let mainData = self.mainData {
            router.presentUserPage(user: user, allUsers: mainData)
        }
    }
    
    func prepareCitiesList(mainData: [UsersDaily]) -> [String] {
        var cities: [String] = []
        mainData.forEach { (user) in
            if let city = user.location?.city_name {
                cities.append(city)
            }
        }
        var unuqueCities = Array(Set(cities))
        
        unuqueCities.sort { (lhs, _) -> Bool in
            if lhs == "Москва" {
                return true
            }
            if lhs == "Санкт-Петербург" {
                return true
            }
            return false
        }
        
        return unuqueCities
    }
    
    func needFilterMainData(predicate: String) {
        cityFilterPredicate = predicate
        if predicate == "Все города" {
            if let mainData = mainData {
                view.updateResults(mainData)
                isCityFilterActive = false
            }
        } else {
            let result = applyCityFilter()
            isCityFilterActive = true
            view.updateResults(result)
        }
    }
    
    private func applyCityFilter() -> [UsersDaily] {
        let result = mainData?.filter({ (user) -> Bool in
            if let location = user.location {
                return location.city_name == cityFilterPredicate
            }
            return false
        })
        guard let results = result else { return [] }
        return results
    }
    
    func presentPaymentView(paymentTypeId: Int, entityId: Int) {
        router.presentPaymentPage(paymentTypeId: paymentTypeId, entityId: entityId)
    }
    
    
    
}

// MARK: -
// MARK: MainScreenInteractorOutput
extension MainScreenPresenter: MainScreenInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func returnData(_ usersDaily: [UsersDaily], headers: [AnyHashable : Any]) {
        self.mainData = usersDaily
        self.headers = HttpHeaders.prepareHeaders(headers)
//        view.isHideTableView(show: false)
        if isCityFilterActive {
            view.updateResults(applyCityFilter())
        } else {
            view.presentData(usersDaily, headers: HttpHeaders.prepareHeaders(headers))
            print("Всего юзеров в презентере: \(usersDaily.count)")
        }
    }
    
    func returnAutocompleteData(languages: [AutocompleteModel], religions: [AutocompleteModel], nationality: [AutocompleteModel], professions: [AutocompleteModel], cities: [AutocompleteModel], age: [AutocompleteModel], profarea: [AutocompleteModel], goals: [AutocompleteModel]) {
        
        PersonalConstants.allLanguages = languages
        PersonalConstants.allReligions = religions
        PersonalConstants.allNationality = nationality
        PersonalConstants.allProfessions = professions
        PersonalConstants.allCities = cities
        PersonalConstants.age = age
        PersonalConstants.profarea = profarea
        PersonalConstants.goals = goals
    }
}

// MARK: -
// MARK: MainScreenModuleInput
extension MainScreenPresenter: MainScreenModuleInput {

}
