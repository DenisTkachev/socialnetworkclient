//
//  MainScreenSectionController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

//import IGListKit
//import UIKit
//import SDWebImage
//
//class MainScreenSectionController: ListSectionController {
//    
//    private var feedItem: UsersDaily!
//    weak var delegateCell: SelectedCellType!
//    weak var headerCellDelegate: HeaderCellActionType!
//    var selectedCity = "Город"
//    
//    override init() {
//        super.init()
//        self.supplementaryViewSource = self
//    }
//    
//    override func numberOfItems() -> Int {
//        return 1
//    }
//    
//    override func didUpdate(to object: Any) {
//        feedItem = object as? UsersDaily
//    }
//    
//    override func sizeForItem(at index: Int) -> CGSize {
//        return CGSize(width: collectionContext!.containerSize.width, height: 90)
//    }
//    
//    override func didSelectItem(at index: Int) {
//        delegateCell.prepareForShowUserPage(user: feedItem)
//    }
//    
//    override func cellForItem(at index: Int) -> UICollectionViewCell {
//        let cell = collectionContext?.dequeueReusableCell(withNibName: "MainScreenCollectionViewCell", bundle: nil, for: self, at: index) as! MainScreenCollectionViewCell
//        
//        cell.setDataIntoCell(data: feedItem)
//
//        
//        guard let urlPath = feedItem.avatar else { return cell }
//        guard let fullUrlPath = urlPath.small ?? urlPath.origin else { return cell }
//        
//        cell.userPicImageView.sd_setImage(with: URL(string: "https:\(fullUrlPath)")) { (image, error, cache, url) in
//            cell.userPicImageView.setRoundedImage(image)
//        }
//        
//        return cell
//    }
//}

//extension MainScreenSectionController: ListSupplementaryViewSource {
//
//    func supportedElementKinds() -> [String] {
//        return [UICollectionElementKindSectionHeader]
//    }
//
//    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
//        switch elementKind {
//        case UICollectionElementKindSectionHeader:
//            return userHeaderView(atIndex: index)
//        default:
//            fatalError()
//        }
//    }
//
//    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
//        if self.section == 0 {
//            return CGSize(width: collectionContext!.containerSize.width, height: 50)
//        } else {
//            return CGSize(width: collectionContext!.containerSize.width, height: 0)
//        }
//    }
//
//    // MARK: Private
//    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
//        guard let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
//                                                                             for: self,
//                                                                             nibName: "HearderMainCollectionViewCell",
//                                                                             bundle: nil,
//                                                                             at: index) as? HearderMainCollectionViewCell else {
//                                                                                fatalError()
//        }
//
//        view.cityLabel.setTitle(selectedCity, for: .normal)
//        view.backgroundColor = .clear
//        view.delegate = headerCellDelegate
//        return view
//    }
//
//}
