
//
//  HearderMainCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class HearderMainCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var leftBtnView: UIView!
    @IBOutlet weak var rightBtnView: UIView!
    @IBOutlet weak var rowSelectImageView: UIImageView!
    @IBOutlet weak var premiumBtn: UIButton!
    @IBOutlet weak var cityLabel: UIButton!
    
    @IBOutlet weak var textFiled: UITextField!
    
    weak var delegate: HeaderCellActionType!
    var isSelectedBtn = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        premiumBtn.layer.cornerRadius = rightBtnView.frame.height / 2
        premiumBtn.clipsToBounds = true
        
        leftBtnView.layer.cornerRadius = leftBtnView.frame.height / 2
        leftBtnView.layer.borderWidth = 1
        leftBtnView.layer.borderColor = UIColor.paleGrey.cgColor
        leftBtnView.clipsToBounds = true
        
    }
    
    override func prepareForReuse() {
//        userPicImageView.image = nil
        //userPicImageView.sd_cancelCurrentImageLoad()
    }

    @IBAction func selectTownBtn(_ sender: UIButton) {
        isSelectedBtn = !isSelectedBtn
        if isSelectedBtn {
            rowSelectImageView.image = #imageLiteral(resourceName: "row-selector-open")
        } else {
            rowSelectImageView.image = #imageLiteral(resourceName: "row-selector-close")
        }
        delegate.pressBtn(sender)
    }
    
    @IBAction func rightBtnPress(_ sender: UIButton) {
        let _ = PopUpPaymentVCStep1.init(tizerType: TizerType.toMainPage)
    }
    
}
