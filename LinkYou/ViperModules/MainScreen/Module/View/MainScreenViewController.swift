//
//  MainScreenMainScreenViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import UIKit
import PKHUD
import OneSignal

protocol SelectedCellType: class {
    func prepareForShowUserPage(user: UsersDaily)
}

protocol HeaderCellActionType: class {
    func pressBtn(_ sender: UIButton)
}

final class MainScreenViewController: UIViewController, SelectedCellType, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: -
    // MARK: Properties
    var output: MainScreenViewOutput!
    var mainData: [UsersDaily] = []
    var selectedUser: UsersDaily?
    
    var cityFilterFlag = false
    var selectedCity = "Все города"
    var citiesList: [String] = []
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.lightGray
        
        return refreshControl
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var slideUpView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var paginationCurrent = 1
    var paginationIsEnd = 0
    var paginationLimit = 0
    var paginationTotal = 0
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AppBadgesManager().updateTabbarBadges(vc: self.tabBarController)
        HUD.show(.progress, onView: self.view)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "MainScreenCollectionViewCell", bundle: nil), forCellReuseIdentifier: "MainScreenCollectionViewCell")
        pickerView.delegate = self
        pickerView.dataSource = self
        
        UIApplication.shared.statusBarStyle = .lightContent // TODO: заменить на актуальное
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        slideUpViewSetup()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        pushNotificationRequest()
        
    }
    
    private func pushNotificationRequest() {
//        DeviceCurrent.pushNotification.notificationRequest() // выключил запрос так onesignal может запрашивать тоже самое! надо проверить
        
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
    }
    
    func slideUpViewSetup() {
        slideUpView.frame = CGRect(x: 0, y: self.view.frame.maxY, width: UIScreen.main.bounds.width, height: 200)
        slideUpView.layer.cornerRadius = 10
        slideUpView.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if mainData.count == 0 {
            output.viewIsReady(page: paginationCurrent)
        }
    }
    
    @IBAction func searchBtnPressed(_ sender: UIBarButtonItem) {
        self.tabBarController!.selectedIndex = 2
    }
    
    func prepareForShowUserPage(user: UsersDaily) {
        output.showUserPage(user: user, userOfDay: mainData)
    }
    
    @objc private func handleRefresh(_ sender: Any) {
        resetSettings()
    }
    
    @IBAction func citySearchCancelBtn(_ sender: UIButton) {
        output.needFilterMainData(predicate: "Все города")
        selectedCity = "Все города"
        cityFilterFlag = false
        showHidePicker()
    }
    @IBAction func citySearchApplyBtn(_ sender: UIButton) {
        selectedCity = citiesList[pickerView.selectedRow(inComponent: 0)]
        output.needFilterMainData(predicate: selectedCity)
        cityFilterFlag = false
        showHidePicker()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 116
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 50
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainScreenCollectionViewCell", for: indexPath) as? MainScreenCollectionViewCell
        cell!.setDataIntoCell(data: mainData[indexPath.row])
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainData.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = Bundle.main.loadNibNamed("HearderMainCollectionViewCell", owner: self, options: nil)?.first as! HearderMainCollectionViewCell
            
        view.cityLabel.setTitle(selectedCity, for: .normal)
        view.delegate = self
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        output.showUserPage(user: mainData[indexPath.row], userOfDay: mainData)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
        if indexPath.row == mainData.count - 5, selectedCity == "Все города" {
            if paginationCurrent < paginationTotal {
                output.viewIsReady(page: paginationCurrent + 1)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    private func resetSettings() {
        paginationCurrent = 1
        paginationIsEnd = 0
        paginationLimit = 0
        paginationTotal = 0
        mainData.removeAll()
        tableView.reloadData()
        tableView.beginUpdates()
        output.viewIsReady(page: paginationCurrent)
        tableView.endUpdates()
    }
    
}

// MARK: -
// MARK: MyLikesViewInput
extension MainScreenViewController: MainScreenViewInput {
    
    func showPaymentPage() {
        let _ = PopUpPaymentVCStep1.init(tizerType: .registrationTax)
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
            self.refreshControl.endRefreshing()
        }
    }
    
    func updateResults(_ usersDaily: [UsersDaily]) {
        mainData = usersDaily
        tableView.reloadData()
        HUD.hide()
        self.refreshControl.endRefreshing()
    }
    
    func presentData(_ usersDaily: [UsersDaily], headers: [XPaginationHeaders : Int]) {
        guard let current = headers[XPaginationHeaders.XPaginationCurrent],
            let isEnd = headers[XPaginationHeaders.XPaginationIsEnd], let limit = headers[XPaginationHeaders.XPaginationLimit],
            let total = headers[XPaginationHeaders.XPaginationTotal] else { return }
        paginationCurrent = current
        paginationIsEnd = isEnd
        paginationLimit = limit
        paginationTotal = total
        
        usersDaily.forEach { (user) in
            mainData.append(user)
        }
        print("Всего юзеров на вью: \(mainData.count)")
        citiesList = output.prepareCitiesList(mainData: mainData)
        tableView.reloadData()
        self.refreshControl.endRefreshing()
        HUD.hide()
        output.checkUserStatus()
    }

}

extension MainScreenViewController: HeaderCellActionType {
    
    func pressBtn(_ sender: UIButton) {
        cityFilterFlag = !cityFilterFlag
        showCitiesPicker()
    }
    
    func showCitiesPicker() {
        pickerView.reloadAllComponents()
        showHidePicker()
    }
    
    func showHidePicker() {
        if cityFilterFlag {
            self.view.addSubview(slideUpView)
            UIView.animate(withDuration: 0.3) {
//                self.slideUpView.frame.origin.y = self.view.frame.maxY - self.slideUpView.layer.bounds.height - 49
                let tabBarHeight = self.tabBarController?.tabBar.bounds.maxY ?? 0
                let navigationBar = self.navigationController?.navigationBar.bounds.height ?? 0
                self.slideUpView.frame.origin.y = UIScreen.main.bounds.height - self.slideUpView.bounds.height - tabBarHeight - navigationBar
            }
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.slideUpView.frame.origin.y = self.view.frame.maxY
            }) { (_) in
                self.slideUpView.removeFromSuperview()
            }
        }
    }
}

extension MainScreenViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return citiesList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return citiesList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCity = citiesList[row]
    }
}

extension MainScreenViewController: PaymentPageVCType {

    
    func showPaymentView(paymentTypeId: Int, entityId: Int) {
        output.presentPaymentView(paymentTypeId: paymentTypeId, entityId: entityId)
    }
    
}
