//
//  MainScreenMainScreenViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

protocol MainScreenViewInput: class {
    /// - author: KlenMarket
    func presentData(_ usersDaily: [UsersDaily], headers: [XPaginationHeaders : Int])
    func updateResults(_ usersDaily: [UsersDaily])
    func showAlert(title: String, msg: String)
    func showPaymentPage()
}
