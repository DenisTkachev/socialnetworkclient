//
//  MainScreenMainScreenViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

protocol MainScreenViewOutput: class {
    /// - author: KlenMarket
    func viewIsReady(page: Int)
    func showUserPage(user: UsersDaily, userOfDay: [UsersDaily])
    func needFilterMainData(predicate: String)
    func prepareCitiesList(mainData: [UsersDaily]) -> [String]
    func presentPaymentView(paymentTypeId: Int, entityId: Int)
    func checkUserStatus()
}
