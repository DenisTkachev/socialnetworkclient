//
//  MainScreenMainScreenRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import UIKit

protocol MainScreenRouterInput: class {
    func presentUserPage(user: UsersDaily, allUsers: [UsersDaily])
    func presentPaymentPage(paymentTypeId: Int, entityId: Int)
}
