//
//  MainScreenMainScreenRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import LightRoute

final class MainScreenRouter: MainScreenRouterInput {
    
    enum StorybordsID: String {
        case userPage = "UserpageSB"
        case paymentPage = "PaymentPageSB"
    }
    
    weak var transitionHandler: TransitionHandler!
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    // MARK: Routing
    
    func presentUserPage(user: UsersDaily, allUsers: [UsersDaily]) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .userPage), to: UserpageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
             moduleInput in
                moduleInput.configure(with: user, allUser: allUsers)
            }
//            .perform()
    }
    
    func presentPaymentPage(paymentTypeId: Int, entityId: Int) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .paymentPage), to: PaymentPageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.getPaymentUrl(paymentTypeId: paymentTypeId, entityId: entityId)
        }
    }

}
