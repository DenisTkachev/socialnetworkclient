//
//  ApplicationAssembly.swift
//  LinkYou
//
//  Created by Denis Tkachev on 19.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class ApplicationAssembly {
    
    //Use default dependency
    class var assembler: Assembler {
        return Assembler([
            RegistrationAssemblyContainer(),
            MainScreenAssemblyContainer(),
//          MyLikesAssemblyContainer(), // was removed
            MessagesAssemblyContainer(),
            MenuTabAssemblyContainer(),
            ReviewTabAssemblyContainer(),            
            SearchAssemblyContainer(),
            UserpageAssemblyContainer(),
            PersonalPageAssemblyContainer(),
            MyBlogAssemblyContainer(),
            MyBlogViewPostAssemblyContainer(),
            SettingsScreenAssemblyContainer(),
            EditorRegistrationAssemblyContainer(),
            FavoritesListAssemblyContainer(),
            MyBlogAddPostAssemblyContainer(),
            GiftsGalleryAssemblyContainer(),
            GiftSendPageAssemblyContainer(),
            Top100PageAssemblyContainer(),
            AboutUsPageAssemblyContainer(),
            OurBlogAssemblyContainer(),
            UsersBlogsAssemblyContainer(),
            UsersMatchedAssemblyContainer(),
            DialogsAssemblyContainer(),
            PhotoAlbumAssemblyContainer(),
            LinkYouLikeCardsAssemblyContainer(),
            MyReviewsAssemblyContainer(),
            PaymentPageAssemblyContainer(),
            
            UserStatusCoordinatorAssemblyContainer(),
            ])
    }
    
    var assembler: Assembler
    
    //If you want use custom Assembler
    init(with assemblies: [Assembly]) {
        assembler = Assembler(assemblies)
    }
    
}

//Inject dependency in Main Storyboard
extension SwinjectStoryboard {
    
    @objc class func setup() {
        defaultContainer = ApplicationAssembly.assembler.resolver as! Container
    }
    
}
