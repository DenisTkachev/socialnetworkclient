//
//  UserStatusCoordinatorAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 12/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Swinject
import SwinjectStoryboard


final class UserStatusCoordinatorAssemblyContainer: Assembly {
    
    func assemble(container: Container) {
        container.register(UserStatusCoordinatorInteractorInput.self) { _ in UserStatusCoordinator() }.inObjectScope(.container) // singletone
        
    }
}

protocol UserStatusCoordinatorInteractorInput: class {
    func checkUserStatus() -> User.RoleStatus
}

class UserStatusCoordinator: UserStatusCoordinatorInteractorInput {
    
    func checkUserStatus() -> User.RoleStatus {
    
        guard let userRole = SuperUser.shared.superUser?.roleStatus else { return User.RoleStatus.disabled }
        print(userRole)
        switch userRole {
        case User.RoleStatus.normal.rawValue:
            return User.RoleStatus.normal
        case User.RoleStatus.emailConfirm.rawValue:
            return User.RoleStatus.emailConfirm
        default:
            print("UserStatusCoordinator отредактируй меня");
            return User.RoleStatus.disabled
        }
    }
    
}

