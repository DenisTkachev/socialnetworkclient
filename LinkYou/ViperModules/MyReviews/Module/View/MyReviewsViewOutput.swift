//
//  MyReviewsMyReviewsViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

protocol MyReviewsViewOutput: class {
  /// - author: KlenMarket
    func viewIsReady()
    func showUserPage(user: Guest)
    func requestNextPage()

}
