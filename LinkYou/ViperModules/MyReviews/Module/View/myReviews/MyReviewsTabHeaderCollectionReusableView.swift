//
//  MyReviewsTabHeaderCollectionReusableView.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SwiftDate

class MyReviewsTabHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var headerNameLabel: UILabel!
    @IBOutlet weak var headerBtn: RoundButtonCustom!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setupCell(date: Date) {
        
        if date.compare(.isToday) {
            setHeader(str: "Сегодня")
        } else if date.compare(.isYesterday) {
            setHeader(str: "Вчера")
        } else if date.compare(.isLastYear) {
             setHeader(str: date.toFormat("dd MMMM yyyy"))
        } else {
            setHeader(str: date.toFormat("dd MMMM"))
        }
        
    }
    
    private func setHeader(str: String) {
        headerNameLabel.text = str
    }
    
    @IBAction func pressHeaderBtn(_ sender: RoundButtonCustom) {
        // delegate
    }
}
