//
//  MyReviewsCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 27.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MyReviewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userPic: MyAvatarView!
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var prefessionUserLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var photoCount: UILabel!
    @IBOutlet weak var userCityLabel: UILabel!
    
    override func prepareForReuse() {
        userPic.setStartSettings(hidden: true)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userPic.layer.cornerRadius = userPic.frame.height / 2
        userPic.clipsToBounds = true
    }
    
    func setupCell(cellData: Guest) {
        guard let data = cellData.user else { return }
        guard let name = data.name, let age = data.birthday?.age, let dateVisit = cellData.datetime, let profession = data.job?.profession, let photoCount = data.photos_count, let city = data.location?.city_name else { return }
        nameUserLabel.text = name + ", " + age.toString()
        timeLabel.text = dateVisit.toString()
        prefessionUserLabel.text = profession
        self.photoCount.text = photoCount.toString()
        userCityLabel.text = city
        
        guard let avatarStr = data.avatar?.src?.square else { return }
        userPic.userAvatar.sd_setImage(with: URL(string: avatarStr)) { (image, error, cache, url) in
            self.userPic.userAvatar.toRoundedImage()
            self.userPic.addTunning()
            
            if let premium = data.is_premium {
                self.userPic.enablePremiumStatus(on: premium)
            }
            if let online = data.is_online {
                self.userPic.enableOnlineStatus(on: online)
            }
            if let vip = data.is_vip {
                self.userPic.enableGoldRing(on: vip)
            }
        }
    }
}
