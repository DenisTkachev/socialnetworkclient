//
//  MyReviewsMyReviewsViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

protocol MyReviewsViewInput: class {
  /// - author: KlenMarket
    func presentData(data: [[Guest]])
    func showAlert(title: String, msg: String)
    func showNoDataView(condition: Bool)
}
