//
//  MyReviewsMyReviewsViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

protocol SelectedMyReviewsCellType: class {
    func prepareForShowUserPage(user: Guest)
}

final class MyReviewsViewController: UIViewController, SelectedMyReviewsCellType {

	// MARK: -
	// MARK: Properties
	var output: MyReviewsViewOutput!
    var mainData: [[Guest]] = []
    let noDataView = NoDataView()
    
    @IBOutlet weak var reviewCollectionView: UICollectionView!

    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.lightGray
        
        return refreshControl
    }()
    
	// MARK: -
	// MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigatioBar()
        
        output.viewIsReady()
        reviewCollectionView.dataSource = self
        reviewCollectionView.delegate = self
        
        reviewCollectionView.register(UINib(nibName: "MyReviewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MyReviewsCollectionViewCell")
        reviewCollectionView.register(UINib(nibName: "MyReviewsTabHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "MyReviewsTabHeaderCollectionReusableView")
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            reviewCollectionView.refreshControl = refreshControl
        } else {
            reviewCollectionView.addSubview(refreshControl)
        }
        setupNoDataView()
    }
    
    private func setupNavigatioBar() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = reviewCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemWidth = view.bounds.width
            let itemHeight = CGFloat(108)
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
            layout.invalidateLayout()
            layout.headerReferenceSize = CGSize(width: view.frame.size.width, height: 50.0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        output.viewIsReady()
        HUD.show(.progress, onView: reviewCollectionView.superview)
        
    }
    
    func prepareForShowUserPage(user: Guest) {
        output.showUserPage(user: user)
    }
    
    @objc private func handleRefresh(_ sender: Any) {
        output.viewIsReady()
    }
    
    private func setupNoDataView() {
        noDataView.isHidden = true
        noDataView.frame = self.view.frame
        noDataView.delegate = self
        noDataView.setText(title: "Просмотров нет", body: "Ещё никто не посетил вашу страничку.", buttonText: nil)
        
        self.view.addSubview(noDataView)
    }
    
    func showNoDataView(condition: Bool) {
        noDataView.setHidden(condition: !condition)
        HUD.hide()
    }
    
}

// MARK: -
// MARK: MyReviewsViewInput
extension MyReviewsViewController: MyReviewsViewInput {

    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
            self.refreshControl.endRefreshing()
        }
    }
    
    func presentData(data: [[Guest]]) {
        mainData = data
        reviewCollectionView.reloadData()
        HUD.hide()
        refreshControl.endRefreshing()
    }

}

extension MyReviewsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return mainData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainData[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = reviewCollectionView.dequeueReusableCell(withReuseIdentifier: "MyReviewsCollectionViewCell", for: indexPath) as? MyReviewsCollectionViewCell else { return UICollectionViewCell() }
        cell.setupCell(cellData: mainData[indexPath.section][indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output.showUserPage(user: mainData[indexPath.section][indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == mainData.count - 1 {
            output.requestNextPage()
        }
    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    //        return CGSize(width:collectionView.frame.size.width, height: 30.0)
    //    }
    
    // Section Header View
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        let sectionHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MyReviewsTabHeaderCollectionReusableView", for: indexPath) as! MyReviewsTabHeaderCollectionReusableView
        if let sectionDate = mainData[indexPath.section].first?.datetime {
            sectionHeaderView.setupCell(date: sectionDate)
        }
        if indexPath != [0,0] {
            sectionHeaderView.headerBtn.isHidden = true
        } else {
            sectionHeaderView.headerBtn.isHidden = false
        }
        //    let category = photoCategories[indexPath.section]
        //    sectionHeaderView.photoCategory = category
        
        return sectionHeaderView
    }
    
}

extension MyReviewsViewController: NoDataViewDelegate {
    
    func buttonWasPressed() {
        print("pressed btn! MyReviews")
    }
    
}
