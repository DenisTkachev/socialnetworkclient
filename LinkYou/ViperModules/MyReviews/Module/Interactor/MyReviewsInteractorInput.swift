//
//  MyReviewsMyReviewsInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Foundation

protocol MyReviewsInteractorInput: class {
    func loadData(page: Int)
}
