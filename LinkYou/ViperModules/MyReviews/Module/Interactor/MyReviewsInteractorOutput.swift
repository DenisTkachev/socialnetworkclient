//
//  MyReviewsMyReviewsInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Foundation

protocol MyReviewsInteractorOutput: class {
    func returnData(data: [Guest], headers: [AnyHashable : Any])
    func showAlert(title: String, msg: String)
}
