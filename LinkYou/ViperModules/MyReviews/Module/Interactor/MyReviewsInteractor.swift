//
//  MyReviewsMyReviewsInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//
import Moya

final class MyReviewsInteractor: MyReviewsInteractorInput {

  weak var output: MyReviewsInteractorOutput!

    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    func loadData(page: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
        let usersProvider = MoyaProvider<UsersService>(plugins: [debugPlagin, authPlagin])
        
        usersProvider.request(.userGuests(page: page)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userGuests = try JSONDecoder().decode([Guest].self, from: response.data)
                    if let httpResponse = response.response {
                        let headers = httpResponse.allHeaderFields
                        self.output.returnData(data: userGuests, headers: headers)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
}
