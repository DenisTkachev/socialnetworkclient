//
//  MyReviewsMyReviewsAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class MyReviewsAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(MyReviewsInteractor.self) { (r, presenter: MyReviewsPresenter) in
			let interactor = MyReviewsInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(MyReviewsRouter.self) { (r, viewController: MyReviewsViewController) in
			let router = MyReviewsRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(MyReviewsPresenter.self) { (r, viewController: MyReviewsViewController) in
			let presenter = MyReviewsPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(MyReviewsInteractor.self, argument: presenter)
			presenter.router = r.resolve(MyReviewsRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(MyReviewsViewController.self) { r, viewController in
			viewController.output = r.resolve(MyReviewsPresenter.self, argument: viewController)
		}
	}

}
