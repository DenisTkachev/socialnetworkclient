//
//  MyReviewsMyReviewsRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import LightRoute

final class MyReviewsRouter: MyReviewsRouterInput {

    weak var transitionHandler: TransitionHandler!
    
    enum StorybordsID: String {
        case userPage = "UserpageSB"
//        case searchPage = "SearchSB"
    }
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func presentUserPage(user: Guest, allUsers: [Guest]) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .userPage), to: UserpageModuleInput.self)
            
            // Set transition case.
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(with: user, allUser: allUsers)
        }
    }
    
    func switchTab() {
//        try! transitionHandler
//            .forStoryboard(factory: self.useFactory(storyboardID: .searchPage), to: SearchModuleInput.self)
//            .to(preferred: TransitionStyle.navigation(style: .push))
//            .perform()
    }

}
