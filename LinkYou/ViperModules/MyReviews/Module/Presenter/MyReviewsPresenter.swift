//
//  MyReviewsMyReviewsPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 18/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

final class MyReviewsPresenter: MyReviewsViewOutput {

    // MARK: -
    // MARK: Properties
    
    weak var view: MyReviewsViewInput!
    var interactor: MyReviewsInteractorInput!
    var router: MyReviewsRouterInput!
    
    // pagination
    var paginationCurrent = 1
    var paginationIsEnd = 0
    var paginationLimit = 0
    var paginationTotal = 0
    var users = [Guest]()
    
    // MARK: -
    // MARK: MyReviewsViewOutput
    func viewIsReady() {
        interactor.loadData(page: paginationCurrent)
    }
    
    func showUserPage(user: Guest) {
        router.presentUserPage(user: user, allUsers: self.users)
    }
    
    func requestNextPage() {
        if users.count != paginationTotal, paginationIsEnd != 1  {
            paginationCurrent += 1
            interactor.loadData(page: paginationCurrent)
        }
    }
    
}

// MARK: -
// MARK: MyReviewsInteractorOutput
extension MyReviewsPresenter: MyReviewsInteractorOutput {
    
    func returnData(data: [Guest], headers: [AnyHashable : Any]) {
        setHeaders(HttpHeaders.prepareHeaders(headers))
        self.users += data
        view.presentData(data: sortUsersByDate(users: self.users))
        if data.count == 0 {
            view.showNoDataView(condition: true)
        } else {
            view.showNoDataView(condition: false)
        }
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
}

// MARK: -
// MARK: MyReviewsModuleInput
extension MyReviewsPresenter: MyReviewsModuleInput {
    
    
}

extension MyReviewsPresenter {
    fileprivate func setHeaders(_ headers: [XPaginationHeaders : Int]) {
        guard let current = headers[XPaginationHeaders.XPaginationCurrent],
            let isEnd = headers[XPaginationHeaders.XPaginationIsEnd], let limit = headers[XPaginationHeaders.XPaginationLimit],
            let total = headers[XPaginationHeaders.XPaginationTotal] else { return }
        paginationCurrent = current
        paginationIsEnd = isEnd
        paginationLimit = limit
        paginationTotal = total
    }
    
    fileprivate func sortUsersByDate(users: [Guest]) -> [[Guest]] {
        var results = [[Guest]]()
        
        let groupedDialogs = Dictionary(grouping: users) { (element) -> String in
            if let sectionHeader = element.datetime {
                return sectionHeader.toFormat("yyyy-MM-dd")
            } else {
                return "" // исключительная ситуация, бэк всегда должен возвращать last_update для сообщения
            }
        }
        
        let sortedKeys = groupedDialogs.keys.sorted()
        sortedKeys.forEach { (key) in
            let values = groupedDialogs[key]
            results.append(values ?? [])
        }
        
        return results
    }
    
}
