//
//  MyBlogViewPostMyBlogViewPostViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

protocol PressHeaderBtnType: class {
    func showActionSheet()
    func showUserPage(id: Int)
    func setPostLike()
}

final class MyBlogViewPostViewController: UIViewController, PressHeaderBtnType {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextView: UIView!
    @IBOutlet weak var commentTextField: TextFieldCustom!
    @IBOutlet weak var sendMsgBtn: UIImageView!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    
    // MARK: -
    // MARK: Properties
    var output: MyBlogViewPostViewOutput!
    var post: BlogPostDetail?
    var comments: [BlogPostComments]?
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    
    private enum EventSheetType {
        case delete
        case claim
    }

    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "MyPostHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "MyPostHeaderTableViewCell")
        tableView.register(UINib(nibName: "MyBlogCommentTVCell", bundle: nil), forCellReuseIdentifier: "MyBlogCommentTVCell")
//        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        title = "Запись"
        output.viewIsReady()
        setupNotification()
        
    }
    
    func setupNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let sendMsgAction = UITapGestureRecognizer(target: self, action: #selector(tappedSendMsg(_:)))
        sendMsgAction.numberOfTapsRequired = 1
        sendMsgBtn.isUserInteractionEnabled = true
        sendMsgBtn.addGestureRecognizer(sendMsgAction)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !Reachability.isConnectedToNetwork() {
            showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
    }
    
    @objc func tappedSendMsg(_ recognizer: UITapGestureRecognizer) {
        // поискать среди родителей подписанного на протокол PhotoCommentType
        if let text = commentTextField.text, text.count > 0, let id = post?.id {
            output.sendBlogComment(postId: id, comment: text)
            commentTextField.text = nil
            commentTextField.endEditing(true)

        }
    }
    
    @objc func keyboardHandle(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            bottomViewConstraint.constant = isKeyboardShowing ? frame.height - 50 : 0
            
            UIView.animate(withDuration: 0, animations: {
                self.view.layoutIfNeeded()
            }) { (completed) in
                if isKeyboardShowing, let comments = self.comments, comments.count > 0 {
                    let indexPath = IndexPath(row: comments.count - 1, section: 1)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    func showLoader() {
//        HUD.show(.progress, onView: self.view) пролаг из-за быстрой загрузки
    }
    
    func hideLoader() {
        HUD.hide(animated: false)
    }
    
//    func setupMainData() {
//
//    }
    
    func showActionSheet() {
        guard let childViewControllers = parent?.children else { return }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for vc in childViewControllers {
            if vc is UserpageViewController {
                let claim = UIAlertAction(title: "Пожаловаться", style: .default) { action in
                    self.actionSheetHandler(type: .claim)
                }
                actionSheet.addAction(claim)
            }
            if vc is PersonalPageViewController {
                let delete = UIAlertAction(title: "Удалить запись", style: .destructive) { action in
                    self.actionSheetHandler(type: .delete)
                }
                actionSheet.addAction(delete)
            }
        }
        
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)

        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func actionSheetHandler(type: EventSheetType) {
        switch type {
        case .claim: break
        case .delete:
        if let id = post?.id {
            output.needDeletePost(postId: id)
            }
        }
    }
    
    func setPostLike() {
        guard let postId = post?.id else { return }
        output.setPostLike(postId: postId)
    }
    
    @objc func showUserPage(id: Int = 0) {
        if id != 0 {
            output.showUserPage(id)
        } else {
            let userId = post?.user?.id
            output.showUserPage(userId ?? 0)
        }
    }
}

// MARK: -
// MARK: MyBlogViewPostViewInput
extension MyBlogViewPostViewController: MyBlogViewPostViewInput {
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true) {
            HUD.hide()
//          self.refreshControl.endRefreshing()
        }
    }
    
    func setupInitialState(post: BlogPostDetail) {
        self.post = post
        HUD.hide()
    }
    
    func setComments(_ comments: [BlogPostComments]) {
        self.comments = comments
        tableView.reloadData()
        HUD.hide()
    }
    
    func postWasDeleted() {
       NotificationCenter.default.post(name: .deletedBlogPost, object: nil)
       self.navigationController?.popViewController(animated: true)
    }
    
}

extension MyBlogViewPostViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0, self.post != nil {
            return 1
        } else {
            if let comments = self.comments {
                return comments.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        commentTextField.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath == [0,0] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyPostHeaderTableViewCell", for: indexPath) as! MyPostHeaderTableViewCell
            cell.delegate = self
            if let postData = self.post {
                cell.setupHeader(post: postData)
            }
            
            if cell.userAvatarImageView.gestureRecognizers?.count ?? 0 == 0 {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showUserPage))
                cell.userAvatarImageView.addGestureRecognizer(tapGesture)
                cell.userAvatarImageView.isUserInteractionEnabled = true
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBlogCommentTVCell", for: indexPath) as! MyBlogCommentTVCell
            if let commentsData = comments {
                cell.setup(commentsData[indexPath.row])
                tableView.layoutIfNeeded()
                cell.delegate = self
            }
            
            return cell
        }
    }
    
    //MARK: Delete Cell
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Удалить"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let _ = tableView.cellForRow(at: indexPath) as? MyBlogCommentTVCell else { return false }
        if let commentOwner = comments?[indexPath.row].user?.id, let selfId = SuperUser.shared.superUser?.id, commentOwner == selfId {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            guard let commentId = comments?[indexPath.row].id, let post = post?.id else { return }
            output.deleteCommentFromPost(commentId: commentId, postId: post)
        }
    }
    
}


