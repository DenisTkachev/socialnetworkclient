//
//  MyBlogViewPostMyBlogViewPostViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MyBlogViewPostViewOutput: class {
    /// - author: KlenMarket
    func viewIsReady()
    func loadPostDetails(_ postId: Int)
    func needDeletePost(postId: Int)
    func showUserPage(_ userId: Int)
    func sendBlogComment(postId: Int, comment: String)
    func deleteCommentFromPost(commentId: Int, postId: Int)
    func setPostLike(postId: Int)
}
