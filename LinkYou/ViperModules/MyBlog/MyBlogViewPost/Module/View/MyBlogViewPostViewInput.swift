//
//  MyBlogViewPostMyBlogViewPostViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MyBlogViewPostViewInput: class {
    /// - author: KlenMarket
    func setupInitialState(post: BlogPostDetail)
    func showLoader()
    func hideLoader()
    func setComments(_ comments: [BlogPostComments])
    func postWasDeleted()
    func showAlert(title: String, msg: String)
}
