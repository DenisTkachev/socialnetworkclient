//
//  MyPostHeaderTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MyPostHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    
    @IBOutlet weak var textPostTextView: UITextView!
    @IBOutlet weak var postImageView: UIImageView!
    
    @IBOutlet weak var postLikesBtn: UIButton!
    @IBOutlet weak var postViewBtn: UIButton!
    @IBOutlet weak var postCommentBtn: UILabel!
    
    weak var delegate: PressHeaderBtnType!
    var selfUser = 0
    
    @IBAction func pressMoreBtn(_ sender: UIButton) {
        delegate.showActionSheet()
    }
    
    func setupHeader(post: BlogPostDetail) {
            guard let user = post.user, let id = user.id else { return }
            selfUser = id
            if let avatarUrl = user.avatar?.src?.square {
                let url = URL(string: avatarUrl)
                userAvatarImageView.sd_setImage(with: url, completed: nil)
                userAvatarImageView.clipsToBounds = true
                userAvatarImageView.layer.cornerRadius = userAvatarImageView.bounds.width / 2
            }
            
            if let name = user.name, let age = user.birthday?.age {
                userNameLabel.text = ("\(name), \(age.toString())")
            }
            
            postDateLabel.text = post.datetime?.tohhddYYYY()
            textPostTextView.text = post.text_short
            postLikesBtn.setTitle(String(format: NSLocalizedString("NumberOfLikes", comment: ""), post.likes_count ?? 0), for: .normal)
            postViewBtn.setTitle("\(post.views_count ?? 0)", for: .normal)
            
            if post.is_liked ?? false {
                postLikesBtn.setImage(#imageLiteral(resourceName: "heart-select-userPage"), for: .normal)
            } else {
                postLikesBtn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
            }
            
            if let urlString = post.photos?.first?.src {
                postImageView.sd_setImage(with: URL(string: urlString), placeholderImage: nil)
            }
            if let commentCount = post.comments_count {
                postCommentBtn.text = commentCount.toString()
            }

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
        // Configure the view for the selected state
    }
    
    @IBAction func pressSetLike(_ sender: UIButton) {
        delegate.setPostLike()
    }
    
}
