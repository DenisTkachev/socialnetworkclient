//
//  MyBlogCommentTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 03.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MyBlogCommentTVCell: UITableViewCell {

    @IBOutlet weak var userAvatarImageView: MyAvatarView!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var textPostTextView: UILabel!
    @IBOutlet weak var backGroundMessageView: UIView!
    weak var delegate: PressHeaderBtnType!
    var id = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        userAvatarImageView.clipsToBounds = true
        userAvatarImageView.layer.cornerRadius = userAvatarImageView.frame.height / 2
        
        userAvatarImageView.addTapGestureRecognizer {
            self.pressToAvatar()
        }
    }
    
    override func prepareForReuse() {
        userAvatarImageView.setStartSettings(hidden: true)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    private func pressToAvatar() {
        
        delegate.showUserPage(id: id)
    }
    
    func setup(_ commentsData: BlogPostComments) {
        
        if let avatarUrl = commentsData.user?.avatar?.src?.square, let data = commentsData.user, let userId = commentsData.user?.id {
            let url = URL(string: avatarUrl)
            self.id = userId
            userAvatarImageView.userAvatar.sd_setImage(with: url, completed: nil)
            self.userAvatarImageView.addTunning()
            
            if let premium = data.is_premium {
                self.userAvatarImageView.enablePremiumStatus(on: premium)
            }
            if let online = data.is_online {
                self.userAvatarImageView.enableOnlineStatus(on: online)
            }
            if let vip = data.is_vip {
                self.userAvatarImageView.enableGoldRing(on: vip)
            }
        }
        
        postDateLabel.text = commentsData.datetime?.tohhddYYYY()
        textPostTextView.numberOfLines = 0
        textPostTextView.text = commentsData.comment
        backGroundMessageView.clipsToBounds = true

//        messageViewHeightConstr.constant = CGFloat(16 * textPostTextView.numberOfVisibleLines)
//        if textPostTextView.numberOfVisibleLines != 1 {
//            backGroundHeightConstr.constant = CGFloat(16 * textPostTextView.numberOfVisibleLines)
//        } else {
//            backGroundHeightConstr.constant = CGFloat(32)
//            messageViewHeightConstr.constant = CGFloat(58)
//        }
        
        backGroundMessageView.layer.cornerRadius = 8
        backGroundMessageView.layoutIfNeeded()
    }
    
}
