//
//  MyBlogViewPostMyBlogViewPostInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MyBlogViewPostInteractorInput: class {
    func loadPostData(_ postId: Int)
    func deletePost(_ postId: Int)
    func sendComment(postId: Int, comment: String)
    func deleteComment(commentId: Int, postId: Int)
    func setPostLike(postId: Int)
}
