//
//  MyBlogViewPostMyBlogViewPostInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MyBlogViewPostInteractorOutput: class {
    func postGetted(_ post: BlogPostDetail)
    func commentsGetted(_ comments: [BlogPostComments])
    func postWasDeleted()
    func showAlert(title: String, msg: String)
}
