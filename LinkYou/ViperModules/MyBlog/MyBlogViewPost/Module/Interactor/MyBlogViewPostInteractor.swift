//
//  MyBlogViewPostMyBlogViewPostInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Moya

final class MyBlogViewPostInteractor: MyBlogViewPostInteractorInput {

    weak var output: MyBlogViewPostInteractorOutput!
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    func sendComment(postId: Int, comment: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
    
        
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        
        usersProvider.request(.addCommentToPost(postId: postId, text: comment)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    self.loadPostData(postId)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
        
        
    }
    
    func deletePost(_ postId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        usersProvider.request(.deletePost(postId: postId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(BlogPostDetail.self, from: response.data)
                    print(serverResponse)
                    self.output.postWasDeleted()
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadPostData(_ postId: Int) {
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        usersProvider.request(.postDetail(postId: postId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(BlogPostDetail.self, from: response.data)
                    print(serverResponse)
                    self.output.postGetted(serverResponse)
                    self.loadComments(postId)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadComments(_ postId: Int) {
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        usersProvider.request(.postComments(postId: postId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([BlogPostComments].self, from: response.data)
                    print(serverResponse)
                    self.output.commentsGetted(serverResponse)

                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func deleteComment(commentId: Int, postId: Int) {
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        usersProvider.request(.deleteComment(postId: postId, commentId: commentId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    print(serverResponse)
                    self.loadPostData(postId)
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func setPostLike(postId: Int) {
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        usersProvider.request(.setPostLike(postId: postId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    self.loadPostData(postId)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }

}
