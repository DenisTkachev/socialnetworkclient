//
//  MyBlogViewPostMyBlogViewPostRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MyBlogViewPostRouterInput: class {
    func showUserPage(_ id: Int)
}
