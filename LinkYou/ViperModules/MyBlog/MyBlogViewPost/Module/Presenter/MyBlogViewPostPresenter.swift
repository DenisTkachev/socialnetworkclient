//
//  MyBlogViewPostMyBlogViewPostPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class MyBlogViewPostPresenter: MyBlogViewPostViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: MyBlogViewPostViewInput!
    var interactor: MyBlogViewPostInteractorInput!
    var router: MyBlogViewPostRouterInput!
    var postId: Int!
    
    // MARK: -
    // MARK: MyBlogViewPostViewOutput
    func viewIsReady() {
        view.showLoader()
    }

    func loadPostDetails(_ postId: Int) {
        interactor.loadPostData(postId)
    }
    
    func needDeletePost(postId: Int) {
        interactor.deletePost(postId)
    }
    
    func showUserPage(_ userId: Int) {
        router.showUserPage(userId)
    }
    
    func sendBlogComment(postId: Int, comment: String) {
        interactor.sendComment(postId: postId, comment: comment)
    }
    
    func deleteCommentFromPost(commentId: Int, postId: Int) {
        interactor.deleteComment(commentId: commentId, postId: postId)
    }
    
    func setPostLike(postId: Int) {
        interactor.setPostLike(postId: postId)
    }
    
    
}

// MARK: -
// MARK: MyBlogViewPostInteractorOutput
extension MyBlogViewPostPresenter: MyBlogViewPostInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func commentsGetted(_ comments: [BlogPostComments]) {
        view.setComments(comments)
    }
    
    
    func postGetted(_ post: BlogPostDetail) {
        view.setupInitialState(post: post)
        
    }
    
    func postWasDeleted() {
        view.postWasDeleted()
    }
    
}

// MARK: -
// MARK: MyBlogViewPostModuleInput
extension MyBlogViewPostPresenter: MyBlogViewPostModuleInput {
    func configure(_ postId: Int) {
        self.postId = postId
        
        loadPostDetails(self.postId)
    }
}
