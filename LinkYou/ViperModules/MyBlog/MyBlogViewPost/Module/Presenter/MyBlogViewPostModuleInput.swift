//
//  MyBlogViewPostMyBlogViewPostModuleInput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MyBlogViewPostModuleInput: class {
    func configure(_ postId: Int)
}
