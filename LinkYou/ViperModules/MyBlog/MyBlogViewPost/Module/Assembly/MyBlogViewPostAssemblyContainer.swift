//
//  MyBlogViewPostMyBlogViewPostAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 03/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class MyBlogViewPostAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(MyBlogViewPostInteractor.self) { (r, presenter: MyBlogViewPostPresenter) in
			let interactor = MyBlogViewPostInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(MyBlogViewPostRouter.self) { (r, viewController: MyBlogViewPostViewController) in
			let router = MyBlogViewPostRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(MyBlogViewPostPresenter.self) { (r, viewController: MyBlogViewPostViewController) in
			let presenter = MyBlogViewPostPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(MyBlogViewPostInteractor.self, argument: presenter)
			presenter.router = r.resolve(MyBlogViewPostRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(MyBlogViewPostViewController.self) { r, viewController in
			viewController.output = r.resolve(MyBlogViewPostPresenter.self, argument: viewController)
		}
	}

}
