//
//  MyBlogAddPostMyBlogAddPostModuleInput.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MyBlogAddPostModuleInput: class {
    func setPostData(post: BlogPostDetail)
}
