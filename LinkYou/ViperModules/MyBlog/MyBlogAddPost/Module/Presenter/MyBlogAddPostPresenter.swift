//
//  MyBlogAddPostMyBlogAddPostPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit
final class MyBlogAddPostPresenter: MyBlogAddPostViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: MyBlogAddPostViewInput!
    var interactor: MyBlogAddPostInteractorInput!
    var router: MyBlogAddPostRouterInput!
    var editablePost: BlogPostDetail?
    
    // MARK: -
    // MARK: MyBlogAddPostViewOutput
    func viewIsReady() {
        guard let editablePost = editablePost else { return }
        view.setPostData(editablePost)
    }
    
    func sendPost(text: String, images: [UIImage]) {
        interactor.preparePostForSend(text: text, images: images)
    }
}

// MARK: -
// MARK: MyBlogAddPostInteractorOutput
extension MyBlogAddPostPresenter: MyBlogAddPostInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func postSaved(_ userBlog: UserBlog) {
        view.postWasSaved()
    }

}

// MARK: -
// MARK: MyBlogAddPostModuleInput
extension MyBlogAddPostPresenter: MyBlogAddPostModuleInput {
    func setPostData(post: BlogPostDetail) {
        self.editablePost = post
    }
}
