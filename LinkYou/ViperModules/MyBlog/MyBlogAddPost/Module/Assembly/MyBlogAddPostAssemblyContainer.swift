//
//  MyBlogAddPostMyBlogAddPostAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class MyBlogAddPostAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(MyBlogAddPostInteractor.self) { (r, presenter: MyBlogAddPostPresenter) in
			let interactor = MyBlogAddPostInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(MyBlogAddPostRouter.self) { (r, viewController: MyBlogAddPostViewController) in
			let router = MyBlogAddPostRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(MyBlogAddPostPresenter.self) { (r, viewController: MyBlogAddPostViewController) in
			let presenter = MyBlogAddPostPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(MyBlogAddPostInteractor.self, argument: presenter)
			presenter.router = r.resolve(MyBlogAddPostRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(MyBlogAddPostViewController.self) { r, viewController in
			viewController.output = r.resolve(MyBlogAddPostPresenter.self, argument: viewController)
		}
	}

}
