//
//  MyBlogAddPostMyBlogAddPostInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MyBlogAddPostInteractorOutput: class {
    func postSaved(_ userBlog: UserBlog)
    func showAlert(title: String, msg: String)
}
