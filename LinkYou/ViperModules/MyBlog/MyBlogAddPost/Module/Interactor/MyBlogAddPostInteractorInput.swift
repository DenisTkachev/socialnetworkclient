//
//  MyBlogAddPostMyBlogAddPostInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
protocol MyBlogAddPostInteractorInput: class {
    func preparePostForSend(text: String, images: [UIImage])
}
