//
//  MyBlogAddPostMyBlogAddPostInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Moya

final class MyBlogAddPostInteractor: MyBlogAddPostInteractorInput {
    
    weak var output: MyBlogAddPostInteractorOutput!
    let dispatchGroup = DispatchGroup()
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    private func convertFotoToData(_ images: [UIImage]) -> [Data] {
        var imagesData = [Data]()
        
        images.forEach { (image) in
            if let data = image.jpegData(compressionQuality: 0.5) {
                imagesData.append(data)
            } else {
                print("Ошибка конвертации фото, пропускаем")
            }
        }
        return imagesData
    }
    
    
    
    func preparePostForSend(text: String, images: [UIImage]) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let imagesData = self.convertFotoToData(images)
        dispatchGroup.enter()
        var imagesURL = [String]()
        
        let photosProvider = MoyaProvider<UploadPhotosService>(plugins: [debugPlagin, authPlagin])
        // send data image and get url
        imagesData.forEach { (imageData) in
            photosProvider.request(.uploadUserPhoto(data: imageData)) { (result) in
                switch result {
                case .success(let response):
                    do {
                        let userPhoto = try JSONDecoder().decode(ImageDataResponse.self, from: response.data)
                        if let photosURL = userPhoto.data, let urlString = photosURL.first?.src  {
                            imagesURL.append(urlString)
                            if imagesURL.count == images.count {
                                self.dispatchGroup.leave()
                            }
                        }
                    } catch {
                        print(error)
                        self.dispatchGroup.leave()
                    }
                case let .failure(error):
                    print(error)
                    self.dispatchGroup.leave()
                }
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            var imagesURLArr = [String:String]()
            for (photoIndex, photoUrl) in imagesURL.enumerated() {
                imagesURLArr["photos[\(photoIndex)]"] = photoUrl
            }
           
            self.sendToServer(text: text, imagesURL: imagesURLArr)
        }
        
        
    }
    
    private func sendToServer(text: String, imagesURL: [String:String]) {
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.savePost(text: text, videoLink: "", audioLink: "", photos: imagesURL)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userBlog = try JSONDecoder().decode(UserBlog.self, from: response.data)
                    self.output.postSaved(userBlog)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
                print("Отправка поста на сервер не прошла")
            }
        }
    }
    
}
