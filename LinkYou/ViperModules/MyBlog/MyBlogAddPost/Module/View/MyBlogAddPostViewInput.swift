//
//  MyBlogAddPostMyBlogAddPostViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MyBlogAddPostViewInput: class {
  /// - author: KlenMarket
    func postWasSaved()
    func showAlert(title: String, msg: String)
    func setPostData(_ editablePost: BlogPostDetail)
}
