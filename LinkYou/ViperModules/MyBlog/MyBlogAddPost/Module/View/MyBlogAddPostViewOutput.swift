//
//  MyBlogAddPostMyBlogAddPostViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit
protocol MyBlogAddPostViewOutput: class {
    /// - author: KlenMarket
    func viewIsReady()
    func sendPost(text: String, images: [UIImage])
}
