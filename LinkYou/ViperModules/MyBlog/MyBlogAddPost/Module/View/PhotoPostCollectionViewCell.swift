//
//  PhotoPostCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/05/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class PhotoPostCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    weak var delegate: PhotoPostType!
    var selfIndex: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        deleteBtn.layer.cornerRadius = deleteBtn.frame.height / 2
    }

    func setupCell(_ postPhoto: UIImage, _ index: Int) {
        photo.image = postPhoto
        selfIndex = index
    }
    
    @IBAction func pressDeleteBtn(_ sender: UIButton) {
        delegate.deletePostPhoto(index: selfIndex)
    }
}
