//
//  MyBlogAddPostMyBlogAddPostViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 12/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD
import Photos
//import DeviceKit

protocol PhotoPostType: class {
    func deletePostPhoto(index: Int)
}

final class MyBlogAddPostViewController: UIViewController, PhotoPostType {
    
    // MARK: -
    // MARK: Properties
    var output: MyBlogAddPostViewOutput!
    
    @IBOutlet weak var btnBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var postTextFiled: UITextView!
    @IBOutlet weak var extensionBtnView: UIView!
    var titleHeaderBtn = "Написать в блог"
    @IBOutlet weak var photosCollectionView: UICollectionView!
    var postPhotos = [UIImage]()
    //    let groupOfSmallDevices: [DeviceKit.Device] = [.iPhoneSE, .simulator(.iPhoneSE)]
//    let device = Device()
    var isKeyboardShowing: Bool = false

    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        setupNavBar()
        postTextFiled.delegate = self
        extensionBtnView.layer.cornerRadius = 10
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        photosCollectionView.register(UINib(nibName: "PhotoPostCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoPostCollectionViewCell")
    }
    
    func setupNavBar() {
        let rightBtn = UIBarButtonItem(title: titleHeaderBtn, style: .plain, target: self, action: #selector(pressSendBtn))
        rightBtn.tintColor = .white
        navigationItem.rightBarButtonItem = rightBtn
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    @objc func pressSendBtn() {
        if postTextFiled.text.count > 0 {
            navigationItem.rightBarButtonItem?.isEnabled = false
            HUD.show(.progress)
            output.sendPost(text: postTextFiled.text, images: postPhotos)
        } else {
            navigationItem.rightBarButtonItem?.isEnabled = false
            showAlert(title: "", msg: "Нужно что-нибудь написать")
        }
    }
    
    func postWasSaved() {
        NotificationCenter.default.post(name: .savedBlogPost, object: nil)
        HUD.hide()
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    @objc func keyboardHandle(notification: NSNotification) {
        isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let tabBarHeight = self.tabBarController?.tabBar.frame.height ?? 0.0
            let keyboardHeight = keyboardRectangle.height - tabBarHeight
            btnBottomConstraint.constant = isKeyboardShowing ? keyboardHeight + 8 : 8
        }
        
        UIView.animate(withDuration: 0) {
            self.view.layoutIfNeeded()
        }
        
       
    }
    
    private func addPhotos(image: UIImage) {
        postPhotos.append(image)
        photosCollectionView.reloadData()
    }
    
    @IBAction func pressTextViewGes(_ sender: UITapGestureRecognizer) {
        if isKeyboardShowing {
            postTextFiled.resignFirstResponder()
        } else {
            postTextFiled.becomeFirstResponder()
        }
    }
    
    @IBAction func addPhotoBtn(_ sender: UIButton) {
        self.showGalleryActionSheet()
    }
    
    func deletePostPhoto(index: Int) {
        postPhotos.remove(at: index)
        photosCollectionView.reloadData()
    }
    
}

// MARK: -
// MARK: MyBlogAddPostViewInput
extension MyBlogAddPostViewController: MyBlogAddPostViewInput {
    func setPostData(_ editablePost: BlogPostDetail) {
        guard let text = editablePost.text else { return }
        self.postTextFiled.text = text // TODO: Андрей. Приходят html тэги как редактировать не ясно в таком виде.
        titleHeaderBtn = "Отредактировать"
    }
    
}

extension MyBlogAddPostViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (postTextFiled.text == "Расскажите, что у вас нового?") {
            postTextFiled.text = ""
            postTextFiled.textColor = UIColor.charcoalGrey
        }
        textView.becomeFirstResponder() //Optional
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (postTextFiled.text == "") {
            postTextFiled.text = "Расскажите, что у вас нового?"
            postTextFiled.textColor = .lightGray
        }
        postTextFiled.resignFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}
extension MyBlogAddPostViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            imagePicker.navigationBar.isTranslucent = false
            imagePicker.navigationBar.barTintColor = .cerulean
            imagePicker.navigationBar.tintColor = .white
            imagePicker.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        checkPermission()
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
//        output.uploadImage(image: image)
        self.addPhotos(image: image)
        dismiss(animated:true, completion: nil)
//        HUD.show(.progress)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            print("User do not have access to photo album.")
        case .denied:
            print("User has denied the permission.")
        }
    }
}


extension MyBlogAddPostViewController: CellButtonActionHandlerType {
    func pressPayBtn(type: TizerType) {
    // not used
    }
    
    func showGalleryActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Камера", style: .default) { action in
            self.openCamera()
        }
        let photoGallery = UIAlertAction(title: "Галлерея", style: .default) { action in
            self.openPhotoLibrary()
        }
        actionSheet.addAction(photoGallery)
        actionSheet.addAction(camera)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func showBlogActionSheet(option: String) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
//        if self.user.ublogs != nil {
//            let edit = UIAlertAction(title: "Отредактировать", style: .default) { action in
//                self.actionSheetHandler(type: .edit)
//            }
//            let delete = UIAlertAction(title: "Удалить", style: .destructive) { action in
//                self.actionSheetHandler(type: .delete)
//            }
//            actionSheet.addAction(edit)
//            actionSheet.addAction(delete)
//        }
        
        //        if option == "full" {
        //            actionSheet.addAction(blackList)
        //        }
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
//    private func actionSheetHandler(type: EventSheetType) {
//        guard let postLast = self.user.ublogs?.last, let lastId = self.user.ublogs?.last?.id else { return }
//        switch type {
//        case .edit:
//            print("edit")
//            let post = BlogPostDetail.init(last: postLast, user: self.user!)
//            output.editBlogPost(post)
//        case .delete:
//            print("delete")
//            output.deletePost(lastId)
//        }
//    }
    
    func buttonPressed(cell: UITableViewCell, actionType: UserpageViewController.ActionBtnType) {
        switch cell {
            
        case is PersonalBlogTableViewCell:
            if actionType == UserpageViewController.ActionBtnType.pressMoreBtn {
                showBlogActionSheet(option: "short")
            }
            if actionType == UserpageViewController.ActionBtnType.showMoreBlog {
//                output.showUserBlog(user)
            }
            if actionType == UserpageViewController.ActionBtnType.addNewPost {
//                output.addNewPost()
            }
            
        default:
            break
        }
    }
    
}

fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}


extension MyBlogAddPostViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = photosCollectionView.dequeueReusableCell(withReuseIdentifier: "PhotoPostCollectionViewCell", for: indexPath) as! PhotoPostCollectionViewCell
        cell.setupCell(postPhotos[indexPath.row], indexPath.row)
        cell.delegate = self
        return cell
    }
    
    
}
