//
//  MyBlogMyBlogViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MyBlogViewOutput: class {
    /// - author: Denis Tkachev
    func viewIsReady()
    func choosePost(postId: Int)
    func addNewPost()
    func needDeletePost(_ postId: Int)
}
