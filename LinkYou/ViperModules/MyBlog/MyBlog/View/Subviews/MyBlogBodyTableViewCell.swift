//
//  MyBlogBodyTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 02.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MyBlogBodyTableViewCell: UITableViewCell {
    
    weak var delegate: CellBlogButtonActionHandlerType!
    
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    
    @IBOutlet weak var textPostTextView: UITextView!
    @IBOutlet weak var postImageView: UIImageView!
    
    @IBOutlet weak var postLikesBtn: UIButton!
    @IBOutlet weak var postViewBtn: UIButton!
    @IBOutlet weak var postCommentBtn: UIButton!
    
    var postId = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        // Configure the view for the selected state
    }
    
    func setupCell(post: UserBlog) {
        guard let user = post.user else { return }
        
        if let name = user.name, let age = user.birthday?.age {
            userNameLabel.text = ("\(name), \(age.toString())")
        }
        
        if let urlString = user.avatar?.src?.square {
            userAvatarImageView.sd_setImage(with: URL(string: urlString), placeholderImage: nil)
            userAvatarImageView.layer.cornerRadius = userAvatarImageView.bounds.height / 2
            userAvatarImageView.clipsToBounds = true
        }
        
        postDateLabel.text = post.datetime?.toyyyyMMdd()
        textPostTextView.text = post.text_short
        
        if let urlString = post.photos?.first?.src {
            postImageView.sd_setImage(with: URL(string: urlString), placeholderImage: nil)
        }
        postLikesBtn.setTitle(String(format: NSLocalizedString("NumberOfLikes", comment: ""), post.likes_count ?? 0), for: .normal)
        postViewBtn.setTitle("\(post.views_count ?? 0)", for: .normal)
        postCommentBtn.setTitle(String(format: NSLocalizedString("NumberOfComments", comment: ""), post.comments_count ?? 0), for: .normal)
        
        if post.is_liked ?? false {
            postLikesBtn.setImage(#imageLiteral(resourceName: "heart-select-userPage"), for: .normal)
        } else {
            postLikesBtn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
        }
        if let postid = post.id {
            self.postId = postid
        }
        
    }
    @IBAction func moreDetailsBtn(_ sender: UIButton) {
        delegate.buttonPressed(cell: self, actionType: .delete)
    }
    
    @IBAction func pressCommentBtn(_ sender: UIButton) {
        delegate.buttonPressed(cell: self, actionType: .showPost)
    }
    
    
    
    
    
}
