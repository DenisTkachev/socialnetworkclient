//
//  MyBlogHeaderTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 02.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MyBlogHeaderTableViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var addNewPostField: TextFieldCustom!
    @IBOutlet weak var sendIconImageView: UIImageView!
    
    override func awakeFromNib() {
        sendIconImageView.addTapGestureRecognizer {
            
        }
    }
    
    
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        self.selectionStyle = .none
//        // Configure the view for the selected state
//    }
    
}
