//
//  MyBlogMyBlogViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

enum ActionBtnType {
    case claimReport
    case delete
    case sendPost
    case showPost
}

protocol CellBlogButtonActionHandlerType: class {
    func buttonPressed(cell: UITableViewCell, actionType: ActionBtnType)
}

protocol ExternalBlogActionHandlerType: class {
    func pressButton(data: Any, actionType: ActionBtnType)
}

final class MyBlogViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    enum EventSheetType {
        case claim
        case delete
    }
    
    // MARK: -
    // MARK: Properties
    var output: MyBlogViewOutput!
    var user: User?
    var blogs: [UserBlog]?
    var isSetHeader = false
    @IBOutlet weak var tableView: UITableView!
    var selectedPost = 0
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.show(.progress, onView: self.view)
        tableView.dataSource = self
        tableView.delegate = self
        
        let headerNib = UINib.init(nibName: "MyBlogHeaderTableViewCell", bundle: Bundle.main)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "MyBlogHeaderTableViewCell")
        tableView.register(UINib(nibName: "MyBlogBodyTableViewCell", bundle: nil), forCellReuseIdentifier: "MyBlogBodyTableViewCell")
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        setupNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        output.viewIsReady()
    }
    
    func setupNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateBlogCell(notification:)), name: .deletedBlogPost, object: nil)
    }
    
    private func setTitle() {
        title = "Блог"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if user?.id == SuperUser.shared.superUser?.id {
            return 100
        } else {
            return CGFloat.Magnitude.leastNonzeroMagnitude
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return blogs?.count ?? 0
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 12
//    }
//    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let view = UIView.init()
//        view.backgroundColor = .clear
//        return view
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBlogBodyTableViewCell") as! MyBlogBodyTableViewCell
        if let blogs = blogs {
            cell.setupCell(post: blogs[indexPath.section])
            cell.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if user?.id == SuperUser.shared.superUser?.id {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MyBlogHeaderTableViewCell") as? MyBlogHeaderTableViewCell
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapAddNewPost(_:)))
            headerView!.addGestureRecognizer(tap)
            
            //        headerView?.addNewPostField.addTarget(self, action: #selector(tapAddNewPost(_:)), for: .touchUpInside)
            
            return headerView
        } else {
            return nil
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let blogs = blogs, blogs.count > 0 {
            if let id = blogs[indexPath.section].id {
                output.choosePost(postId: id)
                HUD.hide()
            }
        }
    }
    
    @objc private func tapAddNewPost(_ sender: Any)  {
        output.addNewPost()
    }
    
    @objc func updateBlogCell(notification: NSNotification) {
        output.viewIsReady()
    }
    
    func postWasSeleted() {
        NotificationCenter.default.post(name: .deletedBlogPost, object: nil)
    }
}

// MARK: -
// MARK: MyBlogViewInput
extension MyBlogViewController: MyBlogViewInput {
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
//            self.refreshControl.endRefreshing()
        }
    }
    
    func setupInitialState(_ user: User) {
        self.user = user
        setTitle()
    }
    
    func setUserBlog(_ blogs: [UserBlog]) {
        self.blogs = blogs
        tableView.reloadData()
        HUD.hide()
    }
    
}

extension MyBlogViewController: CellBlogButtonActionHandlerType {
    
    func buttonPressed(cell: UITableViewCell, actionType: ActionBtnType) {
        switch cell {
        case is MyBlogBodyTableViewCell:
            
            if actionType == ActionBtnType.claimReport {
                showActionSheet()
            }
            if actionType == ActionBtnType.delete {
                if let cell = cell as? MyBlogBodyTableViewCell, cell.postId != 0 {
                    self.selectedPost = cell.postId // TODO: какой пост удалить?
                    showActionSheet()
                }
            }
            if actionType == ActionBtnType.showPost {
                if let cell = cell as? MyBlogBodyTableViewCell, cell.postId != 0 {
                    output.choosePost(postId: cell.postId)
                }
            }
            
        default:
            break
        }
    }
}

extension MyBlogViewController {
    
    func showActionSheet() {
        
        guard let childViewControllers = parent?.children else { return }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for vc in childViewControllers {
            if vc is UserpageViewController {
                let claim = UIAlertAction(title: "Пожаловаться", style: .default) { action in
                    self.actionSheetHandler(type: .claim)
                }
                actionSheet.addAction(claim)
            }
            if vc is PersonalPageViewController {
                let delete = UIAlertAction(title: "Удалить запись", style: .destructive) { action in
                    self.actionSheetHandler(type: .delete)
                }
                actionSheet.addAction(delete)
            }
            
        }
        
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func actionSheetHandler(type: EventSheetType) {
        switch type {
        case .claim:
            self.navigationController?.pushViewController(ClaimViewController(passDataDelegate: self, ticketType: .user), animated: true)
        case .delete:
            output.needDeletePost(selectedPost)
            HUD.show(.progress)
        }

    }
}

extension MyBlogViewController: ExternalBlogActionHandlerType {
    func pressButton(data: Any, actionType: ActionBtnType) {
        
    }
}


