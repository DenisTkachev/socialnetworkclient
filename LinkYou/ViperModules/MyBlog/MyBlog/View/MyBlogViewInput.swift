//
//  MyBlogMyBlogViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MyBlogViewInput: class {
  /// - author: Denis Tkachev
    func setupInitialState(_ user: User)
    func setUserBlog(_ blogs: [UserBlog])
    func postWasSeleted()
    func showAlert(title: String, msg: String)
}
