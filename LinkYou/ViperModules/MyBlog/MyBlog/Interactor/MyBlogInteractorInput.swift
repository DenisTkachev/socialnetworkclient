//
//  MyBlogMyBlogInteractorInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MyBlogInteractorInput: class {
    func loadUserBlogList(id: Int)
    func deletePost(_ postId: Int)
}
