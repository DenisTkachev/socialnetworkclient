//
//  MyBlogMyBlogInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MyBlogInteractorOutput: class {
    func blogsGetted(_ blog: [UserBlog])
    func postWasDeleted()
    func showAlert(title: String, msg: String)
}
