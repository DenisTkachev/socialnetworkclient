//
//  MyBlogMyBlogInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Moya

final class MyBlogInteractor: MyBlogInteractorInput {
  weak var output: MyBlogInteractorOutput!

    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    func loadUserBlogList(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
        
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin, debugPlagin])
        // TODO: Список постов пользователя {user_id}. Параметр limit по умолчанию равен 15
        // Если постов больше 15, нужно их тоже получать и выводить.
        usersProvider.request(.listBlogs(userId: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([UserBlog].self, from: response.data)
                    self.output.blogsGetted(serverResponse)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func deletePost(_ postId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        
        usersProvider.request(.deletePost(postId: postId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(BlogPostDetail.self, from: response.data)
                    print(serverResponse)
                    self.output.postWasDeleted()
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
}
