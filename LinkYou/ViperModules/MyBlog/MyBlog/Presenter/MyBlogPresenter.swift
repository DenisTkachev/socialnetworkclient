//
//  MyBlogMyBlogPresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class MyBlogPresenter: MyBlogViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: MyBlogViewInput!
    var interactor: MyBlogInteractorInput!
    var router: MyBlogRouterInput!

    var user: User!
    
    // MARK: -
    // MARK: MyBlogViewOutput
    func viewIsReady() {
        if let user = user {
            view.setupInitialState(user)
            guard let id = user.id else { return }
            interactor.loadUserBlogList(id: id)
        }
    }
    
    func choosePost(postId: Int) {
        router.openPost(postId)
    }
    
    func addNewPost() {
        router.addNewPost()
    }
    
    func needDeletePost(_ postId: Int) {
        interactor.deletePost(postId)
    }
}

// MARK: -
// MARK: MyBlogInteractorOutput
extension MyBlogPresenter: MyBlogInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func blogsGetted(_ blog: [UserBlog]) {
        view.setUserBlog(blog)
    }
    
    func postWasDeleted() {
        guard let id = user.id else { return }
        interactor.loadUserBlogList(id: id)
        view.postWasSeleted()
    }
}

// MARK: -
// MARK: MyBlogModuleInput
extension MyBlogPresenter: MyBlogModuleInput {
    func configure(_ user: User) {
        self.user = user
    }
}
