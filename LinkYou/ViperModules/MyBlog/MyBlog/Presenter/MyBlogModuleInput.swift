//
//  MyBlogMyBlogModuleInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MyBlogModuleInput: class {
    func configure(_ user: User)
}
