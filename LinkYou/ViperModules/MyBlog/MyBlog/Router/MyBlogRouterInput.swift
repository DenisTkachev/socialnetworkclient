//
//  MyBlogMyBlogRouterInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MyBlogRouterInput: class {
    func openPost(_ postId: Int)
    func addNewPost()
}
