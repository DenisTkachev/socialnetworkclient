//
//  MyBlogMyBlogAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class MyBlogAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(MyBlogInteractor.self) { (r, presenter: MyBlogPresenter) in
			let interactor = MyBlogInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(MyBlogRouter.self) { (r, viewController: MyBlogViewController) in
			let router = MyBlogRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(MyBlogPresenter.self) { (r, viewController: MyBlogViewController) in
			let presenter = MyBlogPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(MyBlogInteractor.self, argument: presenter)
			presenter.router = r.resolve(MyBlogRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(MyBlogViewController.self) { r, viewController in
			viewController.output = r.resolve(MyBlogPresenter.self, argument: viewController)
		}
	}

}
