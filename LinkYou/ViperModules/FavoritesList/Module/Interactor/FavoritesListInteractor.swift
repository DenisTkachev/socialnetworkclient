//
//  FavoritesListFavoritesListInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class FavoritesListInteractor: FavoritesListInteractorInput {

  weak var output: FavoritesListInteractorOutput!
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    func getFavorites() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.favorites) { (result) in
            switch result {
            case .success(let response):
                do {
                    let favList = try JSONDecoder().decode([Favorite].self, from: response.data)
                    self.output.listsFavGetted(favList)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getBlacklist() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.blacklist) { (result) in
            switch result {
            case .success(let response):
                do {
                    let blackList = try JSONDecoder().decode([Favorite].self, from: response.data)
                    self.output.blacklistGetted(blackList)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addUserToBlackList(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<ClaimTicketService>(plugins: [authPlagin])
        
        usersProvider.request(.addUserToBL(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    print(serverResponse)
//                    self.output.claimIdGeted(id: id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func deleteUserFromBlackList(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<ClaimTicketService>(plugins: [authPlagin])
        
        usersProvider.request(.deleteUserFromBl(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    print(serverResponse)
//                    self.output.claimIdGeted(id: id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addUserToFav(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.addUserToFav(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    print(serverResponse)
//                    self.output.claimIdGeted(id: id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func deleteUserFromFavList(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.deleteUserFromFav(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    print(serverResponse)
//                    self.output.claimIdGeted(id: id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    
    
}




