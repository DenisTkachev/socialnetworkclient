//
//  FavoritesListFavoritesListInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol FavoritesListInteractorOutput: class {
    func listsFavGetted(_ fav: [Favorite])
    func blacklistGetted(_ blacklist: [Favorite])
    func showAlert(title: String, msg: String)
}
