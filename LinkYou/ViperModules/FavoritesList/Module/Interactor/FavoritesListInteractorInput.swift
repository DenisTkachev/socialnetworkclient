//
//  FavoritesListFavoritesListInteractorInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol FavoritesListInteractorInput: class {
    func getFavorites()
    func getBlacklist()
    func addUserToBlackList(id: Int)
    func deleteUserFromBlackList(id: Int)
    func addUserToFav(id: Int)
    func deleteUserFromFavList(id: Int)
}
