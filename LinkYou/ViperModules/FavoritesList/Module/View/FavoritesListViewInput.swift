//
//  FavoritesListFavoritesListViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol FavoritesListViewInput: class {
    /// - author: Denis Tkachev
    func setupInitialState(fav: [Favorite])
    func setBlackList(_ blackList: [Favorite])
    func showAlert(title: String, msg: String)
}
