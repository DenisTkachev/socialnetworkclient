//
//  FavoritesTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class FavoritesTableViewCell: UITableViewCell {

    @IBOutlet weak var userPicImageView: MyAvatarView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userInfoLabel: UILabel!
    @IBOutlet weak var userCountPhotoLabel: UILabel!
    @IBOutlet weak var userlocationLabel: UILabel!
    @IBOutlet weak var normalView: UIView!
    @IBOutlet weak var deletedView: UIView!
    weak var delegate: ShowHideCellType!
    
    override func prepareForReuse() {
        userPicImageView.setStartSettings(hidden: true)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none

    }
    
    @IBAction func undoPressed(_ sender: RoundButtonCustom) {
        normalView.isHidden = false
        deletedView.isHidden = true
        UIView.animate(withDuration: 0.2) {
            self.contentView.layoutIfNeeded()
        }
        self.delegate.restoreUser(cell: self)
    }
    
    func hideCell() {
        normalView.isHidden = true
        deletedView.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.contentView.layoutIfNeeded()
        }
        delegate.deleteUserFromList(cell: self)
    }
    
    func setDataIntoCell(data: Favorite) {
        if let job = data.job {
            userNameLabel.text = data.name
            userInfoLabel.text = "\(job.profession ?? "")\n\(job.occupation ?? "")"
        }
        
        if let name = data.name {
            userNameLabel.text = name
        }
        
        if let countPhoto = data.photos_count {
            userCountPhotoLabel.text = String(countPhoto)
        } else {
            userCountPhotoLabel.text = "0"
        }
        
        if let location = data.location {
            userlocationLabel.text = location.city_name
        } else {
            userCountPhotoLabel.text = ""
        }
        guard let urlPath = data.avatar else { return }
        guard let fullUrlPath = urlPath.src?.origin else { return }
        userPicImageView.userAvatar.sd_setImage(with: URL(string: fullUrlPath)) { (image, error, cache, url) in
            self.userPicImageView.userAvatar.toRoundedImage()
            self.userPicImageView.addTunning()
            
            if let premium = data.is_premium {
                self.userPicImageView.enablePremiumStatus(on: premium)
            }
            if let online = data.is_online {
                self.userPicImageView.enableOnlineStatus(on: online)
            }
            if let vip = data.is_vip {
                self.userPicImageView.enableGoldRing(on: vip)
            }
        }
    }
}
