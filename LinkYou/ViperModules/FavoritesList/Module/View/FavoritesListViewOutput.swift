//
//  FavoritesListFavoritesListViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol FavoritesListViewOutput: class {
    /// - author: Denis Tkachev
    func viewIsReady()
    func showUserPage(user: Favorite)
    func deleteFromFav(id: Int)
    func deleteFromBL(id: Int)
    func addToFav(id: Int)
    func addToBL(id: Int)
}
