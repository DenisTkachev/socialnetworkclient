//
//  FavoritesListFavoritesListViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD
//import DeviceKit

protocol ShowHideCellType: class {
    func restoreUser(cell: UITableViewCell)
    func deleteUserFromList(cell: UITableViewCell)
}

final class FavoritesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ShowHideCellType {
    
    // MARK: -
    // MARK: Properties
    var output: FavoritesListViewOutput!
    var favList: [Favorite] = []
    var blackList: [Favorite] = []
    
    @IBOutlet weak var segmentControlView: CustomSegmentedContrl!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.show(.progress)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: "FavoritesTableViewCell", bundle: nil), forCellReuseIdentifier: "FavoritesTableViewCell")
        output.viewIsReady()
        title = "Избранные"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        setupSegment()
    }
    
    private func setupSegment() {
        segmentControlView.selectorLine.frame = CGRect(x: segmentControlView.selectorLine.frame.minX, y: segmentControlView.selectorLine.frame.minY, width: UIScreen.main.bounds.width - 32, height: 1)
//        let groupOfAllowedDevices: [DeviceKit.Device] = [.iPhone6, .iPhone6s, .iPhoneSE, .simulator(.iPhone6), .simulator(.iPhone6s), .simulator(.iPhoneSE)]
//        let device = DeviceKit.Device()
//
//        if device.isOneOf(groupOfAllowedDevices) {
//
//        }
        
    }
    
    @IBAction func pressSegmentControl(_ sender: CustomSegmentedContrl) {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentControlView.selectedSegmentIndex == 0 {
            return favList.count
        } else {
            return blackList.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segmentControlView.selectedSegmentIndex == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesTableViewCell", for: indexPath) as? FavoritesTableViewCell
            cell!.setDataIntoCell(data: favList[indexPath.row])
            cell!.delegate = self
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesTableViewCell", for: indexPath) as? FavoritesTableViewCell
            cell!.setDataIntoCell(data: blackList[indexPath.row])
            cell!.delegate = self
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? FavoritesTableViewCell else { return }
        if cell.normalView.isHidden {
            return
        } else {
            if segmentControlView.selectedSegmentIndex == 0 {
                tableView.deselectRow(at: indexPath, animated: false)
                output.showUserPage(user: favList[indexPath.row])
            } else {
                tableView.deselectRow(at: indexPath, animated: false)
                output.showUserPage(user: blackList[indexPath.row])
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Удалить"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let cell = tableView.cellForRow(at: indexPath) as? FavoritesTableViewCell else { return false }
        if cell.normalView.isHidden {
            return false
        } else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            let cell = tableView.cellForRow(at: indexPath) as! FavoritesTableViewCell
            cell.hideCell()
        }
    }
    
    func restoreUser(cell: UITableViewCell) {
        if let restoreCell = cell as? FavoritesTableViewCell {
            if let indexPath = tableView.indexPath(for: restoreCell) {
                if segmentControlView.selectedSegmentIndex == 0 {
                    output.addToFav(id: favList[indexPath.row].id!)
                } else {
                    output.addToBL(id: blackList[indexPath.row].id!)
                }
            }
        }
    }
    
    func deleteUserFromList(cell: UITableViewCell) {
        if let deletedCell = cell as? FavoritesTableViewCell {
            if let indexPath = tableView.indexPath(for: deletedCell) {
                if segmentControlView.selectedSegmentIndex == 0 {
                    output.deleteFromFav(id: favList[indexPath.row].id!)
                } else {
                    output.deleteFromBL(id: blackList[indexPath.row].id!)
                }
            }
        }
    }
}

// MARK: -
// MARK: FavoritesListViewInput
extension FavoritesListViewController: FavoritesListViewInput {
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
//            self.refreshControl.endRefreshing()
        }
    }
    
    func setupInitialState(fav: [Favorite]) {
        self.favList = fav
        if segmentControlView.selectedSegmentIndex == 0 {
            tableView.reloadData()
            HUD.hide()
        }
    }
    
    func setBlackList(_ blackList: [Favorite]) {
        self.blackList = blackList
        if segmentControlView.selectedSegmentIndex == 1 {
            tableView.reloadData()
            HUD.hide()
        }
    }
    
}
