//
//  FavoritesListFavoritesListAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class FavoritesListAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(FavoritesListInteractor.self) { (r, presenter: FavoritesListPresenter) in
			let interactor = FavoritesListInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(FavoritesListRouter.self) { (r, viewController: FavoritesListViewController) in
			let router = FavoritesListRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(FavoritesListPresenter.self) { (r, viewController: FavoritesListViewController) in
			let presenter = FavoritesListPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(FavoritesListInteractor.self, argument: presenter)
			presenter.router = r.resolve(FavoritesListRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(FavoritesListViewController.self) { r, viewController in
			viewController.output = r.resolve(FavoritesListPresenter.self, argument: viewController)
		}
	}

}
