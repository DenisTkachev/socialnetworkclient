//
//  FavoritesListFavoritesListPresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class FavoritesListPresenter: FavoritesListViewOutput {
    
    // MARK: -
    // MARK: Properties

    weak var view: FavoritesListViewInput!
    var interactor: FavoritesListInteractorInput!
    var router: FavoritesListRouterInput!

    // MARK: -
    // MARK: FavoritesListViewOutput
    func viewIsReady() {
        interactor.getFavorites()
        interactor.getBlacklist()
    }
    
    func deleteFromFav(id: Int) {
        interactor.deleteUserFromFavList(id: id)
    }
    
    func deleteFromBL(id: Int) {
        interactor.deleteUserFromBlackList(id: id)
    }
    
    
    func showUserPage(user: Favorite) {
        router.presentUserPage(user: user)
    }
    
    func addToFav(id: Int) {
        interactor.addUserToFav(id: id)
    }
    
    func addToBL(id: Int) {
        interactor.addUserToBlackList(id: id)
    }

}

// MARK: -
// MARK: FavoritesListInteractorOutput
extension FavoritesListPresenter: FavoritesListInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func blacklistGetted(_ blacklist: [Favorite]) {
        view.setBlackList(blacklist)
    }
    
    func listsFavGetted(_ fav: [Favorite]) {
        view.setupInitialState(fav: fav)
    }
}

// MARK: -
// MARK: FavoritesListModuleInput
extension FavoritesListPresenter: FavoritesListModuleInput {


}
