//
//  DialogsDialogsModuleInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol DialogsModuleInput: class {
    func configure(_ dialogs: ChatMessages, pictures: [String], _ headers: [AnyHashable : Any])
    func newConfigureDialog(user: User)
}
