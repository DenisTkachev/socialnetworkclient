//
//  DialogsDialogsPresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

final class DialogsPresenter: DialogsViewOutput {
    
    // MARK: -
    // MARK: Properties

    weak var view: DialogsViewInput!
    var interactor: DialogsInteractorInput!
    var router: DialogsRouterInput!

    // pagination
    var paginationCurrent = 1
    var paginationIsEnd = 0
    var paginationLimit = 0
    var paginationTotal = 0
//    var users = [Guest]()
    
    var dialogs: [Message]! {
        didSet {
           dialogsForTableView = converting(dialogFromServer: dialogs)
        }
    }
    var parentMessage: Dialog!
    var dialogsForTableView = [[Message]]()
    
//    var dialog: Message!
    
    // MARK: -
    // MARK: DialogsViewOutput
    func viewIsReady() {
        view.setupInitialState(dialogsForTableView, parentMessage)
    }
    
    func sendMessage(recipientId: Int, message: String, pictures: [String]) {
        interactor.sendMessage(recipientId: recipientId, message: message, pictures: [])
    }
    
    func removeMessage(messageId: Int) {
        interactor.removeMessage(messageId)
    }
    
    func loadGiftsList() {
        interactor.loadGiftsCategory()
    }
    
    func sendPhoto(recipientId: Int, photo: UIImage) {
        if let data = photo.jpegData(compressionQuality: 0.5) {
            interactor.uploadImageData(imageData: data, recipientId: recipientId)
        } else {
            // convert picture error
        }
    }
    
    func extractCooedinates(_ dialog: Message) -> (Double, Double, String, String) { // тут же найти текст и вернуть если он есть -> String
        guard let message = dialog.comment else { return (55.753563, 37.623729, "", "") }  // Кр.Площадь
        var temp = [String]()
        var msg = [String]()
        var address = [String]()
        
        do {//TODO: Андрей обещал не заменять " на &quot и тогда будет срабатывать регулярка 
            let regex = try NSRegularExpression(pattern: "(?<=\\?text=)(.*?)(?=\\\" target=)") // поиск координат в строке
            let results = regex.matches(in: message,
                                        range: NSRange(message.startIndex..., in: message))
            temp = results.map {
                String(message[Range($0.range, in: message)!])
            }
            
            let regex2 = try NSRegularExpression(pattern: "(^)(.*?)(?=\\<a class=)") // поиск сообщения в общей строке
            let results2 = regex2.matches(in: message,
                                        range: NSRange(message.startIndex..., in: message))
            msg = results2.map {
                String(message[Range($0.range, in: message)!])
            }
            
            let regex3 = try NSRegularExpression(pattern: ">(.*?)a>") // TODO: регулярка не работает  <a.*?>(.*?)<\/a> или <a[^>]+>([^<]+)<\/a> Андрей, пришли строкой адрес
            let results3 = regex3.matches(in: message,
                                        range: NSRange(message.startIndex..., in: message))
            address = results3.map {
                String(message[Range($0.range, in: message)!])
            }
            
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return (55.753563, 37.623729, msg.first ?? "", "") // error
        }
        
        if let oneLocation = temp.first {
            let latLon = oneLocation.split(separator: ",")
            if let lat = latLon.first, let lon = latLon.last, let dLat = Double(lat), let dLon = Double(lon), let msg = msg.first, let address = address.first {
                return (dLat, dLon, msg, address)
            } else {
                return (55.753563, 37.623729, msg.first  ?? "", "") // error
            }
        } else {
            return (55.753563, 37.623729, msg.first ?? "", "") // error
        }
    }
    
    func requestNextPage() {
        if dialogs.count != paginationTotal, paginationIsEnd != 1 {
            paginationCurrent += 1
            interactor.loadDialog(selectedMessage: parentMessage, page: paginationCurrent)
        }
    }
    
    func gotoUserProfile(_ user: User) {
        let emptyUser: [User] = []
        router.presentUserPage(user: user, allUsers: emptyUser)
    }
    
    func updateChat(notification: NSNotification) {
        if let messageId = notification.userInfo?["messageId"] as? Int {
            interactor.loadNewMessage(id: messageId)
        }
        if let dialogId = notification.userInfo?["dialog_id"] as? String {
            // прочитал сообщения в этом диалоге
            if let currentDialogId = parentMessage.id {
                if currentDialogId.toString() == dialogId {
                    dialogsForTableView.forEach { (dialogs) in
                        dialogs.forEach({ (dialog) in
                            dialog.setRead()
                        })
                    }
                    view.setupInitialState(dialogsForTableView, parentMessage)
                }
            }
        }
        // interactor.loadOneNewMessage(id: id) <<== сделать метод
    }
    
    func messageWasRead(_ dialogId: Int) {
        interactor.messageIsRead(dialogId: dialogId)
    }
    
    func giftWasSelected(gift: GiftItem) {
        guard let user = parentMessage.fromUser else { return }
        router.showGiftSendPage(gift: gift, selectedUser: user)
    }
    
    func presentPaymentView(paymentTypeId: Int, entityId: Int) {
        router.presentPaymentPage(paymentTypeId: paymentTypeId, entityId: entityId)
    }
}

// MARK: -
// MARK: DialogsInteractorOutput
extension DialogsPresenter: DialogsInteractorOutput {    

    func messageWasDeleted(id: Int) {
        self.dialogs.removeAll { (dialog) -> Bool in
            return dialog.id == id
        }
        view.updateAfterDeleteMessage(dialogsForTableView, parentMessage)
    }
    
    func returnSendedMessage(message: Message) {
        self.dialogs.append(message)
        view.setupInitialState(dialogsForTableView, parentMessage)
    }
    
    func returnGiftsList(_ categories: [GiftsCategories]) {
        view.setGiftsCategories(categories)
    }
    
    func appendNextPage(dialog: [Message], headers: [AnyHashable : Any]) {
        setHeaders(HttpHeaders.prepareHeaders(headers))
        
        self.dialogs.insert(contentsOf: dialog, at: 0)
//        self.dialogs.insert(dialog, at: 0)
        
        view.appendPreviousMessage(dialogsForTableView)
    }
}

// MARK: -
// MARK: DialogsModuleInput
extension DialogsPresenter: DialogsModuleInput {

    func configure(_ dialogs: ChatMessages, pictures: [String], _ headers: [AnyHashable : Any]) {
        if let messages = dialogs.messages {
            self.dialogs = messages.reversed()
        }
        if let dialog = dialogs.dialog {
            self.parentMessage = dialog
        }

//        self.parentMessage = parentMessage
        setHeaders(HttpHeaders.prepareHeaders(headers))
    }
    
    func newConfigureDialog(user: User) {
        let dialogs = [Message]()
        let blankMsg = Dialog.init(user: user)
        self.dialogs = dialogs
        self.parentMessage = blankMsg
    }

}

extension DialogsPresenter {    
    
    fileprivate func converting(dialogFromServer: [Message]) -> [[Message]] {
        var results = [[Message]]()

        let groupedDialogs = Dictionary(grouping: dialogFromServer) { (element) -> String in
            if let last_update = element.datetime {
                return last_update.toyyyyMMdd()
            } else {
                return "" // исключительная ситуация, бэк всегда должен возвращать last_update для сообщения
            }
        }
        
        let sortedKeys = groupedDialogs.keys.sorted()
        sortedKeys.forEach { (key) in
            let values = groupedDialogs[key]
            results.append(values ?? [])
        }
        
        return results
    }

    func updateBadgesCounterTabBar(vc: UIViewController) {
        if let badge = vc.tabBarController?.children[1] {
            DispatchQueue.main.async {
                NetworkRequest().loadBadges(completion: { (badges) in
                    if badges.messages == 0 {
                        badge.tabBarItem.badgeValue = nil
                    } else {
                        badge.tabBarItem.badgeValue = badges.messages.toString()
                    }
                })
            }
        }
    }
    
}

extension DialogsPresenter {
    func setHeaders(_ headers: [XPaginationHeaders : Int]) {
        guard let current = headers[XPaginationHeaders.XPaginationCurrent],
            let isEnd = headers[XPaginationHeaders.XPaginationIsEnd], let limit = headers[XPaginationHeaders.XPaginationLimit],
            let total = headers[XPaginationHeaders.XPaginationTotal] else { return }
        paginationCurrent = current
        paginationIsEnd = isEnd
        paginationLimit = limit
        paginationTotal = total
    }
    
}
