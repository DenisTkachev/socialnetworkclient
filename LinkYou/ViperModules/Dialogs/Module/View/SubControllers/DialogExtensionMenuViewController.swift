//
//  DialogExtensionMenuViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 11/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import Photos

class DialogExtensionMenuViewController: UIViewController {
    
    weak var delegate: ExtentionmenuActionType!
    
    let arr_img = NSMutableArray()
    @IBOutlet weak var segmentControl: HBSegmentedControl!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    struct Constants {
        static let offset: CGFloat = 200
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(updateView), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ExtentionMenuPhotosCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ExtentionMenuPhotosCollectionCell")
        collectionView.register(UINib(nibName: "MapExtensionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MapExtensionCollectionViewCell")
        collectionView.register(UINib(nibName: "GiftsMessageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GiftsMessageCollectionViewCell")
        
        view.frame = CGRect(x: 0, y: Constants.offset, width: view.frame.width, height: view.frame.height)
        
        setupSegmetControl()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadPhotoGallery{}
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate.hideExtMenu()
    }
    
    func setupSegmetControl() {
        let segmentedControl = BMSegmentedControl.init(
            withIcon: CGRect(x: 0, y: 0, width: segmentControl.frame.width, height: segmentControl.frame.height),
            items: ["", "", ""],
            icons: [UIImage(named: "messagePhotoOn")!, UIImage(named: "messageGeoPointOn")!, UIImage(named: "messageGiftOn")!],
            selectedIcons: [UIImage(named: "messagePhotoOffBlue")!, UIImage(named: "messageGeoPointOnBlue")!, UIImage(named: "messageGiftOffBlue")!],
            backgroundColor: UIColor.white,
            thumbColor: UIColor.veryLightBlue,
            textColor: UIColor.paleGrey,
            selectedTextColor: UIColor.cerulean,
            orientation: ComponentOrientation.leftRight)
        
        segmentedControl.selectedIndex = 0
        
        segmentedControl.addTarget(self, action: #selector(self.switchMenuPress(_:)), for: .valueChanged)
        segmentControl.addSubview(segmentedControl)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemWidth = layout.collectionView!.bounds.width
            let itemHeight = layout.collectionView!.bounds.height
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
            layout.invalidateLayout()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //MARK: Анимация при появлении
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    //    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
    //
    //        let translation = recognizer.translation(in: self.view)
    //        let velocity = recognizer.velocity(in: self.view)
    //
    //        let y = self.view.frame.minY
    //        if (y + translation.y >= Constants.offset) {
    //            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
    //            recognizer.setTranslation(CGPoint.zero, in: self.view)
    //        }
    //
    //        if recognizer.state == .ended {
    //            UIView.animate(withDuration: 0.2, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] in
    //                guard let unwrappedSelf = self else { return }
    //                print(velocity.y)
    //
    //                if velocity.y > 0 {
    //                    unwrappedSelf.delegate?.hideExtMenu()
    //                }
    //                }, completion: nil )
    //        }
    //    }
    
    @IBAction func switchMenuPress(_ sender: BMSegmentedControl) {
        let indexPath = IndexPath(row: 0, section: sender.selectedIndex)
        activityIndicator.startAnimating()
        collectionView.isHidden = true
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
    }
    
    //    @IBAction func switchMenuPress(_ sender: UISegmentedControl) {
    //        let indexPath = IndexPath(row: 0, section: sender.selectedSegmentIndex)
    //        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
    //
    //    }
    
    @IBAction func hideExtMenuPress(_ sender: RoundButtonCustom) {
        delegate.hideExtMenu()
    }
    
    @objc func updateView() {
//        loadPhotoGallery {  // только фотографии
//            let indexPath = IndexPath(row: 0, section: 0)
//            self.collectionView.reloadItems(at: [indexPath])
//        }
        delegate.hideExtMenu()
    }
}


extension DialogExtensionMenuViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExtentionMenuPhotosCollectionCell", for: indexPath) as! ExtentionMenuPhotosCollectionCell
            cell.setPhotos(arr_img)
            cell.masterDelegate = delegate
            return cell
            
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MapExtensionCollectionViewCell", for: indexPath) as! MapExtensionCollectionViewCell
            cell.masterDelegate = delegate
            return cell
            
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftsMessageCollectionViewCell", for: indexPath) as! GiftsMessageCollectionViewCell
            let colors: [UIColor] = [.red, .green, .yellow, .blue]
            cell.backgroundColor = colors[indexPath.section]
            cell.masterDelegate = delegate
            
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        collectionView.isHidden = false
        activityIndicator.stopAnimating()
    }
    
}

extension DialogExtensionMenuViewController {
    
    private func loadPhotoGallery(completion: @escaping ()->()) {
        
        DispatchQueue.main.async {
            self.arr_img.removeAllObjects()
            let allPhotosOptions : PHFetchOptions = PHFetchOptions.init()
            allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            let allPhotosResult = PHAsset.fetchAssets(with: .image, options: allPhotosOptions)
            allPhotosResult.enumerateObjects({ (asset, idx, stop) in
                
                self.arr_img.add(asset)
                
            })
            completion()
            self.collectionView.reloadData()
        }
    }
}

extension DialogExtensionMenuViewController: PaymentPageVCType {
    func showPaymentView(paymentTypeId: Int, entityId: Int) {
        delegate.showPaymentView(paymentTypeId: paymentTypeId, entityId: entityId)
    }
 
}
