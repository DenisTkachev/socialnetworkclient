//
//  PhotoViewerViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PhotoViewerViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topCounterPhotos: UIBarButtonItem!
    
//    var selectedIndex: Int!
    var photosUrls: [String]!
    let user: User!
    
    init(photosUrl: [String], user: User) {
        self.photosUrls = photosUrl
//        self.selectedIndex = selectedIndex
        self.user = user
        super.init(nibName: "PhotoViewerViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "PhotoGalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoGalleryCollectionViewCell")
        setupSwipe()
    }

    override func viewWillAppear(_ animated: Bool) {
        topCounterPhotos.title = "1 из \(photosUrls.count)"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemWidth = view.bounds.width
            let itemHeight = layout.collectionView!.bounds.height
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
//            layout.scrollDirection = .horizontal
            layout.invalidateLayout()
        }
    }
    
    func setupSwipe() {
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        downSwipe.direction = .down
        self.view.addGestureRecognizer(downSwipe)
        
    }
    
    @IBAction func pressCloseBtn(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        var startLocation = CGPoint()
        if (sender.state == UIGestureRecognizer.State.began) {
            startLocation = sender.location(in: self.view)
        }
        else if (sender.state == UIGestureRecognizer.State.ended) {
            let stopLocation = sender.location(in: self.view)
            let dx = stopLocation.x - startLocation.x
            let dy = stopLocation.y - startLocation.y
            let distance = sqrt(dx*dx + dy*dy )
            
            if distance > 300 {
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
}


extension PhotoViewerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosUrls.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoGalleryCollectionViewCell", for: indexPath) as! PhotoGalleryCollectionViewCell
        if let url = URL(string: "https:\(photosUrls[indexPath.row])") {
            cell.photoImageView.sd_setImage(with: url, completed: nil)
        }
        
        return cell
    }
    
}
