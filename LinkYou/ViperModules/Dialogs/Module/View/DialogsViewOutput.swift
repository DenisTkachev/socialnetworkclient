//
//  DialogsDialogsViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

protocol DialogsViewOutput: class {
    /// - author: Denis Tkachev
    func viewIsReady()
    func sendMessage(recipientId: Int, message: String, pictures: [String])
    func removeMessage(messageId: Int)
    func loadGiftsList()
    func sendPhoto(recipientId: Int, photo: UIImage)
    func extractCooedinates(_ dialog : Message) -> (Double, Double, String, String)
    func requestNextPage()
    func gotoUserProfile(_ user: User)
    func updateBadgesCounterTabBar(vc: UIViewController)
    func updateChat(notification: NSNotification)
    func messageWasRead(_ dialogId: Int)
    func giftWasSelected(gift: GiftItem)
    func presentPaymentView(paymentTypeId: Int, entityId: Int)
}
