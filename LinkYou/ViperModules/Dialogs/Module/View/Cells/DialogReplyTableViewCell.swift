//
//  DialogReplyTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 22/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class DialogReplyTableViewCell: UITableViewCell {

    @IBOutlet weak var picImageView: UIImageView!
    @IBOutlet weak var userFioLabel: UILabel!
    @IBOutlet weak var userLastMessageLabel: UILabel!
    @IBOutlet weak var userNewMessageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
