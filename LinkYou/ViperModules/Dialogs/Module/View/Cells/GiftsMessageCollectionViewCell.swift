//
//  GiftsMessageCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 15/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import DeviceKit

class GiftsMessageCollectionViewCell: UICollectionViewCell {
    
    weak var masterDelegate: ExtentionmenuActionType! {
        didSet {
            loadGifts()
        }
    }
    
    var giftCategories = [GiftsCategories]() {
        didSet {
            collectionViewBody.reloadData()
        }
    }
    
    var selectedIndex = IndexPath(row: 0, section: 0)
    var queue = OperationQueue()
    
    let groupOfAllowedDevices: [DeviceKit.Device] = [.iPhone6, .iPhone6s, .iPhoneSE, .simulator(.iPhone6), .simulator(.iPhone6s), .simulator(.iPhoneSE)]
    let device = Device()
    
    @IBOutlet weak var collectionViewHeader: UICollectionView!
    @IBOutlet weak var collectionViewBody: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionViewHeader.register(UINib(nibName: "GiftMenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GiftMenuCollectionViewCell")
        collectionViewHeader.delegate = self
        collectionViewHeader.dataSource = self
        
        collectionViewBody.register(UINib(nibName: "GiftCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GiftCollectionViewCell")
        collectionViewBody.delegate = self
        collectionViewBody.dataSource = self
        
//        if let layout = collectionViewBody.collectionViewLayout as? UICollectionViewFlowLayout {
//            let itemWidth = CGFloat(160)
//            let itemHeight = CGFloat(160)
//            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
//            layout.invalidateLayout()
//
//        }
        
        setupSwipe()
    }
    
    func setupSwipe() {
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        leftSwipe.direction = .left
        collectionViewBody.addGestureRecognizer(rightSwipe)
        collectionViewBody.addGestureRecognizer(leftSwipe)
    }
    
    func loadGifts() {
        giftCategories = masterDelegate.getGiftsCategory()
    }
    
}


extension GiftsMessageCollectionViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewHeader {
            selectedIndex = indexPath
            let currentRow = collectionView.cellForItem(at: indexPath) as! GiftMenuCollectionViewCell
            currentRow.lineSelector.isHidden = false
            collectionViewHeader.reloadData()
            collectionViewBody.reloadData()
        } else {
            // подарок выбран
            let i = selectedIndex.row
            guard let gift = giftCategories[i].items?[indexPath.row] else { return }
            if let freeGifts = SuperUser.shared.currentUser?.gifts?.free_gifts, freeGifts > 0 {
                // есть бесплатные подарки
                masterDelegate.giftWasSelected(gift: gift)
            } else {
                // надо оплатить
                let _ = PopUpPaymentVCStep1.init(tizerType: .gift, giftID: gift.id)
                // если подарок успешно оплачен, то метод
                let resumeAfterPay = BlockOperation(block: {
                    self.masterDelegate.allowPresentSendPage(gift)
                })
                queue.isSuspended = true
                queue.addOperation(resumeAfterPay)
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewHeader {
            if let prevSelectRow = collectionView.cellForItem(at: indexPath) as? GiftMenuCollectionViewCell {
                prevSelectRow.lineSelector.isHidden = true
            }
        }
    }
    
}

extension GiftsMessageCollectionViewCell {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == collectionViewHeader {
            return 1
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewHeader {
            return giftCategories.count
        } else {
            if giftCategories.count > 0, let items = giftCategories[selectedIndex.row].items {
                return items.count
            } else {
                return 0
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewHeader {
            let cell = collectionViewHeader.dequeueReusableCell(withReuseIdentifier: "GiftMenuCollectionViewCell", for: indexPath) as! GiftMenuCollectionViewCell
            if indexPath.row == selectedIndex.row {
                cell.lineSelector.isHidden = false
            }
            cell.giftCategoryName.text = giftCategories[indexPath.row].name!
            return cell
            
        } else {
            let cell = collectionViewBody.dequeueReusableCell(withReuseIdentifier: "GiftCollectionViewCell", for: indexPath) as! GiftCollectionViewCell
            let currentCatategory = giftCategories[selectedIndex.row]
            if let currentItems = currentCatategory.items?[indexPath.row], let price = currentItems.cost {
                cell.giftPriceLabel.text = "\(price.toString()) руб."
            }
            if let url = currentCatategory.items?[indexPath.row].picture {
                //                print(url)
                cell.giftImageView.sd_setImage(with: URL(string: url)) { (image, error, cache, url) in
                }
            }
            return cell
        }
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        if sender.state == .ended {
            switch sender.direction {
            case .right:
                selectedIndex = updateIndexCategory(category: IndexPath(row: selectedIndex.row - 1, section: selectedIndex.section))
                collectionViewBody.reloadData()
                collectionViewHeader.reloadData()
                collectionViewHeader.scrollToItem(at: selectedIndex, at: .centeredHorizontally, animated: true)
                
            case .left:
                selectedIndex = updateIndexCategory(category: IndexPath(row: selectedIndex.row + 1, section: selectedIndex.section))
                collectionViewBody.reloadData()
                collectionViewHeader.reloadData()
                collectionViewHeader.scrollToItem(at: selectedIndex, at: .centeredHorizontally, animated: true)
                
            default: break
            }
        }
    }
    
    private func updateIndexCategory(category: IndexPath) -> IndexPath {
        var result = IndexPath(row: 0, section: 0)
        
        if category.row == giftCategories.count {
            return IndexPath(row: giftCategories.count - 1, section: 0)
        } else if category.row < 0 {
            result = IndexPath(row: 0, section: 0)
        } else if category.row > 0, category.row <= giftCategories.count - 1 {
            result = category
        }
        return result
    }
}

extension GiftsMessageCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewHeader {
            let wordWidth = giftCategories[indexPath.row].name!.widthOfString(usingFont: UIFont.systemFont(ofSize: 13, weight: .semibold))
            return CGSize(width: wordWidth + 8, height: 40)
        } else {
            if device.isOneOf(groupOfAllowedDevices) { // small screen
                return CGSize(width: 90, height: 90)
            } else {
                return CGSize(width: 110, height: 110)
            }
        }

    }
}
