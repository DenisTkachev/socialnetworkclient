//
//  PhotoCellCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 12/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PhotoCellCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
