//
//  ExtentionMenuPhotosCollectionCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 12/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import Photos
import DeviceKit

class ExtentionMenuPhotosCollectionCell: UICollectionViewCell {
    
    weak var masterDelegate: ExtentionmenuActionType!
    let groupOfAllowedDevices: [DeviceKit.Device] = [.iPhoneSE, .simulator(.iPhoneSE)]
    let device = Device()
    
    @IBOutlet weak var insideCollectionView: UICollectionView!
    var arr_img = NSMutableArray()
    override func awakeFromNib() {
        super.awakeFromNib()
        
        insideCollectionView.delegate = self
        insideCollectionView.dataSource = self
        
        insideCollectionView.register(UINib(nibName: "PhotoCellCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCellCollectionViewCell")
        setLayout()

    }
    
    func setPhotos(_ photos: NSMutableArray) {
        self.arr_img = photos
        setLayout()
        insideCollectionView.reloadData()
    }
    
    private func setLayout() {
        if let layout = insideCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let widthView = contentView.bounds.width
            var itemWidth:CGFloat = 0
            var itemHeight:CGFloat = 0
            if device.isOneOf(groupOfAllowedDevices) {
                itemWidth = widthView
                itemHeight = widthView
            } else {
                itemWidth = (widthView / 2) - 5
                itemHeight = CGFloat(130)
            }
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
            layout.invalidateLayout()
        }
    }
}

extension ExtentionMenuPhotosCollectionCell: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_img.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCellCollectionViewCell", for: indexPath) as! PhotoCellCollectionViewCell
        cell.image.image = self.getAssetThumbnail(asset: self.arr_img.object(at: indexPath.row) as! PHAsset, size: 150)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let image = self.getAssetThumbnail(asset: self.arr_img.object(at: indexPath.row) as! PHAsset, size: 1024)
        masterDelegate.addPhotoToDialog(image)
    }
    
    private func getAssetThumbnail(asset: PHAsset, size: CGFloat) -> UIImage {
        let retinaScale = UIScreen.main.scale
        let retinaSquare = CGSize(width: size * retinaScale, height: size * retinaScale)//CGSizeMake(size * retinaScale, size * retinaScale)
        let cropSizeLength = min(asset.pixelWidth, asset.pixelHeight)
        let square = CGRect(x: 0, y: 0, width: cropSizeLength, height: cropSizeLength)//CGRectMake(0, 0, CGFloat(cropSizeLength), CGFloat(cropSizeLength))
        let cropRect = square.applying(CGAffineTransform(scaleX: 1.0/CGFloat(asset.pixelWidth), y: 1.0/CGFloat(asset.pixelHeight)))
        
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        var thumbnail = UIImage()
        
        options.isSynchronous = true
        options.deliveryMode = .highQualityFormat
        options.resizeMode = .exact
        options.normalizedCropRect = cropRect
        
        manager.requestImage(for: asset, targetSize: retinaSquare, contentMode: .aspectFit, options: options, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        
        return thumbnail
    }
}
