//
//  ChatImageCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 26/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class ChatImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var fadeView: UIView!
    @IBOutlet weak var morePhotoLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
}
