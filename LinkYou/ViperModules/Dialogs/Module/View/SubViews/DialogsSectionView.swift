//
//  DialogsSectionView.swift
//  LinkYou
//
//  Created by Denis Tkachev on 09/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class DateHeaderlabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.paleGreyTwo
        textColor = UIColor.steelTwo
        textAlignment = .center
        translatesAutoresizingMaskIntoConstraints = false
        font = UIFont.systemFont(ofSize: 10)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override var intrinsicContentSize: CGSize {
        let originalContentSize = super.intrinsicContentSize
        let height = originalContentSize.height + 10  
        layer.cornerRadius = height / 2
        layer.masksToBounds = true
        return CGSize(width: originalContentSize.width + 20, height: height)
    }
    
}
