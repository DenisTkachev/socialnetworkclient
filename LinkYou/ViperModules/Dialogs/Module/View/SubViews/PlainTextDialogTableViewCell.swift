//
//  PlainTextDialogTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07.10.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PlainTextDialogTableViewCell: UITableViewCell, AnyDialogCellType {
    
    weak var tableViewDelegate: DialogCellActionType!
    
    let messageLabel = UILabel()
    let bubbleBackgroundView = UIView()
    let timeLabel = UILabel()
    let msgImageView = UIImageView()
    
    var trailingConstr: NSLayoutConstraint!
    var leadingConstr: NSLayoutConstraint!
    var timeLabelIncomConstr: NSLayoutConstraint!
    var timeLabelOutComConstr: NSLayoutConstraint!
    
    var leftTimeLabelConstr: NSLayoutConstraint!
    var rightTimeLabelConstr: NSLayoutConstraint!
    
    var message: Message! {
        didSet {
            let isIncome = SuperUser.shared.superUser?.id != message.user?.id
            bubbleBackgroundView.backgroundColor = isIncome ? UIColor.paleGreyTwo : UIColor.veryLightBlue
            if isIncome { // входящие
                
                leadingConstr.isActive = true
                trailingConstr.isActive = false
                
                timeLabelOutComConstr.isActive = false
                timeLabelIncomConstr.isActive = true
                
                leftTimeLabelConstr.isActive = false
                rightTimeLabelConstr.isActive = true
                
            } else { // исходящие
                leadingConstr.isActive = false
                trailingConstr.isActive = true
                
                timeLabelOutComConstr.isActive = true
                timeLabelIncomConstr.isActive = false
                
                leftTimeLabelConstr.isActive = true
                rightTimeLabelConstr.isActive = false
                
                if message.read ?? true {
                    messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
                } else {
                    messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
                }
            }
            messageLabel.textColor = UIColor.gunmetal
            
            if let msg = message.comment {
                messageLabel.text = msg
            }
            //            msgImageView.contentMode = .scaleAspectFit
            timeLabel.text = message.datetime?.toDateHHmm()
            timeLabel.layer.cornerRadius = 6
            timeLabel.clipsToBounds = true
            
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = .clear
        bubbleBackgroundView.addGestureRecognizer(getGestureRecognize())
        bubbleBackgroundView.layer.cornerRadius = 20
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(bubbleBackgroundView)
        messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        
        addSubview(messageLabel)
        messageLabel.numberOfLines = 0
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(timeLabel)
        timeLabel.numberOfLines = 1
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.backgroundColor = .white
        timeLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        timeLabel.textColor = UIColor.steelTwo
        timeLabel.textAlignment = .center
        
        msgImageView.addGestureRecognizer(getGestureRecognize())
        addSubview(msgImageView)
        msgImageView.translatesAutoresizingMaskIntoConstraints = false
        msgImageView.clipsToBounds = true
        msgImageView.isUserInteractionEnabled = true
        
        let constraints = [
            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            
            bubbleBackgroundView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -15),
            bubbleBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -15),
            bubbleBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 15),
            bubbleBackgroundView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 15),
            
            timeLabel.widthAnchor.constraint(equalToConstant: 32),
            timeLabel.heightAnchor.constraint(equalToConstant: 20),
            timeLabel.centerYAnchor.constraint(equalTo: bubbleBackgroundView.centerYAnchor),
            
            ]
        
        NSLayoutConstraint.activate(constraints)
        
        leadingConstr = messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30)
        leadingConstr.isActive = false
        
        trailingConstr = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30)
        trailingConstr.isActive = true
        
        timeLabelOutComConstr = timeLabel.trailingAnchor.constraint(equalTo: bubbleBackgroundView.leadingAnchor, constant: -8)
        timeLabelOutComConstr.isActive = true
        
        timeLabelIncomConstr = timeLabel.leadingAnchor.constraint(equalTo: bubbleBackgroundView.trailingAnchor, constant: 8)
        timeLabelIncomConstr.isActive = false
        
        leftTimeLabelConstr = timeLabel.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 8)
        leftTimeLabelConstr.isActive = true
        
        rightTimeLabelConstr = timeLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -8)
        rightTimeLabelConstr.isActive = false
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(msg: Message) {
        self.message = msg
    }
    
    func update() {
        messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
    
    private func getGestureRecognize() -> UILongPressGestureRecognizer {
        var panRecognizer = UILongPressGestureRecognizer()
        panRecognizer = UILongPressGestureRecognizer (target: self, action: #selector(showContextMenu(_:)))
        return panRecognizer
    }
    
    @objc func showContextMenu(_ sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began, message.user?.id == SuperUser.shared.superUser?.id {
            let position = sender.location(in: superview)
            let centerMessage = bubbleBackgroundView.center
            var center: CGPoint {
                get { // для того чтобы меню не выходило за границы экрана
                    if centerMessage.x < 100 {
                        return CGPoint(x: 100, y: position.y)
                    } else {
                        return CGPoint(x: centerMessage.x, y: position.y)
                    }
                }
            }
            tableViewDelegate.showContextMenu(position: center, messageId: message.id!, indexPath: self.getIndexPath())
        }
    }
    
    override func prepareForReuse() {
        tableViewDelegate.removeContextMenu()
    }

    
}


