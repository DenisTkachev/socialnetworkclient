//
//  ImagesTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 25/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class ImagesTableViewCell: UITableViewCell, ChatGalleryType {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var leftConstr: NSLayoutConstraint!
    @IBOutlet weak var rightConstr: NSLayoutConstraint!
    
    var message: Message!
    weak var tableViewDelegate: DialogCellActionType!
    
    lazy var h = innerView.frame.height / 2
    lazy var w = innerView.frame.width / 2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        collectionView.register(UINib(nibName: "ChatImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChatImageCollectionViewCell")
        
        outerView.clipsToBounds = true
        outerView.layer.cornerRadius = 20
        
        innerView.clipsToBounds = true
        innerView.layer.cornerRadius = 20
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setConstr(income: Bool) {
        if income {
            leftConstr.constant = 15
            rightConstr.constant = 0
        } else {
            leftConstr.constant = 0
            rightConstr.constant = 15
        }
        self.updateConstraints()
    }
    
    func setupCell(msg: Message) {
        self.message = msg
        let isIncome = SuperUser.shared.superUser?.id != message.user?.id
        setConstr(income: isIncome)
        
        outerView.backgroundColor = isIncome ? UIColor.paleGreyTwo : UIColor.veryLightBlue
        innerView.backgroundColor = isIncome ? UIColor.paleGreyTwo : UIColor.veryLightBlue
        collectionView.reloadData()
    }
    
    
}

extension ImagesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatImageCollectionViewCell", for: indexPath) as! ChatImageCollectionViewCell
        if let src = message.pictures?[indexPath.row] {
            cell.imageView.sd_setImage(with: URL(string: "http:\(src)"), placeholderImage: nil, completed: { (image, error, cache, url) in
                let resizedImage = image?.cropToBounds(image: image!, width: 106, height: 106)
                cell.imageView.image = resizedImage
            })
        }
        
        if indexPath.row == 3, message.pictures!.count > 4 {
            cell.morePhotoLabel.text = "+ \(message.pictures!.count - 4)"
            cell.fadeView.isHidden = false
        } else {
            cell.morePhotoLabel.text = ""
            cell.fadeView.isHidden = true
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selfIndexPath = self.getIndexPath() else { return }
        tableViewDelegate.showPhotoOnFullScreen(selfIndexPath)
    }

}

extension ImagesTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: w - 5, height: h - 5)
        
    }
}
