//
//  TextImageTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class TextImageTableViewCell: UITableViewCell, AnyDialogCellType {
        
        weak var tableViewDelegate: DialogCellActionType!
    
        let messageLabel = UILabel()
        let msgImageView = UIImageView()
        let timeLabel = UILabel()
        let bubbleBackgroundView = UIView()
    
        var leadingConstrImage: NSLayoutConstraint!
        var trailingConstrImage: NSLayoutConstraint!
        
        var imageWidthConstr: NSLayoutConstraint!
        var imageHeightConstr: NSLayoutConstraint!
        
        var timeLabelIncomConstr: NSLayoutConstraint!
        var timeLabelOutComConstr: NSLayoutConstraint!
    
        var trailingConstr: NSLayoutConstraint!
        var leadingConstr: NSLayoutConstraint!
    
        var leftTimeLabelConstr: NSLayoutConstraint!
        var rightTimeLabelConstr: NSLayoutConstraint!
    
        var message: Message! {
            didSet {
                let isIncome = SuperUser.shared.superUser?.id != message.user?.id
                bubbleBackgroundView.backgroundColor = isIncome ? UIColor.paleGreyTwo : UIColor.veryLightBlue
                if isIncome {
                    timeLabelOutComConstr.isActive = false
                    timeLabelIncomConstr.isActive = true
                    leadingConstrImage.isActive = true
                    trailingConstrImage.isActive = false
                    leadingConstr.isActive = true
                    trailingConstr.isActive = false
                } else {
                    timeLabelOutComConstr.isActive = true
                    timeLabelIncomConstr.isActive = false
                    leadingConstrImage.isActive = false
                    trailingConstrImage.isActive = true
                    leadingConstr.isActive = false
                    trailingConstr.isActive = true
                    
                    if message.read ?? true {
                        messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
                    } else {
                        messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
                    }
                }
                
                if let msg = message.comment {
                    messageLabel.text = msg
                }
                
                msgImageView.contentMode = .scaleAspectFill
                
                if let pictures = message.pictures, pictures.count == 1 {
                    let singleImgURL = URL(string: "http:\(pictures.first!)")
                    msgImageView.sd_setImage(with: singleImgURL) { (image, error, cache, url) in
                        self.imageWidthConstr.constant = image?.size.width ?? 0 //250
                        self.imageHeightConstr.constant = 250
                        self.msgImageView.image = image
                        
                    }
                } else {
                    imageWidthConstr.constant = 0
                    imageHeightConstr.constant = 0
                    
                }
                timeLabel.text = message.datetime?.toDateHHmm()
                timeLabel.layer.cornerRadius = 6
                timeLabel.clipsToBounds = true
                
            }
        }
        
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            selectionStyle = .none
            backgroundColor = .clear
            bubbleBackgroundView.addGestureRecognizer(getGestureRecognize())
            bubbleBackgroundView.layer.cornerRadius = 20
            bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(bubbleBackgroundView)
            
            messageLabel.textColor = UIColor.gunmetal
            messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            addSubview(messageLabel)
            messageLabel.numberOfLines = 0
            messageLabel.translatesAutoresizingMaskIntoConstraints = false
            
            addSubview(timeLabel)
            timeLabel.numberOfLines = 1
            timeLabel.translatesAutoresizingMaskIntoConstraints = false
            timeLabel.backgroundColor = .white
            timeLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
            timeLabel.textColor = UIColor.steelTwo
            timeLabel.textAlignment = .center
            
            msgImageView.addGestureRecognizer(getGestureRecognize())
            addSubview(msgImageView)
            msgImageView.translatesAutoresizingMaskIntoConstraints = false
            msgImageView.clipsToBounds = true
            msgImageView.isUserInteractionEnabled = true
            
            let constraints = [
                msgImageView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
//                msgImageView.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
                msgImageView.bottomAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -8),
                
                bubbleBackgroundView.topAnchor.constraint(equalTo: msgImageView.topAnchor, constant: -8),
                bubbleBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -8),
                bubbleBackgroundView.trailingAnchor.constraint(equalTo: msgImageView.trailingAnchor, constant: 0),
                bubbleBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 4),
                
                timeLabel.widthAnchor.constraint(equalToConstant: 32),
                timeLabel.heightAnchor.constraint(equalToConstant: 20),
                timeLabel.centerYAnchor.constraint(equalTo: bubbleBackgroundView.centerYAnchor),
              
                messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
                messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
                messageLabel.trailingAnchor.constraint(equalTo: msgImageView.trailingAnchor, constant: -8),
                messageLabel.leftAnchor.constraint(equalTo: msgImageView.leftAnchor, constant: 0)
                ]
            
            NSLayoutConstraint.activate(constraints)
            
            leadingConstr = messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32)
            leadingConstr.isActive = false
            
            trailingConstr = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
            trailingConstr.isActive = true
            
            timeLabelOutComConstr = timeLabel.trailingAnchor.constraint(equalTo: bubbleBackgroundView.leadingAnchor, constant: -8)
            timeLabelOutComConstr.isActive = true

            timeLabelIncomConstr = timeLabel.leadingAnchor.constraint(equalTo: bubbleBackgroundView.trailingAnchor, constant: 8)
            timeLabelIncomConstr.isActive = false
            
            leftTimeLabelConstr = timeLabel.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 0)
            leftTimeLabelConstr.isActive = true
            
            rightTimeLabelConstr = timeLabel.trailingAnchor.constraint(greaterThanOrEqualTo: trailingAnchor, constant: 0)
            rightTimeLabelConstr.isActive = true
            
            leadingConstrImage = msgImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32)
            leadingConstrImage.isActive = false
            
            trailingConstrImage = msgImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
            trailingConstrImage.isActive = true
            
            imageWidthConstr = msgImageView.widthAnchor.constraint(equalToConstant: 250)
            imageWidthConstr.isActive = true
            imageHeightConstr = msgImageView.heightAnchor.constraint(equalToConstant: 250)
            imageHeightConstr.isActive = true
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setupCell(msg: Message) {
            self.message = msg
        }
    
        func update() {
            messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        }
    
        private func getGestureRecognize() -> UILongPressGestureRecognizer {
            var panRecognizer = UILongPressGestureRecognizer()
            panRecognizer = UILongPressGestureRecognizer (target: self, action: #selector(showContextMenu(_:)))
            return panRecognizer
        }
        
        @objc func showContextMenu(_ sender: UILongPressGestureRecognizer) {
            if sender.state == UIGestureRecognizer.State.began, message.user?.id == SuperUser.shared.superUser?.id {
                let position = sender.location(in: superview)
                let centerMessage = bubbleBackgroundView.center
                var center: CGPoint {
                    get { // для того чтобы меню не выходило за границы экрана
                        if centerMessage.x < 100 {
                            return CGPoint(x: 100, y: position.y)
                        } else {
                            return CGPoint(x: centerMessage.x, y: position.y)
                        }
                    }
                }
                tableViewDelegate.showContextMenu(position: center, messageId: message.id!, indexPath: self.getIndexPath())
            }
        }
        
        override func prepareForReuse() {
            tableViewDelegate.removeContextMenu()
            msgImageView.image = nil
        }
        
//        func getIndexPath() -> IndexPath? {
//            let superView = self.superview as? UITableView
//            let index = superView?.indexPath(for: self)
//            return index
//        }
    
    }

