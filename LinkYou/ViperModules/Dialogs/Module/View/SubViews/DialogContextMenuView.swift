//
//  DialogContextMenuView.swift
//  LinkYou
//
//  Created by Denis Tkachev on 09/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class DialogContextMenuView: UIView {
    
    let buttonDelete = UIButton()
    let delimiter = UIView()
//    let buttonReply = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .black
        clipsToBounds = true
        layer.cornerRadius = 10
        
        // TODO: на будущие задел
//        addSubview(buttonReply)
//        buttonReply.translatesAutoresizingMaskIntoConstraints = false
//        buttonReply.backgroundColor = .clear
//        buttonReply.setTitle("Ответить", for: .normal)
        
        addSubview(buttonDelete)
        buttonDelete.translatesAutoresizingMaskIntoConstraints = false
        buttonDelete.backgroundColor = .clear
        buttonDelete.setTitle("Удалить", for: .normal)
        
        let constraints = [

//            buttonReply.centerYAnchor.constraint(equalTo: centerYAnchor),
//            buttonReply.leftAnchor.constraint(equalTo: leftAnchor, constant: 4),
//            buttonReply.rightAnchor.constraint(equalTo: buttonDelete.leftAnchor, constant: 8),
           
//            buttonDelete.leftAnchor.constraint(equalTo: buttonReply.rightAnchor, constant: 8),
            buttonDelete.centerXAnchor.constraint(equalTo: centerXAnchor),
            buttonDelete.centerYAnchor.constraint(equalTo: centerYAnchor),
//            buttonDelete.rightAnchor.constraint(equalTo: rightAnchor, constant: 8)
        ]
        NSLayoutConstraint.activate(constraints)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
