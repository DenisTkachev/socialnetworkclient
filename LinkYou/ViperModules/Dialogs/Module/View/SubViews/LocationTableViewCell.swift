//
//  LocationTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import MapKit

class LocationTableViewCell: UITableViewCell, AnyDialogCellType {
    
    weak var tableViewDelegate: DialogCellActionType!
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    
//    let myTextLabel = UILabel()
    let myTextButton = UIButton()
    let bubbleBackgroundView = UIView()
    let timeLabel = UILabel()
    
    var trailingConstr: NSLayoutConstraint!
    var leadingConstr: NSLayoutConstraint!
    var timeLabelIncomConstr: NSLayoutConstraint!
    var timeLabelOutComConstr: NSLayoutConstraint!
    
    var message: Message! {
        didSet {
            let isIncome = SuperUser.shared.superUser?.id != message.user?.id
            bubbleBackgroundView.backgroundColor = isIncome ? UIColor.paleGreyTwo : UIColor.veryLightBlue
            if isIncome {
                leadingConstr.isActive = true
                trailingConstr.isActive = false
                
                timeLabelOutComConstr.isActive = false
                timeLabelIncomConstr.isActive = true
                
            } else {
                leadingConstr.isActive = false
                trailingConstr.isActive = true
                
                timeLabelOutComConstr.isActive = true
                timeLabelIncomConstr.isActive = false
                
                if message.read ?? true {
//                    myTextLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
                } else {
//                    myTextLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
                }
            }
//            myTextLabel.textColor = UIColor.cerulean
            
            timeLabel.text = message.datetime?.toDateHHmm()
            timeLabel.layer.cornerRadius = 6
            timeLabel.clipsToBounds = true

        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        myTextButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        myTextButton.setTitle("Показать точку на карте", for: .normal)
        myTextButton.setTitleColor(UIColor.cerulean, for: .normal)
        myTextButton.addTarget(self, action: #selector(openNavigator(_:)), for: .touchUpInside)
        let longTapToMap = UILongPressGestureRecognizer(target: self, action: #selector(showContextMenu(_:)))
        
        selectionStyle = .none
        backgroundColor = .clear
        bubbleBackgroundView.layer.cornerRadius = 8
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(bubbleBackgroundView)
        
        myTextButton.addGestureRecognizer(longTapToMap)
        addSubview(myTextButton)
        myTextButton.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(timeLabel)
        timeLabel.numberOfLines = 1
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.backgroundColor = .white
        timeLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        timeLabel.textColor = UIColor.steelTwo
        timeLabel.textAlignment = .center
        
        let geoImage = UIImageView(image: #imageLiteral(resourceName: "messageGeoPointOnBlue"))
        geoImage.translatesAutoresizingMaskIntoConstraints = false
        geoImage.contentMode = .scaleAspectFit
        addSubview(geoImage)
        
        let constraints = [
            
            myTextButton.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            myTextButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2),
            myTextButton.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
            myTextButton.leadingAnchor.constraint(equalTo: geoImage.trailingAnchor, constant: 8),
            myTextButton.heightAnchor.constraint(equalToConstant: 40),
            bubbleBackgroundView.topAnchor.constraint(equalTo: myTextButton.topAnchor, constant: -2),
            bubbleBackgroundView.leadingAnchor.constraint(equalTo: geoImage.leadingAnchor, constant: -15),
            bubbleBackgroundView.bottomAnchor.constraint(equalTo: myTextButton.bottomAnchor, constant: 2),
            bubbleBackgroundView.trailingAnchor.constraint(equalTo: myTextButton.trailingAnchor, constant: 15),
            
            timeLabel.widthAnchor.constraint(equalToConstant: 32),
            timeLabel.heightAnchor.constraint(equalToConstant: 20),
            timeLabel.centerYAnchor.constraint(equalTo: bubbleBackgroundView.centerYAnchor),
            
            geoImage.topAnchor.constraint(equalTo: timeLabel.topAnchor)
            
            ]
        
        NSLayoutConstraint.activate(constraints)
        
        leadingConstr = geoImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32)
        leadingConstr.isActive = false
        
        trailingConstr = myTextButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
        trailingConstr.isActive = true
        
        timeLabelOutComConstr = timeLabel.trailingAnchor.constraint(equalTo: bubbleBackgroundView.leadingAnchor, constant: -8)
        timeLabelOutComConstr.isActive = true
        
        timeLabelIncomConstr = timeLabel.leadingAnchor.constraint(equalTo: bubbleBackgroundView.trailingAnchor, constant: 8)
        timeLabelIncomConstr.isActive = false
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(msg: Message, lat: Double, lon: Double, userMsg: String, address: String) {
        message = msg
        latitude = lat
        longitude = lon
        if userMsg.isEmpty {
            myTextButton.setTitle(address, for: .normal) //TODO: С сервера приходит &quot и поэтому нет актуальной подписи. Андрей обещал исправить.
        } else {
            myTextButton.setTitle(userMsg, for: .normal)
        }
    }
    
    func update() {
//        messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
    
    @objc func openNavigator(_ sender: UIButton!) {
        guard let latitude = latitude, let longitude = longitude else { return }
        let regionDistance: CLLocationDistance = 10
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion.init(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Точка"
        mapItem.openInMaps(launchOptions: options)
    }
    
    @objc func showContextMenu(_ sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began, message.user?.id == SuperUser.shared.superUser?.id {
            let position = sender.location(in: superview)
            let centerMessage = bubbleBackgroundView.center
            var center: CGPoint {
                get { // ограничить меню шириной экрана
                    if centerMessage.x < 100 {
                        return CGPoint(x: 100, y: position.y)
                    } else {
                        return CGPoint(x: centerMessage.x, y: position.y)
                    }
                }
            }
            tableViewDelegate.showContextMenu(position: center, messageId: message.id!, indexPath: self.getIndexPath())
        }
    }
    
    override func prepareForReuse() {
        tableViewDelegate.removeContextMenu()
    }
    
}

