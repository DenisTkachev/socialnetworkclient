//
//  ImageTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell, AnyDialogCellType, ChatGalleryType {

    weak var tableViewDelegate: DialogCellActionType!
    let msgImageView = UIImageView()
    let timeLabel = UILabel()
    let bubbleBackgroundView = UIView()
    
    var leadingConstrImage: NSLayoutConstraint!
    var trailingConstrImage: NSLayoutConstraint!
    
    var imageWidthConstr: NSLayoutConstraint!
    var imageHeightConstr: NSLayoutConstraint!
    
    var timeLabelIncomConstr: NSLayoutConstraint!
    var timeLabelOutComConstr: NSLayoutConstraint!
    
    var leftTimeLabelConstr: NSLayoutConstraint!
    var rightTimeLabelConstr: NSLayoutConstraint!
    
    var message: Message! {
        didSet {
            let isIncome = SuperUser.shared.superUser?.id != message.user?.id
            bubbleBackgroundView.backgroundColor = isIncome ? UIColor.paleGreyTwo : UIColor.veryLightBlue
            if isIncome {
                timeLabelOutComConstr.isActive = false
                timeLabelIncomConstr.isActive = true
                leadingConstrImage.isActive = true
                trailingConstrImage.isActive = false
                leftTimeLabelConstr.isActive = false
                rightTimeLabelConstr.isActive = true
            } else {
                timeLabelOutComConstr.isActive = true
                timeLabelIncomConstr.isActive = false
                leadingConstrImage.isActive = false
                trailingConstrImage.isActive = true
                leftTimeLabelConstr.isActive = true
                rightTimeLabelConstr.isActive = false
            }
//            msgImageView.contentMode = .center //.scaleToFill//.scaleAspectFit
            if let pictures = message.pictures, pictures.count == 1 {
                msgImageView.isHidden = true
                let singleImgURL = URL(string: "http:\(pictures.first!)")
                msgImageView.sd_setImage(with: singleImgURL) { (image, error, cache, url) in
//                    self.imageHeightConstr.constant = 250
                    guard let image = image else { return }
                    
                    if image.size.width > self.imageWidthConstr.constant {
//                        self.imageWidthConstr.constant = UIScreen.main.bounds.width - 40
                        self.msgImageView.image = image.resizedImage(newSize: CGSize(width: self.imageHeightConstr.constant, height: self.imageWidthConstr.constant))
                    } else {
//                        self.imageWidthConstr.constant = image.size.width
                        self.msgImageView.image = image
                    }
                    
                    self.layoutIfNeeded()
                    self.msgImageView.layer.cornerRadius = self.bubbleBackgroundView.layer.cornerRadius
                    self.msgImageView.isHidden = false
                }
            } else {
                imageWidthConstr.constant = 0
                imageHeightConstr.constant = 0
                timeLabel.backgroundColor = UIColor.clear
            }
            timeLabel.text = message.datetime?.toDateHHmm()
            timeLabel.layer.cornerRadius = 6
            timeLabel.clipsToBounds = true

        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        var panRecognizer = UILongPressGestureRecognizer()
        panRecognizer = UILongPressGestureRecognizer (target: self, action: #selector(showContextMenu(_:)))
        panRecognizer.minimumPressDuration = 0.3
        panRecognizer.delegate = self
        
        
        var panRecognizer2 = UITapGestureRecognizer()
        panRecognizer2 = UITapGestureRecognizer (target: self, action: #selector(showFullPhoto(_:)))
        panRecognizer2.delegate = self
        panRecognizer2.numberOfTapsRequired = 1
        
        self.addGestureRecognizer(panRecognizer)
        self.addGestureRecognizer(panRecognizer2)
        
        selectionStyle = .none
        backgroundColor = .clear
//        bubbleBackgroundView.addGestureRecognizer(getGestureRecognize())
        bubbleBackgroundView.layer.cornerRadius = 20
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bubbleBackgroundView)
        
        addSubview(timeLabel)
        timeLabel.numberOfLines = 1
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.backgroundColor = .white
        timeLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        timeLabel.textColor = UIColor.steelTwo
        timeLabel.textAlignment = .center
        
//        msgImageView.addGestureRecognizer(getTapGestureRecognize())
        msgImageView.isUserInteractionEnabled = true
        
        addSubview(msgImageView)
        msgImageView.translatesAutoresizingMaskIntoConstraints = false
        msgImageView.clipsToBounds = true
        
        let constraints = [
            msgImageView.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            msgImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            //msgImageView.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
            
            bubbleBackgroundView.heightAnchor.constraint(equalTo: msgImageView.heightAnchor, constant: 26),
            bubbleBackgroundView.widthAnchor.constraint(equalTo: msgImageView.widthAnchor, constant: 26),
            bubbleBackgroundView.centerXAnchor.constraint(equalTo: msgImageView.centerXAnchor, constant: 0),
            bubbleBackgroundView.centerYAnchor.constraint(equalTo: msgImageView.centerYAnchor, constant: 0),
            
            timeLabel.widthAnchor.constraint(equalToConstant: 32),
            timeLabel.heightAnchor.constraint(equalToConstant: 20),
            timeLabel.centerYAnchor.constraint(equalTo: bubbleBackgroundView.centerYAnchor),
            
            ]
        
        NSLayoutConstraint.activate(constraints)
        
        timeLabelOutComConstr = timeLabel.trailingAnchor.constraint(equalTo: bubbleBackgroundView.leadingAnchor, constant: -8)
        timeLabelOutComConstr.isActive = true

        timeLabelIncomConstr = timeLabel.leadingAnchor.constraint(equalTo: bubbleBackgroundView.trailingAnchor, constant: 8)
        timeLabelIncomConstr.isActive = false
        
        leftTimeLabelConstr = timeLabel.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 8)
        leftTimeLabelConstr.isActive = true
        
        rightTimeLabelConstr = timeLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -8)
        rightTimeLabelConstr.isActive = true
        
        leadingConstrImage = msgImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30)
        leadingConstrImage.isActive = false

        trailingConstrImage = msgImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30)
        trailingConstrImage.isActive = true

        imageWidthConstr = msgImageView.widthAnchor.constraint(equalToConstant: 250)
        imageWidthConstr.isActive = true
        imageHeightConstr = msgImageView.heightAnchor.constraint(equalToConstant: 250)
        imageHeightConstr.isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(msg: Message) {
        self.message = msg
    }
    
    func update() {
//        messageLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
    
    private func getTapGestureRecognize() -> UITapGestureRecognizer {
        var panRecognizer = UITapGestureRecognizer()
        panRecognizer = UITapGestureRecognizer (target: self, action: #selector(showFullPhoto(_:)))
        
        return panRecognizer
    }
    
    @objc func showFullPhoto (_ sender: UITapGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.ended {
            guard let selfIndexPath = self.getIndexPath() else { return }
            self.tableViewDelegate.showPhotoOnFullScreen(selfIndexPath)
        }
    }
    
    private func getGestureRecognize() -> UILongPressGestureRecognizer {
        var panRecognizer = UILongPressGestureRecognizer()
        panRecognizer = UILongPressGestureRecognizer (target: self, action: #selector(showContextMenu(_:)))
        panRecognizer.delegate = self
        return panRecognizer
    }
    
    @objc func showContextMenu(_ sender: UILongPressGestureRecognizer) {
        
        if sender.state == UIGestureRecognizer.State.began, message.user?.id == SuperUser.shared.superUser?.id {
            let position = sender.location(in: superview)
            let centerMessage = bubbleBackgroundView.center
            var center: CGPoint {
                get { // для того чтобы меню не выходило за границы экрана
                    if centerMessage.x < 100 {
                        return CGPoint(x: 100, y: position.y)
                    } else {
                        return CGPoint(x: centerMessage.x, y: position.y)
                    }
                }
            }
            tableViewDelegate.showContextMenu(position: center, messageId: message.id!, indexPath: self.getIndexPath())
        }
    }
    
    override func prepareForReuse() {
        tableViewDelegate.removeContextMenu()
        msgImageView.image = nil
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true // нужно для того чтобы не срабатывали оба рекогнайзера на одно действие
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool { // чтобы не было конфликта между 2 рекогнайзерами

        if (gestureRecognizer is UILongPressGestureRecognizer || gestureRecognizer is UITapGestureRecognizer) {
            return true
        } else {
            return false
        }
    }
    
}
