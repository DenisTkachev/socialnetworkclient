//
//  DialogsDialogsViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol DialogsViewInput: class {
  /// - author: Denis Tkachev
    func setupInitialState(_ dialogs: [[Message]], _ parentMessage: Dialog)
    func updateAfterDeleteMessage(_ dialogs: [[Message]], _ parentMessage: Dialog)
    func setGiftsCategories(_ categories: [GiftsCategories])
    func appendPreviousMessage(_ dialogs: [[Message]])
}
