//
//  DialogsDialogsViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit

protocol ChatGalleryType: class {
    var message: Message! { get set }
}

protocol AnyDialogCellType: class {
    var message: Message! { get set }
    func update()
}

protocol ExtentionmenuActionType: class {
    func showExtMenu()
    func hideExtMenu()
    func addPhotoToDialog(_ image: UIImage)
    func getGiftsCategory() -> [GiftsCategories]
    func addCoordinatesToDialog(lat: Double, lot: Double, title: String)
    func giftWasSelected(gift: GiftItem)
    func allowPresentSendPage(_ gift: GiftItem)
    func showPaymentView(paymentTypeId: Int, entityId: Int)
}

protocol DialogCellActionType: class {
    func showContextMenu(position: CGPoint, messageId: Int, indexPath: IndexPath?)
    func removeContextMenu()
    func showPhotoOnFullScreen(_ index: IndexPath)
}

//protocol ContextMenuBtnActionType: class { // TODO: на будущие DialogReplyTableViewCell
//    func deleteMessage(messageId: Int)
//    func replyMessage(messageId: Int)
//}

final class DialogsViewController: UIViewController, ExtentionmenuActionType {
    
    // MARK: -
    // MARK: Properties
    var output: DialogsViewOutput!
    let cellPlainTextNibName = "PlainTextDialogTableViewCell"
    let cellImageNibName = "ImageTableViewCell"
    let cellTextImageNibName = "TextImageTableViewCell"
    let cellLocationNibName = "LocationTableViewCell"
    let cellImagesNibName = "ImagesTableViewCell"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendMsgBtn: UIImageView!
//    @IBOutlet weak var messageTextField: TextFieldCustom!
    @IBOutlet weak var messageInputHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendMsgIndicator: UIActivityIndicatorView!
    @IBOutlet weak var stickerBtn: UIButton!
    @IBOutlet weak var keyBoardPlusBtn: UIButton!
    
    var dialogs = [[Message]]()
    var parentMessage: Dialog?
    var cellForDelete: IndexPath?
    let blackView = UIView()
    var isShowingExtensionMenu = false
    var giftCategories: [GiftsCategories]?
    var isProgress = false
    
    private lazy var bottomViewController: DialogExtensionMenuViewController = {
        var viewController = DialogExtensionMenuViewController(nibName: String(describing: DialogExtensionMenuViewController.self), bundle: nil)
        viewController.delegate = self
        viewController.modalPresentationStyle = .overCurrentContext
        return viewController
    }()
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        setupTableView()
        setupNotification()
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton

        output.loadGiftsList()
        setupMessageTextView()
        output.updateBadgesCounterTabBar(vc: self)
        
    }
    
    @objc private func executeNotification(notification: NSNotification) {
        switch notification.name {
        case NSNotification.Name.socketMessageNew:
            output.updateChat(notification: notification)
            
        case NSNotification.Name.socketMessageIsRead: // собеседник прочитал наше сообщение
            output.updateChat(notification: notification) 
            
        default:
            break
        }
    }
    
    func setupMessageTextView() {
        messageTextView.delegate = self
        messageTextView.clipsToBounds = true
        messageTextView.layer.cornerRadius = 20
        messageTextView.text = "Напишите сообщение"
        messageTextView.textColor = UIColor.steelTwo
        messageTextView.textContainerInset = UIEdgeInsets(top: 10, left: 8, bottom: 12, right: 2)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let buttonController = self.presentedViewController as? DialogExtensionMenuViewController {
            isShowingExtensionMenu = false
            buttonController.dismiss(animated: false, completion: nil)
        }
    }
    
    func setupTableView() {
        tableView.register(PlainTextDialogTableViewCell.self, forCellReuseIdentifier: cellPlainTextNibName)
        tableView.register(ImageTableViewCell.self, forCellReuseIdentifier: cellImageNibName)
        tableView.register(TextImageTableViewCell.self, forCellReuseIdentifier: cellTextImageNibName)
        tableView.register(LocationTableViewCell.self, forCellReuseIdentifier: cellLocationNibName)
        tableView.register(UINib(nibName: "ImagesTableViewCell", bundle: nil), forCellReuseIdentifier: "ImagesTableViewCell")
        
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    func setupNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(executeNotification(notification:)), name: .socketMessageNew, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(executeNotification(notification:)), name: .socketMessageIsRead, object: nil)
        
        let sendMsgAction = UITapGestureRecognizer(target: self, action: #selector(tappedSendMsg(_:)))
        sendMsgAction.numberOfTapsRequired = 1
        sendMsgBtn.isUserInteractionEnabled = true
        sendMsgBtn.addGestureRecognizer(sendMsgAction)
        
    }
    
    @objc func keyboardHandle(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            bottomViewConstraint.constant = isKeyboardShowing ? frame.height - 50 : 0
            
            if isKeyboardShowing {
                stickerBtn.isHidden = true
                keyBoardPlusBtn.isHidden = true
                messageTextView.layoutIfNeeded()
            } else if messageTextView.text.isEmpty {
                messageTextView.text = "Напишите сообщение"
                messageTextView.textColor = UIColor.steelTwo
                messageTextView.textContainerInset = UIEdgeInsets(top: 10, left: 8, bottom: 12, right: 2)
                stickerBtn.isHidden = false
                keyBoardPlusBtn.isHidden = false
            } else {
                stickerBtn.isHidden = false
                keyBoardPlusBtn.isHidden = false
            }
            
            UIView.animate(withDuration: 0, animations: {
                self.view.layoutIfNeeded()
            }) { (completed) in
                if isKeyboardShowing, self.dialogs.count > 0 {
                    let indexPath = IndexPath(row: (self.dialogs.last?.count ?? 0) - 1, section: self.dialogs.count - 1)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    @objc func tappedSendMsg(_ recognizer: UITapGestureRecognizer) {
        if let text = messageTextView.text, text.count > 0, let userId = parentMessage?.fromUser?.id, text != "Напишите сообщение" {
            msgLoading(showIndicator: true)
            output.sendMessage(recipientId: userId, message: text, pictures: [])
            messageTextView.text = nil
        }
    }
    
    fileprivate func msgLoading(showIndicator: Bool) {
        if showIndicator {
            sendMsgIndicator.startAnimating()
            sendMsgBtn.isHidden = showIndicator
        } else {
            sendMsgIndicator.stopAnimating()
            sendMsgBtn.isHidden = showIndicator
        }
    }
    
    @objc func tapToDeleteMsg(_ sender: UITapGestureRecognizer) {
        let msgId = sender.view!.tag
        output.removeMessage(messageId: msgId)
    }
    
    @IBAction func addStickerBtn(_ sender: UIButton) {
       let _ = StickersViewController.init(delegate: self)
    }
    
    @IBAction func showExtensionMenuBtn(_ sender: UIButton) {
        if isShowingExtensionMenu { // скрыть меню
            hideExtMenu()
        } else { // показать меню
            showExtMenu()
        }
    }
    
    func showExtMenu() {
        isShowingExtensionMenu = true
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            self.view.addSubview(blackView)
            blackView.frame = window.frame
            blackView.alpha = 0
            UIView.animate(withDuration: 0.3) {
                self.blackView.alpha = 1
            }
        }
        present(bottomViewController, animated: true, completion: nil)
        
    }
    
    func hideExtMenu() {
        if let buttonController = self.presentedViewController as? DialogExtensionMenuViewController {
            isShowingExtensionMenu = false
            buttonController.dismiss(animated: true, completion: nil)
            UIView.animate(withDuration: 0.3) {
                self.blackView.alpha = 0
                self.blackView.removeFromSuperview()
            }
        }
    }
    
    func addPhotoToDialog(_ image: UIImage ) {
        if let userId = parentMessage?.fromUser?.id {
            msgLoading(showIndicator: true)
            output.sendPhoto(recipientId: userId, photo: image)
        }
        hideExtMenu()
    }
    
    func loadGifts() {
        output.loadGiftsList()
    }
    
    func setGiftsCategories(_ categories: [GiftsCategories]) {
        giftCategories = categories
    }
    
    func getGiftsCategory() -> [GiftsCategories] {
        if let giftCategories = giftCategories {
            return giftCategories
        } else {
            output.loadGiftsList()
            return []
        }
    }
    
    func addCoordinatesToDialog(lat: Double, lot: Double, title: String) {
        if let userId = parentMessage?.fromUser?.id {
             let str = "<a class=\"compose__field-textarea-link\" href=\"https://yandex.ru/maps/?text=\(lat),\(lot)\" target=\"_blank\">\(title)</a>"
            output.sendMessage(recipientId: userId, message: str, pictures: [])
            hideExtMenu()
        }
    }
    
    func giftWasSelected(gift: GiftItem) {
        output.giftWasSelected(gift: gift)
    }
    
    func allowPresentSendPage(_ gift: GiftItem) {
        print("Оплачено и можно дарить подарок")
        output.giftWasSelected(gift: gift)
    }
    
    func showPaymentView(paymentTypeId: Int, entityId: Int) {
        // unload extensionMenu!
        self.hideExtMenu()
        output.presentPaymentView(paymentTypeId: paymentTypeId, entityId: entityId)
    }

}

// MARK: -
// MARK: DialogsViewInput
extension DialogsViewController: DialogsViewInput {    
    
    func setupInitialState(_ dialogs: [[Message]], _ parentMessage: Dialog) {
        guard let name = parentMessage.fromUser?.name else { return }
        self.parentMessage = parentMessage

        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
        titleView.backgroundColor = .clear
        titleView.isUserInteractionEnabled = true
        let label = UILabel(frame: titleView.frame)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.text = name
        label.textColor = .white
        titleView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false

        navigationItem.titleView = titleView
        label.centerXAnchor.constraint(equalTo: navigationItem.titleView!.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: navigationItem.titleView!.centerYAnchor).isActive = true
        navigationItem.titleView!.isUserInteractionEnabled = true
        navigationItem.titleView!.addTapGestureRecognizer(action: {
            guard let user = parentMessage.fromUser else { return }
            self.output.gotoUserProfile(user)
        })
        
        
        //        self.dialogs = dialogs.filter({ (dialog) -> Bool in
        //            return !dialog.message!.isEmpty // для тестирования убираем пустые сообщения
        //        })
        self.dialogs = dialogs
        
        msgLoading(showIndicator: false)
        tableView.reloadData {
            if dialogs.count > 0 {
                let indexPath = IndexPath(row: (dialogs.last?.count ?? 0) - 1, section: dialogs.count - 1)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
    
    func appendPreviousMessage(_ dialogs: [[Message]]) {
        self.dialogs = dialogs
        tableView.reloadData {
            //self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            self.isProgress = false
        }
    }
    
    func updateAfterDeleteMessage(_ dialogs: [[Message]], _ parentMessage: Dialog) {
        self.parentMessage = parentMessage
        self.dialogs = dialogs
        if let _ = cellForDelete {
            //            tableView.deleteRows(at: [cellForDelete], with: .right)
            tableView.reloadData()
        } else { // TODO: падает если в секции было одно сообщение и мы его удаляем
            // можно подменять текст на сообещние было удалено
            // или подумать что с секцией делать. Презентер удаляет секцию если там 0 сообщений, а View обновляет строку по indexPath не зная о том что секций стало на 1 меньше.
            tableView.reloadData() // при  reloadData не падает, т.к. обновляет и кол-во секций.
        }
        cellForDelete = nil
        removeContextMenu()
    }
}

extension DialogsViewController: UITableViewDelegate, UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if section == dialogs?.count {
//            let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width , height: 8))
//            view.backgroundColor = .white
//            return view
//        } else {
//            return UIView()
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.Magnitude.leastNonzeroMagnitude
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        guard let dialogs = dialogs else { return 0 }
        return dialogs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        guard let dialogs = dialogs else { return 0 }
        return dialogs[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard dialogs.indices.contains(indexPath.section) else { return UITableViewCell() }
        
        let dialog = dialogs[indexPath.section][indexPath.row]
        switch analizeCellType(dialog) {
        case .plainText:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellPlainTextNibName, for: indexPath) as! PlainTextDialogTableViewCell
            cell.setupCell(msg: dialog)
            cell.tableViewDelegate = self
            return cell
        case .image:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellImageNibName, for: indexPath) as! ImageTableViewCell
            cell.setupCell(msg: dialog)
            cell.tableViewDelegate = self

            return cell
        case .images:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellImagesNibName, for: indexPath) as! ImagesTableViewCell
            cell.setupCell(msg: dialog)
            cell.tableViewDelegate = self
            return cell
            
        case .textImage:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellTextImageNibName, for: indexPath) as! TextImageTableViewCell
            cell.setupCell(msg: dialog)
            cell.tableViewDelegate = self
            return cell
        case .coordinates:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellLocationNibName, for: indexPath) as! LocationTableViewCell
            let coordinates = output.extractCooedinates(dialog)
            cell.setupCell(msg: dialog, lat: coordinates.0, lon: coordinates.1, userMsg: coordinates.2, address: coordinates.3)
            cell.tableViewDelegate = self
            return cell
            
        case .empty:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        messageTextView.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let firstMessageInSection = dialogs[section].first {
            var dateString = ""
            if firstMessageInSection.datetime?.toyyyy() == firstMessageInSection.datetime?.currentYear() { // этот год
                dateString = firstMessageInSection.datetime?.todMMMM() ?? ""
            } else { // прошлый год
                dateString = firstMessageInSection.datetime?.toyyyyMMdd() ?? ""
            }
            
            let label = DateHeaderlabel()
            label.text = dateString
            
            let containerView = UIView()
            
            containerView.addSubview(label)
            label.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
//            label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: -8).isActive = true
            label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: 0).isActive = true
            
            return containerView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if dialogs.count > 0 {
            if tableView.contentOffset.y < 0 {
                if !isProgress {
                    output.requestNextPage()
                    isProgress = true
                }
            }
        }
        // message was read
        if let dialogCell = cell as? AnyDialogCellType, dialogCell.message.read == false, let dialogId = parentMessage?.id {
            output.messageWasRead(dialogId)
        }
        
    }
}

extension DialogsViewController: DialogCellActionType {
    
    func showPhotoOnFullScreen(_ index: IndexPath) {
        // MARK: show photo gallery
        let selectedCell = tableView.cellForRow(at: index)
        if let photoCell = selectedCell as? ChatGalleryType, let photos = photoCell.message.pictures, photos.count > 0, let user = photoCell.message.user {
            let photoViewerController = PhotoViewerViewController(photosUrl: photos, user: user)
            self.present(photoViewerController, animated: true)
        }
    }
    
    func showContextMenu(position: CGPoint, messageId: Int, indexPath: IndexPath?) {
        removeContextMenu()
        cellForDelete = indexPath
        let contextMenu = DialogContextMenuView(frame: CGRect(x: position.x - 80, y: position.y, width: 100, height: 36))
        let tapToBtn = UITapGestureRecognizer(target: self, action: #selector(DialogsViewController.tapToDeleteMsg(_:)))
        
        contextMenu.buttonDelete.addGestureRecognizer(tapToBtn)
        
        tapToBtn.view!.tag = messageId
        tableView.addSubview(contextMenu)
    }
    
    func removeContextMenu() {
        self.tableView.subviews.forEach { (subview) in
            if subview is DialogContextMenuView {
                subview.removeFromSuperview()
                UIView.animate(withDuration: 0.2, animations: {
                    self.tableView.layoutIfNeeded()
                })
            }
        }
    }
}

extension DialogsViewController {
    enum DialogCellType {
        case plainText
        case image
        case images
        case textImage
        case coordinates
        
        case empty
    }
    
    private func analizeCellType(_ dialog: Message) -> DialogCellType {
        guard let message = dialog.comment, let pictures = dialog.pictures else { return DialogCellType.empty }
        
        if message.count > 0 && pictures.count == 0 && !message.contains("yandex.ru/maps/?text=") {
            return .plainText
        } else if message.count == 0 && pictures.count == 1 {
            return .image
        } else if message.count == 0 && pictures.count > 0 {
            return .images
        } else if message.count > 0 && pictures.count > 0 && !message.contains("yandex.ru/maps/?text=") {
            return .textImage
        } else if message.contains("yandex.ru/maps/?text=") && pictures.count == 0 {
            return .coordinates
        } else {
            return .empty
        }
    }
}

extension DialogsViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Напишите сообщение" {
            textView.text = ""
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let size = CGSize(width: self.messageTextView.frame.width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        
        if textView.text.count > 0 {
            messageTextView.textColor = UIColor.gunmetal
        } else {
            messageTextView.textColor = UIColor.steelTwo
        }
        
        if estimatedSize.height > 100 {
            messageTextView.isScrollEnabled = true
        } else {
            messageTextView.isScrollEnabled = false
            messageInputHeightConstraint.constant = estimatedSize.height
        }
    }
}

extension DialogsViewController: StickersViewType {
    
    func returnSelectedSticker(sticker: UIImage) {
        if let userId = parentMessage?.fromUser?.id {
            msgLoading(showIndicator: true)
            output.sendPhoto(recipientId: userId, photo: sticker)
        }
    }
    
}
