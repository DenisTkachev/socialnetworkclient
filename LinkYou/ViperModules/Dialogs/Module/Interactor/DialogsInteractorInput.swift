//
//  DialogsDialogsInteractorInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol DialogsInteractorInput: class {
    func sendMessage(recipientId: Int, message: String, pictures: [String])
    func removeMessage(_ messageId: Int)
    func loadGiftsCategory()
    func uploadImageData(imageData: Data, recipientId: Int)
    func loadDialog(selectedMessage: Dialog, page: Int)
    func loadNewMessage(id: Int)
    func messageIsRead(dialogId: Int)
}
