//
//  DialogsDialogsInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class DialogsInteractor: DialogsInteractorInput {

    weak var output: DialogsInteractorOutput!
    
    let authPlagin = AccessTokenPlugin { DeviceCurrent.current.sessionToken }
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    
    func sendMessage(recipientId: Int, message: String, pictures: [String]) {
        if !Reachability.isConnectedToNetwork() {
            //output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            //TODO: Нужен обработчик события в чатике
            return
        }
        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.sendMessage(recipientId: recipientId, content: message, pictures: pictures)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let dialog = try JSONDecoder().decode(Message.self, from: response.data) // в случае успеха возвращает новое сообщение
                    self.output.returnSendedMessage(message: dialog)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func removeMessage(_ messageId: Int) {
        
        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.deleteMessage(id: messageId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if response["done"] == true { // message was deleted
                        self.output.messageWasDeleted(id: messageId)
                    } else {
                        print("!ошибка удаления сообщения!")
                        
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func messageIsRead(dialogId: Int) {
        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin])
        usersProvider.request(.messageIsReading(id: dialogId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    print(response) // сообщение прочитано
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadGiftsCategory() {        
        let usersProvider = MoyaProvider<GiftsService>(plugins: [authPlagin])
        usersProvider.request(.getCategoriesOfGifts) { (result) in
            switch result {
            case .success(let response):
                do {
                    let categories = try JSONDecoder().decode([GiftsCategories].self, from: response.data)
                    self.output.returnGiftsList(categories)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    // SEND image data and send image to user gallery
    func uploadImageData(imageData: Data, recipientId: Int) {
//        if !Reachability.isConnectedToNetwork() {
//            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
//            return
//        }
        
        let photosProvider = MoyaProvider<UploadPhotosService>(plugins: [debugPlagin, authPlagin])
        
        // send data image and get url
        photosProvider.request(.uploadUserPhoto(data: imageData)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userPhotos = try JSONDecoder().decode(ImageDataResponse.self, from: response.data)
                    if let photos = userPhotos.data {
                        let urls: [String] = photos.compactMap({ (photo) -> String? in
                            return photo.src!
                        })
                        self.sendMessage(recipientId: recipientId, message: "", pictures: urls)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }

    }
    
    func loadDialog(selectedMessage: Dialog, page: Int) {
        if !Reachability.isConnectedToNetwork() {
            //output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        guard let messageId = selectedMessage.id else { return }
        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin,debugPlagin])
        usersProvider.request(.getMessages(id: messageId, page: page)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let dialogs = try JSONDecoder().decode(ChatMessages.self, from: response.data)
                    if let httpResponse = response.response {
//                        print("***\(dialogs.count)")
                        let headers = httpResponse.allHeaderFields
                        if let message = dialogs.messages {
                            self.output.appendNextPage(dialog: message.reversed(), headers: headers)
                        }
                        
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadNewMessage(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            //output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin,debugPlagin])
        usersProvider.request(.getOneMessage(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let dialog = try JSONDecoder().decode(Message.self, from: response.data)
                    self.output.returnSendedMessage(message: dialog)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
