//
//  DialogsDialogsInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol DialogsInteractorOutput: class {
    func returnSendedMessage(message: Message)
    func messageWasDeleted(id: Int)
    func returnGiftsList(_ categories: [GiftsCategories])
    func appendNextPage(dialog: [Message], headers: [AnyHashable : Any])
}
