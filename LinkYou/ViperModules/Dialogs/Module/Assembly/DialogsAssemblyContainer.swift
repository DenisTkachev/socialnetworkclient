//
//  DialogsDialogsAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class DialogsAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(DialogsInteractor.self) { (r, presenter: DialogsPresenter) in
			let interactor = DialogsInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(DialogsRouter.self) { (r, viewController: DialogsViewController) in
			let router = DialogsRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(DialogsPresenter.self) { (r, viewController: DialogsViewController) in
			let presenter = DialogsPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(DialogsInteractor.self, argument: presenter)
			presenter.router = r.resolve(DialogsRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(DialogsViewController.self) { r, viewController in
			viewController.output = r.resolve(DialogsPresenter.self, argument: viewController)
		}
	}

}
