//
//  DialogsDialogsRouterInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol DialogsRouterInput: class {
    func presentUserPage(user: User, allUsers: [User])
    func showGiftSendPage(gift: GiftItem, selectedUser: User)
    func presentPaymentPage(paymentTypeId: Int, entityId: Int)
}
