//
//  DialogsDialogsRouter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/10/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class DialogsRouter: DialogsRouterInput {
    
    enum StorybordsID: String {
        case userPage = "UserpageSB"
        case giftPage = "GiftSendPageSB"
        case paymentPage = "PaymentPageSB"
    }
    
    weak var transitionHandler: TransitionHandler!
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func presentUserPage(user: User, allUsers: [User]) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .userPage), to: UserpageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(with: user, allUser: allUsers)
        }
    }
    
    func showGiftSendPage(gift: GiftItem, selectedUser: User) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .giftPage), to: GiftSendPageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(gift: gift, selectedUser: selectedUser)
        }
    }
    
    func presentPaymentPage(paymentTypeId: Int, entityId: Int) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .paymentPage), to: PaymentPageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.getPaymentUrl(paymentTypeId: paymentTypeId, entityId: entityId)
        }
    }
    
}
