//
//  UsersBlogsUsersBlogsInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class UsersBlogsInteractor: UsersBlogsInteractorInput {
    
    weak var output: UsersBlogsInteractorOutput!
    
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    func loadPopularBlogs() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
        
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.popularBlog) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([UserBlog].self, from: response.data)
                    self.output.popularBlogsGetted(serverResponse)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func sendClaimReport(_ data: ClaimReport) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<ClaimTicketService>(plugins: [authPlagin])
        
        usersProvider.request(.sendTicket(id: data.userId, title: data.header, message: data.body, entity: data.ticketType.rawValue)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Int].self, from: response.data)
                    guard let id = serverResponse["id"] else { return }
                    self.output.claimIdGeted(id: id)
                } catch let errorC {
                    if let errorLynkYou = errorC as? ErrorResponse {
                        do {
                            let bodyError = try JSONSerialization.data(withJSONObject: errorLynkYou, options: [])
                            print(bodyError)
                        } catch {
                            print(error)
                        }
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
