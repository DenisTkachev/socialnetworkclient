//
//  UsersBlogsUsersBlogsInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol UsersBlogsInteractorInput: class {
    func loadPopularBlogs()
    func sendClaimReport(_ data: ClaimReport)
}
