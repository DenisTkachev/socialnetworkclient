//
//  UsersBlogsUsersBlogsInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol UsersBlogsInteractorOutput: class {
    func popularBlogsGetted(_ blogs: [UserBlog])
    func claimIdGeted(id: Int)
    func showAlert(title: String, msg: String)
}
