//
//  UsersBlogsUsersBlogsViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol UsersBlogsViewInput: class {
  /// - author: KlenMarket
    func setupInitialState(_ blogs: [UserBlog])
    func showClaimIdAlert(id: Int)
    func showAlert(title: String, msg: String)
}
