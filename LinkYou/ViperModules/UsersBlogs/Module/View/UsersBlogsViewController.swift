//
//  UsersBlogsUsersBlogsViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

final class UsersBlogsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	// MARK: -
	// MARK: Properties
	var output: UsersBlogsViewOutput!
    var blogs: [UserBlog]?
    @IBOutlet weak var tableView: UITableView!
    var selectedUserId = 0
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    
    enum EventSheetType {
        case claim
        case delete
    }
    
    // MARK: -
	// MARK: Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
        title = "Блоги"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        setupTable()
	}
    
    override func viewWillAppear(_ animated: Bool) {
        output.viewIsReady()
    }

    private func setupTable() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "MyBlogBodyTableViewCell", bundle: nil), forCellReuseIdentifier: "MyBlogBodyTableViewCell")
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return blogs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBlogBodyTableViewCell") as! MyBlogBodyTableViewCell
        if let blogs = blogs {
            cell.setupCell(post: blogs[indexPath.section])
            if let id = blogs[indexPath.section].user?.id {
                cell.tag = id
            }
            cell.delegate = self

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let blogs = blogs, blogs.count > 0 {
            if let id = blogs[indexPath.section].id {
                output.choosePost(postId: id)
            }
        }
    }
    
    
}

// MARK: -
// MARK: UsersBlogsViewInput
extension UsersBlogsViewController: UsersBlogsViewInput {
    
    func setupInitialState(_ blogs: [UserBlog]) {
        self.blogs = blogs
        tableView.reloadData()
    }
    
    func showClaimIdAlert(id: Int) {
        HUD.hide()
        showAlert(title: "", msg: "Номер Вашей жалобы \(id)")
    }

}

extension UsersBlogsViewController {
    
    func showActionSheet() {
        
        guard let childViewControllers = parent?.children else { return }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for vc in childViewControllers {
            if vc is UsersBlogsViewController {
                let claim = UIAlertAction(title: "Пожаловаться", style: .default) { action in
                    self.actionSheetHandler(type: .claim)
                }
                actionSheet.addAction(claim)
            }
//            if vc is PersonalPageViewController {
//                let delete = UIAlertAction(title: "Удалить запись", style: .destructive) { action in
//                    self.actionSheetHandler(type: .delete)
//                }
//                actionSheet.addAction(delete)
//            }
            
        }
        
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func actionSheetHandler(type: EventSheetType) {
        switch type {
        case .claim:
            self.navigationController?.pushViewController(ClaimViewController(passDataDelegate: self, ticketType: .user), animated: true)
//            HUD.show(.progress)
        case .delete:
            break
        }
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension UsersBlogsViewController: CellBlogButtonActionHandlerType {
    
    func buttonPressed(cell: UITableViewCell, actionType: ActionBtnType) {
        switch cell {
        case is MyBlogBodyTableViewCell:
            if actionType == ActionBtnType.claimReport {
                selectedUserId = cell.tag
                showActionSheet()
            }
            if actionType == ActionBtnType.delete {
                if let cell = cell as? MyBlogBodyTableViewCell, cell.postId != 0 {
//                    self.selectedPost = cell.postId // TODO: какой пост удалить?
                    selectedUserId = cell.tag
                    showActionSheet()
                }
            }
            
        default:
            break
        }
    }
}

extension UsersBlogsViewController: ExternalActionHandlerType {
    func showPhotoAlbum() {
        
    }
    
    func pressButton(data: Any, actionType: UserpageViewController.ActionBtnType) {
        if let claimReport = data as? ClaimReport, actionType == .claimReport {
            claimReport.userId = selectedUserId
            output.sendClaimReport(claimReport)
            HUD.show(.progress)
        }
    }
}
