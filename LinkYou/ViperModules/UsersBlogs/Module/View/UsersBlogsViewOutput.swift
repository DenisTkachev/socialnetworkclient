//
//  UsersBlogsUsersBlogsViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol UsersBlogsViewOutput: class {
    /// - author: KlenMarket
    func viewIsReady()
    func choosePost(postId: Int)
    func sendClaimReport(_ data: ClaimReport)
}
