//
//  UsersBlogsUsersBlogsAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class UsersBlogsAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(UsersBlogsInteractor.self) { (r, presenter: UsersBlogsPresenter) in
			let interactor = UsersBlogsInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(UsersBlogsRouter.self) { (r, viewController: UsersBlogsViewController) in
			let router = UsersBlogsRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(UsersBlogsPresenter.self) { (r, viewController: UsersBlogsViewController) in
			let presenter = UsersBlogsPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(UsersBlogsInteractor.self, argument: presenter)
			presenter.router = r.resolve(UsersBlogsRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(UsersBlogsViewController.self) { r, viewController in
			viewController.output = r.resolve(UsersBlogsPresenter.self, argument: viewController)
		}
	}

}
