//
//  UsersBlogsUsersBlogsPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class UsersBlogsPresenter: UsersBlogsViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: UsersBlogsViewInput!
    var interactor: UsersBlogsInteractorInput!
    var router: UsersBlogsRouterInput!

    // MARK: -
    // MARK: UsersBlogsViewOutput
    func viewIsReady() {
        interactor.loadPopularBlogs()
    }
    
    func choosePost(postId: Int) {
        router.openPost(postId)
    }
    
    func sendClaimReport(_ data: ClaimReport) {
        interactor.sendClaimReport(data)
    }

}

// MARK: -
// MARK: UsersBlogsInteractorOutput
extension UsersBlogsPresenter: UsersBlogsInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func popularBlogsGetted(_ blogs: [UserBlog]) {
        view.setupInitialState(blogs)
    }
    
    func claimIdGeted(id: Int) {
        view.showClaimIdAlert(id: id)
    }


}

// MARK: -
// MARK: UsersBlogsModuleInput
extension UsersBlogsPresenter: UsersBlogsModuleInput {


}
