//
//  UsersBlogsUsersBlogsRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol UsersBlogsRouterInput: class {
    func openPost(_ postId: Int)
}
