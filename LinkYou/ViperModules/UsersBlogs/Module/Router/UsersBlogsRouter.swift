//
//  UsersBlogsUsersBlogsRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class UsersBlogsRouter: UsersBlogsRouterInput {
  weak var transitionHandler: TransitionHandler!
    
    enum StorybordsID: String {
        case viewBlogPost = "MyBlogViewPostSB"
    }
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func openPost(_ postId: Int) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .viewBlogPost), to: MyBlogViewPostModuleInput.self)
            // Set transition case.
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(postId)
        }
        // .perform()
    }
    
    
}
