//
//  UserpageUserpageInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol UserpageInteractorInput: class {
    func loadUserData(userID: Int)
    func loadUserPhotos(userID: Int, photoLimit: Int)
    func sendClaimReport(_ data: ClaimReport)
    func sendLike(_ userId: Int)
    func deleteUserFromBlackList(id: Int)
    func addUserFromBlackList(id: Int)
    func loadCounterForPhoto(_ idPhoto: Int)
    func addPhotoLike(_ photoId: Int)
    func getUserGifts(userID: Int)
    func sendCommentForPhoto(id: Int, text: String)
    func deletePhotoComment(_ commentId: Int, _ photoId: Int)
}
