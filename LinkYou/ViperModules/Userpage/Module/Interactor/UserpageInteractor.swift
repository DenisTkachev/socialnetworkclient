//
//  UserpageUserpageInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class UserpageInteractor: UserpageInteractorInput {

    weak var output: UserpageInteractorOutput!
    let dispatchGroup = DispatchGroup()
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    func deletePhotoComment(_ commentId: Int, _ photoId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.deletePhotoComment(commentId: commentId, photoId: photoId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    print(serverResponse)
                    self.loadCounterForPhoto(photoId)    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func sendCommentForPhoto(id: Int, text: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin])
        
        usersProvider.request(.addComment(id: id, comment: text)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    print(serverResponse)
                    //                    output
                    self.loadCounterForPhoto(id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addPhotoLike(_ photoId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.userPhotoLike(id: photoId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    self.loadCounterForPhoto(photoId)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadCounterForPhoto(_ idPhoto: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        //request first
        var likes: [PhotoLike] = []
        var comments: [PhotoComment] = []
        dispatchGroup.enter()
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])

        usersProvider.request(.userPhotoComments(id: idPhoto)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([PhotoComment].self, from: response.data)
                    comments = serverResponse
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // request second
        dispatchGroup.enter()
        usersProvider.request(.userPhotosLikes(id: idPhoto)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([PhotoLike].self, from: response.data)
                    likes = serverResponse
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
                
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        dispatchGroup.notify(queue: .main) {
            self.output.likesAndCommentGeted(likes, comments)
        }
    }
    
    func deleteUserFromBlackList(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        
        let usersProvider = MoyaProvider<ClaimTicketService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.deleteUserFromBl(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    print(serverResponse)
                    //                    self.output.claimIdGeted(id: id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addUserFromBlackList(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        
        let usersProvider = MoyaProvider<ClaimTicketService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.addUserToBL(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    print(serverResponse)
                    //                    self.output.claimIdGeted(id: id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func sendClaimReport(_ data: ClaimReport) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        
        let usersProvider = MoyaProvider<ClaimTicketService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.sendTicket(id: data.userId, title: data.header, message: data.body, entity: data.ticketType.rawValue)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Int].self, from: response.data)
                    guard let id = serverResponse["id"] else { return }
                    self.output.claimIdGeted(id: id)
                } catch let errorC {
                    if let errorLynkYou = errorC as? ErrorResponse {
                        do {
                            let bodyError = try JSONSerialization.data(withJSONObject: errorLynkYou, options: [])
                            print(bodyError)
                        } catch {
                            print(error)
                        }
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func sendLike(_ userId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }

        let usersProvider = MoyaProvider<LikesService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.userLike(id: userId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode(LikesCurrentUser.self, from: response.data)
                    self.loadUserData(userID: userId)
                    //self.output.userDataGeted(data: userFullData)
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadUserData(userID: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        // FULL DATA Request
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.userFullData(id: userID)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                    self.output.userDataGeted(data: userFullData)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadUserPhotos(userID: Int, photoLimit: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let photosProvider = MoyaProvider<PhotosService>(plugins: [authPlagin])        
        photosProvider.request(.userPhotos(id: userID, limit: photoLimit)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userPhotosUrl = try JSONDecoder().decode([UserPhotos].self, from: response.data)
                    self.output.userPhotoUrlGeted(data: userPhotosUrl)
                    // были случаи краша из-за отсутствия output когда несколько раз запускается этот метод (вышли и зашли на страницу)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getUserGifts(userID: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let photosProvider = MoyaProvider<GiftsService>(plugins: [authPlagin])
        photosProvider.request(.getUserGifts(userId: userID)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userGifts = try JSONDecoder().decode([UserGift].self, from: response.data)
                    if userGifts.count > 0 {
                      self.output.userGiftsGeted(data: userGifts)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
