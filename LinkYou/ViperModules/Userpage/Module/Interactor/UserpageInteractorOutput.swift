//
//  UserpageUserpageInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol UserpageInteractorOutput: class {
    func userDataGeted(data: User)
    func userPhotoUrlGeted(data: [UserPhotos])
    func claimIdGeted(id: Int)
    func likesAndCommentGeted(_ likes: [PhotoLike], _ comments: [PhotoComment])
    func userGiftsGeted(data: [UserGift])
    func showAlert(title: String, msg: String)
}
