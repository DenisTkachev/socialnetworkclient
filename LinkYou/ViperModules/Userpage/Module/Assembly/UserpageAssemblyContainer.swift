//
//  UserpageUserpageAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class UserpageAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(UserpageInteractor.self) { (r, presenter: UserpagePresenter) in
			let interactor = UserpageInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(UserpageRouter.self) { (r, viewController: UserpageViewController) in
			let router = UserpageRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(UserpagePresenter.self) { (r, viewController: UserpageViewController) in
			let presenter = UserpagePresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(UserpageInteractor.self, argument: presenter)
			presenter.router = r.resolve(UserpageRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(UserpageViewController.self) { r, viewController in
			viewController.output = r.resolve(UserpagePresenter.self, argument: viewController)
		}
	}

}
