//
//  UserpageUserpagePresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

final class UserpagePresenter: UserpageViewOutput {

    var userData: User?

    func showMessageDialog(with user: User) {
        router.presentDialogView(user)
    }
    
    func deleteComment(_ commentId: Int, _ photoId: Int) {
        interactor.deletePhotoComment(commentId, photoId)
    }
    
    func sendCommentForPhoto(id: Int, text: String) {
        interactor.sendCommentForPhoto(id: id, text: text)
    }
    
    func showGiftGallery(userId: User) {
        router.presentGiftGallery(userID: userId)
    }
    
    
    func showUserBlog(_ user: User) {
        router.presentUserBlog(user)
    }
    
    func sendPhotoLike(_ photoID: Int) {
        interactor.addPhotoLike(photoID)
    }
    
    func requestCountersGallery(_ idPhoto: Int) {
        return interactor.loadCounterForPhoto(idPhoto)
    }
    
    func deleteUserFromBL(id: Int) {
        interactor.deleteUserFromBlackList(id: id)
    }
    
    func addUserToBL(id: Int) {
        interactor.addUserFromBlackList(id: id)
    }
    
    func showSinglePost(_ id: Int) {
        router.showSinglePost(id)
    }
    
    func showUserPage(_ userId: Int) {
        router.presentUserPage(userId)
    }
    
    // MARK: -
    // MARK: Properties
    
    weak var view: UserpageViewInput!
    var interactor: UserpageInteractorInput!
    var router: UserpageRouterInput!
    
    var usersDaily: UsersDaily?
    var onlyView = false
    
    // MARK: -
    // MARK: UserpageViewOutput
    
    func sendClaimReport(_ data: ClaimReport) {
        interactor.sendClaimReport(data)
    }
    
    func sendUserLike(_ userID: Int) {
        interactor.sendLike(userID)
    }
    
    func pressMoreManOfDays(userId: Int) {
        interactor.loadUserData(userID: userId)
    }
    
    func loadUserPhotos(userId: Int, limit: Int) { // for Photo Gallery delegate
        interactor.loadUserPhotos(userID: userId, photoLimit: limit)
    }
    
    func showPhotoAlbum(_ photos: [UserPhotos], user: User?) {
        guard let user = user else { return }
        router.showPhotoAlbum(photos, user: user)
    }
    
    func updateUserData() {
        guard let id = userData?.id else { return }
        interactor.loadUserData(userID: id)
    }
}

// MARK: -
// MARK: UserpageInteractorOutput
extension UserpagePresenter: UserpageInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func userGiftsGeted(data: [UserGift]) {
        view.setUserGifts(data)
    }
    
    func likesAndCommentGeted(_ likes: [PhotoLike], _ comments: [PhotoComment]) {
        view.setCounters(likes: likes, comments: comments) // возврат установка на странице юзера лайков
    }
    
    func claimIdGeted(id: Int) {
        view.showClaimIdAlert(id: id)
    }
    
    func userPhotoUrlGeted(data: [UserPhotos]) {
        self.view.setUserPhoto(userPhotos: data)
    }
    
    func userDataGeted(data: User) {
        self.userData = data
        view.setUserData(cloudHeadTags: prepareCloudTags(data: data), tagsInterestings: prepareInterestingsTags(data: data))
        // загрузить фотосы
        guard let userId = data.id else { return }
        interactor.loadUserPhotos(userID: userId, photoLimit: data.photos_count ?? 0)
        
    }
}

// MARK: -
// MARK: UserpageModuleInput
extension UserpagePresenter: UserpageModuleInput {
    
    func setOnlyViewMode() {
        onlyView = true
    }
    
    func configure<T>(with data: T, allUser: [T]) {
        // TODO: Сделать абстрактно
        
        switch data {
        case is UserLikes:
            if let data = data as? UserLikes, let userID = data.id {
                interactor.loadUserData(userID: userID)
                interactor.getUserGifts(userID: userID)
            }
        case is UsersDaily:
            if let data = data as? UsersDaily, let allUser = allUser as? [UsersDaily], let userID = data.id {
                self.usersDaily = data
                interactor.loadUserData(userID: userID)
                interactor.getUserGifts(userID: userID)
                view.setAlluserOfDays(alluser: removeCurrentUserFromList(id: userID, allUser: allUser))
            }
            
        case is Favorite:
            if let data = data as? Favorite, let userID = data.id {
                interactor.loadUserData(userID: userID)
                interactor.getUserGifts(userID: userID)
            }
        case is Guest:
            if let data = data as? Guest, let userID = data.user?.id {
                interactor.loadUserData(userID: userID)
                interactor.getUserGifts(userID: userID)
            }
            
        case is Top100:
            if let data = data as? Top100, let userID = data.id {
                interactor.loadUserData(userID: userID)
                interactor.getUserGifts(userID: userID)
            }
            
        case is User:
            if let data = data as? User, let userID = data.id {
                interactor.loadUserData(userID: userID)
                interactor.getUserGifts(userID: userID)
            }
            
        case is Int:
            if let userID = data as? Int {
                interactor.loadUserData(userID: userID)
                interactor.getUserGifts(userID: userID)
            }
        default:
            print("Нет обработчика события!")
            break
        }
    }

}

extension UserpagePresenter {
    
    func removeCurrentUserFromList(id: Int, allUser: [UsersDaily]) -> [UsersDaily] {
        let userArray = allUser.filter( { $0.id != id } )
        return userArray
    }
    
    func prepareCloudTags(data: User) -> [TagCloudElement] {
        var cloudTags = [TagCloudElement]()
        if let zodiac = data.birthday?.zodiac?.name {
            cloudTags.append(TagCloudElement(text: zodiac, icon: nil))
        }
        if let goal = data.goal?.name {
            cloudTags.append(TagCloudElement(text: "Цель: \(goal)", icon: nil))
        }
        if let lookingFor = data.looking_for?.name, let from = data.age?.from, let to = data.age?.to {
            cloudTags.append(TagCloudElement(text: "Ищу \(lookingFor) от \(from) до \(to) лет", icon: nil))
        }
        if let nationality = data.nationality {
            if nationality.visibility?.id == 1 { // "Показывать мою национальность всем"
                cloudTags.append(TagCloudElement(text: "Национальность: \(nationality.name!)", icon: nil))
            }
        }
        if let religion = data.religion?.name {
            cloudTags.append(TagCloudElement(text: "Религия: \(religion)", icon: nil))
        }
        return cloudTags
    }
    
    func prepareInterestingsTags(data: User) -> [TagCloudElement] {
        var cloudTags = [TagCloudElement]()
        guard let interestings = data.interests?.interests else { return cloudTags }
        for element in interestings {
            if !element.isEmpty {
                cloudTags.append(TagCloudElement(text: element, icon: nil))
            }
        }
        return cloudTags
    }
}



