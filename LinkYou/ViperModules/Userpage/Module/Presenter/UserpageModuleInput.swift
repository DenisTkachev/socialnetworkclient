//
//  UserpageUserpageModuleInput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol UserpageModuleInput: class {
    func configure<T>(with data: T, allUser: [T])
    func setOnlyViewMode()
}
