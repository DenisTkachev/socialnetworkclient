//
//  UserpageUserpageViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

protocol CellButtonActionHandlerType: class {
    func buttonPressed(cell: UITableViewCell, actionType: UserpageViewController.ActionBtnType)
    func pressPayBtn(type: TizerType)
}

protocol ExternalActionHandlerType: class {
    func pressButton(data: Any, actionType: UserpageViewController.ActionBtnType)
    func showPhotoAlbum()
}

final class UserpageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PhotoGalleryDelegateType {
    
    enum ActionBtnType {
        case pressMoreBtn
        case addGiftBtn
        case pressShowMoreUserOfDay
        case showPhotoGallery
        case claimReport
        case sendLike
        case sendPhotoLike
        case showMoreBlog
        case addNewPost
        case zoomMainPhoto
        case showUserGifts
        case sendMessage
        case setAvatar
        case sendUpdateAvatar
        case showUserPage
        case showSelectedPost
    }
    
    enum EventSheetType {
        case blackList
        case claim
    }
    
    // MARK: -
    // MARK: Properties
    var output: UserpageViewOutput!
    
    @IBOutlet weak var tableView: UITableView!
//    var userData: User?
    var scrollPosition: CGFloat = 0
    var userId : Int {
        get {
            return output.userData?.id ?? 0
        }
    }
    var userPhotos = [UserPhotos]()
    var imageGiftsArray = [UIImage]()
    var tagsArray = [TagCloudElement()]
    var tagsInterestings = [TagCloudElement()]
    var userOfDay = [UsersDaily]()
    var userGifts = [UserGift]()
    var isOpenUserOfDay = false
    weak var photoGalleryController: PhotoGalleryViewController?
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.show(.progress, onView: self.view)
        tableView.isHidden = true
        setupTableView()
        setupNavigatioBar()
        if output.onlyView {
            self.view.backgroundColor = UIColor.cerulean
            self.view.addTapGestureRecognizer {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func setupNavigatioBar() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: "UserPageHeaderTVCell", bundle: nil), forCellReuseIdentifier: "UserPageHeaderTVCell")
        tableView.register(UINib(nibName: "UserpagePhotoTVCell", bundle: nil), forCellReuseIdentifier: "UserpagePhotoTVCell")
        tableView.register(UINib(nibName: "UserpageRatingTVCell", bundle: nil), forCellReuseIdentifier: "UserpageRatingTVCell")
        tableView.register(UINib(nibName: "UserPageGiftTableViewCell", bundle: nil), forCellReuseIdentifier: "UserPageGiftTableViewCell")
        tableView.register(UINib(nibName: "UserpageBlogTVCell", bundle: nil), forCellReuseIdentifier: "UserpageBlogTVCell")
        tableView.register(UINib(nibName: "UserPageAboutMeTVCell", bundle: nil), forCellReuseIdentifier: "UserPageAboutMeTVCell")
        tableView.register(UINib(nibName: "UserPageInterestsTVCell", bundle: nil), forCellReuseIdentifier: "UserPageInterestsTVCell")
        tableView.register(UINib(nibName: "UserPageEducationTVCell", bundle: nil), forCellReuseIdentifier: "UserPageEducationTVCell")
        tableView.register(UINib(nibName: "UserpageCareerTVCell", bundle: nil), forCellReuseIdentifier: "UserpageCareerTVCell")
        tableView.register(UINib(nibName: "UserpageFavBookTVCell", bundle: nil), forCellReuseIdentifier: "UserpageFavBookTVCell")
        tableView.register(UINib(nibName: "UserpageFavMusicTVCell", bundle: nil), forCellReuseIdentifier: "UserpageFavMusicTVCell")
        tableView.register(UINib(nibName: "UserpagePetsTVCell", bundle: nil), forCellReuseIdentifier: "UserpagePetsTVCell")
        tableView.register(UINib(nibName: "UserpageManOfDayTVCell", bundle: nil), forCellReuseIdentifier: "UserpageManOfDayTVCell")
        tableView.register(UINib(nibName: "MoreBtnTVCell", bundle: nil), forCellReuseIdentifier: "MoreBtnTVCell")
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 6 {
            if tagsInterestings.isEmpty {
                return CGFloat.Magnitude.leastNonzeroMagnitude
            }
        } else if section == 10 {
            if output.userData?.pets == nil {
                return CGFloat.Magnitude.leastNonzeroMagnitude
            }
        } else if section == 9 {
            if output.userData?.books == nil {
                return CGFloat.Magnitude.leastNonzeroMagnitude
            }
        } else if section == 4 {
            if output.userData?.ublogs?.count == 0 {
                return CGFloat.Magnitude.leastNonzeroMagnitude
            }
        } else if section == 7 {
            if output.userData?.education == nil {
                return CGFloat.Magnitude.leastNonzeroMagnitude
            }
        }
        return 12
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 5 {
            if output.userData?.about == nil {
                return 0
            }
        } else if section == 6 {
            if tagsInterestings.isEmpty {
                return 0
            }
        } else if section == 7 {
            if output.userData?.education == nil {
                return 0
            }
        } else if section == 10 {
            if output.userData?.pets == nil {
                return 0
            }
        } else if section == 9 {
            if output.userData?.books == nil {
                return 0
            }
        } else if section == 4 {
            if output.userData?.ublogs?.count == 0 {
                return 0
            }
        } else if section == 11 {
            if isOpenUserOfDay {
                return userOfDay.count
            } else {
                return 4 // 3 user + 1 button cell
            }
            
        }
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if userOfDay.count > 0 {
            return 12
        } else { return 11 }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 11 {
            return 44
        } else {
            return CGFloat.Magnitude.leastNonzeroMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 11 {
            let headerView = HeadrerManOfDayView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
            return headerView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath { // for collection view
        case [10,0]: if output.userData?.pets != nil { return 160 } else { return 0 }
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserPageHeaderTVCell") as! UserPageHeaderTVCell
            cell.createTagCloud(withArray: tagsArray)
            guard let userData = output.userData else { return UITableViewCell() }
            cell.setupCell(data: userData)
            cell.delegate = self // cell pressed delegate
            cell.btnActionDelegate = self
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserpagePhotoTVCell") as! UserpagePhotoTVCell
            cell.setupImageToScrollView(imageArray: userPhotos)
            cell.delegate = self
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserpageRatingTVCell") as! UserpageRatingTVCell
            if let userData = output.userData { cell.setupCell(data: userData) }
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserPageGiftTableViewCell") as! UserPageGiftTableViewCell
            cell.setupCell(data: userGifts)
            cell.delegate = self
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserpageBlogTVCell") as! UserpageBlogTVCell
            if let userData = output.userData { cell.setupCell(blogData: userData) }
            cell.delegate = self
            return cell
            
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserPageAboutMeTVCell") as! UserPageAboutMeTVCell
            if let userData = output.userData { cell.setupCell(aboutMe: userData) }
            return cell
            
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserPageInterestsTVCell") as! UserPageInterestsTVCell
            cell.createTagCloud(withArray: tagsInterestings)
            return cell
            
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserPageEducationTVCell") as! UserPageEducationTVCell
            if let userData = output.userData { cell.setupCell(data: userData) }
            return cell
            
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserpageCareerTVCell") as! UserpageCareerTVCell
            if let userData = output.userData { cell.setupCell(data: userData) }
            return cell
            
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserpageFavBookTVCell") as! UserpageFavBookTVCell
            if let userData = output.userData { cell.setupCell(data: userData) }
            return cell
            
            //        case [10,0]:
            //            let cell = tableView.dequeueReusableCell(withIdentifier: "UserpageFavMusicTVCell") as! UserpageFavMusicTVCell
            //            return cell
            
        case 10:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserpagePetsTVCell") as! UserpagePetsTVCell
            if let userData = output.userData { cell.setupCell(data: userData) }
            return cell
            
        case 11:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserpageManOfDayTVCell") as! UserpageManOfDayTVCell
            if indexPath.row == 3, !isOpenUserOfDay {
                let moreBtnCell = tableView.dequeueReusableCell(withIdentifier: "MoreBtnTVCell") as! MoreBtnTVCell
                moreBtnCell.delegate = self
                return moreBtnCell
            } else {
                cell.setupCell(dataUser: userOfDay[indexPath.row])
            }
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !isSelfUser() else { return }
        switch indexPath.section {
        case 11:
            let selectedCell = tableView.cellForRow(at: indexPath)
            guard let cell = selectedCell else { break }
            userOfDay = userOfDay.filter({ $0.id != cell.tag })
            tableView.isHidden = true
            tableView.scrollToRow(at: [0,0], at: .top, animated: false)
            output.pressMoreManOfDays(userId: cell.tag)

            
        default:
            break
        }
    }
    /// Хозяин профиля
    private func isSelfUser() -> Bool {
        return userId == SuperUser.shared.superUser?.id
    }
    
    func actionSheetHandler(type: EventSheetType) {
        switch type {
        case .blackList:
            guard let user = output.userData, let userId = user.id else { return }
            if output.userData!.is_blacklisted! {
                output.deleteUserFromBL(id: userId)
            } else {
                output.addUserToBL(id: userId)
            }
            output.userData!.is_blacklisted = !output.userData!.is_blacklisted! // TODO: сервер может теоретически вернуть не ок, и статус локально не будет верным
        case .claim:
            self.navigationController?.pushViewController(ClaimViewController(passDataDelegate: self, ticketType: .user), animated: true)
        }
    }
    
    func updateUserPhotos(userId: Int) {
        output.loadUserPhotos(userId: userId, limit: userPhotos.count)
    }
    
    func showPhotoComments(view: PhotoCommentView) {
        if let gallery = self.navigationController?.presentedViewController as? PhotoGalleryViewController {
                gallery.present(view, animated: true, completion: nil)
        }
    }
}


// MARK: -
// MARK: UserpageViewInput
extension UserpageViewController: UserpageViewInput {
    
    func setUserGifts(_ data: [UserGift]) {
        userGifts = data
    }
    
    func setCounters(likes: [PhotoLike], comments: [PhotoComment]) {
        photoGalleryController?.setCounters(likes: likes, comments: comments)
    }
    
    func showClaimIdAlert(id: Int) {
        HUD.hide()
        showAlert(title: "", msg: "Номер Вашей жалобы \(id)")
    }
    
    func setAlluserOfDays(alluser: [UsersDaily]) {
        userOfDay = alluser
    }
    
    func setUserPhoto(userPhotos: [UserPhotos]) {
        self.userPhotos = userPhotos
        if let photoGallery = self.navigationController?.viewControllers.last?.presentedViewController as? PhotoGalleryViewController {
            photoGallery.photos = userPhotos
            photoGallery.updateCounters()
        }
    }
    
    func setUserData(cloudHeadTags cludHeadTags: [TagCloudElement], tagsInterestings: [TagCloudElement]) {
        tagsArray = cludHeadTags
        self.tagsInterestings = tagsInterestings
        
        title = output.userData!.name!
        tableView.reloadData()
        tableView.isHidden = false
        HUD.hide()
    }
    
}

// MARK: -
// MARK: Buttons press action delegate
extension UserpageViewController: CellButtonActionHandlerType {
    
    func pressPayBtn(type: TizerType) {
        let _ = PopUpPaymentVCStep1.init(tizerType: type)
    }
    
    func buttonPressed(cell: UITableViewCell, actionType: UserpageViewController.ActionBtnType) {
        guard !isSelfUser() else { return }
        
        switch cell {
        case is UserPageGiftTableViewCell:
            guard DeviceCurrent.current.guestModeStatus() == false else { DeviceCurrent.current.showGuestModeAlert(); break } // не для гостевого режима
            if cell is UserPageGiftTableViewCell {
                if actionType == .addGiftBtn {
                    output.showGiftGallery(userId: output.userData!)
                }
            }
            if actionType == .showUserGifts {
                if self.userGifts.count > 0 {
                    let giftListVC = GiftsListViewController.init(gifts: self.userGifts, delegate: self)
                    self.navigationController?.pushViewController(giftListVC, animated: true)
                }
            }
            
        case is UserPageHeaderTVCell:
            if actionType == ActionBtnType.pressMoreBtn {
                guard DeviceCurrent.current.guestModeStatus() == false else { DeviceCurrent.current.showGuestModeAlert(); break } // не для гостевого режима
                showActionSheet(option: "full")
            }
            if actionType == ActionBtnType.zoomMainPhoto {
                let avatarPhoto = UserPhotos.init(id: 0, user_id: output.userData!.id!, datetime: "", description: "", src: output.userData!.avatar!.src!, isLiked: false, likesCount: 0, commentsCount: 0)
                
                let photoVC = PhotoGalleryViewController.init(userPhotos: [avatarPhoto], selectedIndex: 0, user: output.userData!, enabledBottomToolBarElements: !DeviceCurrent.current.guestModeStatus())
                photoVC.delegateParent = self
                present(photoVC, animated: true, completion: nil)
            }
            
        case is MoreBtnTVCell:
            isOpenUserOfDay = !isOpenUserOfDay
            let indexPath = tableView.indexPath(for: cell)
            let indexSet = NSMutableIndexSet()
            indexPath?.forEach(indexSet.add)
            tableView.reloadSections(indexSet as IndexSet, with: .automatic)
            
        case is UserpageBlogTVCell:
            if actionType == ActionBtnType.pressMoreBtn {
                showActionSheet(option: "short")
            } else {
                if actionType == ActionBtnType.showMoreBlog {
                    if let user = output.userData {
                        output.showUserBlog(user)
                    }
                } else if actionType == ActionBtnType.showSelectedPost {
                    if let postId = (cell as? UserpageBlogTVCell)?.currentPostId {
                        output.showSinglePost(postId)
                    }
                }
            }
            
        case is MyBlogBodyTableViewCell:
            guard DeviceCurrent.current.guestModeStatus() == false else { DeviceCurrent.current.showGuestModeAlert(); break } // не для гостевого режима
            if actionType == ActionBtnType.claimReport {
                showActionSheet(option: "short")
            }
            
        default:
            break
        }
    }
    
    //    func addGiftClicked(sender: UIButton) {
    //
    //    }
}

// MARK: -
// MARK: External controllers buttons press action delegate

extension UserpageViewController: ExternalActionHandlerType {
    
    func showPhotoAlbum() {
        guard !isSelfUser() else { return }
        output.showPhotoAlbum(userPhotos, user: output.userData)
    }
    
    func pressButton(data: Any, actionType: UserpageViewController.ActionBtnType) {
        guard !isSelfUser() else { return }
        
        if let claimReport = data as? ClaimReport, actionType == .claimReport {
            claimReport.userId = output.userData?.id
            output.sendClaimReport(claimReport)
            HUD.show(.progress)
        } else if let user = data as? User, let userId = user.id, actionType == .sendLike {
            output.userData = user //update local user data
            output.sendUserLike(userId)
        } else if let photoId = data as? Int, actionType == .sendPhotoLike {
            output.sendPhotoLike(photoId)
        } else if actionType == .showPhotoGallery {
            if let indexPath = data as? IndexPath {
                let photoGalleryController = PhotoGalleryViewController(userPhotos: userPhotos, selectedIndex: indexPath.row, user: output.userData!, enabledBottomToolBarElements: !DeviceCurrent.current.guestModeStatus())
                photoGalleryController.delegateParent = self
                self.present(photoGalleryController, animated: true)
            }
        } else if actionType == .sendMessage, let user = data as? User {
            output.showMessageDialog(with: user)
        } else if actionType == .showUserPage, let userId = data as? Int  {
            output.showUserPage(userId)
        }
        
    }
}

// MARK: -
// MARK: Handlers for Action Sheets

extension UserpageViewController {
    
    func requestCountersGallery(_ idPhoto: Int) {
        return output.requestCountersGallery(idPhoto)
    }
    
    func showActionSheet(option: String) {
        guard !isSelfUser() else { return }
        guard let isblackList = output.userData?.is_blacklisted else { return }
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        let claim = UIAlertAction(title: "Пожаловаться", style: .default) { action in
            self.actionSheetHandler(type: .claim)
        }
        
        let blackList = UIAlertAction(title: isblackList ? "Удалить из чёрного списка" : "Добавить в чёрный список", style: .default) { action in
            self.actionSheetHandler(type: .blackList)
        }
        
        actionSheet.addAction(claim)
        if option == "full" {
            actionSheet.addAction(blackList)
        }
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension UserpageViewController: PhotoCommentType {
    func showUserPage(id: Int) {
        self.dismiss(animated: false) {
            self.output.showUserPage(id)
        }
    }
    
    func setLikePhoto(photoId: Int) {
        guard !isSelfUser() else { return }
        output.sendPhotoLike(photoId)
    }
    
    func deleteComment(commentId: Int, photoId: Int) {
        guard !isSelfUser() else { return }
        output.deleteComment(commentId, photoId)
    }
    
    func sendComment(id: Int, text: String) {
        guard !isSelfUser() else { return }
        output.sendCommentForPhoto(id: id, text: text)
    }
}
