//
//  ClaimViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 14.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class ClaimViewController: UIViewController {
    
    @IBOutlet weak var mainViewConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var headerMsgTextField: UITextField!
    @IBOutlet weak var msgTextView: UITextView!
    
    weak var delegate: AnyObject!
    var ticketType: ClaimReport.TicketType!
    
    init(passDataDelegate: AnyObject, ticketType: ClaimReport.TicketType) {
        self.delegate = passDataDelegate
        self.ticketType = ticketType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = .white
        
        msgTextView.text = "Опишите, пожалуйста, вашу проблему"
        msgTextView.textColor = UIColor.lightGray
        
        msgTextView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        msgTextView.layer.borderWidth = 1.0
        msgTextView.layer.cornerRadius = 5
        
        headerMsgTextField.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        headerMsgTextField.layer.borderWidth = 1.0
        headerMsgTextField.layer.cornerRadius = 5
        headerMsgTextField.attributedPlaceholder = NSAttributedString(string: "Суть проблемы",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        headerMsgTextField.delegate = self
        msgTextView.delegate = self
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @IBAction func btnSendPress(_ sender: RoundButtonCustom) {
        
        if msgTextView.text.count > 0, let title = headerMsgTextField.text, title.count > 0 {
            let claimReport = ClaimReport(header: title, body: msgTextView.text, ticketType: ticketType, userId: nil)

            if let myblog = delegate as? MyBlogViewController {
                myblog.pressButton(data: claimReport, actionType: .claimReport)
            }
            if let userpage = delegate as? UserpageViewController {
                userpage.pressButton(data: claimReport, actionType: .claimReport)
            }
            if let popularBlog = delegate as? UsersBlogsViewController {
                popularBlog.pressButton(data: claimReport, actionType: .claimReport)
            }
        }
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.mainViewConstraintBottom.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        } else {
            self.mainViewConstraintBottom.constant = keyboardViewEndFrame.height
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        msgTextView.resignFirstResponder()
        headerMsgTextField.resignFirstResponder()
    }
    
}

extension ClaimViewController: UITextFieldDelegate, UITextViewDelegate {

    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Опишите, пожалуйста, вашу проблему"
            textView.textColor = UIColor.lightGray
        }
    }
}

