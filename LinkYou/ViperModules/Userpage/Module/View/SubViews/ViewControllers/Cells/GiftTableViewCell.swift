//
//  GiftTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 19.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class GiftTableViewCell: UITableViewCell {

    @IBOutlet weak var userAvatarImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userProfLabel: UILabel!
    @IBOutlet weak var giftImageView: UIImageView!
    @IBOutlet weak var giftTitleLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setup(_ userGift: UserGift) {
        guard let user = userGift.user, let gift = userGift.gift else { return }
        if let userName = user.name, let prof = user.job?.profession, let userAge = user.birthday?.age, let avatarURL = user.avatar?.src?.square, let occupation = user.job?.occupation {
            userNameLabel.text = "\(userName), \(userAge)"
            userProfLabel.text = "\(prof), \(occupation)"
            userAvatarImage.sd_setImage(with: URL(string: avatarURL)) { (image, error, cache, url) in
                self.userAvatarImage.roundedImage(25)
            }
            
            if let giftImageUrl = gift.picture {
                giftImageView.sd_setImage(with: URL(string: giftImageUrl)) { (image, error, cache, url) in
                }
            }
            
            if let gifttext = userGift.text {
                giftTitleLable.text = gifttext
            }
        }
    }
}
