//
//  PhotoGalleryViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 15.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

protocol PhotoGalleryCellDelegateType: class {
    func zoomDidStart()
    func zoomDidEnd()
}

protocol PhotoGalleryDelegateType: class {
    var photoGalleryController: PhotoGalleryViewController? { get set }
    func updateUserPhotos(userId: Int)
    func requestCountersGallery(_ idPhoto: Int)
    func pressButton(data: Any, actionType: UserpageViewController.ActionBtnType)
    func showPhotoComments(view: PhotoCommentView)
}

class PhotoGalleryViewController: UIViewController {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var topToolBar: UIToolbar!
    @IBOutlet weak var bottomToolBar: UIToolbar!
    
    @IBOutlet weak var commentsBtnIcon: UIBarButtonItem!
    @IBOutlet weak var likesCounter: UIBarButtonItem!
    @IBOutlet weak var commentsCounter: UIBarButtonItem!
    @IBOutlet weak var topCounterPhotos: UIBarButtonItem!
    @IBOutlet weak var likeBtnIcon: UIBarButtonItem!
    @IBOutlet weak var moreToolBarBtn: UIBarButtonItem!
    
    //    weak var delegateParent: UserpageViewController!
    weak var delegateParent: PhotoGalleryDelegateType!
    
    var photos: [UserPhotos]!
    var selectedIndex: Int!
    var photoLikes = [PhotoLike]()
    var photoComment = [PhotoComment]()
    let user: User!

    var hideCellWhenZoom = false
    
    var isEnabledToolBarElements = true
    var userId: Int {
        get {
            return user.id!
        }
    }
    
    var localLikeStatus: Bool = false
    var photoId = 0
    
    init(userPhotos: [UserPhotos], selectedIndex: Int, user: User, enabledBottomToolBarElements: Bool = true, avatarMode: Bool = false) {
        self.photos = userPhotos
        self.selectedIndex = selectedIndex
        self.user = user
        isEnabledToolBarElements = enabledBottomToolBarElements
        super.init(nibName: "PhotoGalleryViewController", bundle: nil)
        if avatarMode { // показать только аватарку
            self.photos.removeAll()
            self.photos.append(UserPhotos.makePhotoFromAvatarObj(avatar: user.avatar))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.isHidden = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isDirectionalLockEnabled = true
        
        delegateParent.photoGalleryController = self
        collectionView.register(UINib(nibName: "PhotoGalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoGalleryCollectionViewCell")
//        delegateParent.updateUserPhotos(userId: userId)
        loadCounters(selectedPhoto: selectedIndex)
        setupSwipe()
        
        if photos.first?.id == 0 {
            bottomToolBar.isHidden = true
        } else {
            bottomToolBar.isHidden = false
        }
        
        likesCounter.isEnabled = isEnabledToolBarElements
        commentsCounter.isEnabled = isEnabledToolBarElements
        likeBtnIcon.isEnabled = isEnabledToolBarElements
        commentsBtnIcon.isEnabled = isEnabledToolBarElements
        topCounterPhotos.isEnabled = isEnabledToolBarElements
        moreToolBarBtn.isEnabled = isEnabledToolBarElements
    }
    
    func setupSwipe() {
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        downSwipe.direction = .down
        self.view.addGestureRecognizer(downSwipe)

    }
    
    func loadCounters(selectedPhoto: Int) {
        topCounterPhotos.title = "\(selectedPhoto + 1) из \(photos.count)"
        guard selectedIndex != nil, photos.count > 0 else { return }
        if let photoId = photos[selectedPhoto].id, let like = photos[selectedPhoto].isLiked, let likeCounter = photos[selectedPhoto].likesCount {
            delegateParent.requestCountersGallery(photoId)
            localLikeStatus = like
            likesCounter.title = likeCounter.toString() // лайки фотографии
            likeBtnIcon.image = localLikeStatus ? #imageLiteral(resourceName: "heart-select-userPage") : #imageLiteral(resourceName: "photoGallery-unlike")
        }
    }
    
    func setCounters(likes: [PhotoLike], comments: [PhotoComment]) {
        guard selectedIndex != nil else { return }
        if let likesCount = photos[selectedIndex].likesCount {
            likesCounter.title = likesCount.toString() // лайки юзера
        }
        commentsCounter.title = comments.count.toString()
        self.photoComment = comments
        self.photoLikes = likes
        NotificationCenter.default.post(name: .photoDetailsWasUpdated, object: nil) // send signal for PhotoCommentView
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemWidth = view.bounds.width
            let itemHeight = layout.collectionView!.bounds.height
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
            layout.invalidateLayout()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if photos.count > 0 {
            collectionView.scrollToItem(at: [0,selectedIndex], at: .centeredHorizontally, animated: false)
            collectionView.isHidden = false
        }
    }
    
    @IBAction func pressLikeBtn(_ sender: UIBarButtonItem) {
        localLikeStatus = !localLikeStatus
        likeBtnIcon.image = localLikeStatus ? #imageLiteral(resourceName: "heart-select-userPage") : #imageLiteral(resourceName: "photoGallery-unlike")
//        guard let incLikeCounter = Int(likesCounter.title ?? "0") else { return }
        if localLikeStatus {
//            let incLikeCounter = incLikeCounter + 1
//            likesCounter.title = incLikeCounter.toString()
            photos[selectedIndex].isLiked = true
            photos[selectedIndex].likesCount = photos[selectedIndex].likesCount! + 1
        } else {
//            let incLikeCounter = incLikeCounter - 1
//            likesCounter.title = incLikeCounter.toString()
            photos[selectedIndex].isLiked = false
            photos[selectedIndex].likesCount = photos[selectedIndex].likesCount! - 1
        }
        
        delegateParent.pressButton(data: photoId, actionType: .sendPhotoLike)
    }
    
    @IBAction func pressCommentsBtn(_ sender: UIBarButtonItem) {
        if let currentCell = collectionView.visibleCells.first as? PhotoGalleryCollectionViewCell, let currentImage = currentCell.photoImageView.image {
            let photoCommentsView = PhotoCommentView(userPhoto: photos[selectedIndex], user: user, image: currentImage, comments: photoComment, selectedIndex: selectedIndex, photoLikes: photoLikes)
            present(photoCommentsView, animated: true, completion: nil)
//            delegateParent.showPhotoComments(view: photoCommentsView)
        }
    }
    
    @IBAction func pressMoreBtn(_ sender: UIBarButtonItem) {
        guard selectedIndex != nil, photos.count > 0 else { return }
        if let photoId = photos[selectedIndex].id {
            showGalleryMoreSheet(photoId: photoId)
        } else {
            print("Нет фото с таким индексом в массиве photos!")
        }
    }
    
    @IBAction func pressCloseBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateCounters() {
        guard selectedIndex != nil else { return }
        guard let isLiked = photos[selectedIndex].isLiked else { return }
        localLikeStatus = isLiked
        likeBtnIcon.image = localLikeStatus ? #imageLiteral(resourceName: "heart-select-userPage") : #imageLiteral(resourceName: "photoGallery-unlike")
    }
    
    func deletePhoto(_ id: Int) {
        photos = photos.filter { (photo) -> Bool in
            photo.id != id
        }
        collectionView.reloadData()
        delegateParent.updateUserPhotos(userId: userId)
    }
    
    func showGalleryMoreSheet(photoId: Int) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        if delegateParent is PersonalPageViewController || delegateParent is EditorRegistrationViewController {
            let deletePhoto = UIAlertAction(title: "Удалить фотографию", style: .destructive) { action in
                self.delegateParent.pressButton(data: photoId, actionType: .pressMoreBtn)
                self.dismiss(animated: true, completion: nil)
            }
            if photoId != SuperUser.shared.superUser?.avatar?.id {
                actionSheet.addAction(deletePhoto)
            }
            
            let setAvatar = UIAlertAction(title: "Установить как фото профиля", style: .default) { action in
                guard let indexPath = self.collectionView.indexPathsForVisibleItems.first,
                    self.photos.indices.contains(indexPath.row),
                    let id = self.photos[indexPath.row].id else { return }
                
                self.delegateParent.pressButton(data: id, actionType: .sendUpdateAvatar)
                self.dismiss(animated: true, completion: nil)
            }
            actionSheet.addAction(setAvatar)
            
        }
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        var startLocation = CGPoint()
        if (sender.state == UIGestureRecognizer.State.began) {
            startLocation = sender.location(in: self.view)
        }
        else if (sender.state == UIGestureRecognizer.State.ended) {
            let stopLocation = sender.location(in: self.view)
            let dx = stopLocation.x - startLocation.x
            let dy = stopLocation.y - startLocation.y
            let distance = sqrt(dx*dx + dy*dy )
            
            if distance > 300 {
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
}

extension PhotoGalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoGalleryCollectionViewCell", for: indexPath) as! PhotoGalleryCollectionViewCell
        cell.delegate = self
        guard let photoId = photos[indexPath.row].id else { return cell }
        self.photoId = photoId
        if let urlString = photos[indexPath.row].src?.origin {
            let url = URL(string: urlString)
            cell.photoImageView.sd_setImage(with: url, completed: nil)
        }
        cell.contentView.isHidden = hideCellWhenZoom
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.selectedIndex = collectionView.indexPathForItem(at: CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y))?.row
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //caused by user
        guard selectedIndex != nil else { return }
        loadCounters(selectedPhoto: selectedIndex)
        photos[selectedIndex].isLiked = localLikeStatus
    }
}

extension PhotoGalleryViewController: PhotoGalleryCellDelegateType {
    func zoomDidStart() {
        hideCellWhenZoom = true
        collectionView.isScrollEnabled = false
    }
    
    func zoomDidEnd() {
        hideCellWhenZoom = false
        collectionView.isScrollEnabled = true
    }
}
