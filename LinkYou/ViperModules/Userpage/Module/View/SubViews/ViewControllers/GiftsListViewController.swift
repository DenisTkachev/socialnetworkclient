//
//  GiftsListViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 19.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class GiftsListViewController: UIViewController {

    var gifts: [UserGift]!
    weak var delegate: ExternalActionHandlerType?
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        setupNavBar()
    }
    
    init(gifts: [UserGift], delegate: ExternalActionHandlerType?) {
        super.init(nibName: "GiftsListViewController", bundle: nil)
        self.gifts = gifts
        self.delegate = delegate
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "GiftTableViewCell", bundle: nil), forCellReuseIdentifier: "GiftTableViewCell")
    }
    
    func setupNavBar() {
        title = "Подарки"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }

}

extension GiftsListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gifts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GiftTableViewCell", for: indexPath) as! GiftTableViewCell
        cell.setup(gifts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let giftSenderUserId = gifts[indexPath.row].user?.id else { return }
        delegate?.pressButton(data: giftSenderUserId, actionType: .showUserPage)
        
    }
}








