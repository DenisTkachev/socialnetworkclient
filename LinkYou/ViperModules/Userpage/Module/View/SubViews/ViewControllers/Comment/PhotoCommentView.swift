//
//  PhotoCommentView.swift
//  LinkYou
//
//  Created by Denis Tkachev on 26.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

protocol PhotoCommentType: class {
    func sendComment(id: Int, text: String)
    func deleteComment(commentId: Int, photoId: Int)
    func setLikePhoto(photoId: Int)
    func showUserPage(id: Int)
}

protocol CellDelegateType: class {
    func showUserProfile(id: Int)
}

class PhotoCommentView: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextView: UIView!
    @IBOutlet weak var commentTextField: TextFieldCustom!
    @IBOutlet weak var sendMsgBtn: UIImageView!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendMsgIndicator: UIActivityIndicatorView!
    
    var photo: UserPhotos!
    var photoLikes = [PhotoLike]()
    var photoComment = [PhotoComment]()
    var selectedIndex: Int!
    let user: User!
    let image: UIImage!
    
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    //var expandedIndexPaths: [IndexPath] = []
    
    init(userPhoto: UserPhotos, user: User, image: UIImage, comments: [PhotoComment], selectedIndex: Int, photoLikes: [PhotoLike]) {
        self.photo = userPhoto
        self.photoComment = comments
        self.user = user
        self.image = image
        self.selectedIndex = selectedIndex
        self.photoLikes = photoLikes
        super.init(nibName: "PhotoCommentView", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "PhotoCommentCell", bundle: nil), forCellReuseIdentifier: "PhotoCommentCell")
        tableView.register(UINib(nibName: "HeaderPhotoCommentCell", bundle: nil), forCellReuseIdentifier: "HeaderPhotoCommentCell")
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateTableView(notification:)), name: .photoDetailsWasUpdated, object: nil)
        
        let sendMsgAction = UITapGestureRecognizer(target: self, action: #selector(tappedSendMsg(_:)))
        sendMsgAction.numberOfTapsRequired = 1
        sendMsgBtn.isUserInteractionEnabled = true
        sendMsgBtn.addGestureRecognizer(sendMsgAction)
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func pressBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tappedSendMsg(_ recognizer: UITapGestureRecognizer) {
        // поискать среди родителей подписанного на протокол PhotoCommentType
        if let text = commentTextField.text, text.count > 0 {
            if let parentVC = self.presentingViewController as? PhotoGalleryViewController {
                if let userPage = parentVC.delegateParent as? PhotoCommentType {
                    guard let id = photo.id, let comment = commentTextField.text else { return }
                    sendMsgIndicator.startAnimating()
                    sendMsgBtn.isUserInteractionEnabled = false
                    userPage.sendComment(id: id, text: comment)
                }
            }
        }
    }
    
    @objc func keyboardHandle(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            bottomViewConstraint.constant = isKeyboardShowing ? frame.height : 0
            
            UIView.animate(withDuration: 0, animations: {
                self.view.layoutIfNeeded()
            }) { (completed) in
                if isKeyboardShowing {
                    let indexPath = IndexPath(row: 0, section: 2)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    @objc func updateTableView(notification: NSNotification) {
        updateCommentSection()
        sendMsgIndicator.stopAnimating()
        sendMsgBtn.isUserInteractionEnabled = true
    }
    
    private func updateCommentSection() {
        if let parentVC = self.presentingViewController as? PhotoGalleryViewController {
            self.photoComment = parentVC.photoComment
            self.photoLikes = parentVC.photoLikes
            self.photo = parentVC.photos[selectedIndex] //теортически возможна ситуация, когда нет фотографии в массиве с таким индексом (удалили фотографии)

            tableView.reloadData()
            self.commentTextField.text = nil
            self.dismissKeyboard()
        }
    }
}

extension PhotoCommentView: CellDelegateType {
    
    func showUserProfile(id: Int) {
        if let parentVC = self.presentingViewController as? PhotoGalleryViewController {
            if let userPage = parentVC.delegateParent as? PhotoCommentType {
                dismiss(animated: true) {
                    userPage.showUserPage(id: id)
                }
            }
        }
    }
}

extension PhotoCommentView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 2 {
            return 20
        } else {
            return CGFloat.leastNonzeroMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init()
        view.backgroundColor = .clear
        return view
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 2 {
            return 1
        } else {
            return photoComment.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath == [0,0] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderPhotoCommentCell", for: indexPath) as! HeaderPhotoCommentCell
            cell.setupCell(image: image, photo: photo, user: user, selfUserLikesCount: self.photoLikes.count)
            if let parentVC = self.presentingViewController as? PhotoGalleryViewController {
                if let userPage = parentVC.delegateParent as? PhotoCommentType {
                    cell.delegate = userPage
                }
            }
            tableView.layoutIfNeeded()
            return cell
            
        } else if indexPath.section == 2 {
            let emptyCell = UITableViewCell()
            return emptyCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCommentCell", for: indexPath) as! PhotoCommentCell
            cell.setup(photoComment[indexPath.row])
            cell.delegate = self
            tableView.layoutIfNeeded()
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        commentTextField.endEditing(true)
    }
    
    //MARK: Delete Cell
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Удалить"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let _ = tableView.cellForRow(at: indexPath) as? PhotoCommentCell else { return false }
        if let commentOwner = photoComment[indexPath.row].user?.id, let selfId = SuperUser.shared.superUser?.id, commentOwner == selfId {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            guard let commentId = photoComment[indexPath.row].id, let photoId = photo.id else { return }
            // поискать среди родителей подписанного на протокол PhotoCommentType
                if let parentVC = self.presentingViewController as? PhotoGalleryViewController {
                    if let userPage = parentVC.delegateParent as? PhotoCommentType {
                        userPage.deleteComment(commentId: commentId, photoId: photoId)
                    }
                }
        }
    }
}

