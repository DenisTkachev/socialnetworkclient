//
//  PhotoCommentCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 28.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SwiftDate

class PhotoCommentCell: UITableViewCell {

    @IBOutlet weak var userAvatarImageView: MyAvatarView!  
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var textPostTextView: UILabel!
    @IBOutlet weak var backGroundMessageView: UIView!
    weak var delegate: CellDelegateType!
    var id = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        userAvatarImageView.clipsToBounds = true
        self.selectionStyle = .none
        userAvatarImageView.addTapGestureRecognizer {
            self.pressToAvatar()
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        userAvatarImageView.setStartSettings(hidden: true)
    }
    
    private func pressToAvatar() {
        self.delegate.showUserProfile(id: id)
    }
    
    func setup(_ commentsData: PhotoComment) {
        
        if let avatarUrl = commentsData.user?.avatar?.src?.square, let user = commentsData.user, let authorId = commentsData.user?.id {
            self.id = authorId
            userAvatarImageView.userAvatar.sd_setImage(with: URL(string: avatarUrl)) { (image, error, cache, url) in
                self.userAvatarImageView.userAvatar.sd_setImage(with: url, completed: nil)
                self.userAvatarImageView.userAvatar.toRoundedImage()
                self.userAvatarImageView.addTunning()
                if let premium = user.is_premium {
                    self.userAvatarImageView.enablePremiumStatus(on: premium)
                }
                if let online = user.is_online {
                    self.userAvatarImageView.enableOnlineStatus(on: online)
                }
                if let vip = user.is_vip {
                    self.userAvatarImageView.enableGoldRing(on: vip)
                }
            }
        }
        postDateLabel.text = prepareDate(commentsData.datetime)
        textPostTextView.numberOfLines = 0
        textPostTextView.text = commentsData.comment
        backGroundMessageView.clipsToBounds = true
        backGroundMessageView.layer.cornerRadius = 20
    }
    
    private func prepareDate(_ dateString: String?) -> String {
        
        guard let dateString = dateString, let commentDate = dateString.toDate(style: .iso(ISOParser.Options(strict: false, calendar: Calendar.current))) else { return "" }
        
        if commentDate.compare(.isToday) { // текущий день
            return commentDate.date.toString()
            
        } else if commentDate.compare(.isThisMonth) { // текущий месяц
            return commentDate.date.toFormat("d MMM")
            
        } else if !commentDate.compare(.isThisYear) { // другой год
            return commentDate.date.toFormat("d MMM, YYYY")
            
        }
        return ""
    }
    
}
