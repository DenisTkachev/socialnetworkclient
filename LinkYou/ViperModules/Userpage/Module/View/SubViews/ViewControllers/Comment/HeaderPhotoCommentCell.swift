//
//  HeaderPhotoCommentCell.swift
//  Alamofire
//
//  Created by Denis Tkachev on 28.09.2018.
//

import UIKit

class HeaderPhotoCommentCell: UITableViewCell {
    
    weak var delegate: PhotoCommentType!
    var photoId: Int?
    var likeStatus = false
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var likesCountBtn: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var photoDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        likesCountBtn.isUserInteractionEnabled = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    func setupCell(image: UIImage, photo: UserPhotos, user: User, selfUserLikesCount: Int) {
        self.photoId = photo.id
        self.photoView.image = image
        self.likesCountBtn.setTitle(selfUserLikesCount.toString(), for: .normal)
        self.likesCountBtn.setImage(photo.isLiked ?? false ? #imageLiteral(resourceName: "heartLikeblack") : #imageLiteral(resourceName: "heartunlikeblack"), for: .normal)
//        self.userNameLabel.text = "Фотографии \(user.name!)"
        self.userNameLabel.text = user.name!
        self.photoDateLabel.text = "Добавлена \(photo.datetime?.toyyyyMMdd() ?? "")"
        self.likeStatus = photo.isLiked ?? false
    }
    
    @IBAction func pressToLikeBtn(_ sender: UIButton) { // isUserInteractionEnabled = false выключил, не обновляется информация о кол-ве лайков, слишком вложная цепочка при передаче данных из родительского контроллера
//        guard let id = self.photoId, let delegate = delegate else { return }
//        likeStatus.toggle()
//        self.likesCountBtn.setImage(likeStatus ? #imageLiteral(resourceName: "heartLikeblack") : #imageLiteral(resourceName: "heartunlikeblack"), for: .normal)
//        delegate.setLikePhoto(photoId: id)
    }
}
