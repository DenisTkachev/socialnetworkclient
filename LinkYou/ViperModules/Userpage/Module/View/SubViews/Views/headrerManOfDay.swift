//
//  headrerManOfDay.swift
//  LinkYou
//
//  Created by Denis Tkachev on 15.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class HeadrerManOfDayView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let label = UILabel(frame: CGRect(x: 16, y: 8, width: self.bounds.width, height: self.bounds.height))
        backgroundColor = .white
        label.text = "Люди дня"
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.textColor = UIColor.charcoalGrey
        addSubview(label)
        return
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented"); }

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
    }

}
