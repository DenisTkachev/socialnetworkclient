//
//  UserpageCareerTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class UserpageCareerTVCell: UITableViewCell {
    
    @IBOutlet weak var proffStack: UIStackView!
    @IBOutlet weak var profOblastStack: UIStackView!
    @IBOutlet weak var plusStack: UIStackView!
    @IBOutlet weak var minusStack: UIStackView!
    @IBOutlet weak var imcomeStack: UIStackView!
    
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var oblastLabel: UILabel!
    @IBOutlet weak var plusLabel: UILabel!
    @IBOutlet weak var minusLabel: UILabel!
    @IBOutlet weak var incomeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
    func setupCell(data: User) {
        guard let job = data.job else { return }
        
        if let profession = job.profession {
            if !profession.isEmpty {
                positionLabel.text = profession
            } else {
                positionLabel.text = ""
            }
        }
        
        if let occupation = job.occupation {
            if !occupation.isEmpty {
                oblastLabel.text = occupation
            } else {
                oblastLabel.text = ""
            }
        }
        
        if let finance = job.finance?.name {
            if !finance.isEmpty {
                incomeLabel.text = finance
            } else {
                incomeLabel.text = ""
            }
        }
        
        if let plus = job.pros {
            if !plus.isEmpty {
                plusLabel.text = plus
            } else {
                plusLabel.text = ""
            }
        }
        
        if let minus = job.cons {
            if !minus.isEmpty {
                minusLabel.text = minus
            } else {
                minusLabel.text = ""
            }
        }
    }
}





