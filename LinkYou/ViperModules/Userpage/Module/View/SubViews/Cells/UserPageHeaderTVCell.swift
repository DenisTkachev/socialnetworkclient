//
//  UserPageHeaderTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 03.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import PKHUD

class UserPageHeaderTVCell: UITableViewCell {
    
    fileprivate let imageLoadQueue = OperationQueue()
    //    fileprivate var imageLoadOperations = [Int: ImageLoadOperation]()
    
    weak var delegate: CellButtonActionHandlerType!
    weak var btnActionDelegate: ExternalActionHandlerType!
    
    @IBOutlet weak var tagCloudHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var tagCloudView: UIView!
    @IBOutlet weak var moreImageView: UIImageView!
    @IBOutlet weak var likeBtn: RoundButtonCustom!
    
    @IBOutlet weak var userOnlineStatus: UIImageView!
    @IBOutlet weak var userPremiumStatus: UIImageView!
    @IBOutlet weak var userCrownEmblem: UIImageView!
    @IBOutlet weak var crownEmblemCount: UILabel!
    
    @IBOutlet weak var userNameAge: UILabel!
    @IBOutlet weak var jobProfessionLabel: UILabel!
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var countLikeLabel: UILabel!
    @IBOutlet weak var countPhotoLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    @IBOutlet weak var likeHeartImage: UIImageView!
    
    var localLikeStatus: Bool = false
    var userID = 0
    var userData: User!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        moreImageView.layer.cornerRadius = moreImageView.frame.height / 2
        moreImageView.clipsToBounds = true
        moreImageView.addTapGestureRecognizer {
            self.delegate.buttonPressed(cell: self, actionType: UserpageViewController.ActionBtnType.pressMoreBtn)
        }
        likeBtn.backgroundColor = UIColor.paleGrey
        
        userAvatarImageView.addTapGestureRecognizer {
            self.delegate.buttonPressed(cell: self, actionType: UserpageViewController.ActionBtnType.zoomMainPhoto)
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    @IBAction func pressLikeBtn(_ sender: RoundButtonCustom) {
        if DeviceCurrent.current.guestModeStatus() {
            DeviceCurrent.current.showGuestModeAlert()
        } else {
            guard userData.id != SuperUser.shared.superUser?.id else { return }
            localLikeStatus = !localLikeStatus
            likeBtn.setImage(localLikeStatus ? #imageLiteral(resourceName: "heart-select-userPage") : #imageLiteral(resourceName: "heart-unselect-userPage"), for: .normal)
            userData.likes?.is_liked = localLikeStatus
            if let userData = userData {
                btnActionDelegate.pressButton(data: userData, actionType: .sendLike)
            }
        }
    }
    
    @IBAction func pressMessageBtn(_ sender: RoundButtonCustom) {
        if DeviceCurrent.current.guestModeStatus() {
            DeviceCurrent.current.showGuestModeAlert()
        } else {
            btnActionDelegate.pressButton(data: userData, actionType: .sendMessage)
        }
    }
    
    func setupCell(data: User) {
        guard let userID = data.id else { return }
        self.userID = userID
        self.userData = data
        
        if let name = userData.name, let age = userData.birthday?.age {
            userNameAge.text = ("\(name), \(age.toString())")
        }
        if let profession = userData.job?.profession, let occupation = userData.job?.occupation {
            jobProfessionLabel.text = "\(profession), \(occupation)"
        }
        if let avatarURL = userData.avatar?.src?.square {
            userAvatarImageView.sd_setImage(with: URL(string: avatarURL)) { (image, error, cache, url) in

            let resizedImage = image?.cropToBounds(image: image!, width: Double(UIScreen.main.bounds.width), height: Double(self.userAvatarImageView.bounds.height))
                self.userAvatarImageView.image = resizedImage!
                
//                self.userAvatarImageView.image = image
                HUD.hide(animated: false)
            }
        }
        
        if let cityName = data.location?.city_name, let countPhoto = data.photos_count, let countLikes = data.likes?.count, let isLiked = data.likes?.is_liked {
            cityNameLabel.text = cityName
            countLikeLabel.text = String(format: NSLocalizedString("NumberOfLikes", comment: ""), countLikes)
            countPhotoLabel.text = "\(countPhoto.toString()) фото"
            likeBtn.setImage(isLiked ? #imageLiteral(resourceName: "heart-select-userPage") : #imageLiteral(resourceName: "heart-unselect-userPage"), for: .normal)
            localLikeStatus = isLiked
        }
         // setTitle(String(format: NSLocalizedString("NumberOfLikes", comment: ""), likesCount ?? 0), for: .normal)
        if let onlineStatus = data.is_online, let premiumStatus = data.is_premium, let top100 = data.is_top100 {
            if onlineStatus { // online
                userOnlineStatus.image = #imageLiteral(resourceName: "userPage-online-status")
            } else {
                userOnlineStatus.image = nil
            }
            
            if premiumStatus {
                userPremiumStatus.image = #imageLiteral(resourceName: "userPage-star")
                userPremiumStatus.isUserInteractionEnabled = true
                userPremiumStatus.addTapGestureRecognizer {
                    self.delegate.pressPayBtn(type: .premium)
                }
            } else {
                userPremiumStatus.image = nil
            }
            
            if top100 {
                userCrownEmblem.isHidden = false
                crownEmblemCount.isHidden = false // TODO: Решили оставить пока так. Андрей. В JSON нет? цифра места в рейтинге? нужно добавить в json
            } else {
                userCrownEmblem.isHidden = true
                crownEmblemCount.isHidden = true
            }
        }
    }
    
}

extension UserPageHeaderTVCell {
    // prepare keyWord icons
    func setupKeyWordIcon(keyWord: String, bgView: UIView) -> UIButton {
        var icon = UIImage()
        switch keyWord {
        case "Овен": icon = #imageLiteral(resourceName: "zodiac-aries")
        case "Телец" : icon = #imageLiteral(resourceName: "zodiac-taurus")
        case "Близнецы" : icon = #imageLiteral(resourceName: "zodiac-gemini")
        case "Рак" : icon = #imageLiteral(resourceName: "zodiac-cancer")
        case "Лев" : icon = #imageLiteral(resourceName: "zodiac-leo")
        case "Дева" : icon = #imageLiteral(resourceName: "zodiac-virgo")
        case "Весы" : icon = #imageLiteral(resourceName: "zodiac-vesi")
        case "Скорпион" : icon = #imageLiteral(resourceName: "zodiac-scorpio")
        case "Стрелец" : icon = #imageLiteral(resourceName: "zodiac-sagittarius")
        case "Козерог" : icon = #imageLiteral(resourceName: "zodiac-capricorn")
        case "Водолей" : icon = #imageLiteral(resourceName: "zodiac-aquarius")
        case "Рыбы" : icon = #imageLiteral(resourceName: "zodiac-pisces")
            
        default: icon = #imageLiteral(resourceName: "userPage-target")
        }
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: bgView.bounds.origin.x + 8, y: 3.0, width: 23.0, height: 23.0)
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
        button.setImage(icon, for: .normal)
        button.tag = tag
        //button.addTarget(self, action: #selector(removeTag(_:)), for: .touchUpInside)
        return button
    }
    
    
    // tagCloud
    func createTagCloud(withArray data: [TagCloudElement]) {
        
        for tempView in tagCloudView.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        var xPos: CGFloat = 0.0
        var yPos: CGFloat = 0.0
        var tag: Int = 1
        for str in data  {
            let startstring = str.text
            let width = startstring.widthOfString(usingFont: UIFont.systemFont(ofSize: 12.0))
            let checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )
            if checkWholeWidth > UIScreen.main.bounds.size.width - 8.0 {
                xPos = 0.0
                yPos = yPos + 29.0 + 8.0
            }
            let bgView = UIView(frame: CGRect(x: xPos, y: yPos, width: width + 27 , height: 29.0))
            
            bgView.layer.cornerRadius = 14.5
            bgView.layer.borderWidth = 1
            bgView.layer.borderColor = UIColor.paleGrey.cgColor
            bgView.backgroundColor = UIColor.white
            bgView.tag = tag
            
            var rect = CGRect(x: 15, y: 0.0, width: width, height: bgView.frame.size.height)
            if bgView.tag < 3 { // Знак зодика или цель
                
                bgView.addSubview(setupKeyWordIcon(keyWord: startstring, bgView: bgView))
                rect = CGRect(x: 35, y: 0.0, width: width, height: bgView.frame.size.height)
                bgView.frame = CGRect(x: xPos, y: yPos, width:width + 10.0 + 38.5 , height: 29.0)
            }
            
            let textlable = UILabel(frame: rect)
            
            textlable.font = UIFont.systemFont(ofSize: 12.0)
            textlable.text = startstring
            textlable.textColor = UIColor.charcoalGrey
            bgView.addSubview(textlable)
            
            
            xPos = CGFloat(xPos) + CGFloat(width) + CGFloat(10.0) + CGFloat(50.0)
            tagCloudView.addSubview(bgView)
            tag = tag  + 1
        }
        tagCloudHeightConstr.constant = 29 + yPos
    }
}


