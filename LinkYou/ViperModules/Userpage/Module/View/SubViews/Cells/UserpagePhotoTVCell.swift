//
//  UserpagePhotoTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 05.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class UserpagePhotoTVCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var photoCountLabel: UILabel!
    
    weak var delegate: ExternalActionHandlerType!
    var imageArray: [UserPhotos] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        collectionView.register(UINib(nibName: "UserPhotosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserPhotosCollectionViewCell")
        photoCountLabel.addTapGestureRecognizer {
            self.delegate.showPhotoAlbum()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
    override func prepareForReuse() {
        imageArray.removeAll()
    }
    
}

extension UserpagePhotoTVCell {
    func setupImageToScrollView(imageArray: [UserPhotos]) {
        photoCountLabel.text = String(format: NSLocalizedString("NumberOfPhotos", comment: ""), imageArray.count)
        self.imageArray = imageArray
        collectionView.reloadData()
    }
}

extension UserpagePhotoTVCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserPhotosCollectionViewCell", for: indexPath) as! UserPhotosCollectionViewCell
        if let src = imageArray[indexPath.row].src, let srcDefault = src.default {
            
            cell.userPhoto.sd_setImage(with: URL(string: srcDefault), placeholderImage: nil, completed: { (image, error, cache, url) in
                let resizedImage = image?.cropToBounds(image: image!, width: 106, height: 106)
                cell.userPhoto.image = resizedImage
            })
        }
        return cell
    }
}

extension UserpagePhotoTVCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.pressButton(data: indexPath, actionType: .showPhotoGallery)
    }
}






