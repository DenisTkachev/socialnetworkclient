//
//  UserpagePetsTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class UserpagePetsTVCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var pets: [Pets] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.dataSource = self
        collectionView.register(UINib(nibName: "UserPetsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserPetsCollectionViewCell")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
    func setupCell(data: User) {
        guard let pets = data.pets else { return }
        self.pets = pets
        collectionView.reloadData()
    }
    
}

extension UserpagePetsTVCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserPetsCollectionViewCell", for: indexPath) as! UserPetsCollectionViewCell
        if let name = pets[indexPath.row].name, let type = pets[indexPath.row].type  {
            cell.namePetLabel.text = name
            cell.typePetLabel.text = type
            
        if let avatar = pets[indexPath.row].avatar, !avatar.isEmpty {
            cell.leftFotoPet.sd_setImage(with: URL(string: "\(avatar)"), placeholderImage: nil, completed: { (image, error, cache, url) in
                let resizedImage = image?.cropToBounds(image: image!, width: 106, height: 106)
                cell.leftFotoPet.image = resizedImage
            })
        }
        }
        return cell
    }
}
