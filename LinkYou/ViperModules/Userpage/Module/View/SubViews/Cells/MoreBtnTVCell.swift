//
//  MoreBtnTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 15.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MoreBtnTVCell: UITableViewCell {

    weak var delegate: CellButtonActionHandlerType!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    @IBAction func pressShowMoreBtn(_ sender: RoundButtonCustom) {
        delegate.buttonPressed(cell: self, actionType: UserpageViewController.ActionBtnType.pressShowMoreUserOfDay)
    }
}
