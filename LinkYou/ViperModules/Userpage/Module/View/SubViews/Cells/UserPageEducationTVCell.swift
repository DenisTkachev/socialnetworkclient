//
//  UserPageEducationTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class UserPageEducationTVCell: UITableViewCell {
    
    @IBOutlet weak var masterStack: UIStackView!
    @IBOutlet weak var typeStack: UIStackView!
    @IBOutlet weak var nameStack: UIStackView!
    @IBOutlet weak var specialityStack: UIStackView!
    @IBOutlet weak var languageStack: UIStackView!
    
    @IBOutlet weak var educationTypeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        masterStack.subviews.forEach( { $0.removeFromSuperview() })
    }
    
    func setupCell(data: User) {
        guard let educationData = data.education else { return }
        
        for education in educationData {
            if !education.education_type!.name!.isEmpty {
                let row = makeStackRow(left: "Образование", right: education.education_type!.name!)
                masterStack.addArrangedSubview(row)
            }
            if !education.institution!.name!.isEmpty {
                let row = makeStackRow(left: "Название учебного заведения", right: education.institution!.name!)
                masterStack.addArrangedSubview(row)
            }
            if !education.speciality!.name!.isEmpty {
                let row = makeStackRow(left: "Специальность", right: education.speciality!.name!)
                masterStack.addArrangedSubview(row)
            }
        
            if let languages = data.languages {
                var summaryArray = [String]()
                for lang in languages {
                    summaryArray.append(lang.language!.name!)
                }
                let string = summaryArray.joined(separator: ", ")
                let row = makeStackRow(left: "Языки:", right: string)
                masterStack.addArrangedSubview(row)
            }
            if education.id != educationData.last?.id {
                let separatorRow = makeStackRow(left: " ", right: " ")
                masterStack.addArrangedSubview(separatorRow)
            }
        
        }
    }
    
    private func makeStackRow(left: String, right: String) -> UIStackView {
        let labelLeft = UILabel()
        labelLeft.numberOfLines = 2
        labelLeft.text = left
        labelLeft.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        labelLeft.textColor = UIColor.charcoalGrey
        labelLeft.textAlignment = .left
        
        
        let labelRight = UILabel()
        labelRight.numberOfLines = 4
        labelRight.text = right
        labelRight.font = UIFont.systemFont(ofSize: 12)
        labelRight.textColor = UIColor.charcoalGrey
        labelRight.textAlignment = .right
        labelRight.lineBreakMode = .byWordWrapping
        
        let labelArray = [labelLeft, labelRight]
        
        let stackView = UIStackView(arrangedSubviews: labelArray)
        
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
//        stackView.spacing = 5
        return stackView
    }
}







