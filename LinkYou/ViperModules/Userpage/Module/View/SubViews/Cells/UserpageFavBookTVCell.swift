//
//  UserpageFavBookTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class UserpageFavBookTVCell: UITableViewCell {
    
    @IBOutlet weak var masterStack: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        masterStack.subviews.forEach( { $0.removeFromSuperview() })
    }
    
    func setupCell(data: User) {
        guard let books = data.books else { return }
        
        for book in books {
            let row = makeStackRow(left: book.author!, right: book.name!)
        masterStack.addArrangedSubview(row)
        }
        
    }
    
    
    private func makeStackRow(left: String, right: String) -> UIStackView {
        let labelLeft = UILabel()
        labelLeft.numberOfLines = 2
        labelLeft.text = left
        labelLeft.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        labelLeft.textColor = UIColor.charcoalGrey
        labelLeft.textAlignment = .left
        
        let labelRight = UILabel()
        labelRight.numberOfLines = 4
        labelRight.text = right
        labelRight.font = UIFont.systemFont(ofSize: 12)
        labelRight.textColor = UIColor.charcoalGrey
        labelRight.textAlignment = .right
        
        let labelArray = [labelLeft, labelRight]
        
        let stackView = UIStackView(arrangedSubviews: labelArray)
        
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
//        stackView.spacing = 5
        return stackView
    }
    
}
