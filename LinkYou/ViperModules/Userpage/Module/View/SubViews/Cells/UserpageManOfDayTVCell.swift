//
//  UserpageManOfDayTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class UserpageManOfDayTVCell: UITableViewCell {

    @IBOutlet weak var userAvatarImageView: MyAvatarView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userProfessionLabel: UILabel!
    @IBOutlet weak var countPhoto: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        userAvatarImageView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        userAvatarImageView.setStartSettings(hidden: true)
    }
    
    func setupCell(dataUser: UsersDaily) {
        guard let userId = dataUser.id else { return }
        self.tag = userId
        if let name = dataUser.name {
            userNameLabel.text = name
        }
        if let profession = dataUser.job?.profession {
            userProfessionLabel.text = profession
        }
        if let avatarUrl = dataUser.avatar?.src?.small {
            let url = URL(string: avatarUrl)
            userAvatarImageView.userAvatar.sd_setImage(with: url) { (image, error, cache, url) in
                self.userAvatarImageView.userAvatar.sd_setImage(with: url, completed: nil)
                self.userAvatarImageView.userAvatar.toRoundedImage()
                self.userAvatarImageView.addTunning()
                if let premium = dataUser.is_premium {
                    self.userAvatarImageView.enablePremiumStatus(on: premium)
                }
                if let online = dataUser.is_online {
                    self.userAvatarImageView.enableOnlineStatus(on: online)
                }
                if let vip = dataUser.is_vip {
                    self.userAvatarImageView.enableGoldRing(on: vip)
                }
            }
            
            
        }
        if let photoCount = dataUser.photos_count {
            countPhoto.text = photoCount.toString()
        }
    }
}
