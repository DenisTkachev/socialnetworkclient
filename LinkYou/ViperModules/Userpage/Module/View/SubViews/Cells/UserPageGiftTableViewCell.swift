//
//  UserPageGiftTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 06.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class UserPageGiftTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var countOfGiftsLabel: UILabel!
    @IBOutlet weak var addGiftBtn: UIImageView!
    
    weak var delegate: CellButtonActionHandlerType!
    var gifts = [UserGift]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        collectionView.register(UINib(nibName: "UserPhotosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserPhotosCollectionViewCell")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(addGiftTapped(tapGestureRecognizer:)))
        addGiftBtn.isUserInteractionEnabled = true
        addGiftBtn.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    override func prepareForReuse() {
        gifts.removeAll()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    @objc func addGiftTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        delegate.buttonPressed(cell: self, actionType: .addGiftBtn)
    }
}

extension UserPageGiftTableViewCell: UICollectionViewDelegate {
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            delegate.buttonPressed(cell: self, actionType: .showUserGifts)
        }
}

extension UserPageGiftTableViewCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if gifts.count <= 3 {
            return 3
        } else {
            return gifts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserPhotosCollectionViewCell", for: indexPath) as! UserPhotosCollectionViewCell
        if gifts.indices.contains(indexPath.row), let src = gifts[indexPath.row].gift, let srcDefault = src.picture {
            cell.userPhoto.sd_setImage(with: URL(string: srcDefault), placeholderImage: nil, completed: { (image, error, cache, url) in
                let resizedImage = image?.cropToBounds(image: image!, width: 106, height: 106)
                cell.userPhoto.image = resizedImage
            })
        } else {
            return cell
        }
        return cell
    }
    
    
    func setupCell(data: [UserGift]) {
        countOfGiftsLabel.text = String(format: NSLocalizedString("NumberOfGifts", comment: ""), data.count)
        gifts = data
        collectionView.reloadData()
    }
}
