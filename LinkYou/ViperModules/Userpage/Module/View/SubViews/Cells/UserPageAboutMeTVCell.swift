//
//  UserPageAboutMeTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 06.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class UserPageAboutMeTVCell: UITableViewCell {
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var relationshipLabel: UILabel!
    @IBOutlet weak var orientationLabel: UILabel!
    @IBOutlet weak var childrenLabel: UILabel!
    @IBOutlet weak var smokeLabel: UILabel!
    @IBOutlet weak var alcoholLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var aboutMeLabel: UILabel!
    @IBOutlet weak var labelBottomConstraint: NSLayoutConstraint!
    
    // stackView
    @IBOutlet weak var heightStack: UIStackView!
    @IBOutlet weak var relationShipStack: UIStackView!
    @IBOutlet weak var orientationStack: UIStackView!
    @IBOutlet weak var childrensStack: UIStackView!
    @IBOutlet weak var smokeStack: UIStackView!
    @IBOutlet weak var alcoStack: UIStackView!
    @IBOutlet weak var weightStack: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    func setupCell(aboutMe: User) {

        if let mainText = aboutMe.about {
            aboutMeLabel.isEnabled = false
            aboutMeLabel.text = mainText
            self.updateConstraints()
        } else {
            self.updateConstraints()
            labelBottomConstraint.constant = 0
        }
        
        if let height = aboutMe.height {
//            heightStack.isHidden = false
            heightLabel.text = height.toString()
        } else {
//            heightStack.isHidden = true
        }
        
        if let relationShip = aboutMe.relationship?.name {
//            relationShipStack.isHidden = false
            relationshipLabel.text = relationShip
        } else {
//            relationShipStack.isHidden = true
        }
        
        if let orientation = aboutMe.orientation?.name {
//            orientationStack.isHidden = false
            orientationLabel.text = orientation
        } else {
//            orientationStack.isHidden = true
        }
        
        if let children = aboutMe.children?.name {
//            childrensStack.isHidden = false
            childrenLabel.text = children
        } else {
//            childrensStack.isHidden = true
        }
        
        if let smoke = aboutMe.smoking?.name {
//            smokeStack.isHidden = false
            smokeLabel.text = smoke
        } else {
//            smokeStack.isHidden = true
        }
        
        if let alcohol = aboutMe.alcohol?.name {
//            alcoStack.isHidden = false
            alcoholLabel.text = alcohol
        } else {
//            alcoStack.isHidden = true
        }
        
        if let weight = aboutMe.weight {
//            weightStack.isHidden = false
            weightLabel.text = weight.toString()
        } else {
//            weightStack.isHidden = true
        }
    }
}







