//
//  UserpageBlogTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 06.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class UserpageBlogTVCell: UITableViewCell {
    
    weak var delegate: CellButtonActionHandlerType!
    var userId = 0
    var currentPostId = 0
    
    @IBOutlet weak var countOfPost: UILabel!
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var textPostTextView: UITextView!
    @IBOutlet weak var postImageView: UIImageView!
    
    @IBOutlet weak var postLikesBtn: UIButton!
    @IBOutlet weak var postViewBtn: UIButton!
    @IBOutlet weak var postCommentBtn: UIButton!
    @IBOutlet weak var morePostBtn: RoundButtonCustom!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textPostTextView.addTapGestureRecognizer {
            self.delegate.buttonPressed(cell: self, actionType: .showSelectedPost)
        }
        postImageView.addTapGestureRecognizer {
            self.delegate.buttonPressed(cell: self, actionType: .showSelectedPost)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        // Configure the view for the selected state
    }
    
    func setupCell(blogData: User) {
        userId = blogData.id ?? 0
        guard let blogs = blogData.ublogs else { return }
        if let count = blogs.count, count > 0 {
            countOfPost.text = String(format: NSLocalizedString("NumberOfBlogs", comment: ""), count)
            guard let last = blogs.last, let id = last.id else { return }
            self.currentPostId = id
            userNameLabel.text = blogData.name ?? ""
            postDateLabel.text = last.last_update
            if let urlString = blogData.avatar?.src?.square {
                userAvatarImageView.sd_setImage(with: URL(string: urlString), placeholderImage: nil)
                userAvatarImageView.layer.cornerRadius = userAvatarImageView.bounds.height / 2
                userAvatarImageView.clipsToBounds = true
            }
            
            textPostTextView.text = last.text_short
            postLikesBtn.setTitle(String(format: NSLocalizedString("NumberOfLikes", comment: ""), last.likes_count ?? 0), for: .normal)
            postViewBtn.setTitle("\(last.views_count ?? 0)", for: .normal)
            postCommentBtn.setTitle(String(format: NSLocalizedString("NumberOfComments", comment: ""), last.comments_count ?? 0), for: .normal)
            
            if last.is_liked ?? false {
                postLikesBtn.setImage(#imageLiteral(resourceName: "heart-select-userPage"), for: .normal)
            } else {
                postLikesBtn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
            }
            
            if let urlString = blogData.ublogs?.last?.photos?.first {
                postImageView.sd_setImage(with: URL(string: "https:\(urlString)"), placeholderImage: nil)
            }
            
        } else {
            countOfPost.text = "0 Записей"
            return
        }
    }
    
    @IBAction func pressMorePost(_ sender: RoundButtonCustom) {
        delegate.buttonPressed(cell: self, actionType: .showMoreBlog)
    }
    
    @IBAction func moreDetailsBtn(_ sender: UIButton) {
        delegate.buttonPressed(cell: self, actionType: .pressMoreBtn)
    }
    
    @IBAction func pressPostLike(_ sender: UIButton) {
        // delegate.buttonPressed(cell: self, actionType: .???) какой метод АПИ
        delegate.buttonPressed(cell: self, actionType: .showSelectedPost)
    }
    
    @IBAction func pressShowComments(_ sender: UIButton) {
        delegate.buttonPressed(cell: self, actionType: .showSelectedPost)
    }
    
}


