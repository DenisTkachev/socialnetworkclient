//
//  UserpageRatingTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 05.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import UICircularProgressRing

class UserpageRatingTVCell: UITableViewCell {
    
    @IBOutlet weak var ratingCycleView: UIView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var cicleValue = 0.0
    let ratingRing = UICircularProgressRing(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ratingRing.maxValue = 10.0
        ratingRing.minValue = 0.0
        
        ratingRing.showFloatingPoint = true
        ratingRing.valueIndicator = ""
        ratingRing.decimalPlaces = 1
        ratingRing.innerRingWidth = 3
        ratingRing.outerRingWidth = 3
        ratingRing.ringStyle = .gradient
        ratingRing.gradientColors = [UIColor.ratingLightBlue, UIColor.cerulean, UIColor.ratingGreen]
        ratingRing.gradientColorLocations = [0.25, 0.5, 0.76]
        ratingRing.startAngle = CGFloat(270)
        ratingRing.endAngle = CGFloat(180)
        ratingRing.fontColor = UIColor.charcoalGrey
        ratingRing.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        cicleValue = 0.0
        ratingCycleView.addSubview(ratingRing)
        ratingRing.outerRingColor = .paleGreyTwo
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
    override func prepareForReuse() {
        
        if let cycle = ratingCycleView.subviews.first as? UICircularProgressRing {
            cycle.startProgress(to: UICircularProgressRing.ProgressValue(cicleValue), duration: 1)
        }
    }
    
    func setupCell(data: User) {
        if let rating = data.rating, let likes = rating.likes, let views = rating.views  {
            ratingLabel.text = "\((data.gender?.code == "male" ? "Понравился" : "Понравилась")) \(likes.toString()) из \(views.toString())"
        }
        if let rating = data.rating, let score = rating.score {
            if score <= 0.0 {
//                ratingRing.outerRingWidth = 3
                ratingLabel.text = "Рейтинг формируется"
            } else {
//                ratingRing.outerRingWidth = 3
                cicleValue = score
            }
        }
    }
}


