//
//  UserPageInterestsTVCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class UserPageInterestsTVCell: UITableViewCell {
    
    @IBOutlet weak var tagsCloudView: UIView!
    @IBOutlet weak var cellHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}

extension UserPageInterestsTVCell {
    // tagCloud
    func createTagCloud(withArray data:[TagCloudElement]) {
        if data.count > 0 {
            data.first!.createTagCloud(withArray: data, tagsCloudView: tagsCloudView, cellHeightConstraint: cellHeightConstraint)
        }
    }
}
