//
//  UserPhotosCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class UserPhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userPhoto: UIImageView!
//    let tempImage = [#imageLiteral(resourceName: "giftTemp1").noir, #imageLiteral(resourceName: "giftTemp2").noir, #imageLiteral(resourceName: "giftTemp3").noir]
    
//    let tempImage = [#imageLiteral(resourceName: "giftTemp1").alpha(0.5), #imageLiteral(resourceName: "giftTemp2").alpha(0.5), #imageLiteral(resourceName: "giftTemp3").alpha(0.5)]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
        override func prepareForReuse() {
            let randomIndex = Int.random(in: 0..<28)
            if let image = UIImage(named: "giftTemp" + "\(randomIndex)") {
                userPhoto.image = image.alpha(0.5)
            } else {
                userPhoto.image = #imageLiteral(resourceName: "giftTemp1").alpha(0.5)
            }
        }
}

