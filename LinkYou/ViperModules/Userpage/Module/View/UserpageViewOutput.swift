//
//  UserpageUserpageViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol UserpageViewOutput: class {
    /// - author: KlenMarket
    func sendClaimReport(_ data: ClaimReport)
    func sendUserLike(_ userID: Int)
    func pressMoreManOfDays(userId: Int)
    func deleteUserFromBL(id: Int)
    func addUserToBL(id: Int)
    func requestCountersGallery(_ idPhoto: Int)
    func sendPhotoLike(_ photoID: Int)
    func loadUserPhotos(userId: Int, limit: Int)
    func showUserBlog(_ user: User)
    func showGiftGallery(userId: User)
    func sendCommentForPhoto(id: Int, text: String)
    func deleteComment(_ commentId: Int, _ photoId: Int)
    func showMessageDialog(with user: User)
    func showPhotoAlbum(_ photos: [UserPhotos], user: User?)
    var onlyView: Bool { get }
    var userData: User? { get set }
    func updateUserData()
    func showSinglePost(_ id: Int)
    func showUserPage(_ userId: Int)
}
