//
//  UserpageUserpageViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

protocol UserpageViewInput: class {
    /// - author: KlenMarket
    func showClaimIdAlert(id: Int)
    func setUserData(cloudHeadTags: [TagCloudElement], tagsInterestings: [TagCloudElement])
    func setUserPhoto(userPhotos: [UserPhotos])
    func setAlluserOfDays(alluser: [UsersDaily])
    func setCounters(likes: [PhotoLike], comments: [PhotoComment])
    func setUserGifts(_ data: [UserGift])
    func showAlert(title: String, msg: String)
}
