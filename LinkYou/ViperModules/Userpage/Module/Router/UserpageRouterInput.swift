//
//  UserpageUserpageRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol UserpageRouterInput: class {
    func presentUserBlog(_ user: User)
    func presentGiftGallery(userID: User)
    func presentDialogView(_ user: User)
    func showPhotoAlbum(_ photos: [UserPhotos], user: User)
    func showSinglePost(_ postId: Int)
    func presentUserPage(_ userId: Int)
}
