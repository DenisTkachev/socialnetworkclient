//
//  UserpageUserpageRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 03/08/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class UserpageRouter: UserpageRouterInput {

    enum StorybordsID: String {
        case myBlog = "MyBlogSB"
        case giftGallery = "GiftsGallerySB"
        case dialog = "DialogsSB"
        case photoAlbum = "PhotoAlbumSB"
        case viewSinglePost = "MyBlogViewPostSB"
        case userPage = "UserpageSB"
        
        case messageList = "MessagesSB"
    }

  weak var transitionHandler: TransitionHandler!

    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func presentUserBlog(_ user: User) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .myBlog), to: MyBlogModuleInput.self)
            // Set transition case.
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(user)
        }
                   // .perform()
    }
    
    func presentGiftGallery(userID: User) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .giftGallery), to: GiftsGalleryModuleInput.self)
            // Set transition case.
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(userId: userID)
        }
        // .perform()
    }
    
    func presentDialogView(_ user: User) { // тут косяк надо показывать существующий диалог, а если его нет, то создавать новый
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .messageList), to: MessagesModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.searchExistChatOrCreateNew(user: user)
        }
        
//        try! transitionHandler
//            .forStoryboard(factory: self.useFactory(storyboardID: .dialog), to: DialogsModuleInput.self)
//            .to(preferred: TransitionStyle.navigation(style: .push))
//            .then {
//                moduleInput in
//                moduleInput.newConfigureDialog(user: user)
//        }
    }
    
    func showPhotoAlbum(_ photos: [UserPhotos], user: User) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .photoAlbum), to: PhotoAlbumModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(photos, user: user)
        }
    }
//    MyBlogViewPostViewController
    func showSinglePost(_ postId: Int) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .viewSinglePost), to: MyBlogViewPostModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(postId)
        }
    }
    
    func presentUserPage(_ userId: Int) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .userPage), to: UserpageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(with: userId, allUser: [])
        }
    }
}
