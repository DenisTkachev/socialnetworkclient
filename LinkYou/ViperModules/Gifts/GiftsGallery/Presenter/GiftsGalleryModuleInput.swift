//
//  GiftsGalleryGiftsGalleryModuleInput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol GiftsGalleryModuleInput: class {
    func configure(userId: User)
}
