//
//  GiftsGalleryGiftsGalleryPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class GiftsGalleryPresenter: GiftsGalleryViewOutput {


    // MARK: -
    // MARK: Properties

    weak var view: GiftsGalleryViewInput!
    var interactor: GiftsGalleryInteractorInput!
    var router: GiftsGalleryRouterInput!
    var currentUser: User!
    
    // MARK: -
    // MARK: GiftsGalleryViewOutput
    func viewIsReady() {
        interactor.loadGiftsCategory()
    }
    
    func giftWasSelected(gift: GiftItem) {
        router.showGiftSendPage(gift: gift, selectedUser: currentUser)
    }

}

// MARK: -
// MARK: GiftsGalleryInteractorOutput
extension GiftsGalleryPresenter: GiftsGalleryInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func giftCategoriesGetted(_ categories: [GiftsCategories]) {
        view.setupInitialState(categories: categories)
    }
}

// MARK: -
// MARK: GiftsGalleryModuleInput
extension GiftsGalleryPresenter: GiftsGalleryModuleInput {
    
    func configure(userId: User) {
        currentUser = userId
    }
    
}
