//
//  GiftsGalleryGiftsGalleryInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol GiftsGalleryInteractorOutput: class {
    func giftCategoriesGetted(_ categories: [GiftsCategories])
    func showAlert(title: String, msg: String)
}
