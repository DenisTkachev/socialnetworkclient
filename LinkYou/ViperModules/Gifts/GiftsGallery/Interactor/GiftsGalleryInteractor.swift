//
//  GiftsGalleryGiftsGalleryInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class GiftsGalleryInteractor: GiftsGalleryInteractorInput {

  weak var output: GiftsGalleryInteractorOutput!
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    func loadGiftsCategory() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<GiftsService>(plugins: [authPlagin])
        
        usersProvider.request(.getCategoriesOfGifts) { (result) in
            switch result {
            case .success(let response):
                do {
                    let categories = try JSONDecoder().decode([GiftsCategories].self, from: response.data)
                    self.output.giftCategoriesGetted(self.checkForFreeGift(categories))
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}

extension GiftsGalleryInteractor {
    
    private func checkForFreeGift(_ gifts: [GiftsCategories]) -> [GiftsCategories] {
        if let gift = SuperUser.shared.currentUser?.gifts?.free_gifts, gift > 0 {
            gifts.forEach { (categories) in
                categories.setZeroPrice()
            }
            return gifts
        } else {
            return gifts
        }
    }
    
}
