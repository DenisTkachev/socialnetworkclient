//
//  GiftsGalleryGiftsGalleryInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol GiftsGalleryInteractorInput: class {
    func loadGiftsCategory()
}
