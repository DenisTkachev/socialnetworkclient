//
//  GiftsGalleryGiftsGalleryRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol GiftsGalleryRouterInput: class {
    func showGiftSendPage(gift: GiftItem, selectedUser: User)
}
