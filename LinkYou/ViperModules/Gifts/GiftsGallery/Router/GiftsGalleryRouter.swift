//
//  GiftsGalleryGiftsGalleryRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class GiftsGalleryRouter: GiftsGalleryRouterInput {
    
    enum StorybordsID: String {
        case giftPage = "GiftSendPageSB"
    }
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func showGiftSendPage(gift: GiftItem, selectedUser: User) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .giftPage), to: GiftSendPageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(gift: gift, selectedUser: selectedUser)
        }
    }

  weak var transitionHandler: TransitionHandler!

}
