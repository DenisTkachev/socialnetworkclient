//
//  GiftsGalleryGiftsGalleryAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class GiftsGalleryAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(GiftsGalleryInteractor.self) { (r, presenter: GiftsGalleryPresenter) in
			let interactor = GiftsGalleryInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(GiftsGalleryRouter.self) { (r, viewController: GiftsGalleryViewController) in
			let router = GiftsGalleryRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(GiftsGalleryPresenter.self) { (r, viewController: GiftsGalleryViewController) in
			let presenter = GiftsGalleryPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(GiftsGalleryInteractor.self, argument: presenter)
			presenter.router = r.resolve(GiftsGalleryRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(GiftsGalleryViewController.self) { r, viewController in
			viewController.output = r.resolve(GiftsGalleryPresenter.self, argument: viewController)
		}
	}

}
