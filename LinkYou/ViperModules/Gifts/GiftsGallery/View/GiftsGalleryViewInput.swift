//
//  GiftsGalleryGiftsGalleryViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol GiftsGalleryViewInput: class {
    /// - author: KlenMarket
    func setupInitialState(categories: [GiftsCategories])
    func showAlert(title: String, msg: String)
}
