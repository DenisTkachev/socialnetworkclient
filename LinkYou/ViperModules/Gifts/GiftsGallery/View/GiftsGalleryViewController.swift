//
//  GiftsGalleryGiftsGalleryViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
//import PKHUD

protocol GiftPaymentType: class {
    func giftWasPayed()
}

final class GiftsGalleryViewController: UIViewController {
    
    // MARK: -
    // MARK: Properties
    var output: GiftsGalleryViewOutput!
    var selectedIndex = IndexPath(row: 0, section: 0)
    
    var giftCategories = [GiftsCategories]()
    let semaphore = DispatchSemaphore(value: 1)
    var queue = OperationQueue()
    
    @IBOutlet weak var collectionViewHeader: UICollectionView!
    @IBOutlet weak var collectionViewBody: UICollectionView!
    
    // MARK: -
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //HUD.show(.progress)
        setupNavBar()
        setupCollection()
        setupSwipe()
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if giftCategories.count > 0 {
            collectionViewHeader.selectItem(at: selectedIndex, animated: false, scrollPosition: .centeredHorizontally)
        }
    }
    
    func setupNavBar() {
        title = "Выбрать подарок"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func setupCollection() {
        collectionViewHeader.register(UINib(nibName: "GiftMenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GiftMenuCollectionViewCell")
        collectionViewHeader.delegate = self
        collectionViewHeader.dataSource = self
        
        collectionViewBody.register(UINib(nibName: "GiftCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GiftCollectionViewCell")
        collectionViewBody.delegate = self
        collectionViewBody.dataSource = self
        output.viewIsReady()
    }
    
    func setupSwipe() {
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        leftSwipe.direction = .left
        collectionViewBody.addGestureRecognizer(rightSwipe)
        collectionViewBody.addGestureRecognizer(leftSwipe)
    }

    
}

// MARK: -
// MARK: GiftsGalleryViewInput
extension GiftsGalleryViewController: GiftsGalleryViewInput {
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            //HUD.hide()
//            self.refreshControl.endRefreshing()
        }
    }
    
    func setupInitialState(categories: [GiftsCategories]) {
        self.giftCategories = categories
        collectionViewHeader.reloadData()
        collectionViewBody.reloadData()
        //HUD.hide()
        self.collectionViewHeader.performBatchUpdates(nil, completion: {
            (result) in
            for i in self.collectionViewHeader.subviews { // удаляем старую полоску, если она уже была
                if i.tag == 777 {
                    i.removeFromSuperview()
                }
            } // добавляем полоску
            let lineView = UIView(frame: CGRect(x: 0, y: 39.5, width: self.collectionViewHeader.contentSize.width, height: 1))
            lineView.backgroundColor = UIColor.paleGrey
            lineView.tag = 777
            lineView.layer.zPosition = -1
            self.collectionViewHeader.addSubview(lineView)
        })
    }
    
}

extension GiftsGalleryViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewHeader {
            selectedIndex = indexPath
            let currentRow = collectionView.cellForItem(at: indexPath) as! GiftMenuCollectionViewCell
            currentRow.lineSelector.isHidden = false
            collectionViewHeader.reloadData()
            collectionViewBody.reloadData()
        } else {
            // подарок выбран
            let i = selectedIndex.row
            guard let gift = giftCategories[i].items?[indexPath.row] else { return }
            if let freeGifts = SuperUser.shared.currentUser?.gifts?.free_gifts, freeGifts > 0 {
                // есть бесплатные подарки
                output.giftWasSelected(gift: gift)
            } else {
                // надо оплатить
                let _ = PopUpPaymentVCStep1.init(tizerType: .gift, giftID: gift.id)
                // если подарок успешно оплачен, то метод
                
                let resumeAfterPay = BlockOperation(block: {
//                    OperationQueue.main.addOperation {
                    self.allowPresentSendPage(gift)
//                    }
                })
                queue.isSuspended = true
                queue.addOperation(resumeAfterPay)
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewHeader {
            if let prevSelectRow = collectionView.cellForItem(at: indexPath) as? GiftMenuCollectionViewCell {
                prevSelectRow.lineSelector.isHidden = true
            }
        }
    }
    
}

extension GiftsGalleryViewController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == collectionViewHeader {
            return 1
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewHeader {
            return giftCategories.count
        } else {
            if giftCategories.count > 0, let items = giftCategories[selectedIndex.row].items {
                return items.count
            } else {
                return 0
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewHeader {
            let cell = collectionViewHeader.dequeueReusableCell(withReuseIdentifier: "GiftMenuCollectionViewCell", for: indexPath) as! GiftMenuCollectionViewCell
            if indexPath.row == selectedIndex.row {
                cell.lineSelector.isHidden = false
            }
            cell.giftCategoryName.text = giftCategories[indexPath.row].name!
            return cell
            
        } else {
            let cell = collectionViewBody.dequeueReusableCell(withReuseIdentifier: "GiftCollectionViewCell", for: indexPath) as! GiftCollectionViewCell
            let currentCatategory = giftCategories[selectedIndex.row]

            if let url = currentCatategory.items?[indexPath.row].picture {
                cell.giftImageView.sd_setImage(with: URL(string: url)) { (image, error, cache, url) in
                    if let currentItems = currentCatategory.items?[indexPath.row] {
                        if let price = currentItems.cost {
                            cell.giftPriceLabel.text = "\(price.toString()) руб."
                        } else {
                            cell.giftPriceLabel.text = "Бесплатно"
                        }
                    }
                }
            }
            return cell
        }
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        if sender.state == .ended {
            switch sender.direction {
            case .right:
            selectedIndex = updateIndexCategory(category: IndexPath(row: selectedIndex.row - 1, section: selectedIndex.section))
            collectionViewBody.reloadData()
            collectionViewHeader.reloadData()
            collectionViewHeader.scrollToItem(at: selectedIndex, at: .centeredHorizontally, animated: true)
            
            case .left:
            selectedIndex = updateIndexCategory(category: IndexPath(row: selectedIndex.row + 1, section: selectedIndex.section))
            collectionViewBody.reloadData()
            collectionViewHeader.reloadData()
            collectionViewHeader.scrollToItem(at: selectedIndex, at: .centeredHorizontally, animated: true)

            default: break
            }
        }
    }
    
   private func updateIndexCategory(category: IndexPath) -> IndexPath {
        var result = IndexPath(row: 0, section: 0)
        
        if category.row == giftCategories.count {
            return IndexPath(row: giftCategories.count - 1, section: 0)
        } else if category.row < 0 {
            result = IndexPath(row: 0, section: 0)
        } else if category.row > 0, category.row <= giftCategories.count - 1 {
            result = category
        }
        return result
    }
}

extension GiftsGalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewHeader {
            let wordWidth = giftCategories[indexPath.row].name!.widthOfString(usingFont: UIFont.systemFont(ofSize: 13, weight: .semibold))
            return CGSize(width: wordWidth + 8, height: 50)
        } else {
            
            let width = (view.bounds.width / 2) - 32
            return CGSize(width: width, height: 175)
        }
        
    }
}

extension GiftsGalleryViewController: GiftPaymentType {
    
    func giftWasPayed() {
        queue.isSuspended = false
        print("Окно оплаты закрыто")
    }
    
    private func allowPresentSendPage(_ gift: GiftItem) {
        print("Оплачено и можно дарить подарок")
        output.giftWasSelected(gift: gift)
    }

}
