//
//  GiftsGalleryGiftsGalleryViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol GiftsGalleryViewOutput: class {
    /// - author: KlenMarket
    func viewIsReady()
    func giftWasSelected(gift: GiftItem)
}
