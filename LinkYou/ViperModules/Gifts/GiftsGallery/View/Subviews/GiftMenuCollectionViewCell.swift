//
//  GiftMenuCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 17.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class GiftMenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lineSelector: UIView!
    @IBOutlet weak var giftCategoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        lineSelector.isHidden = true
    }
    
}
