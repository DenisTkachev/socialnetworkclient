//
//  GiftCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class GiftCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var giftImageView: UIImageView!
    @IBOutlet weak var giftPriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
