//
//  GiftSendPageGiftSendPageViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol GiftSendPageViewInput: class {
  /// - author: KlenMarket
    func setupInitialState(gift: GiftItem, user: User)
    func showAlert(title: String, msg: String)
}
