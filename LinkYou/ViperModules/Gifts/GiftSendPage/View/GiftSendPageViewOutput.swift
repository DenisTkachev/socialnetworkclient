//
//  GiftSendPageGiftSendPageViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol GiftSendPageViewOutput: class {
  /// - author: KlenMarket
  func viewIsReady()
}
