//
//  GiftSendPageGiftSendPageViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 19/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
//import PKHUD
final class GiftSendPageViewController: UIViewController {

	// MARK: -
	// MARK: Properties
	var output: GiftSendPageViewOutput!
    var gift: GiftItem?
    var user: User?
    
    @IBOutlet weak var userAvatarImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userProfLabel: UILabel!
    @IBOutlet weak var giftImageView: UIImageView!
    @IBOutlet weak var greenCheckMarkImage: UIImageView!
    @IBOutlet weak var commentTextField: TextFieldCustom!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!

    // MARK: -
	// MARK: Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		output.viewIsReady()
        setupNavBar()
        setupGesture()
//        HUD.show(.progress, onView: self.view)
	}

    override func viewDidAppear(_ animated: Bool) {
//        HUD.hide()
        if !Reachability.isConnectedToNetwork() {
            showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
    }
    
    @objc func endEditing(_ recognizer: UITapGestureRecognizer) {
        commentTextField.resignFirstResponder()
    }
    
    @objc func pressCheckMark(_ recognizer: UITapGestureRecognizer) {
        greenCheckMarkImage.isHidden = !greenCheckMarkImage.isHidden
    }

    func setupGesture() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        commentTextField.delegate = self
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        let tapCheckMark = UITapGestureRecognizer(target: self.view, action: #selector(pressCheckMark(_:)))
        greenCheckMarkImage.addGestureRecognizer(tapCheckMark)
    }
    
    func setupNavBar() {
        title = "Сделать подарок"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func setupView() {
        guard let gift = self.gift, let user = user else { return }
        if let userName = user.name, let prof = user.job?.profession, let userAge = user.birthday?.age, let avatarURL = user.avatar?.src?.square, let occupation = user.job?.occupation {
            userNameLabel.text = "\(userName), \(userAge)"
            userProfLabel.text = "\(prof), \(occupation)"
            userAvatarImage.sd_setImage(with: URL(string: avatarURL)) { (image, error, cache, url) in
                self.userAvatarImage.roundedImage(25)
            }
            
            if let giftImageUrl = gift.picture {
                giftImageView.sd_setImage(with: URL(string: giftImageUrl)) { (image, error, cache, url) in
                }
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            viewBottomConstraint.constant = -keyboardHeight
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//            let keyboardHeight = keyboardSize.height
            viewBottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func pressSendBtn(_ sender: RoundButtonCustom) {
        //TODO: добавить метод отправки подарка
    }
}

// MARK: -
// MARK: GiftSendPageViewInput
extension GiftSendPageViewController: GiftSendPageViewInput {

    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
//            HUD.hide()
//            self.refreshControl.endRefreshing()
        }
    }
    
    func setupInitialState(gift: GiftItem, user: User) {
        self.gift = gift
        self.user = user
        setupView()
	}

}

extension GiftSendPageViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {

        
    }

}
