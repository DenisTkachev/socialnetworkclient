//
//  GiftSendPageGiftSendPagePresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 19/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class GiftSendPagePresenter: GiftSendPageViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: GiftSendPageViewInput!
    var interactor: GiftSendPageInteractorInput!
    var router: GiftSendPageRouterInput!
    var selectedGift: GiftItem!
    var selectedUser: User!
    
    // MARK: -
    // MARK: GiftSendPageViewOutput
    func viewIsReady() {
        view.setupInitialState(gift: selectedGift, user: selectedUser)
    }

}

// MARK: -
// MARK: GiftSendPageInteractorOutput
extension GiftSendPagePresenter: GiftSendPageInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }

}

// MARK: -
// MARK: GiftSendPageModuleInput
extension GiftSendPagePresenter: GiftSendPageModuleInput {
    func configure(gift: GiftItem, selectedUser: User) {
        self.selectedGift = gift
        self.selectedUser = selectedUser
    }
    
}
