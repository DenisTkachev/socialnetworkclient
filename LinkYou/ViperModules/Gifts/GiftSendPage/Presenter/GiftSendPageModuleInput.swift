//
//  GiftSendPageGiftSendPageModuleInput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol GiftSendPageModuleInput: class {
    func configure(gift: GiftItem, selectedUser: User)
}
