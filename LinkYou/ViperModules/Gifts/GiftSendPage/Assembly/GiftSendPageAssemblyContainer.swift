//
//  GiftSendPageGiftSendPageAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 19/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class GiftSendPageAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(GiftSendPageInteractor.self) { (r, presenter: GiftSendPagePresenter) in
			let interactor = GiftSendPageInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(GiftSendPageRouter.self) { (r, viewController: GiftSendPageViewController) in
			let router = GiftSendPageRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(GiftSendPagePresenter.self) { (r, viewController: GiftSendPageViewController) in
			let presenter = GiftSendPagePresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(GiftSendPageInteractor.self, argument: presenter)
			presenter.router = r.resolve(GiftSendPageRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(GiftSendPageViewController.self) { r, viewController in
			viewController.output = r.resolve(GiftSendPagePresenter.self, argument: viewController)
		}
	}

}
