//
//  GiftSendPageGiftSendPageInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol GiftSendPageInteractorOutput: class {
    func showAlert(title: String, msg: String)
}
