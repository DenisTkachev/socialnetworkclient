//
//  UsersMatchedUsersMatchedPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class UsersMatchedPresenter: UsersMatchedViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: UsersMatchedViewInput!
    var interactor: UsersMatchedInteractorInput!
    var router: UsersMatchedRouterInput!

    // MARK: -
    // MARK: UsersMatchedViewOutput
    func viewIsReady() {
        interactor.loadUsers()
    }

    func showUserPage(user: User, allUser: [User]) {
        router.presentUserPage(user: user, allUsers: allUser)
    }
}

// MARK: -
// MARK: UsersMatchedInteractorOutput
extension UsersMatchedPresenter: UsersMatchedInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func matchedList(_ matchedList: [User]) {
        if matchedList.count > 0 {
        view.setupInitialState(matchedList)
            view.showNoDataView(condition: true)
        } else {
            view.showNoDataView(condition: false)
        }
    }
}

// MARK: -
// MARK: UsersMatchedModuleInput
extension UsersMatchedPresenter: UsersMatchedModuleInput {


}
