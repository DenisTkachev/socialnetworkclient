//
//  UsersMatchedUsersMatchedRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol UsersMatchedRouterInput: class {
    func presentUserPage(user: User, allUsers: [User])
}
