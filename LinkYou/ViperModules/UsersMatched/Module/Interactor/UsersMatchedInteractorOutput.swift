//
//  UsersMatchedUsersMatchedInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol UsersMatchedInteractorOutput: class {
    func matchedList(_ matchedList: [User])
    func showAlert(title: String, msg: String)
}
