//
//  UsersMatchedUsersMatchedInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class UsersMatchedInteractor: UsersMatchedInteractorInput {

  weak var output: UsersMatchedInteractorOutput!
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    func loadUsers() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.matchedUsers) { (result) in
            switch result {
            case .success(let response):
                do {
                    let matchedList = try JSONDecoder().decode([User].self, from: response.data)
                    self.output.matchedList(matchedList)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
