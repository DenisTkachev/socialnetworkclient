//
//  UsersMatchedUsersMatchedViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol UsersMatchedViewInput: class {
    /// - author: KlenMarket
    func setupInitialState(_ matchedList: [User])
    func showAlert(title: String, msg: String)
    func showNoDataView(condition: Bool)
}
