//
//  UsersMatchedUsersMatchedViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol UsersMatchedViewOutput: class {
    /// - author: KlenMarket
    func viewIsReady()
    func showUserPage(user: User, allUser: [User])
}
