//
//  UsersMatchedUsersMatchedViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

final class UsersMatchedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	// MARK: -
	// MARK: Properties
	var output: UsersMatchedViewOutput!
    var matchedUsers = [User]()
    let noDataView = NoDataView()
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: -
	// MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.show(.progress, onView: self.view)
        output.viewIsReady()
        setupTableView()
        
        title = "Вам подходят"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        setupNoDataView()
    }
    
    private func setupNoDataView() {
        noDataView.isHidden = true
        noDataView.frame = self.view.frame
        noDataView.delegate = self
        noDataView.setText(title: "Тут пусто", body: "Пока ничиего нет для вас. Попробуйте заполнить больше полей анкеты.", buttonText: nil) // можно отправлять в редактор анкеты
        
        self.view.addSubview(noDataView)
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: "UsersMatchedTableViewCell", bundle: nil), forCellReuseIdentifier: "UsersMatchedTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchedUsers.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersMatchedTableViewCell", for: indexPath) as! UsersMatchedTableViewCell
        cell.setupCell(data: matchedUsers[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        output.showUserPage(user: matchedUsers[indexPath.row], allUser: matchedUsers)
    }

}

// MARK: -
// MARK: UsersMatchedViewInput
extension UsersMatchedViewController: UsersMatchedViewInput {
    
    func showNoDataView(condition: Bool) {
        noDataView.setHidden(condition: condition)
        HUD.hide()
    }

    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
            //self.refreshControl.endRefreshing()
        }
    }
    
	func setupInitialState(_ matchedList: [User]) {
        self.matchedUsers = matchedList
        tableView.reloadData()
        HUD.hide()
	}

}

extension UsersMatchedViewController: NoDataViewDelegate {
    
    // можно отправлять в редактор анкеты
    func buttonWasPressed() {
       print("pressed btn!")
    }
    
}
