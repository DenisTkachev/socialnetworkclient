//
//  UsersMatchedUsersMatchedAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class UsersMatchedAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(UsersMatchedInteractor.self) { (r, presenter: UsersMatchedPresenter) in
			let interactor = UsersMatchedInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(UsersMatchedRouter.self) { (r, viewController: UsersMatchedViewController) in
			let router = UsersMatchedRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(UsersMatchedPresenter.self) { (r, viewController: UsersMatchedViewController) in
			let presenter = UsersMatchedPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(UsersMatchedInteractor.self, argument: presenter)
			presenter.router = r.resolve(UsersMatchedRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(UsersMatchedViewController.self) { r, viewController in
			viewController.output = r.resolve(UsersMatchedPresenter.self, argument: viewController)
		}
	}

}
