//
//  OurBlogOurBlogAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class OurBlogAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(OurBlogInteractor.self) { (r, presenter: OurBlogPresenter) in
			let interactor = OurBlogInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(OurBlogRouter.self) { (r, viewController: OurBlogViewController) in
			let router = OurBlogRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(OurBlogPresenter.self) { (r, viewController: OurBlogViewController) in
			let presenter = OurBlogPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(OurBlogInteractor.self, argument: presenter)
			presenter.router = r.resolve(OurBlogRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(OurBlogViewController.self) { r, viewController in
			viewController.output = r.resolve(OurBlogPresenter.self, argument: viewController)
		}
	}

}
