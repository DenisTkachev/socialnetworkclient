//
//  OurBlogOurBlogInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol OurBlogInteractorInput: class {
    func requestOurBlog()
    func loadFullText(_ id: Int)
}
