//
//  OurBlogOurBlogInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class OurBlogInteractor: OurBlogInteractorInput {
    weak var output: OurBlogInteractorOutput!
    
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)

    
    
    func requestOurBlog() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        usersProvider.request(.ourBlog) { (result) in
            switch result {
            case .success(let response):
                do {
                    let ourBlog = try JSONDecoder().decode([OurBlog].self, from: response.data)
                    self.output.blogGetted(ourBlog)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
                print("Отправка поста на сервер не прошла")
            }
        }
    }
    
    
    func loadFullText(_ id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UblogService>(plugins: [authPlagin])
        usersProvider.request(.getArticle(articleId: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let onePost = try JSONDecoder().decode(OurBlog.self, from: response.data)
                    self.output.returnFullText(onePost)
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
                print("Отправка поста на сервер не прошла")
            }
        }
    }
    
    
}
