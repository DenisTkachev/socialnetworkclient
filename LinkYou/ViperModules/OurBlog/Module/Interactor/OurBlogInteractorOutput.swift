//
//  OurBlogOurBlogInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol OurBlogInteractorOutput: class {
    func blogGetted(_ blogs: [OurBlog])
    func showAlert(title: String, msg: String)
    func returnFullText(_ post: OurBlog)
}
