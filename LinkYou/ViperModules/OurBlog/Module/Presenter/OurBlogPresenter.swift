//
//  OurBlogOurBlogPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class OurBlogPresenter: OurBlogViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: OurBlogViewInput!
    var interactor: OurBlogInteractorInput!
    var router: OurBlogRouterInput!
    var blogs = [OurBlog]()
    
    // MARK: -
    // MARK: OurBlogViewOutput
    func viewIsReady() {
        interactor.requestOurBlog()
    }
    
    func loadFullText(id: Int?) {
        if let idInt = id {
            interactor.loadFullText(idInt)
        }
    }

}

// MARK: -
// MARK: OurBlogInteractorOutput
extension OurBlogPresenter: OurBlogInteractorOutput {
    
    func returnFullText(_ post: OurBlog) {
        view.presentFullPost(post)
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func blogGetted(_ blogs: [OurBlog]) {
        self.blogs = blogs
        view.setupInitialState()
    }
    
}

// MARK: -
// MARK: OurBlogModuleInput
extension OurBlogPresenter: OurBlogModuleInput {


}
