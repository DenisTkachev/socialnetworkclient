//
//  OurBlogTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 21.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class OurBlogTableViewCell: UITableViewCell {

    
    @IBOutlet weak var postNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    
    @IBOutlet weak var shortPostTextView: UITextView!
    @IBOutlet weak var postImageView: UIImageView!
    
    @IBOutlet weak var postLikesBtn: UIButton!
    @IBOutlet weak var postViewBtn: UIButton!
    @IBOutlet weak var postCommentBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        // Configure the view for the selected state
    }
    
    func setupCell(post: OurBlog) {
        guard let postName = post.name, let postDate = post.datetime?.toyyyyMMdd(), let shortText = post.text_short else { return }
        postNameLabel.text = postName
        postDateLabel.text = postDate
        shortPostTextView.text = shortText
        
        if let imgUrl = post.images?.first, let url = URL(string: imgUrl) {
            postImageView.sd_setImage(with: url, completed: nil)
        }
        
        if let postLike = post.likes_count, let postComment = post.comments_count {            
            postLikesBtn.setTitle(String(format: NSLocalizedString("NumberOfLikes", comment: ""), postLike), for: .normal)
            postCommentBtn.setTitle(String(format: NSLocalizedString("NumberOfComments", comment: ""), postComment), for: .normal)
        }
        
        if post.is_liked ?? false {
            postLikesBtn.setImage(#imageLiteral(resourceName: "heart-select-userPage"), for: .normal)
        } else {
            postLikesBtn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
        }

        
    }
    
}
