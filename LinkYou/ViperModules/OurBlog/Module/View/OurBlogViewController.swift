//
//  OurBlogOurBlogViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

final class OurBlogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	// MARK: -
	// MARK: Properties
	var output: OurBlogViewOutput!
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: -
	// MARK: Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		output.viewIsReady()
        setupTableView()
        
        title = "Наш блог"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        HUD.show(.progress)
        
	}
    
    func setupTableView() {
        tableView.register(UINib(nibName: "OurBlogTableViewCell", bundle: nil), forCellReuseIdentifier: "OurBlogTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        HUD.hide()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.blogs.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OurBlogTableViewCell", for: indexPath) as! OurBlogTableViewCell
        cell.setupCell(post: output.blogs[indexPath.row])
    
        // TODO: добавить вывод видео через webview после переходна на xcode 10
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        output.loadFullText(id: output.blogs[indexPath.row].id)
    }
    
}

// MARK: -
// MARK: OurBlogViewInput
extension OurBlogViewController: OurBlogViewInput {
    
    func presentFullPost(_ post: OurBlog) {
        let fullTextController = UIViewController()
        fullTextController.view.backgroundColor = .white
        let webView = UIWebView.init(frame: fullTextController.view.bounds)
        webView.backgroundColor = .white
        
        webView.loadHTMLString("<html>\(post.text ?? "")</html>", baseURL: nil)
        fullTextController.view.addSubview(webView)
        self.navigationController?.pushViewController(fullTextController, animated: true)
        
    }

    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
//            self.refreshControl.endRefreshing()
        }
    }
    
	func setupInitialState() {
        tableView.reloadData()
        HUD.hide()
	}

}
