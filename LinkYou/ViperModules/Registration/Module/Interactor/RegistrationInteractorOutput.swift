//
//  RegistrationRegistrationInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import Foundation

protocol RegistrationInteractorOutput: class {
    func showAlert(title: String, msg: String)
    func loadMainSB()
    func loadEditorRegistration()
    func forgotEmailWasSend()
    func viewInput(on: Bool)
}
