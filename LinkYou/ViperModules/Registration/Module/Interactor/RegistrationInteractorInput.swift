//
//  RegistrationRegistrationInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import Foundation

protocol RegistrationInteractorInput: class {
    func authorization(mode: Int, login: String, password: String)
    func sendRequestForgotPassword(email: String)
}
