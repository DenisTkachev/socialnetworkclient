//
//  RegistrationRegistrationInteractor.swift
//  LinkYou
//
//  Createvary KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//
import Foundation
import Moya
import SwiftKeychainWrapper
import OneSignal

final class RegistrationInteractor: RegistrationInteractorInput {
    weak var output: RegistrationInteractorOutput!
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    
    func sendRequestForgotPassword(email: String) { // TODO: добавить email как параметр
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let authPlagin = AccessTokenPlugin { () -> String in
            DeviceCurrent.current.sessionToken
        }
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.forgotPassword(email: email)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if serverResponse["done"] == true {
                        self.output.forgotEmailWasSend()
                    } else {
                        self.showAlert(title: "Ошибка отправки", error: NSError())
                    }
                } catch {
                    self.showAlert(response: response)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func authorization(mode: Int, login: String, password: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        if mode == 0 { // login
            let authPlagin = AccessTokenPlugin { () -> String in
                DeviceCurrent.current.sessionToken
            }
            let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
            usersProvider.manager.session.configuration.timeoutIntervalForRequest = 10
            
            usersProvider.request(.login(login: login, pass: password)) { (result) in
                switch result {
                case .success(let response):
                    do {
                        let serverResponse = try JSONDecoder().decode(UserSession.self, from: response.data)
                        SuperUser.shared.superUser = serverResponse.user
                        if let token = serverResponse.token, !token.isEmpty, let userId = serverResponse.user.id  {
                            DeviceCurrent.current.sessionToken = token
                            KeychainWrapper.standard.set(login, forKey: "login")
                            KeychainWrapper.standard.set(password, forKey: "pass")
                            OneSignal.setExternalUserId(userId.toString())
                        } else {
                            DeviceCurrent.current.sessionToken = ""
                        }
                        // проверка заполненности профиля
                        if DeviceCurrent.checkRequiredFields() == nil {
                            self.output.loadMainSB()
                        } else {
                            self.output.loadEditorRegistration()
                        }
                    } catch {
                        self.showAlert(response: response)
                    }
                case let .failure(error):
                    if error._code == 6 {
                        //timeout error
                        self.showAlert(title: "Ошибка", error: error as NSError)
                    } else {
                        print(error)
                    }
                }
            }
        } else if mode == 1 { // registration
            let authPlagin = AccessTokenPlugin { () -> String in
                DeviceCurrent.current.sessionToken
            }
            let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin, debugPlagin])
            
            usersProvider.request(.registration(login: login, pass: password)) { (result) in
                switch result {
                case .success(let response):
                    do {
                        let serverResponse = try JSONDecoder().decode(UserSession.self, from: response.data)
                        SuperUser.shared.superUser = serverResponse.user
                        if let token = serverResponse.token, !token.isEmpty, let userId = serverResponse.user.id {
                            DeviceCurrent.current.sessionToken = token
                            KeychainWrapper.standard.set(login, forKey: "login")
                            KeychainWrapper.standard.set(password, forKey: "pass")
                            OneSignal.setExternalUserId(userId.toString())
                        } else {
                            DeviceCurrent.current.sessionToken = ""
                        }
                        self.output.loadEditorRegistration()
                    } catch {
                        self.showAlert(response: response)
                    }
                case let .failure(error):
                    print(error)
                }
            }
        }
    }
    
    func showAlert(response: Response) {
        do {
            let errorResponse = try JSONDecoder().decode(ErrorResponse.self, from: response.data)
            self.output.showAlert(title: "Ошибка!", msg: errorResponse.message ?? "Ошибка парсинга входных данных")
            self.output.viewInput(on: true)
        } catch {
            self.output.showAlert(title: "Ошибка!", msg: error.localizedDescription)
            self.output.viewInput(on: true)
        }
    }
    
    func showAlert(title: String, error: NSError) {
        DispatchQueue.main.async {
            self.output.showAlert(title: title, msg: error.localizedDescription)
            self.output.viewInput(on: true)
        }
    }
    
}

