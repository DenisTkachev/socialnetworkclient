//
//  RegistrationRegistrationViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

protocol RegistrationViewOutput: class {
    /// - author: KlenMarket
    func needAuthorization(mode: Int, login: String, password: String)
    func sendCodeToEmail(email: String)
    func loadGuestMode()
}
