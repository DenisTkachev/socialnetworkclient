//
//  RegistrationRegistrationViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import UIKit
import UIView_Shake
import Simplicity
import DeviceKit
import SwiftKeychainWrapper

final class RegistrationViewController: UIViewController, UITextFieldDelegate {

    // MARK: -
	// MARK: Properties
	var output: RegistrationViewOutput!
    var authorizationMode = 0

    @IBOutlet var forgotPassView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordFiled: UITextField!
    @IBOutlet weak var enterBtn: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var loginBtn: UILabel!
    @IBOutlet weak var segmentControl: CustomSegmentedContrl!
    
    @IBOutlet weak var forgotEmailField: TextFieldCustom!
    @IBOutlet var addItemView: UIView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var separatorLineView: UIView!
    @IBOutlet weak var validateStatusImage: UIImageView!
    
    @IBOutlet weak var versionLable: UILabel!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var forgotBtn: UILabel!
    
    @IBOutlet var wellcomeView: UIView!
    @IBOutlet weak var wellcomeEnterBtn: RoundButtonCustom!
    
    var effect: UIVisualEffect!
        let groupOfSmallDevices: [DeviceKit.Device] = [.iPhoneSE, .simulator(.iPhoneSE)]
        let device = Device()
    
    // MARK: -
	// MARK: Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
        segmentControl.isHidden = true
        prepareViewElements()
        
        effect = visualEffectView.effect
        visualEffectView.effect = nil
        visualEffectView.isHidden = true
        addItemView.layer.cornerRadius = 5
        addItemView.clipsToBounds = true
        self.hideKeyboardWhenTappedAround()
        
        versionLable.text = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
	}
    
    func prepareViewElements() {
        loginBtn.layer.cornerRadius = loginBtn.frame.height / 2
        loginBtn.clipsToBounds = true
        emailField.layer.cornerRadius = emailField.frame.height / 2
        emailField.clipsToBounds = true
        passwordFiled.layer.cornerRadius = emailField.frame.height / 2
        passwordFiled.clipsToBounds = true
        emailField.layer.borderWidth = 1
        emailField.layer.borderColor = UIColor.paleGrey.cgColor
        passwordFiled.layer.borderWidth = 1
        passwordFiled.layer.borderColor = UIColor.paleGrey.cgColor
        forgotEmailField.addTarget(self, action: #selector(RegistrationViewController.textFieldDidChange(_:)), for: .allEvents)
        
        let tapToLoginBtn = UITapGestureRecognizer(target: self, action: #selector(self.tappedToLoginBtn(_:)))
        tapToLoginBtn.numberOfTapsRequired = 1
        loginBtn.isUserInteractionEnabled = true
        loginBtn.addGestureRecognizer(tapToLoginBtn)
        
        
        forgotBtn.addTapGestureRecognizer {
            if self.authorizationMode == 0 {
                self.animateIn()
            } else {
                if let url = URL(string: "https://linkyou.ru/privacy") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        }
        
        setUserField()
        setupWellcomeView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        segmentControl.selector.frame.size.width = separatorLineView.frame.size.width / 2
        segmentControl.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        activityIndicator.stopAnimating()
        loginBtn.isUserInteractionEnabled = true
    }
    
    func viewInput(on: Bool) {
        self.view.isUserInteractionEnabled = on
    }
    
    private func setupWellcomeView() {
        wellcomeEnterBtn.layer.borderColor = UIColor.white.cgColor
        wellcomeEnterBtn.layer.borderWidth = 1
        self.view.addSubview(wellcomeView)
        self.wellcomeView.frame = self.view.frame
        wellcomeView.layoutIfNeeded()
        
    }
    
    @objc func keyboardHandle(notification: NSNotification) {
        let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
        if device.isOneOf(groupOfSmallDevices), visualEffectView.isHidden {
            logoTopConstraint.constant = isKeyboardShowing ? 0 : 78
            
            UIView.animate(withDuration: 0) {
                self.view.layoutIfNeeded()
            }
        } else if device.isOneOf(groupOfSmallDevices), !visualEffectView.isHidden {
            addItemView.frame.origin.y = isKeyboardShowing ? 50 : 164
            UIView.animate(withDuration: 0) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: -
    // MARK: User action
    
    @objc func tappedToLoginBtn(_ recognizer: UITapGestureRecognizer) {
        if let email = emailField.text, let password = passwordFiled.text {
            if !email.isEmpty && !password.isEmpty {
                activityIndicator.startAnimating()
                output.needAuthorization(mode: authorizationMode, login: email, password: password)
                loginBtn.isUserInteractionEnabled = false
                viewInput(on: false)
            } else {
                showAlert(title: "", msg: "Заполните все поля")
            }
        }
    }
    @IBAction func closeRemaindPass(_ sender: UIButton) {
        animateOut()
    }
    
    @IBAction func onChangeOfSegment(_ sender: CustomSegmentedContrl) {
        
        switch sender.selectedSegmentIndex {
            
        case 0: // login
            authorizationMode = 0
            enterBtn.text = "Войти"
            setUserField()
            
            let attributedString = NSMutableAttributedString(string: "Забыли пароль?", attributes: [
                .font: UIFont.systemFont(ofSize: 12.0),
                .foregroundColor: UIColor.steelTwo
                ])
//            forgotBtn.setAttributedTitle(attributedString, for: .normal)
            forgotBtn.attributedText = attributedString
            
        case 1: // registration
            authorizationMode = 1
            emailField.text = ""
            passwordFiled.text = ""
            enterBtn.text = "Зарегистрироваться"
            
            let attributedString = NSMutableAttributedString(string: "Регистрируясь на сайте вы соглашаетесь\nс условиями использования и политикой\nбезопасности", attributes: [
                .font: UIFont.systemFont(ofSize: 12.0),
                .foregroundColor: UIColor.cerulean
                ])
            attributedString.addAttribute(.foregroundColor, value: UIColor.steelTwo, range: NSRange(location: 0, length: 40))
            attributedString.addAttribute(.foregroundColor, value: UIColor.steelTwo, range: NSRange(location: 65, length: 1))
//            forgotBtn.setAttributedTitle(attributedString, for: .normal)
            forgotBtn.attributedText = attributedString
        default:
            break
        }
    }
    
    @IBAction func pressSendCodeToEmail(_ sender: RoundButtonCustom) {
        if let emailString = forgotEmailField.text, emailString.isValidEmailFormat() {
            output.sendCodeToEmail(email: emailString)
        } else {
            forgotPassView.shake(10,
                withDelta: 5.0, speed: 0.03, shakeDirection: ShakeDirection.horizontal)
        }
    }
    
    
    @IBAction func wellcomeRegistrationBtn(_ sender: RoundButtonCustom) {
        segmentControl.selectedSegmentIndex = 1
        segmentControl.buttons.last!.sendActions(for: .touchUpInside)
        segmentControl.sendActions(for: .valueChanged)
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.wellcomeView.removeFromSuperview()
        }, completion: nil)
    }
    
    @IBAction func wellcomeEnterBtn(_ sender: RoundButtonCustom) {
        segmentControl.selectedSegmentIndex = 0
        segmentControl.buttons.first!.sendActions(for: .touchUpInside)
        segmentControl.sendActions(for: .valueChanged)
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.wellcomeView.removeFromSuperview()
        }, completion: nil)
    }
    
    @IBAction func guestEnterBtn(_ sender: UIButton) {
        // как гость с ограниченным просмотром
        output.loadGuestMode()
    }
    
    
    @IBAction func faceBookLoginBtn(_ sender: UIButton) {
        Simplicity.login(Facebook()) { (accessToken, error) in
            // Handle access token here
            print(accessToken)
            print(error)
        }
    }
    
    @IBAction func vkLoginBtn(_ sender: UIButton) {
        Simplicity.login(VKontakte()) { (accessToken, error) in
            print(accessToken)
            print(error)
        }
    }
    
    @objc public func textFieldDidChange(_ textField: UITextField) {
        if let text = textField.text, text.isValidEmailFormat() {
            validateStatusImage.image = #imageLiteral(resourceName: "greenApproveIcon")
        } else if let text = textField.text, text.count > 0 {
            validateStatusImage.image = #imageLiteral(resourceName: "redCrossIcon")
        } else {
            validateStatusImage.image = nil
        }
    }
    
    private func setUserField() {
        if let login = KeychainWrapper.standard.string(forKey: "login"), let pass =  KeychainWrapper.standard.string(forKey: "pass") {
            emailField.text = login
            passwordFiled.text = pass
        }
    }
}



// MARK: -
// MARK: RegistrationViewInput
extension RegistrationViewController: RegistrationViewInput {
    func hidePopUp() {
        animateOut()
    }

    func showAlert(title: String, msg: String) {
        viewInput(on: true)
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            self.loginBtn.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
        }
    }
}

extension RegistrationViewController {
    func animateIn() {
        self.visualEffectView.isHidden = false
        self.view.addSubview(addItemView)
        addItemView.center = self.view.center
        addItemView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        addItemView.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.visualEffectView.effect = self.effect
            self.addItemView.alpha = 1
            self.addItemView.transform = CGAffineTransform.identity
        }
    }
    
    func animateOut () {
        UIView.animate(withDuration: 0.3, animations: {
            
            self.addItemView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.addItemView.alpha = 0
            
            self.visualEffectView.effect = nil
            
        }) { (success:Bool) in
            self.addItemView.removeFromSuperview()
            self.visualEffectView.isHidden = true
        }
    }
}
