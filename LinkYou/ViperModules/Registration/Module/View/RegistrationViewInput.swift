//
//  RegistrationRegistrationViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

protocol RegistrationViewInput: class {
    /// - author: KlenMarket
    func showAlert(title: String, msg: String)
    func hidePopUp()
    func viewInput(on: Bool)
}
