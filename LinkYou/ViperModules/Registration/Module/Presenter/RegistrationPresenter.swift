//
//  RegistrationRegistrationPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

final class RegistrationPresenter: RegistrationViewOutput {
    
    func needAuthorization(mode: Int, login: String, password: String) {
        self.interactor.authorization(mode: mode, login: login, password: password)
    }

    // MARK: -
    // MARK: Properties

    weak var view: RegistrationViewInput!
    var interactor: RegistrationInteractorInput!
    var router: RegistrationRouterInput!

    // MARK: -
    // MARK: RegistrationViewOutput

    func sendCodeToEmail(email: String) {
        interactor.sendRequestForgotPassword(email: email)
    }
}

// MARK: -
// MARK: RegistrationInteractorOutput
extension RegistrationPresenter: RegistrationInteractorOutput {
    func viewInput(on: Bool) {
        view.viewInput(on: true)
    }
    
    func forgotEmailWasSend() {
        view.hidePopUp()
        view.showAlert(title: "Успех", msg: "Письмо было выслано на указанную почту")
    }
    
    func loadMainSB() {
        router.routeToMainSB(guestMode: false)
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func loadEditorRegistration() {
        router.routeToEditorRegistration()
    }
    
    func loadGuestMode() {
        router.routeToMainSB(guestMode: true)
    }
}

// MARK: -
// MARK: RegistrationModuleInput
extension RegistrationPresenter: RegistrationModuleInput {


}
