//
//  RegistrationRegistrationRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import LightRoute

final class RegistrationRouter: RegistrationRouterInput {
    
    enum StorybordsID: String {
        case mainScreen = "MainSB"
        case editorRegistrationForm = "EditorRegistrationSB"
    }
    
    weak var transitionHandler: TransitionHandler!
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func routeToMainSB(guestMode: Bool) {
        DeviceCurrent.current.setGuestMode(is: guestMode)
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .mainScreen), to: MainScreenModuleInput.self)
            .perform()
    }
    
    func routeToEditorRegistration() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .editorRegistrationForm), to: EditorRegistrationModuleInput.self)
            .perform()
    }
}
