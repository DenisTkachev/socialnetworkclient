//
//  RegistrationRegistrationRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import UIKit

protocol RegistrationRouterInput: class {    
    func routeToMainSB(guestMode: Bool)
    func routeToEditorRegistration()
}
