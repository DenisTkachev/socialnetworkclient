//
//  RegistrationRegistrationAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 19/07/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class RegistrationAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(RegistrationInteractor.self) { (r, presenter: RegistrationPresenter) in
			let interactor = RegistrationInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(RegistrationRouter.self) { (r, viewController: RegistrationViewController) in
			let router = RegistrationRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(RegistrationPresenter.self) { (r, viewController: RegistrationViewController) in
			let presenter = RegistrationPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(RegistrationInteractor.self, argument: presenter)
			presenter.router = r.resolve(RegistrationRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(RegistrationViewController.self) { r, viewController in
			viewController.output = r.resolve(RegistrationPresenter.self, argument: viewController)
		}
	}

}
