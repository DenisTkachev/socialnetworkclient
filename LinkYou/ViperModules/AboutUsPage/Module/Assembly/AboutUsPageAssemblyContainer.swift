//
//  AboutUsPageAboutUsPageAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class AboutUsPageAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(AboutUsPageInteractor.self) { (r, presenter: AboutUsPagePresenter) in
			let interactor = AboutUsPageInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(AboutUsPageRouter.self) { (r, viewController: AboutUsPageViewController) in
			let router = AboutUsPageRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(AboutUsPagePresenter.self) { (r, viewController: AboutUsPageViewController) in
			let presenter = AboutUsPagePresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(AboutUsPageInteractor.self, argument: presenter)
			presenter.router = r.resolve(AboutUsPageRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(AboutUsPageViewController.self) { r, viewController in
			viewController.output = r.resolve(AboutUsPagePresenter.self, argument: viewController)
		}
	}

}
