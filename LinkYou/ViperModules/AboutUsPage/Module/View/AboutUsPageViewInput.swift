//
//  AboutUsPageAboutUsPageViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol AboutUsPageViewInput: class {
    /// - author: KlenMarket
    func setupInitialState()
    func showAlert(title: String, msg: String)
}
