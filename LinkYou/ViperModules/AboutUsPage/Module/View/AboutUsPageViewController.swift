//
//  AboutUsPageAboutUsPageViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import WebKit
import PKHUD

final class AboutUsPageViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {

	// MARK: -
	// MARK: Properties
	var output: AboutUsPageViewOutput!
    var webView: WKWebView!
	// MARK: -
	// MARK: Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		output.viewIsReady()
        HUD.show(.progress, onView: webView)
        
        title = "О нас"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: view.frame, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
        let myURL = URL(string:"https://linkyou.ru/about")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
	}

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) { // не всегда срабатывет
        HUD.hide()
    }
    
}

// MARK: -
// MARK: AboutUsPageViewInput
extension AboutUsPageViewController: AboutUsPageViewInput {

    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
//            self.refreshControl.endRefreshing()
        }
    }
    
	func setupInitialState() {

	}

}
