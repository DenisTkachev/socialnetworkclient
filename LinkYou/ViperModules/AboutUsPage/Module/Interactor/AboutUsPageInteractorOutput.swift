//
//  AboutUsPageAboutUsPageInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol AboutUsPageInteractorOutput: class {
    func showAlert(title: String, msg: String)
}
