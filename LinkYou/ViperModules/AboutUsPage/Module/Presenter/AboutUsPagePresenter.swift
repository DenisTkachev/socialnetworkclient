//
//  AboutUsPageAboutUsPagePresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 21/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class AboutUsPagePresenter: AboutUsPageViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: AboutUsPageViewInput!
    var interactor: AboutUsPageInteractorInput!
    var router: AboutUsPageRouterInput!

    // MARK: -
    // MARK: AboutUsPageViewOutput
    func viewIsReady() {

    }

}

// MARK: -
// MARK: AboutUsPageInteractorOutput
extension AboutUsPagePresenter: AboutUsPageInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }

}

// MARK: -
// MARK: AboutUsPageModuleInput
extension AboutUsPagePresenter: AboutUsPageModuleInput {


}
