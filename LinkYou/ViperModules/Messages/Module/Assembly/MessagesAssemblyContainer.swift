//
//  MessagesMessagesAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class MessagesAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(MessagesInteractor.self) { (r, presenter: MessagesPresenter) in
			let interactor = MessagesInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(MessagesRouter.self) { (r, viewController: MessagesViewController) in
			let router = MessagesRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(MessagesPresenter.self) { (r, viewController: MessagesViewController) in
			let presenter = MessagesPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(MessagesInteractor.self, argument: presenter)
			presenter.router = r.resolve(MessagesRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(MessagesViewController.self) { r, viewController in
			viewController.output = r.resolve(MessagesPresenter.self, argument: viewController)
		}
	}

}
