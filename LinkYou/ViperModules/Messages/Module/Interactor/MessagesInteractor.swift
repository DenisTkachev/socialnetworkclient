//
//  MessagesMessagesInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class MessagesInteractor: MessagesInteractorInput {
    
    weak var output: MessagesInteractorOutput!
    
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    func loadDialogs() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }

        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin])
        usersProvider.request(.getDialogs) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([Dialog].self, from: response.data)
                    self.output.returnDialogs(response)
                } catch {
                    print(error)
                    self.output.showNoDataView()
                }
            case let .failure(error):
                print(error)
            }
        }

    }
    
    func showDialog(_ selectedMessage: Dialog) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        guard let messageId = selectedMessage.id else { return }

        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin])
        
        usersProvider.request(.getMessages(id: messageId, page: 0)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let dialogs = try JSONDecoder().decode(ChatMessages.self, from: response.data)
                    if let httpResponse = response.response {
                        let headers = httpResponse.allHeaderFields
                        self.output.showDialog(dialog: dialogs, headers: headers)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }

    func deleteDialog(_ dialogId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }

        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin])
        
        usersProvider.request(.deleteDialog(id: dialogId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode(BoolServerRespone.self, from: response.data)
                    if response.response ?? false { // true
                        self.output.dialogWasDeleted(id: dialogId)
                    } else { // false
                        self.output.showAlert(title: "Ошибка удаления", msg: "Не удалос удалить диалог")
                    }
                    
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadBadges() {

        let usersProvider = MoyaProvider<GeneralService>(plugins: [authPlagin])
        usersProvider.request(.getCurrentBadges) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(Badges.self, from: response.data)
                    self.output.updateBadges(serverResponse)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func shearchExistDialog(with user: User) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<MessageService>(plugins: [authPlagin])
        usersProvider.request(.getDialogs) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([Dialog].self, from: response.data)
                    let result = response.filter({ (dialog) -> Bool in
                        if dialog.fromUser!.id == user.id! {
                            return true
                        } else {
                            return false
                        }
                    })
                    
                    if result.count == 1 {
                        // чат есть с таким юзером
                        self.output.chatExist(result.first!)
                    } else {
                        // надо создать новый чат

                        // в роутер и .forStoryboard(factory: self.useFactory(storyboardID: .dialog), to: DialogsModuleInput.self)
                        //                moduleInput.newConfigureDialog(user: user)
                    }
                    
                } catch {
                    print(error)
                    self.output.showAlert(title: "Ошибка парсинга", msg: "Сервер прислал некорректные данные. Попробуйте позже.")
                }
            case let .failure(error):
                print(error)
            }
        }
        
    }

    
    
}
