//
//  MessagesMessagesInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MessagesInteractorOutput: class {
    func showAlert(title: String, msg: String)
    func returnDialogs(_ dialogs: [Dialog])
    func showDialog(dialog: ChatMessages, headers: [AnyHashable : Any])
    func dialogWasDeleted(id: Int)
    func updateBadges(_ badges: Badges)
    func showNoDataView()
    func chatExist(_ dialog: Dialog)
}
