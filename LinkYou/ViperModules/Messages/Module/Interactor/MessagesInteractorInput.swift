//
//  MessagesMessagesInteractorInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MessagesInteractorInput: class {
    func loadDialogs()
    func showDialog(_ selectedMessage: Dialog)
    func deleteDialog(_ dialogId: Int)
    func loadBadges()
    
    func shearchExistDialog(with user: User)
}
