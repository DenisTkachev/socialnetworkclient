//
//  MessagesMessagesViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import IGListKit
import PKHUD

protocol MessageCellActionType: class {
    func selectRow(_ currentMessage: Dialog)
    func deleteRow(dialogId: Int)
}

// https://medium.com/ios-os-x-development/learn-master-%EF%B8%8F-the-basics-of-iglistkit-in-10-minutes-3b9ce8a2632b
final class MessagesViewController: UIViewController, ListAdapterDataSource {
    
    // MARK: -
    // MARK: Properties
    var output: MessagesViewOutput!
    var allDialogs = [Dialog]()
    let noDataView = NoDataView()
    
    @IBOutlet weak var messagesCollectionView: UICollectionView!
    
    //    lazy var adapter: ListAdapter = {
    //        return ListAdapter(updater: ListAdapterUpdater(),
    //
    //                           viewController: self)
    //    }()
    
    lazy var adapter: ListAdapter = {
        let updater = ListAdapterUpdater()
        let adapter = ListAdapter(updater: updater,
                                  viewController: self,
                                  workingRangeSize: 1)
        adapter.collectionView = messagesCollectionView
        adapter.dataSource = self
        return adapter
    }()
    
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.updateBadges()
        _ = adapter
        
        //
        //        adapter.collectionView = messagesCollectionView
        //        adapter.dataSource = self
        
        setupNotification() 
        setupNoDataView()
    }
    
    private func setupNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(signalFromSocket(notification:)), name: .socketUserOnline, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalFromSocket(notification:)), name: .socketMessageNew, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        output.viewIsReady()
        output.updateBadges()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        HUD.hide()
    }
    
    func isShowHUD(show: Bool) {
        switch show {
        case true: HUD.show(.progress, onView: view)
        case false: HUD.hide()
        }
    }
    
    @objc func signalFromSocket(notification: NSNotification) {
        if notification.name == NSNotification.Name.socketUserOnline || notification.name == NSNotification.Name.socketMessageNew {
            output.refreshData()
            output.updateBadges()
        }
    }
    
    private func setupNoDataView() {
        noDataView.isHidden = true
        noDataView.frame = self.view.frame
        noDataView.delegate = self
        noDataView.setText(title: "Нет диалогов", body: "Вы еще ни с кем не общались, почитайте наши рекомендации по улучшению вашей анкеты в глазах других пользователей.", buttonText: "Продвинуть анкету")
        
        self.view.addSubview(noDataView)
    }
    
    func showNoDataView(condition: Bool) {
        noDataView.setHidden(condition: !condition)
        HUD.hide()
    }
}

// MARK: -
// MARK: MyLikesViewInput
extension MessagesViewController: MessagesViewInput {
    
    func updateBadges(_ badges: Badges) {
        if let tabBar = self.tabBarController?.children[1], let item = tabBar.tabBarItem {
            if badges.messages == 0 {
                item.badgeValue = nil
            } else {
                item.badgeValue = badges.messages.toString()
            }
        }
    }
    
    func updateView(_ dialogs: [Dialog]) {
        allDialogs = dialogs
        adapter.performUpdates(animated: true, completion: nil)
        noDataView.setHidden(condition: allDialogs.count != 0)
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
            
        }
    }
    
    func setupInitialState(_ dialogs: [Dialog]) {
        allDialogs = dialogs
        //        adapter.performUpdates(animated: true, completion: nil) // в этому случае при уведомлении с сокета инфа сразу не обновляется, потому что adapter не различает обьекты online и не online. Принудительная перезагрузка всей таблицы reloadData решает вопрос.
        adapter.reloadData(completion: nil)
    }
}

extension MessagesViewController {
    // MARK: ListAdapterDataSource
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return allDialogs
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return MessagesSectionController.init(delegate: self)
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil } // Returns a view that is displayed if no objects exist
    
}

extension MessagesViewController: MessageCellActionType {
    
    func deleteRow(dialogId: Int) {
        output.deleteDialog(dialogId)
        
    }
    
    func selectRow(_ currentMessage: Dialog) {
        output.dialogSelected(currentMessage)
    }
    
}

extension MessagesViewController: NoDataViewDelegate {
    
    // можно отправлять в редактор анкеты
    func buttonWasPressed() {
        print("pressed btn!")
    }
    
}
