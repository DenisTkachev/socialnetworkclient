//
//  MessagesCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 27.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SwipeCellKit

class MessagesCollectionViewCell: SwipeCollectionViewCell {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var userAvatar: MyAvatarView!
    @IBOutlet weak var photoCameraIconImage: UIImageView!
    
    @IBOutlet weak var photocameraIconConstr: NSLayoutConstraint!
    //    weak var delegate: SwipeCollectionViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        countLabel.layer.cornerRadius = 13
        countLabel.layer.masksToBounds = true

    }
    
    override func prepareForReuse() {
        timeLabel.text = nil
        countLabel.text = nil
        lastMessageLabel.text = nil
        userAvatar.setStartSettings(hidden: true)
        photocameraIconConstr.constant = 0
    }
    
    func updateWith(message: Dialog) {
        guard let userName = message.fromUser?.name else { return }
        userNameLabel.text = userName
        timeLabel.text = message.last_message?.last_update?.toDateHHmm()
        
        if message.unread_messages_count == 0 {
            countLabel.isHidden = true
        } else {
            countLabel.text = message.unread_messages_count?.toString() ?? ""
            countLabel.isHidden = false
            lastMessageLabel.font = UIFont.boldSystemFont(ofSize: 12)
        }
        
        if let lastMessageText = message.last_message?.comment, lastMessageText.count > 0 && !lastMessageText.contains("yandex.ru/maps/?text=")  {
            lastMessageLabel.text = lastMessageText
        } else if let lastMessageText = message.last_message?.comment, lastMessageText.contains("yandex.ru/maps/?text=") {
            lastMessageLabel.text = "Координаты"
        } else {
            lastMessageLabel.text = " Изображение"
            timeLabel.text = message.last_update?.toDateHHmm()
            photocameraIconConstr.constant = 15
            
        }
        
        userAvatar.setAvatar(userData: message.fromUser)
        
    }
}
