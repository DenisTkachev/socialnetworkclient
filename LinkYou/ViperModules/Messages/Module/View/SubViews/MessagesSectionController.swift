//
//  MessagesSectionController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 27.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import IGListKit
import UIKit
import SwipeCellKit

class MessagesSectionController: ListSectionController {
    
    var currentMessage: Dialog?
    weak var delegate: MessageCellActionType!
    
    override func didUpdate(to object: Any) {
        guard let message = object as? Dialog else {
            return
        }
     currentMessage = message
    }
    
    convenience init(delegate: MessageCellActionType) {
        self.init()
        self.delegate = delegate
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let nibName = String(describing: MessagesCollectionViewCell.self)
        guard let ctx = collectionContext, let message = currentMessage else {
            return UICollectionViewCell()
        }

        let cell = ctx.dequeueReusableCell(withNibName: nibName, bundle: nil, for: self, at: index)
        guard let messageCell = cell as? MessagesCollectionViewCell else { return cell }
        messageCell.updateWith(message: message)
        messageCell.delegate = self
        messageCell.backgroundColor = UIColor.white
        return messageCell
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 108)
    }

    override func didSelectItem(at index: Int) {
        guard let currentMessage = currentMessage else { return }
        delegate.selectRow(currentMessage)
    }
    
    
}

extension MessagesSectionController: SwipeCollectionViewCellDelegate {
    
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let cell = collectionView.cellForItem(at: indexPath)
        
        UIView.animate(withDuration: 0.2) {
            cell?.backgroundColor = UIColor.veryLightBlue
        }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Удалить") { action, indexPath in
            guard let id = self.currentMessage?.id else { return }
            self.delegate.deleteRow(dialogId: id)
        }
        
        return [deleteAction]
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndEditingItemAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        if let indexPath = indexPath, let cell = collectionView.cellForItem(at: indexPath) {
            
            UIView.animate(withDuration: 0.2) {
                cell.backgroundColor = UIColor.white
            }
        }
    }
//    func collectionView(_ collectionView: UICollectionView, editActionsOptionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
//        var options = SwipeOptions()
//        options.expansionStyle = orientation == .left ? .selection : .destructive
////        options.transitionStyle = defaultOptions.transitionStyle
//
////        switch buttonStyle {
////        case .backgroundColor:
////            options.buttonSpacing = 11
////        case .circular:
////            options.buttonSpacing = 4
////            options.backgroundColor = #colorLiteral(red: 0.9467939734, green: 0.9468161464, blue: 0.9468042254, alpha: 1)
////        }
//
//        return options
//    }
    
//    func visibleRect(for collectionView: UICollectionView) -> CGRect? {
//        return CGRect(x: 0, y: 0, width: 10, height: 10)
//        if usesTallCells == false { return nil }
//
//        if #available(iOS 11.0, *) {
//            return collectionView.safeAreaLayoutGuide.layoutFrame
//        } else {
//            let topInset = navigationController?.navigationBar.frame.height ?? 0
//            let bottomInset = navigationController?.toolbar?.frame.height ?? 0
//            let bounds = collectionView.bounds
//
//            return CGRect(x: bounds.origin.x, y: bounds.origin.y + topInset, width: bounds.width, height: bounds.height - bottomInset)
//        }
//    }
    

}
