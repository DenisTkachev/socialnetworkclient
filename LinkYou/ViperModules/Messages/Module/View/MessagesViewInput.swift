//
//  MessagesMessagesViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MessagesViewInput: class {
    /// - author: Denis Tkachev
    func setupInitialState(_ dialogs: [Dialog])
    func showAlert(title: String, msg: String)
    func isShowHUD(show: Bool)
    func updateView(_ dialogs: [Dialog])
    func updateBadges(_ badges: Badges)
    func showNoDataView(condition: Bool)
}
