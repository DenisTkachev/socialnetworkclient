//
//  MessagesMessagesViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MessagesViewOutput: class {
    /// - author: Denis Tkachev
    func viewIsReady()
    func dialogSelected(_ selectedMessage: Dialog)
    func deleteDialog(_ dialogId: Int)
    func refreshData()
    func updateBadges()
}
