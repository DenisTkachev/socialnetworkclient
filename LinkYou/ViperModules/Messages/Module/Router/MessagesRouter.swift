//
//  MessagesMessagesRouter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class MessagesRouter: MessagesRouterInput {
    
    weak var transitionHandler: TransitionHandler!
    
    enum StorybordsID: String {
        case dialog = "DialogsSB"
    }
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func showDialogView(_ dialogs: ChatMessages, _ headers: [AnyHashable : Any]) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .dialog), to: DialogsModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.configure(dialogs, pictures: [], headers)
        }
    }
    

    
}
