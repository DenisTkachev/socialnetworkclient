//
//  MessagesMessagesRouterInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MessagesRouterInput: class {
    func showDialogView(_ dialogs: ChatMessages, _ headers: [AnyHashable : Any])
}
