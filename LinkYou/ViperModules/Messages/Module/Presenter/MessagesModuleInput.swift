//
//  MessagesMessagesModuleInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MessagesModuleInput: class {
    func searchExistChatOrCreateNew(user: User)
    
}
