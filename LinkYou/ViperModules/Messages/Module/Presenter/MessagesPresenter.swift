//
//  MessagesMessagesPresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class MessagesPresenter: MessagesViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: MessagesViewInput!
    var interactor: MessagesInteractorInput!
    var router: MessagesRouterInput!

    var localDialogs: [Dialog]?
    
    // MARK: -
    // MARK: MessagesViewOutput
    func viewIsReady() {
        interactor.loadDialogs()
        view.isShowHUD(show: true)
    }
    
    func dialogSelected(_ selectedMessage: Dialog) {
        interactor.showDialog(selectedMessage)
        view.isShowHUD(show: true)
    }

    func deleteDialog(_ dialogId: Int) {
        interactor.deleteDialog(dialogId)
    }
    
    func refreshData() {
        interactor.loadDialogs()
    }
    
    func updateBadges() {
        interactor.loadBadges()
    }
    
}

// MARK: -
// MARK: MessagesInteractorOutput
extension MessagesPresenter: MessagesInteractorOutput {
    
    func updateBadges(_ badges: Badges) {
        view.updateBadges(badges)
    }
    
    func showDialog(dialog: ChatMessages, headers: [AnyHashable : Any]) {
        router.showDialogView(dialog, headers)
    }
    
    func returnDialogs(_ dialogs: [Dialog]) {
        if dialogs.count == 0 {
            view.showNoDataView(condition: true)
        } else {
            view.showNoDataView(condition: false)
        }
        view.isShowHUD(show: false)
        view.setupInitialState(dialogs)
        localDialogs = dialogs
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func showNoDataView() {
        view.showNoDataView(condition: true)
    }
    
    func dialogWasDeleted(id: Int) {
        self.deleteMessageFromArray(id)
    }
    
    func chatExist(_ dialog: Dialog) {
        self.dialogSelected(dialog)
    }
    
}

// MARK: -
// MARK: MessagesModuleInput
extension MessagesPresenter: MessagesModuleInput {
    
    func searchExistChatOrCreateNew(user: User) {
        
        interactor.shearchExistDialog(with: user)
        interactor.loadDialogs()
        //        router.showDialogView(<#T##dialogs: ChatMessages##ChatMessages#>, <#T##headers: [AnyHashable : Any]##[AnyHashable : Any]#>)
    }

}

extension MessagesPresenter {
    fileprivate func deleteMessageFromArray(_ id: Int) {
        localDialogs = localDialogs?.filter({ (message) -> Bool in
            return message.id != id
        })
        guard let localDialogs = localDialogs else { return }
        view.updateView(localDialogs)
    }
}

