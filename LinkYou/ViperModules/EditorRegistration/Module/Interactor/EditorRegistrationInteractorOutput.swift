//
//  EditorRegistrationEditorRegistrationInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol EditorRegistrationInteractorOutput: class {
    func showAlert(title: String, msg: String)
    func userPhotoUrlGeted(data: [UserPhotos])
    func userPhotosUpload(_ userId: Int)
    func userDataGeted(data: User)
    func likesAndCommentGeted(_ likes: [PhotoLike], _ comments: [PhotoComment])
    func photoWasDeleted(id: Int)
    func returnAutocompleteData(languages: [AutocompleteModel], religions: [AutocompleteModel], nationality: [AutocompleteModel], professions: [AutocompleteModel], cities: [AutocompleteModel], age: [AutocompleteModel], profarea: [AutocompleteModel], goals: [AutocompleteModel])
    func updateHeader()
    func showError()
    func gettedPetUrlAvatar(_ url: String)
    func updateSpecialSection(tag: String)
}
