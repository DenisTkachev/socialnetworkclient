//
//  EditorRegistrationEditorRegistrationInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya
final class EditorRegistrationInteractor: EditorRegistrationInteractorInput {
    
    weak var output: EditorRegistrationInteractorOutput!
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    let dispatchGroup = DispatchGroup()
    
    
    var languages = [AutocompleteModel]()
    var religions = [AutocompleteModel]()
    var nationality = [AutocompleteModel]()
    var professions = [AutocompleteModel]()
    var cities = [AutocompleteModel]()
    var age = [AutocompleteModel]()
    var profarea = [AutocompleteModel]()
    var goals = [AutocompleteModel]()
    
    func addPhotoLike(_ photoId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.userPhotoLike(id: photoId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    self.loadCounterForPhoto(photoId)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getAutocomleteDictionaries() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<AutocompleteService>(plugins: [authPlagin])
        // languages
        dispatchGroup.enter()
        usersProvider.request(.getLanguages) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.languages = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        //religions
        dispatchGroup.enter()
        usersProvider.request(.getReligions) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.religions = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // nationality
        dispatchGroup.enter()
        usersProvider.request(.getNationality) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.nationality = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // professions
        dispatchGroup.enter()
        usersProvider.request(.getProfessions) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.professions = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // profArea
        dispatchGroup.enter()
        usersProvider.request(.getProfarea) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.profarea = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // profArea
        dispatchGroup.enter()
        usersProvider.request(.getGoals) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.goals = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // cities
        dispatchGroup.enter()
        usersProvider.request(.getCities) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.cities = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        // MARK: Age
        dispatchGroup.enter()
        usersProvider.request(.age) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([AutocompleteModel].self, from: response.data)
                    print(response.count)
                    self.age = response
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            self.output.returnAutocompleteData(languages: self.languages, religions: self.religions, nationality: self.nationality, professions: self.professions, cities: self.cities, age: self.age, profarea: self.profarea, goals: self.goals)
        }
    }
    
    func uploadImageData(_ imageData: Data, _ isPetPhoto: Bool, _ isSetAvatar: Bool) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let photosProvider = MoyaProvider<UploadPhotosService>(plugins: [debugPlagin, authPlagin])
        
        // send data image and get url
        photosProvider.request(.uploadUserPhoto(data: imageData)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userPhotos = try JSONDecoder().decode(ImageDataResponse.self, from: response.data)
                    if let photos = userPhotos.data {
                        for photo in photos {
                            if let url = photo.src {
                                if isPetPhoto {
                                    // ну такое себе решение
                                    self.output.gettedPetUrlAvatar(url)
                                } else if isPetPhoto == false, isSetAvatar == false {
                                    self.addPhotoToGallery(url: url)
                                } else if isPetPhoto == false, isSetAvatar == true {
                                    self.setNewAvatar(url) // верная ссылка
                                }
                            }
                        }
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addPhotoToGallery(url: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let photosProvider = MoyaProvider<PhotosService>(plugins: [debugPlagin, authPlagin])
        photosProvider.request(.bindPhotoToGallery(url: url)) { (result) in
            switch result {
            case .success://(let response):
                do { // TODO: лимит фотографий старый без учёта нового загруженного фото
                    //let response = try JSONDecoder().decode([UserPhotos].self, from: response.data)
                    SuperUser.shared.shortUpdate(completion: { (isUpdated) in
                        if isUpdated {
                           self.loadUserPhotos(userID: SuperUser.shared.superUser!.id!, photoLimit: SuperUser.shared.superUser!.photos_count ?? 0)
                        } else {
                            // не удалось обновить профиль
                            self.output.showAlert(title: "Ошибка обновления профиля", msg: "")
                        }
                    })
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    // USER PHOTOS Request
    func loadUserPhotos(userID: Int, photoLimit: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let photosProvider = MoyaProvider<PhotosService>(plugins: [authPlagin])
        photosProvider.request(.userPhotos(id: userID, limit: photoLimit)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userPhotosUrl = try JSONDecoder().decode([UserPhotos].self, from: response.data)
                    self.output.userPhotoUrlGeted(data: userPhotosUrl)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadUserData(userID: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        // FULL DATA Request
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.userFullData(id: userID)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                    SuperUser.shared.save(user: userFullData)
                    self.output.userDataGeted(data: userFullData)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func deletePhoto(id: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let photosProvider = MoyaProvider<PhotosService>(plugins: [authPlagin])
        
        photosProvider.request(.deletePhoto(id: id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if response["done"] == true { // photo was deleted
                        self.output.photoWasDeleted(id: id)
                    }
                } catch {
                    print(error)
                    print("Ошибка удаления фотографии")
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadCounterForPhoto(_ idPhoto: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        //request first
        var likes: [PhotoLike] = []
        var comments: [PhotoComment] = []
        dispatchGroup.enter()
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.userPhotoComments(id: idPhoto)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([PhotoComment].self, from: response.data)
                    comments = serverResponse
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // request second
        dispatchGroup.enter()
        usersProvider.request(.userPhotosLikes(id: idPhoto)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([PhotoLike].self, from: response.data)
                        likes = serverResponse
                        self.dispatchGroup.leave()
                } catch {
                    print("Ошибка загрузки лайков к фото")
                    self.dispatchGroup.leave()
                }
                
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        dispatchGroup.notify(queue: .main) {
            self.output.likesAndCommentGeted(likes, comments)
        }
    }
    
    func setNewAvatar(_ avatarUrl: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let photosProvider = MoyaProvider<PhotosService>(plugins: [debugPlagin, authPlagin])
        photosProvider.request(.bindPhotoToGallery(url: avatarUrl)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([UserPhotos].self, from: response.data)
                    guard let id = response.first?.id else { return }
                    self.bindPhotoToGallery(photoId: id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func bindPhotoToGallery(photoId: Int) {
        let userService = MoyaProvider<UsersService>(plugins: [authPlagin])
        userService.request(.setAvatar(photoId: photoId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if response["avatar"] == true {
                        self.updateUserData()
                    } else {
                        print("ошибка обновления аватара")
                        self.output.showAlert(title: "Ошибка", msg: "Не удалось обновить аватар")
                    }
                } catch {
                    print(error)
                    print("Ошибка установки аватарки")
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    private func updateUserData() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        guard let selfId = SuperUser.shared.superUser?.id else { return }
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        usersProvider.request(.userFullData(id: selfId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                        SuperUser.shared.save(user: userFullData)
                        self.output.updateHeader()
                    self.output.updateSpecialSection(tag: "educationTag") // TODO: стрёмно жуть!
                    self.loadUserPhotos(userID: SuperUser.shared.superUser!.id!, photoLimit: SuperUser.shared.superUser!.photos_count ?? 0)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func saveUserField(_ parameters: [String: String]) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }

        let usersProvider = MoyaProvider<EditUserFieldsService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.set(parameters)) { (result) in
            switch result {
            case .success(let response):
                var result = try? JSONDecoder().decode([String:Bool].self, from: response.data)
                let educationResult = try? JSONDecoder().decode(EducationServerResponse.self, from: response.data)
                let languagesResult = try? JSONDecoder().decode(LanguagesServerResponse.self, from: response.data)
                
                    if result?["success"] == true {
                        self.updateUserData()
                    } else if educationResult?.success == true{
                        self.updateUserData()
                    } else if languagesResult?.success == true {
                        self.updateUserData()
                    } else {
                        print("Сервер не сохранил изменения!")
                        self.output.showError()
                    }
                
            case let .failure(error):
                print(error)
                self.output.showError()
            }
        }
    }
    
    func deleteField(_ parametrs: [String : String]) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<EditUserFieldsService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.delete(parametrs)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let result = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if result["success"] == true {
                        self.updateUserData()
                    } else {
                        print("Ошибка удаления")
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
