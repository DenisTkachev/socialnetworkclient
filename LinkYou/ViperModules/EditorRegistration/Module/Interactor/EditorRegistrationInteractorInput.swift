//
//  EditorRegistrationEditorRegistrationInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol EditorRegistrationInteractorInput: class {
    func loadUserPhotos(userID: Int, photoLimit: Int)
    func loadUserData(userID: Int)
    func uploadImageData(_ imageData: Data, _ isPetPhoto: Bool, _ isSetAvatar: Bool)
    func deletePhoto(id: Int)
    func getAutocomleteDictionaries()
    func loadCounterForPhoto(_ idPhoto: Int)
    func addPhotoLike(_ photoId: Int)
    func setNewAvatar(_ avatarId: String)
    func saveUserField(_ parameters: [String:String])
    func deleteField(_ parametrs: [String : String])
    func bindPhotoToGallery(photoId: Int)
}
