//
//  EditorRegistrationEditorRegistrationRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class EditorRegistrationRouter: EditorRegistrationRouterInput {

  weak var transitionHandler: TransitionHandler!

    enum StorybordsID: String {
        case mainScreen = "MainSB"
    }
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func routeToMainSB() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .mainScreen), to: MainScreenModuleInput.self)
            .perform()
    }
    
}
