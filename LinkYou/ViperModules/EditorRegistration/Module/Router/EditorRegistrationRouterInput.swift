//
//  EditorRegistrationEditorRegistrationRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol EditorRegistrationRouterInput: class {
    func routeToMainSB()
}
