//
//  RegistrationFieldTag.swift
//  LinkYou
//
//  Created by Denis Tkachev on 09/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import Foundation

enum RegistrationFieldTag: String {
    
    case avatar = "MainFormTableViewCell"
    case name = "name"
    case birthday = "birthday"
    case cityId = "location[cityId]"
    case sex = "sex"
    case profession = "career[professionId]"
    case profArea = "career[profAreaId]"
    case lookingFor = "lookingFor[id]"
    case age = "age"
    case goal = "goal[id]"
// edu
    case type_id = "education[type_id]"
    case institution_name = "education[institution_name]"
// about
    case height = "height"
    case weight = "weight"
    case relationship = "relationship[id]"
    case orientation = "orientation[id]"
    case children = "children[id]"
    case smoking = "smoking[id]"
    case alcohol = "alcohol[id]"
// others
    case interests = "interests[interests]"
    case book = "book[id]"
}
