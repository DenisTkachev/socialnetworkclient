//
//  ProgressScores.swift
//  LinkYou
//
//  Created by Denis Tkachev on 09/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit
//import Foundation

struct ProgressScores {
    public var totalScrores: CGFloat = 0
    
    var fieldDict: [String: CGFloat] = [
        RegistrationFieldTag.avatar.rawValue: 5,
        RegistrationFieldTag.name.rawValue: 5,
        RegistrationFieldTag.birthday.rawValue: 15,
        RegistrationFieldTag.cityId.rawValue: 10,
        RegistrationFieldTag.sex.rawValue: 5,
        RegistrationFieldTag.profession.rawValue: 25,
        RegistrationFieldTag.lookingFor.rawValue: 3,
        RegistrationFieldTag.age.rawValue: 4,
        RegistrationFieldTag.goal.rawValue: 4,
        // edu
        RegistrationFieldTag.type_id.rawValue: 2,
        RegistrationFieldTag.institution_name.rawValue: 5,
        // about
        RegistrationFieldTag.height.rawValue: 2,
        RegistrationFieldTag.weight.rawValue: 2,
        RegistrationFieldTag.relationship.rawValue: 3,
        RegistrationFieldTag.orientation.rawValue: 1,
        RegistrationFieldTag.children.rawValue: 1,
        RegistrationFieldTag.smoking.rawValue: 1,
        RegistrationFieldTag.alcohol.rawValue: 1,
        // others
        RegistrationFieldTag.interests.rawValue: 3,
        RegistrationFieldTag.book.rawValue: 2
        
    ]
    
    var fieldStatusDict: [String: Bool]
    
    init() {
        var dictionary: [String:Bool] = [:]
        let array = fieldDict.flatMap { (arg) -> Dictionary<String, Bool> in
            let unit = Dictionary.init(dictionaryLiteral: (arg.key, true))
            return unit
        }
        array.forEach { (arg) in
            let (key, value) = arg
            dictionary[key] = value
        }
        fieldStatusDict = dictionary
    }
    
    mutating func currentScrores(fieldTag: String, isValid: Bool) -> CGFloat {
        guard let modifyFlag = fieldStatusDict[fieldTag] else { return totalScrores }
        
        if let fieldValue = fieldDict[fieldTag]  {
            if isValid, modifyFlag {
                totalScrores += fieldValue
                fieldStatusDict.updateValue(false, forKey: fieldTag)
                
            } else if modifyFlag == false, isValid == false {
                totalScrores -= fieldValue
                fieldStatusDict.updateValue(true, forKey: fieldTag)
            }
        }
        print("Очков: \(totalScrores), поле: \(fieldTag)")
        return totalScrores
    }
    
}
