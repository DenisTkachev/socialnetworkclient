//
//  EditorRegistrationEditorRegistrationPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit
import SwiftDate

final class EditorRegistrationPresenter: EditorRegistrationViewOutput {
    
    func returnKeyForField(key: String, rowTag: String) -> [String:String] {
        switch (key, rowTag) {
            // Пол
        case (ActionSheetType.female.rawValue, "sex"):
            return ["gender[id]":"2"]
        case (ActionSheetType.male.rawValue, "sex"):
            return ["gender[id]":"1"]
            // Я ищу
        case (ActionSheetType.femalet2.rawValue,"lookingFor[id]"):
            return ["lookingFor[id]":"2"]
        case (ActionSheetType.malet2.rawValue,"lookingFor[id]"):
            return ["lookingFor[id]":"1"]
        case (ActionSheetType.friends.rawValue,"lookingFor[id]"):
            return ["lookingFor[id]":"3"]
            // Отношения
        case (ActionSheetType.unknown.rawValue,"relationship[id]"):
            return ["relationship[id]":"0"]
        case (ActionSheetType.no.rawValue,"relationship[id]"):
            return ["relationship[id]":"1"]
        case (ActionSheetType.married.rawValue,"relationship[id]"):
            return ["relationship[id]":"2"]
        case (ActionSheetType.haveArelationship.rawValue,"relationship[id]"):
            return ["relationship[id]":"3"]
            // Ориентация
        case (ActionSheetType.unknown.rawValue,"orientation[id]"):
        return ["orientation[id]":"0"]
        case (ActionSheetType.getero.rawValue,"orientation[id]"):
        return ["orientation[id]":"1"]
        case (ActionSheetType.gey.rawValue,"orientation[id]"):
        return ["orientation[id]":"2"]
        case (ActionSheetType.les.rawValue,"orientation[id]"):
        return ["orientation[id]":"3"]
        case (ActionSheetType.be.rawValue,"orientation[id]"):
        return ["orientation[id]":"4"]
            // Дети
        case (ActionSheetType.unknown.rawValue,"children[id]"):
        return ["children[id]":"0"]
        case (ActionSheetType.childrenYes.rawValue,"children[id]"):
        return ["children[id]":"1"]
        case (ActionSheetType.no.rawValue,"children[id]"):
        return ["children[id]":"2"]
        // smoke
        case (ActionSheetType.unknown.rawValue,"smoking[id]"):
            return ["smoking[id]":"0"]
        case (ActionSheetType.noSmoking.rawValue,"smoking[id]"):
            return ["smoking[id]":"1"]
        case (ActionSheetType.noSmokingNetral.rawValue,"smoking[id]"):
            return ["smoking[id]":"2"]
        case (ActionSheetType.smoke.rawValue,"smoking[id]"):
            return ["smoking[id]":"3"]
        case (ActionSheetType.dropSmoke.rawValue,"smoking[id]"):
            return ["smoking[id]":"5"]
        case (ActionSheetType.smokeFew.rawValue,"smoking[id]"):
            return ["smoking[id]":"4"]
        // alkho
        case (ActionSheetType.unknown.rawValue,"alcohol[id]"):
            return ["alcohol[id]":"0"]
        case (ActionSheetType.alkhoNot.rawValue,"alcohol[id]"):
            return ["alcohol[id]":"1"]
        case (ActionSheetType.alkhoFew.rawValue,"alcohol[id]"):
            return ["alcohol[id]":"2"]
        case (ActionSheetType.alkhoLove.rawValue,"alcohol[id]"):
            return ["alcohol[id]":"3"]

        // Образование
        case (ActionSheetType.unknown.rawValue, "education[type_id]"):
            return ["education[type_id]":"0"]
        case (ActionSheetType.mid.rawValue, "education[type_id]"):
            return ["education[type_id]":"1"]
        case (ActionSheetType.midSpec.rawValue, "education[type_id]"):
            return ["education[type_id]":"2"]
        case (ActionSheetType.halfHight.rawValue, "education[type_id]"):
            return ["education[type_id]":"3"]
        case (ActionSheetType.hight.rawValue, "education[type_id]"):
            return ["education[type_id]":"4"]
        case (_, "education[institution_name]"):
            return ["education[institution_name]": key]
        case (_, "education[speciality_name]"):
            return ["education[speciality_name]": key]
            
        //Языки
        case (ActionSheetType.selfLang.rawValue, "languages[level_id]"):
            return ["languages[level_id]": "1"]
        case (ActionSheetType.baseLavel.rawValue, "languages[level_id]"):
            return ["languages[level_id]": "2"]
        case (ActionSheetType.midLevel.rawValue, "languages[level_id]"):
            return ["languages[level_id]": "3"]
        case (ActionSheetType.heightLevel.rawValue, "languages[level_id]"):
            return ["languages[level_id]": "4"]
            
        // Финансы
        case (ActionSheetType.unknown.rawValue, "career[financeId]"):
            return ["career[financeId]": "1"]
        case (ActionSheetType.financeNone.rawValue, "career[financeId]"):
            return ["career[financeId]": "2"]
        case (ActionSheetType.financeSmall.rawValue, "career[financeId]"):
            return ["career[financeId]": "3"]
        case (ActionSheetType.financeMid.rawValue, "career[financeId]"):
            return ["career[financeId]": "4"]
        case (ActionSheetType.financeHight.rawValue, "career[financeId]"):
            return ["career[financeId]": "5"]
            
            
            
        default:
            print("Нет соответсвия!")
            return ["":""]
        }
    }
        
    // MARK: -
    // MARK: Properties

    weak var view: EditorRegistrationViewInput!
    var interactor: EditorRegistrationInteractorInput!
    var router: EditorRegistrationRouterInput!
    
    var superUser = SuperUser.shared.superUser
    
    var userEducation = UserEducation()
    
    enum ActionSheetType: String {
        case unknown = "Не указано"
        case sex = "sex"
        case male = "Мужчина"
        case female = "Женщина"
        case goal = "goal"
        case malet2 = "Мужчину"
        case femalet2 = "Женщину"
        case friends = "Друзей"
        case orientation = "orientation"
        case getero = "Гетеросексуальная"
        case gey = "Гомосексуальная"
        case les = "Лесбиянка"
        case be = "Бисексуальная"
        case relationShip = "relationShip"
        case no = "Нет"
        case married = "В браке"
        case haveArelationship = "Есть отношения"
        case childrens = "childrens"
        case childrenYes = "Есть"
        case smoking = "smoking"
        case noSmoking = "Не курю и не приемлю"
        case noSmokingNetral = "Не курю и отношусь нейтрально"
        case smoke = "Курю"
        case dropSmoke = "Бросаю"
        case smokeFew = "Изредка курю"
        case alkho = "alkho"
        case alkhoNot = "Не пью"
        case alkhoFew = "Изредка могу поддержать компанию"
        case alkhoLove = "Люблю выпить"
        
        case education = "education"
        case mid = "Среднее"
        case midSpec = "Среднее специальное"
        case halfHight = "Неполное высшее"
        case hight = "Высшее"
        
        case finance = "finance"
        case financeNone = "Непостоянный доход"
        case financeSmall = "Постоянный небольшой доход"
        case financeMid = "Стабильный средний доход"
        case financeHight = "Хорошо зарабатываю"
        
        case languageLevel = "languageLevel"
        case selfLang = "Родной язык"
        case baseLavel = "Базовый уровень"
        case midLevel = "Читаю литературу"
        case heightLevel = "Свободно владею"
        
    }

    var cities: [AutocompleteModel] {
        get {
            return PersonalConstants.allCities
        }
    }
    
    var professions: [AutocompleteModel] {
        get {
            return PersonalConstants.allProfessions
        }
    }
    
    var age: [AutocompleteModel] {
        get {
            return PersonalConstants.age
        }
    }
    
    var profarea: [AutocompleteModel] {
        get {
            return PersonalConstants.profarea
        }
    }
    
    var nationality: [AutocompleteModel] {
        get {
            return PersonalConstants.allNationality
        }
    }
    
    var religions: [AutocompleteModel] {
        get {
            return PersonalConstants.allReligions
        }
    }
    
    var allLanguages: [AutocompleteModel] {
        get {
            return PersonalConstants.allLanguages
        }
    }
    
    var goals: [AutocompleteModel] {
        get {
            return PersonalConstants.goals
        }
    }
    
    // MARK: -
    // MARK: EditorRegistrationViewOutput
    func viewIsReady() {
        guard let user = superUser else { return }
        view.showLoader(isShow: true)
        interactor.loadUserData(userID: user.id!)
        
        if cities.count == 0 {
            interactor.getAutocomleteDictionaries()
        } else {
            view.cities = cities
            view.professions = professions
            view.age = age
            view.nationality = nationality
            view.religions = religions
            view.languages = allLanguages
            view.profarea = profarea
            view.goals = goals
        }
    }

    func uploadImage(_ image: UIImage, _ isPetPhoto: Bool, _ isSetAvatar: Bool) {
        guard let data = image.jpegData(compressionQuality: 0.6) else { return }
        view.showLoader(isShow: true)
        if isSetAvatar == false {
            interactor.uploadImageData(data, isPetPhoto, false)
        } else if isSetAvatar == true, isPetPhoto == false {
            interactor.uploadImageData(data, isPetPhoto, isSetAvatar)
        } else {
            print("Ошибка конвертации аватарки")
        }
    }
    
    func downloadUserPhotos(userID: Int) {
        let countOfPhoto = superUser?.photos_count ?? 0
        interactor.loadUserPhotos(userID: userID, photoLimit: countOfPhoto)
    }
    
    func pressDeletePhoto(photoId: Int) {
        interactor.deletePhoto(id: photoId)
    }
    
    func requestCountersGallery(_ idPhoto: Int) {
        interactor.loadCounterForPhoto(idPhoto)
    }
    
    func sendPhotoLike(_ photoID: Int) {
        interactor.addPhotoLike(photoID)
    }
    // TODO: подумать как сократить однотипный код
    func showActionSheet(type: EditorRegistrationPresenter.ActionSheetType, completion: @escaping (String) -> Void) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        if type == .sex {
            let male = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.male.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.male.rawValue)
            }
            let female = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.female.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.female.rawValue)
            }
            
            actionSheet.addAction(male)
            actionSheet.addAction(female)
            
        } else if type == .goal {
            let male = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.malet2.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.malet2.rawValue)
            }
            let female = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.femalet2.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.femalet2.rawValue)
            }
            let friends = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.friends.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.friends.rawValue)
            }
            
            actionSheet.addAction(male)
            actionSheet.addAction(female)
            actionSheet.addAction(friends)
        } else if type == .alkho {
            let unknown = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.unknown.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.unknown.rawValue)
            }
            let alkhoNot = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.alkhoNot.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.alkhoNot.rawValue)
            }
            let alkhoFew = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.alkhoFew.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.alkhoFew.rawValue)
            }
            let alkhoLove = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.alkhoLove.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.alkhoLove.rawValue)
            }
            
            actionSheet.addAction(unknown)
            actionSheet.addAction(alkhoNot)
            actionSheet.addAction(alkhoFew)
            actionSheet.addAction(alkhoLove)
        } else if type == .smoking {
            let unknown = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.unknown.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.unknown.rawValue)
            }
            let noSmoking = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.noSmoking.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.noSmoking.rawValue)
            }
            let noSmokingNetral = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.noSmokingNetral.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.noSmokingNetral.rawValue)
            }
            let smoke = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.smoke.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.smoke.rawValue)
            }
            let dropSmoke = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.dropSmoke.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.dropSmoke.rawValue)
            }
            let smokeFew = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.smokeFew.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.smokeFew.rawValue)
            }
            
            actionSheet.addAction(unknown)
            actionSheet.addAction(noSmoking)
            actionSheet.addAction(noSmokingNetral)
            actionSheet.addAction(smoke)
            actionSheet.addAction(dropSmoke)
            actionSheet.addAction(smokeFew)
            
        } else if type == .orientation {
            let unknown = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.unknown.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.unknown.rawValue)
            }
            let getero = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.getero.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.getero.rawValue)
            }
            let gey = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.gey.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.gey.rawValue)
            }
            let les = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.les.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.les.rawValue)
            }
            let be = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.be.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.be.rawValue)
            }
            
            actionSheet.addAction(unknown)
            actionSheet.addAction(getero)
            actionSheet.addAction(gey)
            actionSheet.addAction(les)
            actionSheet.addAction(be)
        } else if type == .relationShip {
            let unknown = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.unknown.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.unknown.rawValue)
            }
            let no = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.no.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.no.rawValue)
            }
            let married = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.married.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.married.rawValue)
            }
            let haveArelationship = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.haveArelationship.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.haveArelationship.rawValue)
            }
            
            actionSheet.addAction(unknown)
            actionSheet.addAction(no)
            actionSheet.addAction(married)
            actionSheet.addAction(haveArelationship)
        } else if type == .childrens {
            let unknown = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.unknown.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.unknown.rawValue)
            }
            let no = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.no.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.no.rawValue)
            }
            let childrenYes = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.childrenYes.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.childrenYes.rawValue)
            }
            
            actionSheet.addAction(unknown)
            actionSheet.addAction(no)
            actionSheet.addAction(childrenYes)
        } else if type == .education {
            let unknown = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.unknown.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.unknown.rawValue)
            }
            let mid = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.mid.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.mid.rawValue)
            }
            let midSpec = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.midSpec.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.midSpec.rawValue)
            }
            
            let halfHight = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.halfHight.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.halfHight.rawValue)
            }
            
            let hight = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.hight.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.hight.rawValue)
            }
            
            actionSheet.addAction(unknown)
            actionSheet.addAction(mid)
            actionSheet.addAction(midSpec)
            actionSheet.addAction(halfHight)
            actionSheet.addAction(hight)
        } else if type == .finance {
            let unknown = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.unknown.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.unknown.rawValue)
            }
            let financeNone = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.financeNone.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.financeNone.rawValue)
            }
            let financeSmall = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.financeSmall.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.financeSmall.rawValue)
            }
            
            let financeMid = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.financeMid.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.financeMid.rawValue)
            }
            
            let financeHight = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.financeHight.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.financeHight.rawValue)
            }
            
            actionSheet.addAction(unknown)
            actionSheet.addAction(financeNone)
            actionSheet.addAction(financeSmall)
            actionSheet.addAction(financeMid)
            actionSheet.addAction(financeHight)
        } else if type == .languageLevel {
            let unknown = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.unknown.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.unknown.rawValue)
            }
            let selfLang = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.selfLang.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.selfLang.rawValue)
            }
            let baseLavel = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.baseLavel.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.baseLavel.rawValue)
            }
            
            let midLevel = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.midLevel.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.midLevel.rawValue)
            }
            
            let heightLevel = UIAlertAction(title: EditorRegistrationPresenter.ActionSheetType.heightLevel.rawValue, style: .default) { action in
                completion(EditorRegistrationPresenter.ActionSheetType.heightLevel.rawValue)
            }
            
            actionSheet.addAction(unknown)
            actionSheet.addAction(selfLang)
            actionSheet.addAction(baseLavel)
            actionSheet.addAction(midLevel)
            actionSheet.addAction(heightLevel)
        }
        
        actionSheet.addAction(cancel)
        view.presentActionSheet(actionSheet, true)
    }
    
    func addNewInterestingTag(tags: [TagCloudElement]) {
        var array: [String] = []
        let _ = tags.forEach({ (element) in
            array.append(element.text)
        })
        let stringWithComma = array.joined(separator: ",")
       view.saveFiled(["interests[interests]": stringWithComma])
    }
    
    func sendUpdateAvatar(_ avatarId: Int) {
        interactor.bindPhotoToGallery(photoId: avatarId)
    }
    
    func saveUserField(_ parameters: [String: String]) {
        interactor.saveUserField(parameters)
    }
    
    func prepareAndSaveEducationObject(key: String, id: String, value: String) {
        if id.isEmpty {
            if userEducation.saveField(key: key, rowTag: value) {
                // все обязательные свойста нового образования заполнены, кроме id
                self.saveUserField(userEducation.dictionaryForSave) // после отправки на сервер будет возвращен id для этого образования
            }
        } else {
            var parametr = returnKeyForField(key: value, rowTag: key)
            parametr["education[id]"] = id
            self.saveUserField(parametr)
        }
    }
    
    func prepareAndSaveBirthday(date: Date) {
        var birthdayParametr: [String:String] = [:]
        let timezoneDate = date + 3.hours
        
        birthdayParametr["birthday[day]"] = timezoneDate.day.toString()
        birthdayParametr["birthday[year]"] = timezoneDate.year.toString()
        birthdayParametr["birthday[month]"] = timezoneDate.month.toString()
        
        view.saveFiled(birthdayParametr)
        
    }
    
    func saveLanguage(language: String, level: String, id: String?) {
        var parametrs = returnKeyForField(key: level, rowTag: "languages[level_id]")
        parametrs["languages[id]"] = id
        
        let selectedLanguage = allLanguages.filter({ (element) -> Bool in
            return element.title == language
        })
        let languageId = selectedLanguage.first?.id
        guard let id = languageId else { return }
        parametrs["languages[language_id]"] = String(id)
        view.saveFiled(parametrs)
    }
    
    func deleteField(_ parametrs: [String : String]) {
        interactor.deleteField(parametrs)
    }
    
    func loadMainScreen() {
        router.routeToMainSB()
    }
}

// MARK: -
// MARK: EditorRegistrationInteractorOutput
extension EditorRegistrationPresenter: EditorRegistrationInteractorOutput {
    
    func gettedPetUrlAvatar(_ url: String) {
        view.setPetAvatarUrl(url)
    }
    
    func showError() {
        view.showErrorHUD()
    }
    
    func updateHeader() {
        if let selfUser = SuperUser.shared.superUser {
            view.updateAvatar(updatedUser: selfUser)
        }
    }
    
    func updateSpecialSection(tag: String) {
        if let selfUser = SuperUser.shared.superUser {
            superUser = selfUser
            view.updateUser(user: selfUser)
            view.updateSectionbyTag(tag)
        }
    }
    
    func returnAutocompleteData(languages: [AutocompleteModel], religions: [AutocompleteModel], nationality: [AutocompleteModel], professions: [AutocompleteModel], cities: [AutocompleteModel], age: [AutocompleteModel], profarea: [AutocompleteModel], goals: [AutocompleteModel]) {
        
        PersonalConstants.allLanguages = languages
        PersonalConstants.allReligions = religions
        PersonalConstants.allNationality = nationality
        PersonalConstants.allProfessions = professions
        PersonalConstants.allCities = cities
        PersonalConstants.age = age
        PersonalConstants.allLanguages = languages
        PersonalConstants.profarea = profarea
        PersonalConstants.goals = goals
        
        view.profarea = profarea
        view.cities = cities
        view.professions = professions
        view.age = age
        view.nationality = nationality
        view.religions = religions
        view.languages = languages
        view.goals = goals
    }
    
    func userPhotoUrlGeted(data: [UserPhotos]) {
        self.view.setUserPhoto(userPhotos: appendNeedLoadPhoto(data))
    }
    
    private func appendNeedLoadPhoto(_ userPhotos: [UserPhotos]) -> [UserPhotos] {
        var photos = userPhotos
        var photo = UserPhotos.init()
        while photos.count < 3 {
            photos.append(photo)
        }
        if photos.count >= 3 { // для возможности добавить новое фото
            photo.id = 0
//            photos.append(photo)
            photos.insert(photo, at: 0)
        }
        
        return photos
    }
    
    func userPhotosUpload(_ userId: Int) {
//        interactor.loadUserData(userID: userId)
    }
    
    func userDataGeted(data: User) {
        self.superUser = data
        view.setUserData(data, cloudTags: prepareCloudTags(data: data), tagsInterestings: prepareInterestingsTags(data: data))
        let countOfPhoto = superUser?.photos_count ?? 0
        interactor.loadUserPhotos(userID: data.id!, photoLimit: countOfPhoto)
    }
    
    func photoWasDeleted(id: Int) {
        view.removePhotoFromGallery(id)
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func likesAndCommentGeted(_ likes: [PhotoLike], _ comments: [PhotoComment]) {
        view.setCounters(likes: likes, comments: comments)
    }

}

// MARK: -
// MARK: EditorRegistrationModuleInput
extension EditorRegistrationPresenter: EditorRegistrationModuleInput {


}

extension EditorRegistrationPresenter {
    
    func prepareCloudTags(data: User) -> [TagCloudElement] {
        var cloudTags = [TagCloudElement]()
        if let zodiac = data.birthday?.zodiac?.name {
            cloudTags.append(TagCloudElement(text: zodiac, icon: nil))
        }
        if let goal = data.goal?.name {
            cloudTags.append(TagCloudElement(text: "Цель: \(goal)", icon: nil))
        }
        if let lookingFor = data.looking_for?.name {
            cloudTags.append(TagCloudElement(text: lookingFor, icon: nil))
        }
        if let nationality = data.nationality {
            if nationality.visibility?.id == 1 { // "Показывать мою национальность всем"
                cloudTags.append(TagCloudElement(text: "Национальность: \(nationality.name!)", icon: nil))
            }
        }
        if let religion = data.religion?.name {
            cloudTags.append(TagCloudElement(text: "Религия: \(religion)", icon: nil))
        }
        return cloudTags
    }
    
    func prepareInterestingsTags(data: User) -> [TagCloudElement] {
        var cloudTags = [TagCloudElement]()
        guard let interestings = data.interests?.interests else { return cloudTags }
        for element in interestings {
            if !element.isEmpty {
                cloudTags.append(TagCloudElement(text: element, icon: nil))
            }
        }
        return cloudTags
    }
    
//    private func addTitleForAge(_ age: [AutocompleteModel]) -> [AutocompleteModel] { // куча костылей
//        var temp = age
//        let titleFrom = AutocompleteModel.init(id: 0, title: "От", highlighted: "", subtitle: "")
//        let titleTo = AutocompleteModel.init(id: 0, title: "До", highlighted: "", subtitle: "")
//        temp.insert(titleTo, at: 0)
//        temp.insert(titleFrom, at: 0)
//        return temp
//    }
    
}
