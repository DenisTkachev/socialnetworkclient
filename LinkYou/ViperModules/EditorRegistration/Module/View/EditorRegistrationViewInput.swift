//
//  EditorRegistrationEditorRegistrationViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

protocol EditorRegistrationViewInput: class {
    /// - author: KlenMarket
    func showAlert(title: String, msg: String)
    func showLoader(isShow: Bool)
    func setUserData(_ user: User, cloudTags: [TagCloudElement], tagsInterestings: [TagCloudElement])
    func setUserPhoto(userPhotos: [UserPhotos])
    func removePhotoFromGallery(_ id: Int)
    func setCounters(likes: [PhotoLike], comments: [PhotoComment])
    var cities: [AutocompleteModel] { get set }
    var languages: [AutocompleteModel] { get set }
    var religions: [AutocompleteModel] { get set }
    var professions: [AutocompleteModel] { get set }
    var age: [AutocompleteModel] { get set }
    var nationality: [AutocompleteModel] { get set }
    var profarea: [AutocompleteModel] { get set }
    var goals: [AutocompleteModel] { get set }
    func presentActionSheet(_ actionSheet: UIAlertController, _ animated: Bool)
    //func reloadView(updatedUser: User)
    func showErrorHUD()
    func saveFiled(_ parameters: [String:String])
    func setPetAvatarUrl(_ url: String)
    func updateAvatar(updatedUser: User)
    func updateSectionbyTag(_ tag: String)
    func updateUser(user: User)
}
