//
//  EditorRegistrationEditorRegistrationViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

protocol EditorRegistrationViewOutput: class {
    /// - author: KlenMarket
    func viewIsReady()
    func uploadImage(_ image: UIImage, _ isPetPhoto: Bool, _ isSetAvatar: Bool)
    func downloadUserPhotos(userID: Int)
    func pressDeletePhoto(photoId: Int)
    func requestCountersGallery(_ idPhoto: Int)
    func sendPhotoLike(_ photoID: Int)
    func showActionSheet(type: EditorRegistrationPresenter.ActionSheetType, completion: @escaping (String) -> Void)
    func addNewInterestingTag(tags: [TagCloudElement])
    func sendUpdateAvatar(_ avatarId: Int)
    func saveUserField(_ parameters: [String: String])
    func returnKeyForField(key: String, rowTag: String) -> [String:String]
    func prepareAndSaveEducationObject(key: String, id: String, value: String)
    func saveLanguage(language: String, level: String, id: String?)
    func deleteField(_ parametrs: [String:String])
    func loadMainScreen()
    func prepareAndSaveBirthday(date: Date)
//    var user: User { get set }
//    var userEducation: UserEducation? { get set }
}
