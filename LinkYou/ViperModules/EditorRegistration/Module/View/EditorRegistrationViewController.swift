//
//  EditorRegistrationEditorRegistrationViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Eureka
import PKHUD
import Photos
import SwiftDate

protocol EditorRegistrationViewControllerType: class, UICollectionViewDataSource, UICollectionViewDelegate {
    
}

protocol CellActionHandlerType: class {
    func showPhotoGallery()
    func deleteTagWText(text: String)
    func changeAvatar()
    func addPhoto()
    func setAvatar()
}

protocol SaveFieldsType: class {
    func saveFiled(_ parameters: [String:String])
    var viewOutput: EditorRegistrationViewOutput? { get }
    func preparePickerValueToSave(cellTag: String?, id: Int?)
    func deleteRow(witch: String, indexPath: IndexPath, id: String)
}

final class EditorRegistrationViewController: FormViewController {
    
    // MARK: -
    // MARK: Properties
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet var slideUpView: UIView!
    @IBOutlet weak var customNavigationBar: UINavigationBar!
    @IBOutlet weak var statusBarBackGround: UIView!
    @IBOutlet weak var saveFormBtn: RoundButtonCustom!
    
    @IBOutlet weak var progressLineView: MyProgressView!
    @IBOutlet var progressView: UIView!
    @IBOutlet weak var progressPercentValueLabel: UILabel!
    @IBOutlet weak var progressStatusLabel: UILabel!
    @IBOutlet weak var shadowGradientView: GradientView!
    @IBOutlet weak var safeBackGroundView: NSLayoutConstraint!
    
    
    @IBAction func saveFormPress(_ sender: UIButton) {
        if DeviceCurrent.checkRequiredFields() == nil {
            if self.tabBarController?.tabBar == nil {
                output.loadMainScreen()
            } else {
                navigationController?.popViewController(animated: true)
            }
            self.showLoader(isShow: true)
        } else if let error = DeviceCurrent.checkRequiredFields() {
            self.showLoader(isShow: false)
            print("Валидация НЕ пройдена")
            self.showAlert(title: "Обязательные поля", msg: error)
        }
    }
    
    var output: EditorRegistrationViewOutput!
    var user: User!
    var tagsArray = [TagCloudElement()]
    var userPhotos = [UserPhotos]()
    var tagsInterestings = [TagCloudElement()]
    var mainPhoto = UIImage()
    
    var currentPickerData = [AutocompleteModel]()
    var isShowPicker = true
    var selectedRow: IndexPath = [0,0]
    var selectedValue = ""
    var selectedCell: Cell<String>?
    
    var languages = [AutocompleteModel]()
    var religions = [AutocompleteModel]()
    var nationality = [AutocompleteModel]()
    var professions = [AutocompleteModel]()
    var cities = [AutocompleteModel]()
    var age = [AutocompleteModel]()
    var profarea = [AutocompleteModel]()
    var goals = [AutocompleteModel]()
    
    var isPetPhoto = false
    var isSetAvatar = false
    //    private var cellHeights: [IndexPath: CGFloat?] = [:]
    
    var languagesCounter = 0
    var booksCounter = 0
    var petsCounter = 0
    var scores = ProgressScores()
    
    weak var photoGalleryController: PhotoGalleryViewController?
    
    private enum actionSheetType {
        case sex
        case photo
    }
    // MARK: -
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
        setupTabelView()
        pickerView.delegate = self
        pickerView.dataSource = self
        setupDatePickerView()
        setupProgressView()
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        title = "Редактировать анкету"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showLoader(isShow: false)
    }
    
    func setupProgressView() {
        self.view.addSubview(progressView)
        progressView.translatesAutoresizingMaskIntoConstraints = false
        
        if let tabBarHeight = self.tabBarController?.tabBar.frame.size.height {
            progressView.bottomAnchor.constraint(equalTo: self.tableView.bottomAnchor, constant: -tabBarHeight).isActive = true
        } else {
            progressView.bottomAnchor.constraint(equalTo: self.tableView.bottomAnchor, constant: 0).isActive = true
        }
        
        if #available(iOS 11.0, *), let bottomPadding = UIApplication.shared.keyWindow?.safeAreaInsets.bottom {
//            let topPadding = window?.safeAreaInsets.top
//            let bottomPadding = window?.safeAreaInsets.bottom
            safeBackGroundView.constant = bottomPadding
            progressView.heightAnchor.constraint(equalToConstant: progressView.bounds.height + bottomPadding).isActive = true
        } else {
            safeBackGroundView.constant = bottomLayoutGuide.length
            progressView.heightAnchor.constraint(equalToConstant: progressView.bounds.height + bottomLayoutGuide.length).isActive = true
        }
        
        
        progressView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        progressView.centerXAnchor.constraint(equalTo: self.tableView.centerXAnchor, constant: 0).isActive = true
        progressLineView.layer.cornerRadius = 3
        progressLineView.clipsToBounds = false
        
        shadowGradientView.gradientLayer.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        shadowGradientView.gradientLayer.gradient = GradientPoint.bottomTop.draw()
        shadowGradientView.clipsToBounds = false
        shadowGradientView.layer.cornerRadius = 0
        shadowGradientView.gradientLayer.shadowOpacity = 0.45
        shadowGradientView.gradientLayer.opacity = 0.45
        
    }
    
    func setupDatePickerView() {
        datePickerView.datePickerMode = .date
        let maxDate = Date() - 18.years
        datePickerView.maximumDate = maxDate
        datePickerView.isHidden = true
    }
    
    private func addTitleToPicker() {
        let labelWidth = pickerView.frame.width / CGFloat(2)
        let labelTexts = ["От","До"]
        for index in 0..<2 {
            let label: UILabel = UILabel.init(frame: CGRect(x: pickerView.frame.origin.x + labelWidth * CGFloat(index), y: -4, width: labelWidth, height: 20))
            label.text = labelTexts[index]
            label.textAlignment = .center
            pickerView.addSubview(label)
        }
    }
    
    func setupTabelView() {
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.tableBackGround
        if self.navigationController == nil { // Первичная регистрация
            customNavigationBar.isHidden = false
            statusBarBackGround.isHidden = false
            tableView.frame = CGRect(x: 0, y: 64, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 64)
            UIApplication.shared.statusBarStyle = .lightContent
            //            override var preferredStatusBarStyle: UIStatusBarStyle {
            //                return .lightContent
            //            }
        }
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        slideUpViewSetup()
    }
    
    private func slideUpViewSetup() {
        slideUpView.frame = CGRect(x: 0, y: self.view.frame.maxY, width: UIScreen.main.bounds.width, height: 200)
        slideUpView.layer.cornerRadius = 10
        slideUpView.clipsToBounds = true
    }
    
    func setupCell() { // MARK: Основное
        form +++ Section(){ section in
            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Основное"
                view.secIconImage.image = #imageLiteral(resourceName: "menu-blogs")
                view.sec = section
                view.hideArrow()
            }
            section.header = header
            }
            <<< MainFormRow() {
                $0.cellProvider = CellProvider<MainFormTableViewCell>(nibName: "MainFormTableViewCell", bundle: Bundle.main)
                $0.cell.row.tag = RegistrationFieldTag.avatar.rawValue
                $0.cell.delegate = self
                if let avatarUrl = self.user.avatar?.src?.square {
                    $0.cell.setAvatar(avatarUrl: avatarUrl)
                    self.showLoader(isShow: false)
                }
                $0.tag = "MainFormTableViewCell"
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == "valid") ? nil : ValidationError(msg: "Выберите фотографию пользователя")
                }
                $0.add(rule: ruleRequiredViaClosure)
                $0.validationOptions = .validatesOnChange
                $0.onCellSelection({ (cell, row) in
                    self.changeAvatar()
                })
                $0.onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: row.tag ?? "", isValid: row.isValid)
                })
        }
        
        // MARK: Ваши фотографии
        form +++ Section(){ section in
            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Ваши фотографии"
                view.secIconImage.image = #imageLiteral(resourceName: "photoCamera")
                view.sec = section
                view.photoCounter.text = "\(self.userPhotos.filter { ($0.id != nil && $0.id != 0) }.count)"
                view.photoCounter.isHidden = false
                view.hideArrow()
            }
            section.tag = "photoSectionTag"
            section.header = header
            }
            <<< PhotoCollectionTableViewCellRow("photoRowTag") {
                $0.cellProvider = CellProvider<PhotoCollectionTableViewCell>(nibName: "PhotoCollectionTableViewCell", bundle: Bundle.main)
                
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    if (self.userPhotos.filter { $0.id == nil }.count) == 0 { // нет заглушек
                        return nil
                    } else {
                        return ValidationError(msg: "Необходимо загрузить ещё \(self.userPhotos.filter { $0.id == nil }.count) фото")
                    }
                }
                $0.add(rule: ruleRequiredViaClosure)
                
                $0.validationOptions = .validatesAlways
                $0.cellSetup({ (cell, row) in
                    cell.delegate = self
                }).onChange({ (row) in
                    row.cell.row.value = self.userPhotos.filter { ($0.id != nil && $0.id != 0) }.count.toString()
                })
//                $0.cellUpdate({ (cell, row) in
//                    row.cell.collectionView.reloadData()
//                })
                $0.onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                })
        }
        
        // MARK: Важно знать
        form +++ Section(){ section in
            var header = HeaderFooterView<InfoSectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "InfoSectionHeaderFooterView", bundle: nil))
            
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Важно знать"
            }
            section.header = header
            }
            
            <<< InformationFormTableViewCellRow() {
                $0.cellProvider = CellProvider<InformationFormTableViewCell>(nibName: "InformationFormTableViewCell", bundle: Bundle.main)
            }
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.name.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.row.tag = RegistrationFieldTag.name.rawValue
                $0.cell.titleLabel.text = "Имя:"
                $0.cell.valueTextField.placeholder = "Введите имя"
                if let name = user.name {
                    $0.cell.valueTextField.text = name
                    $0.cell.row.value = name
                }
                $0.cell.switchOnInput()
                $0.cell.delegate = self
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
                $0.add(rule: RuleMinLength(minLength: 3, msg: "Укажите своё имя", id: nil))
                $0.validationOptions = .validatesOnChange
                $0.onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: row.tag ?? "", isValid: row.isValid)
                })
            }
        
                
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.sex.rawValue) { row in
                row.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                row.cell.isUserInteractionEnabled = true
                row.cell.valueTextField.isUserInteractionEnabled = false
                row.cell.titleLabel.text = "Пол:"
                row.cell.valueTextField.placeholder = "Укажите ваш пол"
                row.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
                row.add(rule: RuleMinLength(minLength: 3, msg: "Укажите ваш пол", id: nil))
                row.validationOptions = .validatesOnChange
                row.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                if let sex = user.gender?.code {
                    row.cell.valueTextField.text = (sex == "male" ? "Мужчина" : "Женщина")
                    row.cell.row.value = row.cell.valueTextField.text
                }
                }.onCellSelection({ (cell, row) in
                    self.output.showActionSheet(type: .sex) { (str) in
                        cell.valueTextField.text = str
                        row.value = str
                        self.saveFiled(self.output.returnKeyForField(key: str, rowTag: row.tag!))
                    }
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                })
            
            // День рождения
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.birthday.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Дата рождения:"
                $0.cell.valueTextField.placeholder = "Укажите дату вашего рождения"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.add(rule: RuleMinLength(minLength: 3, msg: "Укажите дату вашего рождения", id: nil))
                $0.validationOptions = .validatesAlways
                
                if let birthday = user.birthday?.date, birthday.count > 0 {
                    $0.cell.valueTextField.text = birthday.toBirthday()
                    $0.cell.row.value = birthday
                }
                }.onCellSelection({ (cell, row) in
                    if let bdString = self.user.birthday?.date, let date = Date(bdString) {
                        self.datePickerView.setDate((date), animated: false)
                    } else {
                        let presetDate = Date() - 20.years
                        self.datePickerView.setDate(presetDate, animated: false)
                    }
                    self.selectedRow = row.indexPath!
                    self.selectedCell = cell
                    self.datePickerView.isHidden = false
                    self.pickerView.isHidden = true
                    self.showHidePicker()
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.cityId.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Город:"
                $0.cell.valueTextField.placeholder = "Укажите местоположение"
                $0.add(rule: RuleMinLength(minLength: 3, msg: "Укажите местоположение", id: nil))
                $0.validationOptions = .validatesAlways
                if let city = user.location?.city_name {
                    $0.cell.valueTextField.text = city
                    $0.cell.row.value = city
                }
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                }.onCellSelection({ (cell, row) in
                    self.selectedCell = cell
                    self.selectedRow = row.indexPath!
                    self.showAutocompleteView(row: self.cities, mapObject: cell, delegate: self)
                    
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.profession.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Профессия:"
                $0.cell.valueTextField.placeholder = "Укажите профессию"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.add(rule: RuleMinLength(minLength: 3, msg: "Укажите профессию", id: nil))
                $0.validationOptions = .validatesAlways
                if let prof = user.job?.profession {
                    $0.cell.valueTextField.text = prof
                    $0.cell.row.value = prof
                }
                }.onCellSelection({ (cell, row) in
                    self.selectedCell = cell
                    self.selectedRow = row.indexPath!
                    self.showAutocompleteView(row: self.professions, mapObject: cell, delegate: self)
                    
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.profArea.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Отрасль:"
                $0.cell.valueTextField.placeholder = "Выберите проф. область"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.add(rule: RuleMinLength(minLength: 3, msg: "Выберите проф. область", id: nil))
                $0.validationOptions = .validatesAlways
                
                if let industry = user.job?.occupation, !industry.isEmpty {
                    $0.cell.valueTextField.text = industry
                    $0.cell.row.value = industry
                }
                }.onCellSelection({ (cell, row) in
                    self.selectedRow = row.indexPath!
                    self.selectedCell = cell
                    self.currentPickerData = self.profarea
                    self.showHidePicker()
                }).onRowValidationChanged({ (cell, row) in
                    (!row.isValid) ? cell.showValidateMessage(show: true) : cell.showValidateMessage(show: false)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.lookingFor.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Я ищу:"
                $0.cell.valueTextField.placeholder = "Друзей"
                $0.cell.isUserInteractionEnabled = true
                $0.cell.valueTextField.isUserInteractionEnabled = false
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.add(rule: RuleMinLength(minLength: 3, msg: "Нужно что-то выбрать", id: nil))
                $0.validationOptions = .validatesAlways
                
                if let lookingFor = user.looking_for?.name {
                    $0.cell.valueTextField.text = lookingFor
                    $0.cell.row.value = lookingFor
                }
                }.onCellSelection({ (cell, row) in
                    self.output.showActionSheet(type: .goal) { (str) in
                        cell.row.value = str
                        cell.row.validate()
                        cell.valueTextField.text = str
                        self.saveFiled(self.output.returnKeyForField(key: str, rowTag: "lookingFor[id]"))
                    }
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                })
            
            <<< AgeFormTableViewCellRow(RegistrationFieldTag.age.rawValue) {
                $0.cellProvider = CellProvider<AgeFormTableViewCell>(nibName: "AgeFormTableViewCell", bundle: Bundle.main)
                $0.validationOptions = .validatesAlways
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || rowValue!.isEmpty || rowValue! == "notValid") ? ValidationError(msg: "Укажите возраст") : nil
                }
                $0.add(rule: ruleRequiredViaClosure)
                
                if let fromAge = user.age?.from, let toAge = user.age?.to {
                    $0.cell.fromAgeField.text = fromAge.toString()
                    $0.cell.toAgeField.text = toAge.toString()
                    $0.cell.checkValidAge()
                }
                }.onCellSelection({ (cell, row) in
                    self.selectedRow = row.indexPath!
                    self.selectedCell = cell
                    self.currentPickerData = self.age
                    self.addTitleToPicker()
                    self.showHidePicker()
                    
                    
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.goal.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Цель знакомства:"
                $0.cell.valueTextField.placeholder = "Дружба и общение"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.add(rule: RuleMinLength(minLength: 3, msg: "Укажите цель знакомства", id: nil))
                $0.validationOptions = .validatesAlways
                
                if let target = user.goal?.name {
                    $0.cell.valueTextField.text = target
                    $0.cell.row.value = target
                }
                $0.onCellSelection({ (cell, row) in
                    self.selectedCell = cell
                    self.selectedRow = row.indexPath!
                    self.currentPickerData = self.goals
                    self.showHidePicker()
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                })
            }
            
            <<< LabelTextFieldFormTableViewCellRow("nationality[id]") {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Национальность:"
                $0.cell.valueTextField.placeholder = "Не отображать"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                if let nationality = user.nationality?.name {
                    $0.cell.valueTextField.text = nationality
                }
                }.onCellSelection({ (cell, row) in
                    self.selectedCell = cell
                    self.selectedRow = row.indexPath!
                    self.showAutocompleteView(row: self.nationality, mapObject: cell, delegate: self)
                })
            
            <<< LabelTextFieldFormTableViewCellRow("religion[id]") {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Религия:"
                $0.cell.valueTextField.placeholder = "Не отображать"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.cell.bottomConstraint.constant = 30
                if let religion = user.religion?.name {
                    $0.cell.valueTextField.text = religion
                }
                }.onCellSelection({ (cell, row) in
                    self.selectedCell = cell
                    self.selectedRow = row.indexPath!
                    self.currentPickerData = self.religions
                    self.showHidePicker()
                })
        
        // MARK: Обо мне
        form +++ Section(){ section in
            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Обо мне"
                view.secIconImage.image = #imageLiteral(resourceName: "menu-suite")
                view.sec = section
            }
            section.header = header
            }
            <<< AboutmeFormTableViewCellRow("about") {
                $0.cellProvider = CellProvider<AboutmeFormTableViewCell>(nibName: "AboutmeFormTableViewCell", bundle: Bundle.main)
                $0.cell.delegate = self
                if let aboutMe = user.about {
                    $0.cell.aboutTextField.text = aboutMe
                    
                }
                $0.onCellSelection({ (cell, row) in
                    self.selectedCell = cell
                    self.selectedRow = row.indexPath!
                })
            }
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.height.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Рост:"
                $0.cell.valueTextField.placeholder = "Не указано"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.cell.valueTextField.keyboardType = .numberPad
                $0.cell.delegate = self
                if let height = user.height {
                    $0.cell.valueTextField.text = String(height)
                }
                $0.cell.switchOnInput()
                
                }.cellUpdate({ (cell, row) in
                    if !cell.valueTextField.text!.isEmpty {
                        self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                    }
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.relationship.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Состоите ли в отношениях:"
                $0.cell.valueTextField.placeholder = "Не указано"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.cell.isUserInteractionEnabled = true
                $0.cell.valueTextField.isUserInteractionEnabled = false
                if let relationShip = user.relationship?.name {
                    $0.cell.valueTextField.text = relationShip
                }
                }.onCellSelection({ (cell, row) in
                    self.output.showActionSheet(type: .relationShip) { (str) in
                        cell.valueTextField.text = str
                        self.saveFiled(self.output.returnKeyForField(key: str, rowTag: "relationship[id]"))
                    }
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    if !cell.valueTextField.text!.isEmpty {
                        self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                    }
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.orientation.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Ориентация:"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.cell.valueTextField.placeholder = "Не указано"
                $0.cell.isUserInteractionEnabled = true
                $0.cell.valueTextField.isUserInteractionEnabled = false
                if let orientation = user.orientation?.name {
                    $0.cell.valueTextField.text = orientation
                }
                }.onCellSelection({ (cell, row) in
                    self.output.showActionSheet(type: .orientation) { (str) in
                        cell.valueTextField.text = str
                        self.saveFiled(self.output.returnKeyForField(key: str, rowTag: "orientation[id]"))
                    }
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    if !cell.valueTextField.text!.isEmpty {
                        self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                    }
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.children.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Дети:"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.cell.valueTextField.placeholder = "Не указано"
                if let child = user.children?.name {
                    $0.cell.valueTextField.text = child
                }
                }.onCellSelection({ (cell, row) in
                    self.output.showActionSheet(type: .childrens) { (str) in
                        cell.valueTextField.text = str
                        self.saveFiled(self.output.returnKeyForField(key: str, rowTag: "children[id]"))
                    }
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    if !cell.valueTextField.text!.isEmpty {
                        self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                    }
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.smoking.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Отношение к курению:"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.cell.valueTextField.placeholder = "Не указано"
                if let smoke = user.smoking?.name {
                    $0.cell.valueTextField.text = smoke
                }
                }.onCellSelection({ (cell, row) in
                    self.output.showActionSheet(type: .smoking) { (str) in
                        cell.valueTextField.text = str
                        self.saveFiled(self.output.returnKeyForField(key: str, rowTag: "smoking[id]"))
                    }
                }).onRowValidationChanged({ (cell, row) in
                    cell.showValidateMessage(show: !row.isValid)
                }).cellUpdate({ (cell, row) in
                    if !cell.valueTextField.text!.isEmpty {
                        self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                    }
                })
            
            <<< LabelTextFieldFormTableViewCellRow(RegistrationFieldTag.alcohol.rawValue) {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Отношение к алкоголю:"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.cell.valueTextField.placeholder = "Не указано"
                $0.cell.bottomConstraint.constant = 30
                if let alkho = user.alcohol?.name {
                    $0.cell.valueTextField.text = alkho
                    $0.cell.row.value = alkho
                }
                }.onCellSelection({ (cell, row) in
                    self.output.showActionSheet(type: .alkho) { (str) in
                        cell.valueTextField.text = str
                        row.value = str
                        self.saveFiled(self.output.returnKeyForField(key: str, rowTag: "alcohol[id]"))
                    }
                }).cellUpdate({ (cell, row) in
                    if !cell.valueTextField.text!.isEmpty {
                        self.updateProgressBar(fieldTag: cell.row.tag ?? "", isValid: row.isValid)
                    }
                })
        
        // MARK: Интересы
        form +++ Section(){ section in
            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Интересы"
                view.secIconImage.image = #imageLiteral(resourceName: "menu-favorites")
                view.sec = section
            }
            section.header = header
            }
            <<< InterestingFormTableViewCellRow(RegistrationFieldTag.interests.rawValue) {
                $0.cellProvider = CellProvider<InterestingFormTableViewCell>(nibName: "InterestingFormTableViewCell", bundle: Bundle.main)
                $0.cell.delegate = self
                $0.cell.createTagCloud(withArray: tagsInterestings)
                
                }.onCellSelection({ [weak self] (cell, row)  in
                    self?.showInputAlert(title: "Новый тэг", message: "Добавьте свои интересы", completion: { (tag) in
                        if tag.count > 0 {
                            
                            let newTag = TagCloudElement(text: tag, icon: nil)
                            self?.tagsInterestings.append(newTag)
                            guard let tagArray = self?.tagsInterestings else { return }
                            cell.createTagCloud(withArray: tagArray)
                            row.reload()
                            //Сохранить на сервере
                            self?.output.addNewInterestingTag(tags: tagArray)
                        }
                    })
                })
        // MARK: Образование
        form +++ Section(){ section in
            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Образование"
                view.secIconImage.image = #imageLiteral(resourceName: "userPage-appleIcon")
                view.sec = section
            }
            section.header = header
        }
        form.last!.tag = "educationTag"
        if let education = user.education {
            for (index, element) in education.enumerated() {
                
                form.last! <<< LabelTextFieldFormTableViewCellRow("educationType \(index)") {
                    $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                    $0.cell.titleLabel.text = "Уровень образования:"
                    $0.cell.valueTextField.placeholder = "Не указано"
                    $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                    if let type = element.education_type?.name, let id = element.id {
                        $0.cell.valueTextField.text = type
                        $0.cell.id = String(id)
                        $0.cell.showDeleteBtn()
                        $0.cell.delegate = self
                    }
                    }.onCellSelection { cell, row in
                        self.output.showActionSheet(type: .education, completion: { (str) in
                            cell.valueTextField.text = str
                            let id = cell.id ?? ""
                            self.output.prepareAndSaveEducationObject(key: RegistrationFieldTag.type_id.rawValue, id: id, value: str)
                        })
                }
                
                form.last! <<< LabelTextFieldFormTableViewCellRow("institution \(index)") {
                    $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                    $0.cell.titleLabel.text = "Название учебного заведения:"
                    $0.cell.valueTextField.placeholder = "Введите название учебного заведения"
                    $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
                    $0.cell.delegate = self
                    if let institut = element.institution?.name, let id = element.id {
                        $0.cell.valueTextField.text = institut
                        $0.cell.id = String(id)
                    }
                    $0.cell.switchOnInput()
                    
                }
                
                form.last! <<< LabelTextFieldFormTableViewCellRow("speciality \(index)") {
                    $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                    $0.cell.titleLabel.text = "Специальность:"
                    $0.cell.valueTextField.placeholder = "Введите специальность"
                    $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
                    $0.cell.delegate = self
                    if let speciality = element.speciality?.name, let id = element.id {
                        $0.cell.valueTextField.text = speciality
                        $0.cell.id = String(id)
                    }
                    $0.cell.switchOnInput()
                }
            }
        } else {
            insertEducationRows()
        }
        
        form.last! <<< AddEducationFormTableViewCellRow("addNewEducation") {
            $0.tag = "addButtonRow"
            $0.cellProvider = CellProvider<AddEducationFormTableViewCell>(nibName: "AddEducationFormTableViewCell", bundle: Bundle.main)
            
            }.onCellSelection({ (cell, row) in
                self.insertEducationRows()
            })
        // MARK: Языки
        form.last! <<< LanguageFormTableViewCellRow("language") {
            $0.cellProvider = CellProvider<LanguageFormTableViewCell>(nibName: "LanguageFormTableViewCell", bundle: Bundle.main)
        }
        
        if let languages = user.languages {
            for element in languages {
                
                form.last! <<< OneLanguageFormTableViewCellRow("language \(languagesCounter)") {
                    $0.cellProvider = CellProvider<OneLanguageFormTableViewCell>(nibName: "OneLanguageFormTableViewCell", bundle: Bundle.main)
                    $0.cell.leftField.text = element.language?.name
                    $0.cell.rightField.text = element.level?.name
                    $0.cell.id = String(element.id!) // текущие значения
                    $0.cell.languageId = element.language!.name! // текущие значения
                    $0.cell.languageLevel = element.level!.name! // текущие значения
                    $0.cell.delegate = self
                    
                    }.onCellSelection({ (cell, row) in
                        self.selectedRow = row.indexPath!
                        self.selectedCell = cell
                        if cell.actionIs == "left" {
                            self.selectedCell = cell
                            self.currentPickerData = self.languages
                            self.showHidePicker()
                            
                        }
                        if cell.actionIs == "right" {
                            self.output.showActionSheet(type: .languageLevel) { (str) in
                                cell.rightField.text = str
                                cell.languageLevel = str
                                cell.saveFields()
                            }
                        }
                        if cell.actionIs == "deleteMe" {
                            guard let rowIndexPath = row.indexPath else { return }
                            let parametrs = ["language[id]":cell.id, "method":"language"]
                            self.output.deleteField(parametrs)
                            self.form.allSections[rowIndexPath.section].remove(at: rowIndexPath.row)
                            self.user.languages = self.user.languages?.filter({ (lang) -> Bool in
                                return lang.language?.id != Int(cell.id)
                            })
                        }
                        
                    })
                languagesCounter += 1
            }
        }
        
        form.last! <<< AddEducationFormTableViewCellRow("addNewLanguage") {
            $0.cellProvider = CellProvider<AddEducationFormTableViewCell>(nibName: "AddEducationFormTableViewCell", bundle: Bundle.main)
            $0.cell.cellNameLabel.text = "Добавить язык"
            $0.cell.row.tag = "addLangButtonRow"
            }.onCellSelection({ (cell, row) in
                self.insertLangRow()
            })
        // MARK: Карьера
        form +++ Section(){ section in
            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Карьера"
                view.secIconImage.image = #imageLiteral(resourceName: "userPage-careerIcon")
                view.sec = section
            }
            section.header = header
            }
            <<< LabelTextFieldFormTableViewCellRow("career[companyName]") {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Работаю в компании:"
                $0.cell.valueTextField.placeholder = "Введите название компании"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
                $0.cell.delegate = self
                if let companyName = user.job?.company_name, let id = user.job?.id {
                    $0.cell.additionalParameters = ["career[id]":"\(id)"]
                    $0.cell.valueTextField.text = companyName
                }
                $0.cell.switchOnInput()
            }
            <<< LabelTextFieldFormTableViewCellRow("career[pros]") {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Плюсы в моей работе:"
                $0.cell.valueTextField.placeholder = "Опишите плюсы вашей работы"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
                $0.cell.delegate = self
                if let pros = user.job?.pros, let id = user.job?.id {
                    $0.cell.valueTextField.text = pros
                    $0.cell.additionalParameters = ["career[id]":"\(id)"]
                }
                $0.cell.switchOnInput()
            }
            <<< LabelTextFieldFormTableViewCellRow("career[cons]") {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Минусы в работе:"
                $0.cell.valueTextField.placeholder = "Опишите минусы вашей работы"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
                $0.cell.delegate = self
                if let cons = user.job?.cons, let id = user.job?.id {
                    $0.cell.valueTextField.text = cons
                    $0.cell.additionalParameters = ["career[id]":"\(id)"]
                }
                $0.cell.switchOnInput()
            }
            <<< LabelTextFieldFormTableViewCellRow("career[achievements]") {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Достижения в карьере:"
                $0.cell.valueTextField.placeholder = "Опишите ваши достижения"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
                $0.cell.delegate = self
                if let achievements = user.job?.achievements, let id = user.job?.id {
                    $0.cell.valueTextField.text = achievements
                    $0.cell.additionalParameters = ["career[id]":"\(id)"]
                }
                $0.cell.switchOnInput()
                
            }
            <<< LabelTextFieldFormTableViewCellRow("career[financeId]") {
                $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
                $0.cell.titleLabel.text = "Материальное положение:"
                $0.cell.valueTextField.placeholder = "Не указано"
                $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.arrow
                $0.cell.delegate = self
                $0.cell.bottomConstraint.constant = 30
                if let finance = user.job?.finance?.name, let id = user.job?.id {
                    $0.cell.valueTextField.text = finance
                    $0.cell.additionalParameters = ["career[id]":"\(id)"]
                }
                }.onCellSelection({ (cell, row) in
                    self.output.showActionSheet(type: .finance, completion: { (str) in
                        cell.valueTextField.text = str
                        if str.count > 0 {
                            var data = self.output.returnKeyForField(key: str, rowTag: "career[financeId]")
                            data[cell.additionalParameters.keys.first!] = cell.additionalParameters.values.first!
                            self.saveFiled(data)
                        }
                    })
                })
        // MARK: Любимые книги
        form +++ Section(){ section in
            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Любимые книги"
                view.secIconImage.image = #imageLiteral(resourceName: "userPage-favBookIcon")
                view.sec = section
            }
            section.header = header
        }
        form.last!.tag = "BooksSection"
        if let books = user.books {
            for book in books {
                
                form.last! <<< OneBookFormTableViewCellRow("authorBook \(booksCounter)") {
                    $0.cellProvider = CellProvider<OneBookFormTableViewCell>(nibName: "OneBookFormTableViewCell", bundle: Bundle.main)
                    if let authorBook = book.author {
                        $0.cell.row1TextField.text = authorBook
                    }
                    if let nameBook = book.name {
                        $0.cell.row2TextField.text = nameBook
                    }
                    if book.id != books.last?.id {
                        $0.cell.sepLine.isHidden = false
                    }
                    $0.cell.id = book.id!
                    $0.cell.switchOnInput()
                    $0.cell.delegate = self
                    }.onCellSelection({ (cell, row) in
                        if cell.actionIs == "deleteMe" {
                            guard let rowIndexPath = row.indexPath else { return }
                            let parametrs = ["book[id]":cell.id, "method":"book"]
                            self.output.deleteField(parametrs)
                            self.form.allSections[rowIndexPath.section].remove(at: rowIndexPath.row)
                            self.user.books = self.user.books?.filter({ (book) -> Bool in
                                return book.id != cell.id
                            })
                            if let lastRow = self.form.allSections[rowIndexPath.section].last?.baseCell as? OneBookFormTableViewCell {
                                lastRow.sepLine.isHidden = true
                            }
                            let lastRow = self.form.allSections[rowIndexPath.section].last(where: { (row) -> Bool in
                                return row is OneBookFormTableViewCellRow
                            })
                            if let lastRow = lastRow?.baseCell as? OneBookFormTableViewCell {
                                lastRow.sepLine.isHidden = true
                                
                            }
                        }
                    })
                booksCounter += 1
            }
        }
        
        form.last! <<< AddEducationFormTableViewCellRow("addNewBook") {
            $0.cellProvider = CellProvider<AddEducationFormTableViewCell>(nibName: "AddEducationFormTableViewCell", bundle: Bundle.main)
            $0.cell.cellNameLabel.text = "Добавить ещё одну книгу"
            $0.cell.row.tag = "addBookButtonRow"
            $0.cell.bottomConstraint.constant = 30
            }.onCellSelection({ (cell, row) in
                self.insertNewBookRows()
            })
        
        if user.books == nil {
            insertNewBookRows()
        }
        // MARK: Любимая музыка
        //        form +++ Section(){ section in
        //            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
        //            header.onSetupView = { view, section in
        //                view.secNameLabel.text = "Любимая музыка"
        //                view.secIconImage.image = #imageLiteral(resourceName: "userPage-favMusicIcon")
        //                view.sec = section
        //            }
        //            section.header = header
        //        }
        //        if let music = user.music {
        //            for (index, mus) in music.enumerated() {
        //                form.last! <<< LabelTextFieldFormTableViewCellRow("music \(index)") {
        //                    $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
        //                    $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
        //                    $0.cell.titleLabel.text = "Трек №:"
        //                    $0.cell.valueTextField.placeholder = "Ссылка на трек в Яндекс Музыке"
        //                    $0.cell.valueTextField.text = mus.link
        //                $0.cell.switchOnInput()
        //                }
        //            }
        //
        //        }
        // TODO Добавить еще один трек
        
        // MARK: Мой питомец
        form +++ Section(){ section in
            var header = HeaderFooterView<SectionHeaderFooterView>(HeaderFooterProvider.nibFile(name: "SectionHeaderFooterView", bundle: nil))
            header.onSetupView = { view, section in
                view.secNameLabel.text = "Мой питомец"
                view.secIconImage.image = #imageLiteral(resourceName: "userPage-petsIcon")
                view.sec = section
            }
            section.header = header
        }
        form.last!.tag = "petsSectionTag"
        if let pets = user.pets {
            for (index, pet) in pets.enumerated() {
                form.last! <<< PetsFormTableViewCellRow("petName \(petsCounter)") {
                    $0.cellProvider = CellProvider<PetsFormTableViewCell>(nibName: "PetsFormTableViewCell", bundle: Bundle.main)
                    $0.cell.delegate = self
                    if let avatarUrl = self.user.pets?[index].avatar {
                        $0.cell.photoUrl = avatarUrl
                    }
                    if let id = self.user.pets?[index].id {
                        $0.cell.id = id.toString()
                    }
                    $0.onCellSelection({ (cell, row) in
                        self.selectedCell = cell
                        
                        if cell.actionIs == "addPhoto" {
                            self.isPetPhoto = true
                            self.showGalleryActionSheet()
                        }
                        
                        if cell.actionIs == "deleteMe" {
                            guard let rowIndexPath = row.indexPath else { return }
                            let parametrs = ["pet[id]":cell.id, "method":"pet"]
                            self.output.deleteField(parametrs)
                            self.form.allSections[rowIndexPath.section].remove(at: rowIndexPath.row)
                            self.user.pets = self.user.pets?.filter({ (pet) -> Bool in
                                return pet.id != Int(cell.id)
                            })
                        }
                        
                    })
                    if let name = pet.name, let type = pet.type {
                        $0.cell.petNameTextField.text = name
                        $0.cell.petTypeTextField.text = type
                    }
                    $0.cell.switchOnInput()
                }
                petsCounter += 1
            }
        }
        
        form.last! <<< AddEducationFormTableViewCellRow("addNewPet") {
            $0.cellProvider = CellProvider<AddEducationFormTableViewCell>(nibName: "AddEducationFormTableViewCell", bundle: Bundle.main)
            $0.cell.cellNameLabel.text = "Добавить животное"
            $0.cell.row.tag = "addPetButtonRow"
            }.onCellSelection({ (cell, row) in
                self.insertPetRow()
            })
        
        if user.books == nil {
            insertPetRow()
        }
        
        form +++ Section(){ section in // проставочка для progressView
            section.header = {
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 60))
                    view.backgroundColor = .clear
                    return view
                }))
                header.height = { 60 }
                return header
            }()
        }
    }
    
    private func insertEducationRows() {
        //        let education = user.education?.count ?? 0
        let education = SuperUser.shared.superUser?.education?.count ?? 0
        
        let row1 = LabelTextFieldFormTableViewCellRow("educationType \(education + 1)") {
            $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
            $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
            $0.cell.titleLabel.text = "Уровень образования:"
            $0.cell.valueTextField.placeholder = "Не указано"
            $0.cell.delegate = self
            }.onCellSelection { cell, row in
                self.output.showActionSheet(type: .education, completion: { (str) in
                    cell.valueTextField.text = str
                    let id = cell.id ?? ""
                    self.output.prepareAndSaveEducationObject(key: RegistrationFieldTag.type_id.rawValue, id: id, value: str)
                })
                // .>>>> delete ME
        }
        
        let row2 = LabelTextFieldFormTableViewCellRow("institution \(education + 1)") {
            $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
            $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
            $0.cell.titleLabel.text = "Название учебного заведения:"
            $0.cell.valueTextField.placeholder = "Введите название учебного заведения"
            $0.cell.delegate = self
            $0.cell.switchOnInput()
        }
        
        let row3 = LabelTextFieldFormTableViewCellRow("speciality \(education + 1)") {
            $0.cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell", bundle: Bundle.main)
            $0.cell.optionBtnState = LabelTextFieldFormTableViewCell.CheckMark.none
            $0.cell.titleLabel.text = "Специальность:"
            $0.cell.valueTextField.placeholder = "Введите специальность"
            $0.cell.delegate = self
            $0.cell.switchOnInput()
        }
        
        let collectionRows = [row1, row2, row3]
        
        let section = form.allSections.filter { (section) -> Bool in
            return section.tag == "educationTag"
        }
        
        let buttonRow = form.allRows.filter { (row) -> Bool in
            return row.tag == "addButtonRow"
        }
        guard var currentSection = section.first else { return }
        if let currentButtonRow = buttonRow.first, let index = currentButtonRow.indexPath, canAppendNewRow(section: section, newRowTag: row1.tag ?? "", checkRowTag: "educationTag") {
            currentSection.insert(contentsOf: collectionRows, at: index.row)
        } else if canAppendNewRow(section: section, newRowTag: row1.tag ?? "", checkRowTag: "educationTag") {
            currentSection.insert(contentsOf: collectionRows, at: section.startIndex)
        }
        
        // не проверял
        let newEducationSection = Education.init(id: 0, user_id: user.id!, active: true, deleted: false, last_update: Date().toString(), speciality: nil, education_type: nil, institution: nil)
        user.education?.append(newEducationSection)
    }
    /// проверка дублирования тэгов строк
    private func canAppendNewRow(section allRows: [Section], newRowTag: String, checkRowTag: String) -> Bool {
        var approval = true
        
        if allRows.first?.count == 0 {
            return approval
        }
        
        if let educationSection = allRows.first, educationSection.tag == checkRowTag {
            educationSection.forEach { (element) in
                if element.baseCell.baseRow.tag == newRowTag {
                    approval = false // тэг дулируется
                }
            }
            return approval
        }
        return approval
    }
    
    private func insertPetRow() {
        
        let row1 = PetsFormTableViewCellRow("petName \(petsCounter)") {
            $0.cellProvider = CellProvider<PetsFormTableViewCell>(nibName: "PetsFormTableViewCell", bundle: Bundle.main)
            $0.cell.delegate = self
            $0.onCellSelection({ (cell, row) in
                if cell.actionIs == "addPhoto" {
                    self.isPetPhoto = true
                    self.showGalleryActionSheet()
                    self.selectedCell = cell
                }
                if cell.actionIs == "deleteMe" {
                    guard let rowIndexPath = row.indexPath else { return }
                    let parametrs = ["pet[id]":cell.id, "method":"pet"]
                    self.output.deleteField(parametrs)
                    self.form.allSections[rowIndexPath.section].remove(at: rowIndexPath.row)
                    self.user.pets = self.user.pets?.filter({ (pet) -> Bool in
                        return pet.id != Int(cell.id)
                    })
                }
                
            })
            $0.cell.switchOnInput()
            petsCounter += 1
        }
        
        let section = form.allSections.filter { (section) -> Bool in
            return section.tag == "petsSectionTag"
        }
        let buttonRow = form.allRows.filter { (row) -> Bool in
            return row.tag == "addPetButtonRow"
        }
        guard var currentSection = section.first, let currentButtonRow = buttonRow.first, let index = currentButtonRow.indexPath else { return }
        currentSection.insert(row1, at: index.row)
        
        // не проверял
        let newPet = Pets.init(id: Int.random(in: 100...200), user_id: user.id!, deleted: false, last_update: Date().toString(), name: "", type: "", description: "", avatar: "")
        user.pets?.append(newPet)
    }
    
    private func insertLangRow() {
        let row1 = OneLanguageFormTableViewCellRow("language \(languagesCounter)") {
            $0.cellProvider = CellProvider<OneLanguageFormTableViewCell>(nibName: "OneLanguageFormTableViewCell", bundle: Bundle.main)
            $0.cell.leftField.placeholder = "Язык"
            $0.cell.rightField.placeholder = "Уровень"
            $0.cell.id = ""
            $0.cell.delegate = self
            languagesCounter += 1
            }.onCellSelection({ (cell, row) in
                self.selectedRow = row.indexPath!
                self.selectedCell = cell
                if cell.actionIs == "left" {
                    self.selectedCell = cell
                    self.currentPickerData = self.languages
                    self.showHidePicker()
                    
                }
                if cell.actionIs == "right" {
                    self.output.showActionSheet(type: .languageLevel) { (str) in
                        cell.rightField.text = str
                        cell.languageLevel = str
                        cell.saveFields()
                    }
                }
                if cell.actionIs == "deleteMe" {
                    guard let rowIndexPath = row.indexPath else { return }
                    let parametrs = ["language[id]":cell.id, "method":"language"]
                    self.output.deleteField(parametrs)
                    self.form.allSections[rowIndexPath.section].remove(at: rowIndexPath.row)
                    self.user.languages = self.user.languages?.filter({ (lang) -> Bool in
                        return lang.language?.id != Int(cell.id)
                    })
                }
            })
        
        let section = form.allSections.filter { (section) -> Bool in
            return section.tag == "educationTag"
        }
        let buttonRow = form.allRows.filter { (row) -> Bool in
            return row.tag == "addLangButtonRow"
        }
        guard var currentSection = section.first, let currentButtonRow = buttonRow.first, let index = currentButtonRow.indexPath else { return }
        currentSection.insert(row1, at: index.row)
        
        // не проверял. Из-за того что id == 0 не будет работать удаление Languages, поставил random
        let newLangRow = Language.init(id: 0, sorting: 0, name: "", deleted: false)
        let newLangLevel = Level.init(name: "")
        let newLanguages = Languages.init(id: Int.random(in: 100...200), user_id: user.id!, deleted: false, last_update: Date().toString(), language: newLangRow, level: newLangLevel)
        user.languages?.append(newLanguages)
    }
    
    private func insertNewBookRows() {
        let row1 = OneBookFormTableViewCellRow("authorBook \(booksCounter)") {
            $0.cellProvider = CellProvider<OneBookFormTableViewCell>(nibName: "OneBookFormTableViewCell", bundle: Bundle.main)
            $0.cell.id = ""
            booksCounter += 1
            $0.cell.switchOnInput()
            $0.cell.delegate = self
            $0.cell.sepLine.isHidden = true
            }.onCellSelection({ (cell, row) in
                if cell.actionIs == "deleteMe" {
                    guard let rowIndexPath = row.indexPath else { return }
                    let parametrs = ["book[id]":cell.id, "method":"book"]
                    self.output.deleteField(parametrs)
                    self.form.allSections[rowIndexPath.section].remove(at: rowIndexPath.row)
                    self.user.books = self.user.books?.filter({ (book) -> Bool in
                        return book.id != cell.id
                    })
                    
                    let lastRow = self.form.allSections[rowIndexPath.section].last(where: { (row) -> Bool in
                        return row is OneBookFormTableViewCellRow
                    })
                    if let lastRow = lastRow?.baseCell as? OneBookFormTableViewCell {
                        lastRow.sepLine.isHidden = true
                    }
                }
            })
        
        let section = form.allSections.filter { (section) -> Bool in
            return section.tag == "BooksSection"
        }
        let buttonRow = form.allRows.filter { (row) -> Bool in
            return row.tag == "addBookButtonRow"
        }
        
        guard var currentSection = section.first, let currentButtonRow = buttonRow.first, let index = currentButtonRow.indexPath else { return }
        currentSection.insert(row1, at: index.row)
        
        let newBook = Books.init(id: "", user_id: user.id!, name: "", author: "", deleted: false, last_update: Date().toString())
        user.books?.append(newBook)
        
        hideSepLineForBookSection()
    }
    
    private func hideSepLineForBookSection() {
        let allBookRow = form.allRows.filter { (row) -> Bool in
            row.tag?.contains("authorBook") ?? false
        }
        allBookRow.forEach { (bookRow) in
            if let book = bookRow.baseCell as? OneBookFormTableViewCell, allBookRow.last != bookRow {
                book.sepLine.isHidden = false
            }
        }
    }
    
    private func showInputAlert(title: String, message: String, completion: @escaping (_ text: String)->() ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Добавить", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            completion(textField.text ?? "")
        }
        alert.addTextField { (textField) in
            textField.placeholder = ""
        }
        alert.addAction(action)
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        }
    }
    
    @objc private func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == [3,0] {
            return 100
        }
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return CGFloat(0)
        } else {
            return CGFloat(11)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .tableBackGround
        return view
    }
    
    private func makeRow(tag: String) -> LabelTextFieldFormTableViewCell {
        if let cell = form.rowBy(tag: tag)?.baseCell, let row = cell as? LabelTextFieldFormTableViewCell {
            return row
        } else {
            return LabelTextFieldFormTableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath
    }
    
    private func showHidePicker() {
        
        if isShowPicker { // add
            pickerView.isHidden = !datePickerView.isHidden
            tableView.isUserInteractionEnabled = false
            self.view.addSubview(slideUpView)
            pickerView.reloadAllComponents()
            
            if pickerView.numberOfComponents == 2 {
                if let from = user.age?.from?.toString(), let to = user.age?.to?.toString() {
                    let ageFrom = currentPickerData.firstIndex(where: { $0.title == from })
                    let ageTo = currentPickerData.lastIndex(where: { $0.title == to })
                    pickerView.selectRow(ageFrom ?? 0, inComponent: 0, animated: true)
                    pickerView.selectRow(ageTo ?? 0, inComponent: 1, animated: true)
                }
            }
            
            UIView.animate(withDuration: 0.3, animations: {
                let tabBarHeight = self.tabBarController?.tabBar.bounds.maxY ?? 0
                let navigationBar = self.navigationController?.navigationBar.bounds.height ?? 0
                self.slideUpView.frame.origin.y = UIScreen.main.bounds.height - self.slideUpView.bounds.height - tabBarHeight - navigationBar
            })
            
        } else { // hide
            tableView.isUserInteractionEnabled = true
            datePickerView.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.slideUpView.frame.origin.y = self.view.frame.maxY
            }) { (_) in
                self.slideUpView.removeFromSuperview()
                self.isShowPicker.toggle()
            }
        }
    }
    
    private func removeTitleFromPicker() {
        let labels = self.pickerView.subviews
        labels.forEach { (view) in
            if view is UILabel {
                view.removeFromSuperview()
            }
        }
    }
    
    @IBAction func pickerSelectValueBtn(_ sender: UIButton) {
        isShowPicker = false
        showHidePicker()
        self.pickerView(self.pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
        if let selectedCell = selectedCell as? LabelTextFieldFormTableViewCell, selectedCell.baseRow.tag == "birthday" {
            selectedCell.valueTextField.text = datePickerView.date.toBirthdayString()
            selectedCell.row.value = datePickerView.date.toBirthdayString()
            output.prepareAndSaveBirthday(date: datePickerView.date)
            
        } else if let selectedCell = selectedCell as? LabelTextFieldFormTableViewCell {
            datePickerView.isHidden = true
            let id = currentPickerData[pickerView.selectedRow(inComponent: 0)]
            selectedCell.valueTextField.text = selectedValue
            selectedCell.row.value = selectedValue
            selectedCell.row.validate()
            self.preparePickerValueToSave(cellTag: selectedCell.baseRow.tag, id: id.id)
            
        } else if let selectedCell = selectedCell as? OneLanguageFormTableViewCell {
            datePickerView.isHidden = true
            selectedCell.leftField.text = selectedValue
            selectedCell.languageId = selectedValue
            selectedCell.saveFields()
        } else if let selectedCell = selectedCell as? AgeFormTableViewCell {
            removeTitleFromPicker()
            datePickerView.isHidden = true
            let from = currentPickerData[pickerView.selectedRow(inComponent: 0)]
            let to = currentPickerData[pickerView.selectedRow(inComponent: 1)]
            selectedCell.checkValidAge()
            if let fromid = from.id, let toid = to.id {
                saveFiled(["ageFrom":"\(fromid)", "ageTo":"\(toid)"])
            }
        }
    }
    
    internal func deleteRow(witch: String, indexPath: IndexPath, id: String) {
        
        if witch.contains("educationType") {
            output.deleteField(["education[id]": id, "method": "education"])   
            self.form.allSections[indexPath.section].remove(at: indexPath.row) // уровень
            self.form.allSections[indexPath.section].remove(at: indexPath.row) // специальность
            self.form.allSections[indexPath.section].remove(at: indexPath.row) // название
        }
        
    }
    
    internal func preparePickerValueToSave(cellTag: String?, id: Int?) {
        guard let cellTag = cellTag, let id = id else { return }
        switch cellTag {
        case "location[cityId]", "career[professionId]", "career[profAreaId]", "goal[id]", "nationality[id]", "religion[id]":
            saveFiled([cellTag:"\(id)"])
        default:
            print("Нет совпадений")
            showErrorHUD()
            break
        }
    }
    
    @IBAction func pickerCancelBtn(_ sender: UIButton) {
        if selectedCell is AgeFormTableViewCell {
            removeTitleFromPicker()
        }
        isShowPicker = false
        showHidePicker()
    }
}

// MARK: -
// MARK: EditorRegistrationViewInput
extension EditorRegistrationViewController: EditorRegistrationViewInput {
    
    func setPetAvatarUrl(_ url: String) {
        if let petCell = selectedCell as? PetsFormTableViewCell {
            petCell.photoUrl = url
            petCell.update()
            petCell.savePetData()
            HUD.hide()
        }
    }
    
    func presentActionSheet(_ actionSheet: UIAlertController, _ animated: Bool) {
        present(actionSheet, animated: animated)
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            //            HUD.hide()
            //            self.refreshControl.endRefreshing()
        }
    }
    
}

extension EditorRegistrationViewController {
    
    private func showAutocompleteView(row: [AutocompleteModel], mapObject: UIView, delegate: SaveFieldsType?) {
        let popVC = AutocompleteController.init(mapObject: mapObject, rows: row, delegate: delegate)
        self.present(popVC, animated: true, completion: nil)
    }
    
    func updateProgressBar(fieldTag: String, isValid: Bool) {
        progressLineView.setDelimiter()
        let currentScores = scores.currentScrores(fieldTag: fieldTag, isValid: isValid)
        progressLineView.setProgress(value: currentScores)
        
        if currentScores >= 76.0 { // минимальное кол-во проходных баллов заполненности полей формы
            progressPercentValueLabel.text = "\(Int(currentScores))%"
            progressStatusLabel.text = "Можно знакомиться"
            saveFormBtn.isEnabled = true
        } else {
            progressPercentValueLabel.text = "\(Int(currentScores))%"
            progressStatusLabel.text = "Заполните поля"
            saveFormBtn.isEnabled = false
        }
    }
    
    func showLoader(isShow: Bool = true) {
        if isShow {
            HUD.show(.progress)
        } else {
            HUD.hide()
        }
    }
    
    func setUserData(_ user: User, cloudTags: [TagCloudElement], tagsInterestings: [TagCloudElement]) {
        self.user = user
        self.tagsInterestings = tagsInterestings
        tagsArray = cloudTags
    }
    
    func updateSectionbyTag(_ tag: String) {
        
        for section in form.allSections where section.tag == tag {
            //TODO: подумать как вернуть id нового образования в ячейку чтобы можно было удалять её без обновления всего экрана
        }
    }
    
    func updateAvatar(updatedUser: User) {
        HUD.hide()
        self.user = updatedUser
        if let avatarCell = form.rowBy(tag: "MainFormTableViewCell")?.baseCell as? MainFormTableViewCell {
            if let avatarUrl = updatedUser.avatar?.src?.square {
                avatarCell.setAvatar(avatarUrl: avatarUrl)
            }
        }
    }
    
    
    func updateUser(user: User) {
        self.user = user
    }
    
    func showErrorHUD() {
        HUD.hide()
        HUD.flash(.error, onView: view, delay: 0.3) { (_) in
            HUD.hide()
        }
    }
    
    func setUserPhoto(userPhotos: [UserPhotos]) {
        self.userPhotos = userPhotos
        
        if form.count == 0 {
            self.setupCell()
        } else {
            let photoSection = form.sectionBy(tag: "photoSectionTag")
            photoSection?.reload()
            let _ = photoSection!.allSatisfy { (row) -> Bool in
                if row.tag == "photoRowTag" {
                    row.reload()
                    return true
                }
                return false
            }
        }
        HUD.hide()
    }
    
    func removePhotoFromGallery(_ id: Int) {
        photoGalleryController?.deletePhoto(id)
    }
    
    func setCounters(likes: [PhotoLike], comments: [PhotoComment]) {
        photoGalleryController?.setCounters(likes: likes, comments: comments)
    }
    
    private func showChangePhotoActionSheet() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        let openGallery = UIAlertAction(title: "Открыть фотографию", style: .default) { action in
            // показать эту фотку с коментами и прочим
        }
        
        let changePhoto = UIAlertAction(title: "Изменить фотографию", style: .default) { action in
            self.showPhotoGallery()
        }
        
        let deletePhoto = UIAlertAction(title: "Удалить фотографию", style: .destructive) { action in
            
        }
        
        let setAvatar = UIAlertAction(title: "Установить как фото профиля", style: .default) { action in
            // setAvatar
        }
        
        actionSheet.addAction(openGallery)
        actionSheet.addAction(changePhoto)
        actionSheet.addAction(deletePhoto)
        actionSheet.addAction(setAvatar)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true) {
        }
        
    }
    
    func addPhoto() {
        isPetPhoto = false
        isSetAvatar = false
        showGalleryActionSheet()
    }
    
    func setAvatar() {
        isSetAvatar = true
        showGalleryActionSheet()
    }
    
    private func showGalleryActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Камера", style: .default) { action in
            self.openCamera()
        }
        let photoGallery = UIAlertAction(title: "Галлерея", style: .default) { action in
            self.openPhotoLibrary()
        }
        actionSheet.addAction(photoGallery)
        actionSheet.addAction(camera)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
}
// MARK: CollectionView delegate
extension EditorRegistrationViewController: EditorRegistrationViewControllerType {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.userPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let addPhotoView = UIImageView(image: UIImage(named: "pesonalAddPhotoPlaceHolder"))
        let addColorView = UIImageView(image: UIImage.init(color: UIColor.paleGreyTwo, size: CGSize(width: 106, height: 106)))
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonalPhotoCollectionViewCell", for: indexPath) as! PersonalPhotoCollectionViewCell
        if let src = userPhotos[indexPath.row].src, let srcDefault = src.small, (userPhotos[indexPath.row].id != nil && userPhotos[indexPath.row].id != 0) {
            cell.userPhoto.sd_setImage(with: URL(string: srcDefault), placeholderImage: UIImage(named: "pesonalAddPhotoPlaceHolder"), completed: { (image, error, cache, url) in
                let resizedImage = image?.cropToBounds(image: image!, width: 106, height: 106)
                cell.userPhoto.image = resizedImage
            })
            return cell
        } else {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonalPhotoCollectionViewCell", for: indexPath) as! PersonalPhotoCollectionViewCell
            if indexPath.row == 0 { // первый элемент
                cell.userPhoto.image = nil
                cell.backgroundView = addPhotoView
            } else {
                cell.userPhoto.image = nil
                cell.backgroundView = addColorView
            }
           
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? PersonalPhotoCollectionViewCell, cell.userPhoto.image == nil {
            addPhoto()
        } else {
            let correctIndex = IndexPath(row: indexPath.row, section: indexPath.section)
            pressButton(data: correctIndex, actionType: .showPhotoGallery)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 106, height: 106)
    }
    
}

extension EditorRegistrationViewController: CellActionHandlerType, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func changeAvatar() {
        let photoGalleryController = PhotoGalleryViewController(userPhotos: userPhotos.filter { ($0.id != nil && $0.id != 0) }, selectedIndex: 0, user: user, enabledBottomToolBarElements: false, avatarMode: false)
        photoGalleryController.delegateParent = self
        self.present(photoGalleryController, animated: true)
    }
    
    func deleteTagWText(text: String) {
        tagsInterestings = tagsInterestings.filter { (element) -> Bool in
            return element.text != text
        }
        output.addNewInterestingTag(tags: tagsInterestings)
        // можно пересоздать всю ячейку с новыми тэгами
        //        let row = form.rowBy(tag: "interests[interests]")?.baseCell as? InterestingFormTableViewCell
        //        row?.createTagCloud(withArray: tagsInterestings)
    }
    
    func showPhotoGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            imagePicker.navigationBar.isTranslucent = false
            imagePicker.navigationBar.barTintColor = .cerulean
            imagePicker.navigationBar.tintColor = .white
            imagePicker.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoLibrary() {
        showPhotoGallery()
        //        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
        //            let imagePicker = UIImagePickerController()
        //            imagePicker.delegate = self
        //            imagePicker.sourceType = .photoLibrary
        //            imagePicker.allowsEditing = true
        //
        //            imagePicker.navigationBar.isTranslucent = false
        //            imagePicker.navigationBar.barTintColor = .cerulean
        //            imagePicker.navigationBar.tintColor = .white
        //            imagePicker.navigationBar.titleTextAttributes = [
        //                NSAttributedString.Key.foregroundColor : UIColor.white
        //            ]
        //
        //            self.present(imagePicker, animated: true, completion: nil)
        //        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        checkPermission()
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        //        imagePicked.image = image
        // send image data to server and ger url, then send url and get id
        
        // если это фото животного, то надо сохранять без добавления в альбом пользователя
        output.uploadImage(image, isPetPhoto, isSetAvatar)
        showLoader()
        
        //let newPhoto = UserPhotos.init(id: 0, user_id: user.id!, deleted: 0, datetime: Date().toString(), description: "", src: nil, isLiked: false)
        //userPhotos.append(newPhoto)
        //tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        
        dismiss(animated:true, completion: nil)
        //        HUD.show(.progress)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        }
    }
}

extension EditorRegistrationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if (tableView.cellForRow(at: selectedRow) is LabelTextFieldFormTableViewCell) || (tableView.cellForRow(at: selectedRow) is OneLanguageFormTableViewCell) {
            return 1
        } else {
            return 2 // когда selectedRow == 00 то это дефолтное состояние
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currentPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currentPickerData[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.numberOfComponents == 2 {
            if pickerView.selectedRow(inComponent: 0) >= pickerView.selectedRow(inComponent: 1) {
                pickerView.selectRow(pickerView.selectedRow(inComponent: 0), inComponent: 1, animated: true)
            }
        }
        
        if let selectedCell = tableView.cellForRow(at: selectedRow) as? AgeFormTableViewCell {
            let ageFrom = pickerView.selectedRow(inComponent: 0)
            let ageTo = pickerView.selectedRow(inComponent: 1)
            selectedCell.fromAgeField.text = age[ageFrom].title!
            selectedCell.toAgeField.text = age[ageTo].title!
        } else if let selectedCell = tableView.cellForRow(at: selectedRow) as? LabelTextFieldFormTableViewCell, selectedCell.baseRow.tag == "birthday"  {
        } else {
            return selectedValue = currentPickerData[row].title ?? "" }
    }
    
}

extension EditorRegistrationViewController: PhotoGalleryDelegateType {
    func showPhotoComments(view: PhotoCommentView) {
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func updateUserPhotos(userId: Int) {
        output.downloadUserPhotos(userID: user.id!)
    }
    
    func requestCountersGallery(_ idPhoto: Int) {
        return output.requestCountersGallery(idPhoto)
    }
    
    func pressButton(data: Any, actionType: UserpageViewController.ActionBtnType) {
        
        if actionType == .showPhotoGallery {
            if let indexPath = data as? IndexPath {
                let photoGalleryController = PhotoGalleryViewController(userPhotos: userPhotos.filter { ($0.id != nil && $0.id != 0) }, selectedIndex: indexPath.row - 1, user: user)
                photoGalleryController.delegateParent = self
                self.present(photoGalleryController, animated: true)
            }
        } else if actionType == .pressMoreBtn {
            if let photoId = data as? Int {
                self.output.pressDeletePhoto(photoId: photoId)
            }
        } else if let photoId = data as? Int, actionType == .sendPhotoLike {
            output.sendPhotoLike(photoId)
        } else if actionType == .sendUpdateAvatar, let photoId = data as? Int  {
            output.sendUpdateAvatar(photoId)
            self.showLoader(isShow: true)
        }
    }
    
}
// MARK: Отправка на сервер данных поля
extension EditorRegistrationViewController: SaveFieldsType {
    weak var viewOutput: EditorRegistrationViewOutput? {
        get {
            return self.output
        }
    }
    
    func saveFiled(_ parameters: [String:String]) { // key: String, value: String
        //HUD.show(.progress, onView: view)
        output.saveUserField(parameters)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

