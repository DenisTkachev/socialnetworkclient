//
// AboutmeFormTableViewCell
// LinkYou
//
// Created by Denis Tkachev on 06.09.2018.
//Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class AboutmeFormTableViewCell: Cell<String>, CellType, UITextFieldDelegate {
    

    @IBOutlet weak var aboutTextField: PaddedTextField!
    weak var delegate: SaveFieldsType!
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
        aboutTextField.delegate = self
        aboutTextField.layer.cornerRadius = 12
        aboutTextField.layer.masksToBounds = true
        aboutTextField.layer.borderWidth = 1
        aboutTextField.layer.borderColor = UIColor.paleGrey.cgColor
        
        
    }
    
    public override func update() {
        super.update()
//        aboutTextField.top = 6
//        aboutTextField.left = 6
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let rowTag = self.baseRow.tag, let value = textField.text {
            delegate.saveFiled([rowTag:value])
        }
        return true
    }
    
}

public final class AboutmeFormTableViewCellRow: Row<AboutmeFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<AboutmeFormTableViewCell>(nibName: "AboutmeFormTableViewCell")
    }
}
