//
// InformationFormTableViewCell
// LinkYou
//
// Created by Denis Tkachev on 06.09.2018.
//Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class InformationFormTableViewCell: Cell<String>, CellType {
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
        self.backgroundColor = UIColor.init(hex: "F6F8FB")
    }
    
    public override func update() {
        super.update()
        
    }
    
}

public final class InformationFormTableViewCellRow: Row<InformationFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<InformationFormTableViewCell>(nibName: "InformationFormTableViewCell")
    }
}
