//
//  PhotoCollectionTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 03.10.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class PhotoCollectionTableViewCell: Cell<String>, CellType {
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
    }
    
    public override func update() {
        super.update()
        
    }
    
}

public final class PhotoCollectionFormTableViewCellRow: Row<PhotoCollectionTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<PhotoCollectionTableViewCell>(nibName: "PhotoCollectionTableViewCell")
    }
}
