//
//  OneLanguageFormTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 08/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class OneLanguageFormTableViewCell: Cell<String>, CellType {
    
    @IBOutlet weak var leftField: TextFieldCustom!
    @IBOutlet weak var rightField: TextFieldCustom!
    @IBOutlet weak var rowSelectorImage: UIImageView!
    @IBOutlet weak var crossImage: UIImageView!
    
    weak var delegate: SaveFieldsType!
    var actionIs = ""
    var id = ""
    var languageId: String = ""
    var languageLevel: String = ""
    
    public override func setup() {
        super.setup()
        self.selectionStyle = .none
        
        rightField.addTapGestureRecognizer {
            self.actionIs = "right"
            self.baseRow.didSelect()
        }
        
        leftField.addTapGestureRecognizer {
            self.actionIs = "left"
            self.baseRow.didSelect()
        }
        
        crossImage.addTapGestureRecognizer {
            self.actionIs = "deleteMe"
            self.baseRow.didSelect()
        }
    }
    
    public override func update() {
        super.update()
    }
    
    func saveFields() {
        print("???language: \(languageId)")
        print("???level: \(languageLevel)")
        if let left = leftField.text, let right = rightField.text {
            if !left.isEmpty && !right.isEmpty {
                self.delegate.viewOutput?.saveLanguage(language: String(languageId), level: String(languageLevel), id: String(id))
            }
        }
    }
    
}

public final class OneLanguageFormTableViewCellRow: Row<OneLanguageFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<OneLanguageFormTableViewCell>(nibName: "OneLanguageFormTableViewCell")
    }
}
