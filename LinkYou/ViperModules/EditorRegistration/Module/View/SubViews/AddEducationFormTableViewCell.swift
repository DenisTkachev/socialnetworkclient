//
//  AddEducationFormTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class AddEducationFormTableViewCell: Cell<String>, CellType {
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cellNameLabel: UILabel!
    public override func setup() {
        super.setup()
        selectionStyle = .none
        self.addTapGestureRecognizer {
            self.baseRow.didSelect()
        }
    }
    
    public override func update() {
        super.update()
    }
}

public final class AddEducationFormTableViewCellRow: Row<AddEducationFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<AddEducationFormTableViewCell>(nibName: "AddEducationFormTableViewCell")
    }
}

