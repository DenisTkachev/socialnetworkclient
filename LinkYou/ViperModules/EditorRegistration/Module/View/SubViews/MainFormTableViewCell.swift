//
//  MainFormTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 06.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka
import SDWebImage

public class MainFormTableViewCell: Cell<String>, CellType {
    
    @IBOutlet weak var cellHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var userAvatarImage: UIImageView!
    @IBOutlet weak var uploadBtn: RoundButtonCustom!
    @IBOutlet weak var validateMsgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var validateMessageLable: UILabel!
    weak var delegate: CellActionHandlerType!
    
    public override func setup() {
        super.setup()
        row.tag = RegistrationFieldTag.avatar.rawValue
        selectionStyle = .none
        row.value = ""
        userAvatarImage.addTapGestureRecognizer {
            self.baseRow.didSelect()
        }
        self.cellHeightConstr.constant = UIScreen.main.bounds.width - 32
        backView.addDashedBorder(width: UIScreen.main.bounds.width - 32, height: cellHeightConstr.constant, lineWidth: 1, lineDashPattern: [6,3], strokeColor: UIColor.cloudyBlue, fillColor: UIColor.clear)
        backView.bounds.origin = CGPoint(x: -16, y: -16)
    }
    
    public override func update() {
        super.update()
        row.validate()
    }
    
    public func setAvatar(avatarUrl: String) {
        if avatarUrl.count > 0, !avatarUrl.contains("avatar-default") {
            userAvatarImage.sd_setImage(with: URL(string: avatarUrl), placeholderImage: nil, completed: { (image, error, cache, url) in
                
//                UIView.animate(withDuration: 0.3, animations: {
                if image != nil {
                let resizedImage = image?.cropToBounds(image: image!, width: Double(UIScreen.main.bounds.width), height: Double(self.userAvatarImage.bounds.width))
                self.userAvatarImage.image = resizedImage!
                self.userAvatarImage.backgroundColor = .black
                    self.userAvatarImage.layoutIfNeeded()
                    self.row.value = "valid"
//                })
                    
                self.userAvatarImage.isHidden = false
                self.backView.isHidden = true
                }
            })
        } else {
            self.row.value = ""
        }
    }
    
    @IBAction func uploadPhotoBtn(_ sender: RoundButtonCustom) {
        delegate.setAvatar()
    }
    
    open func showValidateMessage(show: Bool) {
        if show {
            validateMsgHeight.constant = 17
            validateMessageLable.text = row.validationErrors.first?.msg
        } else {
            validateMsgHeight.constant = 0
            validateMessageLable.text = nil
        }
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
}

public final class MainFormRow: Row<MainFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<MainFormTableViewCell>(nibName: "MainFormTableViewCell")
    }
}
