//
// LabelTextFieldFormTableViewCell
// LinkYou
//
// Created by Denis Tkachev on 06.09.2018.
//Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class LabelTextFieldFormTableViewCell: Cell<String>, CellType, UITextFieldDelegate {
    
    enum CheckMark {
        case arrow
        case zoom
        case none
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueTextField: TextFieldCustom!
    @IBOutlet weak var optionBtn: UIButton!
    @IBOutlet weak var validateMsgHeight: NSLayoutConstraint!
    @IBOutlet weak var validateMessageLable: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var deleteBtnConstr: NSLayoutConstraint! //26
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var id: String?
    var additionalParameters: [String:String] = [:]
    weak var delegate: SaveFieldsType!
    var optionBtnState = CheckMark.none
    
    public override func setup() {
        super.setup()
        row.value = ""
        valueTextField.delegate = self
        valueTextField.addTarget(self, action: #selector(LabelTextFieldFormTableViewCell.textFieldDidChange(_:)), for: .allEvents)
        selectionStyle = .none
        self.addTapGestureRecognizer {
            self.baseRow.didSelect()
        }
        valueTextField.isUserInteractionEnabled = false
        
    }
    
    public override func update() {
        super.update()
        row.validate()
        switch optionBtnState {
        case .none: optionBtn.isHidden = true
        case .arrow: optionBtn.isHidden = false
        optionBtn.setImage(#imageLiteral(resourceName: "row-selector-close"), for: .normal) // row-selector-close
        optionBtn.tintColor = .charcoalGrey
        case .zoom: optionBtn.isHidden = false
        optionBtn.setImage(#imageLiteral(resourceName: "menu-search"), for: .normal)
        optionBtn.tintColor = .lightGray
        }
    }
    
    public func switchOnInput() {
        valueTextField.isUserInteractionEnabled = true
    }
    
    @IBAction func pressOptionBtn(_ sender: UIButton) {
    }
    
    @objc public func textFieldDidChange(_ textField: UITextField) {
        row.value = textField.text
        update()
    }
    
    @IBAction func pressDeleteBtn(_ sender: UIButton) {
        guard let rowIndexPath = row.indexPath, let id = id, let tag = row.tag else { return }
        delegate.deleteRow(witch: tag, indexPath: rowIndexPath, id: id)
    }
    
    func showDeleteBtn() {
        deleteBtnConstr.constant = 26
    }
    
    open func showValidateMessage(show: Bool) { // TODO:  удалить view валицаии ктр выезжает (после окончательного утверждения вида валидации) validateMessageLable
        if show {
            valueTextField.borderColor = .red
            let attributedString = NSMutableAttributedString(string: valueTextField.placeholder ?? "Заполните поле", attributes: [
                .foregroundColor: UIColor.red
                ])
            valueTextField.attributedPlaceholder = attributedString
        } else {
            valueTextField.borderColor = .paleGrey
            valueTextField.attributedPlaceholder = nil
        }
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
    open func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let rowTag = self.baseRow.tag, let value = textField.text, value.count > 0 {
            if rowTag.contains("institution") {
                // костыль из-за того что у Eureka должны быть row с уникальными тэгами!
                delegate.viewOutput?.prepareAndSaveEducationObject(key: "education[institution_name]", id: id ?? "", value: value)
            } else if rowTag.contains("speciality") {
                // костыль костыльный :(
                delegate.viewOutput?.prepareAndSaveEducationObject(key: "education[speciality_name]", id: id ?? "", value: value)
            } else if delegate != nil {
                additionalParameters = [rowTag:value]
                delegate.saveFiled(additionalParameters) // должно было быть просто вот так
            }
        }
        return true
    }
}

public final class LabelTextFieldFormTableViewCellRow: Row<LabelTextFieldFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<LabelTextFieldFormTableViewCell>(nibName: "LabelTextFieldFormTableViewCell")
    }
}
