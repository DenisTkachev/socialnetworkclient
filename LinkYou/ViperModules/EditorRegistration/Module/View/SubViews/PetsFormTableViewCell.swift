//
//  PetsFormTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 12/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class PetsFormTableViewCell: Cell<String>, CellType, UITextFieldDelegate {
    
    @IBOutlet weak var petImageView: UIImageView!
    @IBOutlet weak var petNameTextField: TextFieldCustom!
    @IBOutlet weak var petTypeTextField: TextFieldCustom!
    @IBOutlet weak var deleteBtn: UIImageView!
    
    var actionIs = ""
    var id = ""
    var photoUrl = ""
    weak var delegate: SaveFieldsType!
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
        petNameTextField.delegate = self
        petTypeTextField.delegate = self
        
        petImageView.addTapGestureRecognizer {
            self.actionIs = "addPhoto"
            self.baseRow.didSelect()
        }
        
        deleteBtn.addTapGestureRecognizer {
            self.actionIs = "deleteMe"
            self.baseRow.didSelect()
        }
    }
    
    public override func update() {
        super.update()
            petImageView.sd_setImage(with: URL(string: "\(photoUrl)"), placeholderImage: nil, completed: { (image, error, cache, url) in
                if url == nil {
                    self.petImageView.image = #imageLiteral(resourceName: "pet-default")
                }
            })
    }
    
    public func switchOnInput() {
        petNameTextField.isUserInteractionEnabled = true
        petTypeTextField.isUserInteractionEnabled = true
    }
    
    func savePetData() {
        if let petName = petNameTextField.text, let petType = petTypeTextField.text {
            if !petName.isEmpty && !petType.isEmpty {
                self.delegate.saveFiled(["pet[id]": id, "pet[pet_name]": petName, "pet[pet_type]": petType, "pet[pet_description]": "", "pet[pet_avatar]": photoUrl])
            }
        }
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        savePetData()
        return true
    }
    
}

public final class PetsFormTableViewCellRow: Row<PetsFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<PetsFormTableViewCell>(nibName: "PetsFormTableViewCell")
    }
}
