//
// AgeFormTableViewCell
// LinkYou
//
// Created by Denis Tkachev on 06.09.2018.
//Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class AgeFormTableViewCell: Cell<String>, CellType {
    
    @IBOutlet weak var fromAgeField: TextFieldCustom!
    @IBOutlet weak var toAgeField: TextFieldCustom!
    @IBOutlet weak var validateMsgHeight: NSLayoutConstraint!
    @IBOutlet weak var validateMessageLable: UILabel!
    
    public override func setup() {
        super.setup()
        row.value = ""
        selectionStyle = .none
        fromAgeField.isUserInteractionEnabled = false
        toAgeField.isUserInteractionEnabled = false
        self.addTapGestureRecognizer {
            self.baseRow.didSelect()
        }
    }
    
    public override func update() {
        super.update()
        row.validate()
    }
    
    open func checkValidAge() {
        if let fromAge = fromAgeField.text, let toAge = toAgeField.text, !fromAge.isEmpty, !toAge.isEmpty {
            if let from = Int(fromAge), let to = Int(toAge), from <= to {
                row.value = "valid"
            } else {
                row.value = "notValid"
            }
        } else {
            row.value = "notValid"
        }
    }
    
    open func showValidateMessage(show: Bool) { // TODO:  удалить view валицаии ктр выезжает (после окончательного утверждения вида валидации) validateMessageLable
        if show {
            fromAgeField.borderColor = .red
            toAgeField.borderColor = .red
            let attributedStringFrom = NSMutableAttributedString(string: fromAgeField.placeholder ?? "Возраст от", attributes: [
                .foregroundColor: UIColor.red
                ])
            
            let attributedStringTo = NSMutableAttributedString(string: toAgeField.placeholder ?? "Возраст до", attributes: [
                .foregroundColor: UIColor.red
                ])
            
            fromAgeField.attributedPlaceholder = attributedStringFrom
            toAgeField.attributedPlaceholder = attributedStringTo

        } else {
            fromAgeField.borderColor = .paleGrey
            fromAgeField.attributedPlaceholder = nil
            
            toAgeField.borderColor = .paleGrey
            toAgeField.attributedPlaceholder = nil
            //            validateMsgHeight.constant = 0
            //            validateMessageLable.text = nil
        }
//        if show {
//            validateMsgHeight.constant = 17
//            validateMessageLable.text = row.validationErrors.first?.msg
//        } else {
//            validateMsgHeight.constant = 0
//            validateMessageLable.text = nil
//        }
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
}

public final class AgeFormTableViewCellRow: Row<AgeFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<AgeFormTableViewCell>(nibName: "AgeFormTableViewCell")
    }
}
