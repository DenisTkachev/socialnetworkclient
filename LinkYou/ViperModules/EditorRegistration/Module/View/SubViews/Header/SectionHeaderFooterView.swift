//
//  SectionHeaderFooterView.swift
//  LinkYou
//
//  Created by Denis Tkachev on 09/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import Eureka

class SectionHeaderFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var secNameLabel: UILabel!
    @IBOutlet weak var secIconImage: UIImageView!    
    @IBOutlet weak var hideArrowIcon: UIButton!
    @IBOutlet weak var photoCounter: UILabel!
    
    var sec: Section!
    var isCollapsed: Bool = false
    var rows: [BaseRow]!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func hideTapped(_ sender: UIButton) {
        if !isCollapsed {
            hideArrowIcon.imageView?.rotate180WithAnimation(open: false)
            rows = []
            for row in sec.reversed() {
                rows.append(row)
            }
            sec.removeAll()
        } else {
            for row in rows.reversed() {
                sec <<< row
            }
        }
        isCollapsed.toggle()
        hideArrowIcon.imageView?.rotate180WithAnimation(open: isCollapsed)
    }
    
    func hideArrow() {
        hideArrowIcon.isHidden = true
    }
    
}
