//
//  InterestingFormTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 09/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

protocol ButtonActionType: class {
    func deleteBtnPress(bgView: BgView)
}

public class InterestingFormTableViewCell: Cell<String>, CellType, ButtonActionType {
    
    func deleteBtnPress(bgView: BgView) {
        if bgView.text != nil {
            removeTagFromView(bgView: bgView)
//            delegate.deleteTagWText(text: bgView.text!)
        }
    }
    
    @IBOutlet weak var cellHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagsCloudView: UIView!
    @IBOutlet weak var roundedView: UIView!
    
    weak var delegate: CellActionHandlerType!
    
    var actionIs = ""
    var id = 0
    
    public override func setup() {
        super.setup()
        self.selectionStyle = .none
        roundedView.clipsToBounds = true
        roundedView.layer.borderWidth = 1
        roundedView.layer.borderColor = UIColor.paleGrey.cgColor
        roundedView.layer.cornerRadius = 21
        
        tagsCloudView.addTapGestureRecognizer {
            self.baseRow.didSelect()
        }
    }
    
    public override func update() {
        super.update()
    }
    
    // tagCloud
    func createTagCloud(withArray data: [TagCloudElement]) {
        
        if data.count > 0 {
            data.first!.createTagCloud(withArray: data, tagsCloudView: tagsCloudView, cellHeightConstraint: cellHeightConstraint, deleteBtnisOn: true, deleteBtnDelegate: self)
        }
    }
    
    private func removeTagFromView(bgView: BgView) {
        self.tagsCloudView.subviews.forEach { (view) in
            if view === bgView {
                view.removeFromSuperview()
                if let label = bgView.subviews.first as? UILabel {
                    if let text = label.text, text.count > 0 {
                        delegate.deleteTagWText(text: text)
                    }
                }
            }
        }
    }
}

public final class InterestingFormTableViewCellRow: Row<InterestingFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<InterestingFormTableViewCell>(nibName: "InterestingFormTableViewCell")
    }
}
