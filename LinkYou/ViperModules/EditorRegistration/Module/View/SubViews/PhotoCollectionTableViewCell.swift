//
//  PhotoCollectionTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 03.10.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class PhotoCollectionTableViewCell: Cell<String>, CellType {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var validateMsgHeight: NSLayoutConstraint!
    @IBOutlet weak var validateMessageLable: UILabel!
    
    weak var delegate: EditorRegistrationViewControllerType!
    var imageArray: [UserPhotos] = []
    
    
    public override func setup() {
        super.setup()
        selectionStyle = .none
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(UINib(nibName: "PersonalPhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PersonalPhotoCollectionViewCell")
//        row.value = ""
    }
    
    override public func prepareForReuse() {
        imageArray.removeAll()
        backgroundView = nil
    }
    
    public override func update() {
        super.update()
        collectionView.dataSource = delegate
        collectionView.delegate = delegate
        collectionView.reloadData()
        row.validate()
    }
    
    open func showValidateMessage(show: Bool) {
        if show {
            validateMsgHeight.constant = 17
            validateMessageLable.text = row.validationErrors.first?.msg
        } else {
            validateMsgHeight.constant = 0
            validateMessageLable.text = nil
        }
        UIView.animate(withDuration: 0.2) { // артефакт на коллекции в виде белой полоски
            self.layoutIfNeeded()
        }
    }
}

public final class PhotoCollectionTableViewCellRow: Row<PhotoCollectionTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<PhotoCollectionTableViewCell>(nibName: "PhotoCollectionTableViewCell")
    }
}
