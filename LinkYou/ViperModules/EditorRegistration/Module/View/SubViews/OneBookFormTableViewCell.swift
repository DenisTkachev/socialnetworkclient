//
//  OneBookFormTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 12/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class OneBookFormTableViewCell: Cell<String>, CellType, UITextFieldDelegate {
    
    @IBOutlet weak var titleRow1Label: UILabel!
    @IBOutlet weak var row1TextField: TextFieldCustom!
    
    @IBOutlet weak var titleRow2Label: UILabel!
    @IBOutlet weak var row2TextField: TextFieldCustom!
    
    @IBOutlet weak var deleteCrossImageView: UIImageView!
    @IBOutlet weak var sepLine: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    weak var delegate: SaveFieldsType!
    var actionIs = ""
    var id = ""
    
    public override func setup() {
        super.setup()
        self.selectionStyle = .none
        row1TextField.isUserInteractionEnabled = false
        row2TextField.isUserInteractionEnabled = false
        row1TextField.delegate = self
        row2TextField.delegate = self
        
        deleteCrossImageView.addTapGestureRecognizer {
            self.actionIs = "deleteMe"
            self.baseRow.didSelect()
        }
    }
    
    public override func update() {
        if sepLine.isHidden == true {
            bottomConstraint.constant = 0
        } else {
            bottomConstraint.constant = 16
        }
        super.update()
        
    }
    
    public func switchOnInput() {
        row1TextField.isUserInteractionEnabled = true
        row2TextField.isUserInteractionEnabled = true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let textLeft = row1TextField.text, let textRight = row2TextField.text {
            if !textLeft.isEmpty && !textRight.isEmpty {
                self.delegate.saveFiled(["book[id]" : id, "book[book_author]": textLeft, "book[book_name]": textRight])
            }
        }
        return true
    }
    
}

public final class OneBookFormTableViewCellRow: Row<OneBookFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<OneBookFormTableViewCell>(nibName: "OneBookFormTableViewCell")
    }
}
