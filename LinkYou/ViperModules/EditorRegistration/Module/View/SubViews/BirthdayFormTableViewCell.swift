//
//  BirthdayFormTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/12/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Eureka

public class BirthdayFormTableViewCell: Cell<String>, CellType {

    public override func setup() {
        super.setup()
        row.value = ""
        selectionStyle = .none
        self.addTapGestureRecognizer {
            self.baseRow.didSelect()
        }
    }

    public override func update() {
        super.update()
    }

//    open func checkValidAge() {
//        if let fromAge = fromAgeField.text, let toAge = toAgeField.text, !fromAge.isEmpty, !toAge.isEmpty {
//            row.value = "valid"
//        } else {
//            row.value = "notValid"
//        }
//    }

//    open func showValidateMessage(show: Bool) {
//        if show {
//            validateMsgHeight.constant = 17
//            validateMessageLable.text = row.validationErrors.first?.msg
//        } else {
//            validateMsgHeight.constant = 0
//            validateMessageLable.text = nil
//        }
//        UIView.animate(withDuration: 0.2) {
//            self.layoutIfNeeded()
//        }
//    }

}

public final class BirthdayFormTableViewCellRow: Row<BirthdayFormTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<BirthdayFormTableViewCell>(nibName: "BirthdayFormTableViewCell")
    }
}
