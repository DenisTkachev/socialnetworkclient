//
//  EditorRegistrationEditorRegistrationAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 05/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class EditorRegistrationAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(EditorRegistrationInteractor.self) { (r, presenter: EditorRegistrationPresenter) in
			let interactor = EditorRegistrationInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(EditorRegistrationRouter.self) { (r, viewController: EditorRegistrationViewController) in
			let router = EditorRegistrationRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(EditorRegistrationPresenter.self) { (r, viewController: EditorRegistrationViewController) in
			let presenter = EditorRegistrationPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(EditorRegistrationInteractor.self, argument: presenter)
			presenter.router = r.resolve(EditorRegistrationRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(EditorRegistrationViewController.self) { r, viewController in
			viewController.output = r.resolve(EditorRegistrationPresenter.self, argument: viewController)
		}
	}

}
