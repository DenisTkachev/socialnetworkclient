//
//  Top100PageTop100PageViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol Top100PageViewOutput: class {
  /// - author: KlenMarket
    func viewIsReady(page: Int)
    func showUserPage(user: Top100, allUser: [Top100])
}
