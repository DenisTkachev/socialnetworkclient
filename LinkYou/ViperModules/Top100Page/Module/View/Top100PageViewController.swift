//
//  Top100PageTop100PageViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

final class Top100PageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	// MARK: -
	// MARK: Properties
	var output: Top100PageViewOutput!
    @IBOutlet weak var tableView: UITableView!
    var topUsers = [Top100]()
    
    var paginationCurrent = 1
    var paginationIsEnd = 0
    var paginationLimit = 0
    var paginationTotal = 0
    
    // MARK: -
	// MARK: Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
        HUD.show(.progress)
		setupNavBar()
        
        output.viewIsReady(page: paginationCurrent)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "Top100TableViewCell", bundle: nil), forCellReuseIdentifier: "Top100TableViewCell")
	}
    
    func setupNavBar() {
        title = "Лучшие-100"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Top100TableViewCell", for: indexPath) as! Top100TableViewCell
        cell.setDataIntoCell(data: topUsers[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == topUsers.count - 1 {
            if paginationCurrent < paginationTotal {
                output.viewIsReady(page: paginationCurrent + 1)
            }
        }
    }
    
    func loadTable() {
        self.tableView.reloadData()
        HUD.hide()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        output.showUserPage(user: topUsers[indexPath.row], allUser: topUsers)
    }
}

// MARK: -
// MARK: Top100PageViewInput
extension Top100PageViewController: Top100PageViewInput {

    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
        }
    }
    
	func setupInitialState(data: [Top100], headers: [XPaginationHeaders : Int]) {
        guard let current = headers[XPaginationHeaders.XPaginationCurrent],
        let isEnd = headers[XPaginationHeaders.XPaginationIsEnd], let limit = headers[XPaginationHeaders.XPaginationLimit],
            let total = headers[XPaginationHeaders.XPaginationTotal] else { return }
        paginationCurrent = current
        paginationIsEnd = isEnd
        paginationLimit = limit
        paginationTotal = total
        data.forEach { (user) in
            topUsers.append(user)
        }
        loadTable()
        
	}

}

