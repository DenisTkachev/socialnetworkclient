//
//  Top100PageTop100PageViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol Top100PageViewInput: class {
  /// - author: KlenMarket
    func setupInitialState(data: [Top100], headers: [XPaginationHeaders : Int])
    func showAlert(title: String, msg: String)
}
