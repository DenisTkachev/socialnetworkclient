//
//  Top100PageTop100PagePresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class Top100PagePresenter: Top100PageViewOutput {
    
    // MARK: -
    // MARK: Properties

    weak var view: Top100PageViewInput!
    var interactor: Top100PageInteractorInput!
    var router: Top100PageRouterInput!

    // MARK: -
    // MARK: Top100PageViewOutput
    func viewIsReady(page: Int) {
        interactor.loadUsers(page: page)
    }

    func showUserPage(user: Top100, allUser: [Top100]) {
        router.presentUserPage(user: user, allUsers: allUser)
    }
}

// MARK: -
// MARK: Top100PageInteractorOutput
extension Top100PagePresenter: Top100PageInteractorOutput {
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func top100getted(users: [Top100], headers: [AnyHashable:Any]) {
        view.setupInitialState(data: users, headers: HttpHeaders.prepareHeaders(headers))
    }
}

// MARK: -
// MARK: Top100PageModuleInput
extension Top100PagePresenter: Top100PageModuleInput {

}
