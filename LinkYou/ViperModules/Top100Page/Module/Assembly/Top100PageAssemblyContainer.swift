//
//  Top100PageTop100PageAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class Top100PageAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(Top100PageInteractor.self) { (r, presenter: Top100PagePresenter) in
			let interactor = Top100PageInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(Top100PageRouter.self) { (r, viewController: Top100PageViewController) in
			let router = Top100PageRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(Top100PagePresenter.self) { (r, viewController: Top100PageViewController) in
			let presenter = Top100PagePresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(Top100PageInteractor.self, argument: presenter)
			presenter.router = r.resolve(Top100PageRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(Top100PageViewController.self) { r, viewController in
			viewController.output = r.resolve(Top100PagePresenter.self, argument: viewController)
		}
	}

}
