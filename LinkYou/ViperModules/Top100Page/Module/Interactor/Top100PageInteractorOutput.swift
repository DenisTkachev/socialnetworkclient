//
//  Top100PageTop100PageInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol Top100PageInteractorOutput: class {
    func top100getted(users: [Top100], headers: [AnyHashable : Any])
    func showAlert(title: String, msg: String)
}
