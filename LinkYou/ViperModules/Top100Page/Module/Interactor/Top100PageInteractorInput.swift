//
//  Top100PageTop100PageInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol Top100PageInteractorInput: class {
    func loadUsers(page: Int)
}
