//
//  Top100PageTop100PageInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class Top100PageInteractor: Top100PageInteractorInput {

  weak var output: Top100PageInteractorOutput!
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    func loadUsers(page: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.top(page: page)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([Top100].self, from: response.data)
                    if let httpResponse = response.response {
                        let headers = httpResponse.allHeaderFields
                        self.output.top100getted(users: serverResponse, headers: headers)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
