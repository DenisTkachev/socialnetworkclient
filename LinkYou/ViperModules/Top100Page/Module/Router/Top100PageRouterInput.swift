//
//  Top100PageTop100PageRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 20/09/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol Top100PageRouterInput: class {
    func presentUserPage(user: Top100, allUsers: [Top100])
}
