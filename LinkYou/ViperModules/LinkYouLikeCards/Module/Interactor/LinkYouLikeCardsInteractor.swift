//
//  LinkYouLikeCardsLinkYouLikeCardsInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Moya

final class LinkYouLikeCardsInteractor: LinkYouLikeCardsInteractorInput {
    weak var output: LinkYouLikeCardsInteractorOutput!
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)

    func setCardLike(_ userId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let usersProvider = MoyaProvider<LinkYouLikeCardsService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.like(userID: userId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let user = try JSONDecoder().decode(User.self, from: response.data)
                    self.output.appendUser(user)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func setCardDislike(_ userId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let usersProvider = MoyaProvider<LinkYouLikeCardsService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.disLike(userID: userId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    
                    let user = try JSONDecoder().decode(User.self, from: response.data)
                    print(response) // подсунуть в кучу
                    self.output.appendUser(user)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getLikeList() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<LinkYouLikeCardsService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.getList) { (result) in
            switch result {
            case .success(let response):
                do {
                    let findingPeoples = try JSONDecoder().decode([User].self, from: response.data)
                    if findingPeoples.count > 0 {
                        self.output.usersData(users: findingPeoples)
                    } else {
                        self.output.showAlert(title: "На сегодня всё", msg: "Возможно, позже появится кто-то ещё ¯\\_(ツ)_/¯")
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
