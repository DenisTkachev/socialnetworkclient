//
//  LinkYouLikeCardsLinkYouLikeCardsInteractorInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Foundation

protocol LinkYouLikeCardsInteractorInput: class {
    func getLikeList()
    func setCardLike(_ userId: Int)
    func setCardDislike(_ userId: Int)
}
