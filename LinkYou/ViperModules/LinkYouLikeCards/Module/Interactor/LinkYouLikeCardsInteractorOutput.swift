//
//  LinkYouLikeCardsLinkYouLikeCardsInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Foundation

protocol LinkYouLikeCardsInteractorOutput: class {
    func showAlert(title: String, msg: String)
    func usersData(users: [User])
    func appendUser(_ user: User)
}
