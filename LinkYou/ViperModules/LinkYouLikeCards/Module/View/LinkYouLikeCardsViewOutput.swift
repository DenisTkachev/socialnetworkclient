//
//  LinkYouLikeCardsLinkYouLikeCardsViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

protocol LinkYouLikeCardsViewOutput: class {
    /// - author: Denis Tkachev
    
    var users: [User] { get set }
    func sendUserDisLike(userIndex: Int)
    func sendUserLike(userIndex: Int)
    
}

