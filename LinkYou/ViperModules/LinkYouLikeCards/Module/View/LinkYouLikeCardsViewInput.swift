//
//  LinkYouLikeCardsLinkYouLikeCardsViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

protocol LinkYouLikeCardsViewInput: class {
    /// - author: Denis Tkachev
    func showAlert(title: String, msg: String)
    func reload()
}
