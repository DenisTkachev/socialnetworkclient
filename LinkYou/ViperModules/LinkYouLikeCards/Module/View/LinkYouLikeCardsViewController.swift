//
//  LinkYouLikeCardsLinkYouLikeCardsViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import UIKit
//import PKHUD
import Koloda
import SDWebImage
import pop

final class LinkYouLikeCardsViewController: UIViewController {

	// MARK: -
	// MARK: Properties
	var output: LinkYouLikeCardsViewOutput!
    var screenWidth: CGFloat = 0
    
    private let frameAnimationSpringBounciness: CGFloat = 9
    private let frameAnimationSpringSpeed: CGFloat = 16
    private let kolodaCountOfVisibleCards = 2
    private let kolodaAlphaValueSemiTransparent: CGFloat = 0.1
    
    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet weak var photoHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var disLikeButton: UIButton!
    
    // MARK: -
	// MARK: Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
        setupView()
        kolodaView.delegate = self
        kolodaView.dataSource = self
        //self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
	}

    func setupView() {
        title = "LikeYou"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        photoHeightConstraint.constant = UIScreen.main.bounds.width
        self.updateViewConstraints()
        screenWidth = UIScreen.main.bounds.width
        
        kolodaView.alphaValueSemiTransparent = kolodaAlphaValueSemiTransparent
        kolodaView.countOfVisibleCards = kolodaCountOfVisibleCards
        kolodaView.animator = BackgroundKolodaAnimator(koloda: kolodaView)
        kolodaView.backgroundColor = UIColor.veryLightBlue
    }
    
    @IBAction func yesButtonTap(_ sender: UIButton) {
        kolodaView?.swipe(.left)
    }
    
    @IBAction func noButtonTap(_ sender: UIButton) {
        kolodaView?.swipe(.right)
    }
    
    private func buttonToggle(isOn: Bool) {
        likeButton.isEnabled = isOn
        disLikeButton.isEnabled = isOn
    }

}

// MARK: -
// MARK: LinkYouLikeCardsViewInput
extension LinkYouLikeCardsViewController: LinkYouLikeCardsViewInput {
//    var kolodaIndex: Int {
//        get {
//            return kolodaView.currentCardIndex
//        }
//    }
    
    func reload() {
        kolodaView.backgroundColor = .clear
        kolodaView.reloadData()
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
        }
    }

}

// MARK: KolodaViewDelegate
extension LinkYouLikeCardsViewController: KolodaViewDelegate {
    
    func kolodaPanBegan(_ koloda: KolodaView, card: DraggableCardView) {
        buttonToggle(isOn: false)
    }
    
    func kolodaPanFinished(_ koloda: KolodaView, card: DraggableCardView) {
        buttonToggle(isOn: true)
    }
    
    func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {
        buttonToggle(isOn: true)
    }
    
    
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
//        let position = kolodaView.currentCardIndex
//        kolodaView.insertCardAtIndexRange(position..<position + 1, animated: true)
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        print("Тынц!")
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        
        switch direction {
        case .left:
            output.sendUserLike(userIndex: index)
        case .right:
            output.sendUserDisLike(userIndex: index)
        default:
            break
        }
    }
    
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return true
    }
    
    func kolodaShouldMoveBackgroundCard(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
        return true
    }
    
    func koloda(kolodaBackgroundCardAnimation koloda: KolodaView) -> POPPropertyAnimation? {
        let animation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
        animation?.springBounciness = frameAnimationSpringBounciness
        animation?.springSpeed = frameAnimationSpringSpeed
        return animation
    }
    
    
}

// MARK: KolodaViewDataSource
extension LinkYouLikeCardsViewController: KolodaViewDataSource {
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        let cardView = LikeCardView(frame: kolodaView.frame)
        cardView.setupView(user: output.users[index])
        cardView.backgroundColor = .white

       return cardView
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return output.users.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
//    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
//        return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?[0] as? OverlayView
//    }
    
}
