//
//  LikeCardView.swift
//  LinkYou
//
//  Created by Denis Tkachev on 14/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class LikeCardView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var userOnlineStatus: UIImageView!
    @IBOutlet weak var userPremiumStatus: UIImageView!
    @IBOutlet weak var userCrownEmblem: UIImageView!
    @IBOutlet weak var crownEmblemCount: UILabel!
    
    @IBOutlet weak var userNameAge: UILabel!
    @IBOutlet weak var jobProfessionLabel: UILabel!
    @IBOutlet weak var countLikeLabel: UILabel!
    @IBOutlet weak var countPhotoLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var userAvatarImage: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    fileprivate func commonInit() {
        Bundle.main.loadNibNamed("LikeCardView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.contentView.backgroundColor = UIColor.clear
        
    }
    
    func setupView(user: User) {
        if let name = user.name, let age = user.birthday?.age {
            userNameAge.text = ("\(name), \(age.toString())")
        }
        if let profession = user.job?.profession, let occupation = user.job?.occupation {
            jobProfessionLabel.text = "\(profession), \(occupation)"
        }
        if let avatarURL = user.avatar?.src?.square {
            userAvatarImage.sd_setImage(with: URL(string: avatarURL)) { (image, error, cache, url) in
//                HUD.hide(animated: false)
            }
        }
        
        if let cityName = user.location?.city_name, let countPhoto = user.photos_count, let countLikes = user.likes?.count {
            cityNameLabel.text = cityName
            countLikeLabel.text = String(format: NSLocalizedString("NumberOfLikes", comment: ""), countLikes)
            countPhotoLabel.text = "\(countPhoto.toString()) фото"
        }
        
        if let onlineStatus = user.is_online, let premiumStatus = user.is_premium, let top100 = user.is_top100 {
            if onlineStatus { // online
                userOnlineStatus.image = #imageLiteral(resourceName: "userPage-online-status")
            } else {
                userOnlineStatus.image = nil
            }
            
            if premiumStatus {
                userPremiumStatus.image = #imageLiteral(resourceName: "userPage-star")
            } else {
                userPremiumStatus.image = nil
            }
            
            if top100 {
                userCrownEmblem.isHidden = false
                crownEmblemCount.isHidden = false
            } else {
                userCrownEmblem.isHidden = true
                crownEmblemCount.isHidden = true
            }
        }
    }

}
