//
//  LinkYouLikeCardsLinkYouLikeCardsAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class LinkYouLikeCardsAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(LinkYouLikeCardsInteractor.self) { (r, presenter: LinkYouLikeCardsPresenter) in
			let interactor = LinkYouLikeCardsInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(LinkYouLikeCardsRouter.self) { (r, viewController: LinkYouLikeCardsViewController) in
			let router = LinkYouLikeCardsRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(LinkYouLikeCardsPresenter.self) { (r, viewController: LinkYouLikeCardsViewController) in
			let presenter = LinkYouLikeCardsPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(LinkYouLikeCardsInteractor.self, argument: presenter)
			presenter.router = r.resolve(LinkYouLikeCardsRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(LinkYouLikeCardsViewController.self) { r, viewController in
			viewController.output = r.resolve(LinkYouLikeCardsPresenter.self, argument: viewController)
		}
	}

}
