//
//  LinkYouLikeCardsLinkYouLikeCardsModuleInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

protocol LinkYouLikeCardsModuleInput: class {
    func setData()
}
