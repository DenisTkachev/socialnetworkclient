//
//  LinkYouLikeCardsLinkYouLikeCardsPresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/01/2019.
//  Copyright © 2019 KlenMarket. All rights reserved.
//

final class LinkYouLikeCardsPresenter: LinkYouLikeCardsViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: LinkYouLikeCardsViewInput!
    var interactor: LinkYouLikeCardsInteractorInput!
    var router: LinkYouLikeCardsRouterInput!
    
    var users: [User] = []
    
    // MARK: -
    // MARK: LinkYouLikeCardsViewOutput
    
    func sendUserLike(userIndex: Int) { // TODO: массив с юзерами бесконечно ростёт во время 1 сессии, что потребляет память
        if let selectedUser = users[userIndex].id {
            interactor.setCardLike(selectedUser)
        }
    }
    
    func sendUserDisLike(userIndex: Int) { // тоже самое что и выше
        if let selectedUser = users[userIndex].id {
            interactor.setCardDislike(selectedUser)
        }
    }
    
    func loadMoreUsers() {
        setData()
    }
}

// MARK: -
// MARK: LinkYouLikeCardsInteractorOutput
extension LinkYouLikeCardsPresenter: LinkYouLikeCardsInteractorOutput {
    func appendUser(_ user: User) {
        self.users.append(user)
        // хорошо бы удалять просмотренного юзера, но надо править корректно и kolodaCurrentCardIndex
    }
    
    func usersData(users: [User]) {
        self.users = users
        view.reload()
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
}

// MARK: -
// MARK: LinkYouLikeCardsModuleInput
extension LinkYouLikeCardsPresenter: LinkYouLikeCardsModuleInput {
    func setData() {
        interactor.getLikeList()
    }
}
