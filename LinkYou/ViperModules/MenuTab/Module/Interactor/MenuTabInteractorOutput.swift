//
//  MenuTabMenuTabInteractorOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol MenuTabInteractorOutput: class {
    func showStartScreen()
    func returnInterestingUsers(_ users: InterestingUsers)
    func showAlert(title: String, msg: String)
    func updateInvisableMode(visibleMode: Bool)
    func menuBadges(_ badges: Badges)
}
