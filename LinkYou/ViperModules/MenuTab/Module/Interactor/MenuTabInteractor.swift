//
//  MenuTabMenuTabInteractor.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class MenuTabInteractor: MenuTabInteractorInput {
    
    weak var output: MenuTabInteractorOutput!
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    func needExit() {
        // remove token
        DeviceCurrent().sessionToken = ""
        // show start screen
        output.showStartScreen()
    }
    
    func loadInterestingUsers() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.interestingUsers) { (result) in
            switch result {
            case .success(let response):
                do {
                    let interestingUsers = try JSONDecoder().decode(InterestingUsers.self, from: response.data)
                    self.output.returnInterestingUsers(interestingUsers)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func toggleInvisableMode() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }

        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        
        usersProvider.request(.toggleInvisableMode) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if serverResponse["done"] == true {
                        //self.output.forgotEmailWasSend()
                        if let response = serverResponse["visible"] {
                            self.output.updateInvisableMode(visibleMode: response)
                        }
                        
                    } else {
                        self.output.showAlert(title: "Ошибка сети", msg: "Попробуйте позже")
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadBadges() {

        let usersProvider = MoyaProvider<GeneralService>(plugins: [authPlagin])
        usersProvider.request(.getCurrentBadges) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(Badges.self, from: response.data)
                    self.output.menuBadges(serverResponse)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
