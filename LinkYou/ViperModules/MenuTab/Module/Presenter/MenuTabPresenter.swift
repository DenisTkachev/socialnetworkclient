//
//  MenuTabMenuTabPresenter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

final class MenuTabPresenter: MenuTabViewOutput {

    // MARK: -
    // MARK: Properties
    
    weak var view: MenuTabViewInput!
    var interactor: MenuTabInteractorInput!
    var router: MenuTabRouterInput!
    
    var superUser: User!
    var menuBadges: Badges?
    
    // MARK: -
    // MARK: MenuTabViewOutput
    func viewIsReady() {
        if DeviceCurrent.current.guestModeStatus() {
            let guestUser = User.init()
            view.setUserData(user: guestUser)
        } else {
            view.setUserData(user: superUser)
        }
        interactor.loadInterestingUsers()
    }
    
    func loadBadges() {
        interactor.loadBadges()
    }
    
    func refreshUserData() {
//        SuperUser.shared.shortUpdate { (complete) in
//            if complete {
//                guard let user = SuperUser.shared.superUser else { return }
//                self.superUser = user
////                self.view.setUserData(user: self.superUser) // дублируется секция и строка 0,0
//                self.view.reloadTableView()
//            }
//        }
    }
    
    func menuItemSelected(menuCelltype: MenuTabViewController.TypeCell) {
        
        switch menuCelltype {
        case .profile:
            router.routingToSelectedItem(menuCelltype: .profile)
        case .settings:
            router.routingToSelectedItem(menuCelltype: .settings)
        case .editForm:
            router.routingToSelectedItem(menuCelltype: .editForm)
        case .favorite:
            router.routingToSelectedItem(menuCelltype: .favorite)
        case .writeInBlog:
            router.routingToSelectedItem(menuCelltype: .writeInBlog)
        case .search:
            router.routingToSelectedItem(menuCelltype: .search)
        case .top100:
            router.routingToSelectedItem(menuCelltype: .top100)
        case .aboutUs:
            router.routingToSelectedItem(menuCelltype: .aboutUs)
        case .ourBlog:
            router.routingToSelectedItem(menuCelltype: .ourBlog)
        case .blogs:
            router.routingToSelectedItem(menuCelltype: .blogs)
        case .yourRecomendation:
            router.routingToSelectedItem(menuCelltype: .yourRecomendation)
        case .likeYou:
            router.routingToSelectedItem(menuCelltype: .likeYou)
        case .myReviews:
            router.routingToSelectedItem(menuCelltype: .myReviews)
        case .exit: interactor.needExit()
        
    
            // tab bar controller
        case .myMessages:
            view.switchTabBar(menuCelltype: .myMessages)
        case .myLikes:
            view.switchTabBar(menuCelltype: .myLikes)
        case .peopleOfDay:
            view.switchTabBar(menuCelltype: .peopleOfDay)
//        case .myReviews:
//            view.switchTabBar(menuCelltype: .myReviews)
            
        default:
            print("нет действия для выбранного пукта меню")
            break
        }
    }
    
    func showUserPage(_ userId: Int) {
        router.presentUserPage(userId)
    }
    
    func setInvisableSwitch(_ value: Bool) {
        interactor.toggleInvisableMode()
    }
    
    func exit() {
        router.showStartScreen()
    }
    
    func presentPaymentView(paymentTypeId: Int, entityId: Int) {
        router.presentPaymentPage(paymentTypeId: paymentTypeId, entityId: entityId)
    }
}

// MARK: -
// MARK: MenuTabInteractorOutput
extension MenuTabPresenter: MenuTabInteractorOutput {
    func menuBadges(_ badges: Badges) {
        self.menuBadges = badges
        view.reloadTableView()
    }
    
    func updateInvisableMode(visibleMode: Bool) {
//        superUser.is_invisible = visibleMode
        view.setVisableMode(visibleMode)
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func returnInterestingUsers(_ users: InterestingUsers) {
        view.setInterestingUsers(users)
    }
    
    func showStartScreen() {
        view.dismisView()
    }
    
}

// MARK: -
// MARK: MenuTabModuleInput
extension MenuTabPresenter: MenuTabModuleInput {
    
    
}
