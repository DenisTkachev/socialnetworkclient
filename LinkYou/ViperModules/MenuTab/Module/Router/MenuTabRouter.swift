//
//  MenuTabMenuTabRouter.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class MenuTabRouter: MenuTabRouterInput {

    enum StorybordsID: String {
        case personalPage = "PersonalPageSB"
        case settingsPage = "SettingsScreenSB"
        case editorRegistrationForm = "EditorRegistrationSB"
        case favoritesListPage = "FavoritesListSB"
        case myBlogAddPost = "MyBlogAddPostSB"
        case search = "SearchSB"
        case top100 = "Top100PageSB"
        case aboutUS = "AboutUsPageSB"
        case ourBlog = "OurBlogSB"
        case usersBlogs = "UsersBlogsSB"
        case matchedUsers = "UsersMatchedSB"
        case userPage = "UserpageSB"
        case registrationForm = "RegistrationSB"
        case linkyouLikeCards = "LinkYouLikeCardsSB"
        case myReviews = "MyReviewsSB"
        case paymentPage = "PaymentPageSB"
    }
    
    weak var transitionHandler: TransitionHandler!
    let cellType = MenuTabViewController.TypeCell.self
    
    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    
    func routingToSelectedItem(menuCelltype: MenuTabViewController.TypeCell) {
        switch menuCelltype {
        case .exit:
            print("exit");showStartScreen()
        case .profile: presentPersonalPage()
        case .settings: presentSettingsPage()
        case .editForm: presentEditFormPage()
        case .favorite: presentFavoritesListPage()
        case .writeInBlog: presentAddNewPost()
        case .search: presentSearch()
        case .top100: presentTop100()
        case .aboutUs: presentAboutUs()
        case .ourBlog: presentOurBlog()
        case .blogs: presentUsersBlogs()
        case .yourRecomendation: presentMatchedUser()
        case .likeYou: presentLinkYouLikeCards()
        case .myReviews: presentMyReview()
        default:
            break
        }
    }
    
    // MARK: PersonalPage
    func presentPersonalPage() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .personalPage), to: PersonalPageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
        //            .then {
        //                moduleInput in
        //                moduleInput.configure()
        //        }
    }
    
    // MARK: SettingsPage
    func presentSettingsPage() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .settingsPage), to: SettingsScreenModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
        //            .then {
        //                moduleInput in
        //                moduleInput.configure()
        //        }
    }
    
    // MARK: EditForm
    func presentEditFormPage() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .editorRegistrationForm), to: EditorRegistrationModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
        //            .then {
        //                moduleInput in
        //                moduleInput.configure()
        //        }
    }
    
    func presentFavoritesListPage() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .favoritesListPage), to: FavoritesListModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentAddNewPost() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .myBlogAddPost), to: MyBlogAddPostModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentSearch() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .search), to: SearchModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentTop100() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .top100), to: Top100PageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentAboutUs() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .aboutUS), to: AboutUsPageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentOurBlog() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .ourBlog), to: OurBlogModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentUsersBlogs() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .usersBlogs), to: UsersBlogsModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentMatchedUser() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .matchedUsers), to: UsersMatchedModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentUserPage(_ userId: Int) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .userPage), to: UserpageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            //            .perform()
            .then {
                moduleInput in
                moduleInput.configure(with: userId, allUser: [])
        }
    }
    
    func showStartScreen() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .registrationForm), to: RegistrationModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .present))
            .perform()
    }
    
    func presentLinkYouLikeCards() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .linkyouLikeCards), to: LinkYouLikeCardsModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.setData()
            }
    }
    
    func presentMyReview() {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .myReviews), to: MyReviewsModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .perform()
    }
    
    func presentPaymentPage(paymentTypeId: Int, entityId: Int) {
        try! transitionHandler
            .forStoryboard(factory: self.useFactory(storyboardID: .paymentPage), to: PaymentPageModuleInput.self)
            .to(preferred: TransitionStyle.navigation(style: .push))
            .then {
                moduleInput in
                moduleInput.getPaymentUrl(paymentTypeId: paymentTypeId, entityId: entityId)
        }
    }
    
}



