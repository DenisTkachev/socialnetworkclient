//
//  MenuTabMenuTabViewOutput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MenuTabViewOutput: class {
    /// - author: Denis Tkachev
    func viewIsReady()
    func menuItemSelected(menuCelltype: MenuTabViewController.TypeCell)
    func showUserPage(_ userId: Int)
    func setInvisableSwitch(_ value: Bool)
    func refreshUserData()
    func exit()
    var menuBadges: Badges? { get set }
    func loadBadges()
    func presentPaymentView(paymentTypeId: Int, entityId: Int)
}
