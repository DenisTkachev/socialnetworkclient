//
//  MenuTabMenuTabViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD

protocol CellMenuAction: class {
    func selectedUser(index: Int)
    func toggleInvisableMode(value: Bool)
    func pressPayBtn(type: TizerType)
}

//protocol UserType {
//    var user: User! { get set }
//}

final class MenuTabViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: -
    // MARK: Properties
    var output: MenuTabViewOutput!
    var user: User!
    var interestingUsers: InterestingUsers?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableView.register(UINib(nibName: "MenuAvatarTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuAvatarTableViewCell")
        tableView.register(UINib(nibName: "MenuInvisibleTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuInvisibleTableViewCell")
        tableView.register(UINib(nibName: "MenuTableViewCellFooterBtnCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCellFooterBtnCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func setUserData(user: User) {
        self.user = user
        if let name = user.name, let age = user.birthday?.age?.toString() {
            let element = ("\(name), \(age)", #imageLiteral(resourceName: "userPic-myLikes"), TypeCell.profile)
            section1.insert(element, at: 0)
        } else {
            let element = (user.name!, #imageLiteral(resourceName: "userPic-myLikes"), TypeCell.profile)
            section1.insert(element, at: 0)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
//        DispatchQueue.main.async {
//            self.output.refreshUserData()
//        }
        output.loadBadges()
//        tableView.reloadSections([3], with: .none) артефакт при появлении
    }
    
    var section1 = [("Редактировать анкету", #imageLiteral(resourceName: "menu-pensil"), TypeCell.editForm),
                    ("Мои сообщения", #imageLiteral(resourceName: "menu-msg"), .myMessages),
                    ("Мои лайки", #imageLiteral(resourceName: "menu-heart"), .myLikes),
                    ("Просмотры анкеты", #imageLiteral(resourceName: "menu-eye"), .myReviews),
                    ("Вам подходят", #imageLiteral(resourceName: "menu-suite"), .yourRecomendation),
                    ("Избранное", #imageLiteral(resourceName: "menu-favorites"), .favorite),
                    ("Написать в блог", #imageLiteral(resourceName: "menu-blog"), .writeInBlog),
                    ("Настройки аккаунта", #imageLiteral(resourceName: "menu-settings"), .settings),
                    ("Режим невидимости", #imageLiteral(resourceName: "menu-visibility-mode"), .invisableMode)]
    // premium btn == section2
    var section3 = [("Люди дня", #imageLiteral(resourceName: "menu-home"), TypeCell.peopleOfDay),
                    ("Поиск", #imageLiteral(resourceName: "menu-search"), .search),
                    ("Likeyou", #imageLiteral(resourceName: "menu-two-hearts"), .likeYou),
                    ("Блоги", #imageLiteral(resourceName: "menu-blogs"), .blogs),
                    ("Лучшие-100", #imageLiteral(resourceName: "menu-best100"), .top100),
                    ("О нас", #imageLiteral(resourceName: "menu-aboutUs"), .aboutUs),
                    ("Наш блог", #imageLiteral(resourceName: "menu-ourBlog"), .ourBlog),
                    ("Выход", #imageLiteral(resourceName: "menu-exit"), .exit),
                    ("Интересные анкеты", #imageLiteral(resourceName: "menu-crown"), .crownForm)]
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: return 44
        case 1: return 60 // premium btn
        case 2: //return 44
            if indexPath.row == section3.count - 1 {
                return 70
            } else {
                return 44
            }
        case 3: return 200
        case 4: return 80
        default: return 44
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let separator = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 25))
            let line = UIView(frame: CGRect(x: 0, y: separator.center.y, width: separator.frame.width - 32, height: 1))
            line.center.x = self.view.center.x
            line.backgroundColor = UIColor.paleGrey
            separator.addSubview(line)
            separator.backgroundColor = UIColor.white
            return separator
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 30
        } else {
            return CGFloat.leastNonzeroMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return section1.count
        case 1: return 1
        case 2: return section3.count
        case 3: return 1
        case 4: return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuAvatarTableViewCell", for: indexPath) as? MenuAvatarTableViewCell else { return UITableViewCell() }
                cell.nameItem.text = section1[indexPath.row].0
                cell.selectionStyle = .none
                if let user = SuperUser.shared.superUser {
                    cell.user = user
                } else {
                    cell.user = self.user
                }
                cell.cellType = section1[indexPath.row].2
                cell.setGuestMode(DeviceCurrent.current.guestModeStatus())
                return cell
                
            } else if indexPath.row == section1.count - 1 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuInvisibleTableViewCell", for: indexPath) as? MenuInvisibleTableViewCell else { return UITableViewCell() }
                cell.nameItem.text = section1[indexPath.row].0
                cell.selectionStyle = .none
                cell.user = self.user
                cell.cellType = section1[indexPath.row].2
                cell.itemImageView.image = section1[indexPath.row].1
                cell.tapDelegate = self
                cell.setupState()
                cell.setGuestMode(DeviceCurrent.current.guestModeStatus())
                return cell
                
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell else { return UITableViewCell() }
                cell.nameItem.text = section1[indexPath.row].0
                cell.selectionStyle = .none
                cell.user = self.user
                cell.cellType = section1[indexPath.row].2
                cell.itemImageView.image = section1[indexPath.row].1
                cell.setupBadge(output.menuBadges)
                cell.setGuestMode(DeviceCurrent.current.guestModeStatus())
                return cell
                
            }
            
        case 1: // button premium
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCellPremiumBtn", for: indexPath) as? MenuTableViewCellPremiumBtn else {
                return UITableViewCell() }
            cell.tapDelegate = self
            cell.selectionStyle = .none
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell else { return UITableViewCell() }
            cell.nameItem.text = section3[indexPath.row].0
            cell.itemImageView.image = section3[indexPath.row].1
            cell.itemCount.isHidden = true
            cell.selectionStyle = .none
            cell.cellType = section3[indexPath.row].2
            cell.setGuestMode(DeviceCurrent.current.guestModeStatus())
            if indexPath.row == section3.count - 1 {
                cell.separatorView.isHidden = false
                cell.nameItem.font = UIFont.boldSystemFont(ofSize: 16)
                cell.shiftCenter(isOn: true)
            }
            return cell
        case 3: // collectionView
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCollectionTableViewCell", for: indexPath) as! MenuCollectionTableViewCell
            cell.selectionStyle = .none
            cell.backgroundColor = .white
            if let interestingUsers = interestingUsers {
                cell.setupData(interestingUsers)
            }
            cell.delegate = self
            return cell
            
        case 4: // секция с кнопкой "попасть сюда"
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCellFooterBtnCell", for: indexPath) as? MenuTableViewCellFooterBtnCell else { return UITableViewCell() }
            cell.delegate = self
            if let howManyPeople = interestingUsers?.cntUsers {
                cell.countLabel.text = "\(howManyPeople) человек увидят вас"
            }
            
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    @IBAction func pressSearchBarItem(_ sender: UIBarButtonItem) {
        self.tabBarController!.selectedIndex = 2
    }
}

extension MenuTabViewController {
    
    enum TypeCell {
        case profile
        case editForm
        case myMessages
        case myLikes
        case myReviews
        case yourRecomendation
        case favorite
        case writeInBlog
        case settings
        case invisableMode
        case peopleOfDay
        case search
        case likeYou
        case blogs
        case top100
        case aboutUs
        case ourBlog
        case exit
        case crownForm
        case `default`
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? MenuTableViewCell, let cellType = cell.cellType {
            output.menuItemSelected(menuCelltype: cellType)
            print("Выбрали: \(String(describing: cell.nameItem.text))")
        } else if let cell = tableView.cellForRow(at: indexPath) as? MenuAvatarTableViewCell, let cellType = cell.cellType {
            output.menuItemSelected(menuCelltype: cellType)
            print("Выбрали: \(String(describing: cell.nameItem.text))")
        }
    }
}

// MARK: -
// MARK: MenuTabViewInput
extension MenuTabViewController: MenuTabViewInput {
    
    func setVisableMode(_ visibleMode: Bool) {
//        user.is_invisible = visibleMode
        let interestingSection = IndexSet(arrayLiteral: 9) // invisable switch
        tableView.reloadSections(interestingSection, with: .automatic)
    }
    
    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true) {
            HUD.hide()
            //            self.refreshControl.endRefreshing()
        }
    }
    
    func setInterestingUsers(_ users: InterestingUsers) {
        self.interestingUsers = users
        let interestingSection = IndexSet(arrayLiteral: 3)
        tableView.reloadSections(interestingSection, with: .automatic)
        
    }
    
    func switchTabBar(menuCelltype: MenuTabViewController.TypeCell) {
        switch menuCelltype {
        case .peopleOfDay:
            switchTab(tabNum: 0)
        case .myMessages:
            switchTab(tabNum: 1)
        case .myLikes:
            switchTab(tabNum: 3)
//        case .myReviews:
//            switchTab(tabNum: 3) // больше нет такого таба. Убрать из навигации
        default:
            print("Это не Tab")
            break
        }
    }
    
    func switchTab(tabNum: Int) {
        self.tabBarController!.selectedIndex = tabNum
    }
    
    func dismisView() {
//        HUD.show(.label("Сохраняю данные..."))
//        HUD.hide()
        output.exit()
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }
    
}

extension MenuTabViewController: CellMenuAction, PaymentPageVCType {    
    
    func showPaymentView(paymentTypeId: Int, entityId: Int) {
        output.presentPaymentView(paymentTypeId: paymentTypeId, entityId: entityId)
    }
    
    func pressPayBtn(type: TizerType) {
        let _ = PopUpPaymentVCStep1.init(tizerType: type)
    }
    
    func selectedUser(index: Int) {
        output.showUserPage(index)
    }
    
    func toggleInvisableMode(value: Bool) {
        if SuperUser.shared.currentUser?.is_stealth ?? false {
            output.setInvisableSwitch(value)
        } else {
            let _ = PopUpPaymentVCStep1.init(tizerType: .invisible)
        }
    }
}
