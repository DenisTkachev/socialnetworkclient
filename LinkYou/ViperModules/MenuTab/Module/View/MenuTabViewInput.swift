//
//  MenuTabMenuTabViewInput.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol MenuTabViewInput: class {
    /// - author: Denis Tkachev
    func dismisView()
    func setUserData(user: User)
    func switchTabBar(menuCelltype: MenuTabViewController.TypeCell)
    func setInterestingUsers(_ users: InterestingUsers)
    func showAlert(title: String, msg: String)
    func setVisableMode(_ visibleMode: Bool)
    func reloadTableView()
}
