//
//  MenuInvisibleTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 19/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class MenuInvisibleTableViewCell: UITableViewCell {
    
    weak var tapDelegate: CellMenuAction!
    var cellType: MenuTabViewController.TypeCell?
    var user: User!
    
    @IBOutlet weak var invisableSwitch: UISwitch!
    @IBOutlet weak var nameItem: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var guestModeCover: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        guestModeCover.addTapGestureRecognizer {
            DeviceCurrent.current.showGuestModeAlert()
        }
    }
    
    override func prepareForReuse() {
        nameItem.isEnabled = true
        guestModeCover.isHidden = true
    }
    
    func setupState() {
        invisableSwitch.isOn = SuperUser.shared.currentUser?.is_stealth ?? false
    }
    
    @IBAction func invisableSwitchPressed(_ sender: UISwitch) {
//        if SuperUser.shared.currentUser?.is_invisible ?? false {
//            tapDelegate.toggleInvisableMode(value: sender.isOn)
//        } else {
//            tapDelegate.toggleInvisableMode(value: sender.isOn)
//        }
    }
    
    @IBAction func invisableSwitchPress(_ sender: UISwitch) {
        tapDelegate.toggleInvisableMode(value: sender.isOn)
        invisableSwitch.isOn = SuperUser.shared.currentUser?.is_stealth ?? false
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setGuestMode(_ status: Bool) {
        if status, let cellType = cellType {
            
            switch cellType { case .myMessages, .editForm, .myLikes, .myReviews, .invisableMode, .profile, .settings, .writeInBlog, .yourRecomendation, .favorite:
                nameItem.isEnabled = false
                guestModeCover.isHidden = false
                
            default:
                nameItem.isEnabled = true
                guestModeCover.isHidden = true
            }
        }
    }
}
