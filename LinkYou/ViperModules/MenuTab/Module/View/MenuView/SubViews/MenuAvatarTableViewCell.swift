//
//  MenuAvatarTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 19/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class MenuAvatarTableViewCell: UITableViewCell {

    //var tapDelegate: CellSelectedType?
    var cellType: MenuTabViewController.TypeCell?
    var user: User?
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameItem: UILabel!
    @IBOutlet weak var guestModeCover: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImage.clipsToBounds = true
        avatarImage.layer.cornerRadius = avatarImage.bounds.height / 2
        
        guestModeCover.addTapGestureRecognizer {
            DeviceCurrent.current.showGuestModeAlert()
        }
    }
    
    override func prepareForReuse() {
        nameItem.isEnabled = true
        guestModeCover.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if self.cellType == MenuTabViewController.TypeCell.profile {
            if let avatar = user?.avatar?.src?.square {
                avatarImage.isHidden = false
                let url = URL(string: avatar)
                avatarImage.sd_setImage(with: url)
                
                
            } else {
                avatarImage.isHidden = true
            }
        } else {
            avatarImage.isHidden = true
        }
        
    }
    
    func setGuestMode(_ status: Bool) {
        if status, let cellType = cellType {
            
            switch cellType { case .myMessages, .editForm, .myLikes, .myReviews, .invisableMode, .profile, .settings, .writeInBlog, .yourRecomendation, .favorite:
                nameItem.isEnabled = false
                guestModeCover.isHidden = false
                
            default:
                nameItem.isEnabled = true
                guestModeCover.isHidden = true
            }
        }
    }
    
}
