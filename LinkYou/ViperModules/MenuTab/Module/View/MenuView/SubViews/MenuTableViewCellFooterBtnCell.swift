//
//  MenuTableViewCellFooterBtnCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 20/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MenuTableViewCellFooterBtnCell: UITableViewCell {

    @IBOutlet weak var countLabel: UILabel!
    weak var delegate: CellMenuAction!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func pressAdwords(_ sender: RoundButtonCustom) {
        delegate.pressPayBtn(type: .interestingDatingForm)
    }
}
