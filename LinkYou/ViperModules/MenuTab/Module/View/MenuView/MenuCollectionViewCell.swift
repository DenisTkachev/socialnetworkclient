//
//  MenuCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 28.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userPicImageView: MyAvatarView!
    
    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func prepareForReuse() {
        userPicImageView.setStartSettings(hidden: true)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userPicImageView.contentMode = .scaleAspectFit
        self.userPicImageView.layer.masksToBounds = true
        self.backgroundColor = .white
    }
    
    func setAvatar(user: InterestingUsers?, indexPath: IndexPath) {
        if let user = user, let items = user.items, let avatar = items[indexPath.row].avatar?.src?.square {
            let imgUrl = URL(string: avatar)
            userPicImageView.userAvatar.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "no-photo-user")) { (image, error, cache, url) in
                self.userPicImageView.userAvatar.toRoundedImage()
                self.userPicImageView.addTunning()
                
                let userInfo = items[indexPath.row]
                
                if let premium = userInfo.is_premium {
                    self.userPicImageView.enablePremiumStatus(on: premium)
                }
                if let online = userInfo.is_online {
                    self.userPicImageView.enableOnlineStatus(on: online)
                }
                if let vip = userInfo.is_vip {
                    self.userPicImageView.enableGoldRing(on: vip)
                }
            }
        }
    }
}
