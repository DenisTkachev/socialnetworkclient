//
//  MenuCollectionTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 28.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import DeviceKit

fileprivate class ConstantSize {
    
    static let column: CGFloat = 3
    static let minLineSpacing: CGFloat = 10.0
    static let minItemSpacing: CGFloat = 0.0
    static let offsetTop: CGFloat = 0.0
    static let offsetLeft: CGFloat = 0.0
    static let offsetBottom: CGFloat = 0.0
    static let offsetRight: CGFloat = 0.0
    
    //    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
    //        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
    //        return totalWidth / column
    //    }
}

class MenuCollectionTableViewCell: UITableViewCell, UITabBarDelegate {
    
    let groupOfAllowedDevices: [DeviceKit.Device] = [.iPhoneSE, .simulator(.iPhoneSE)]
    let device = Device()
    
    var delegate: CellMenuAction!
    var users: InterestingUsers?
    
    var greenView = UIImageView()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionHeightConstr: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = UIEdgeInsets.init(
                top: ConstantSize.offsetTop,    // top
                left: ConstantSize.offsetLeft,    // left
                bottom: ConstantSize.offsetBottom,    // bottom
                right: ConstantSize.offsetRight     // right
            )
            
            layout.minimumInteritemSpacing = ConstantSize.minItemSpacing
            layout.minimumLineSpacing = ConstantSize.minLineSpacing
        }
        
        collectionView.isScrollEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData(_ users: InterestingUsers) {
        self.users = users
        collectionView.reloadItems(at: [IndexPath(item: 0, section: 0)])
//        collectionView.reloadData()
    }

}

extension MenuCollectionTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath == [0,0] { // кнопка +
            print("Добавить сюда мою анкету")
        } else {
            if let userId = users?.items?[indexPath.row].id {
                delegate.selectedUser(index: userId)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard let items = users?.items else { return 0 }
        
        if device.isOneOf(groupOfAllowedDevices) {
            if items.count >= 6 {
                return 6
            } else {
                return items.count
            }
        } else {
            if items.count >= 8 {
                return 8
            } else {
                return items.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath) as! MenuCollectionViewCell
        cell.backgroundColor = .white
        cell.setAvatar(user: users, indexPath: indexPath)
        
        if indexPath == [0,0] {
            let plusImageView = UIImageView(image: #imageLiteral(resourceName: "avatarPlusIcon"))
            greenView = UIImageView.init(frame: cell.frame)
            greenView.backgroundColor = UIColor.emerald.withAlphaComponent(0.5)
            DispatchQueue.main.async {
                self.greenView.scaleMe() // первый раз в момент создания анимируем
            }

            greenView.clipsToBounds = true
            greenView.layer.cornerRadius = cell.frame.width / 2
            cell.userPicImageView.addSubview(greenView)
            plusImageView.center = cell.center
            cell.userPicImageView.addSubview(plusImageView)
            
            if let url = SuperUser.shared.superUser?.avatar?.src?.square {
                let imgUrl = URL(string: url)
                cell.userPicImageView.userAvatar.sd_setImage(with: imgUrl) { (image, error, cache, url) in
                    cell.userPicImageView.userAvatar.toRoundedImage()
                    
                }
            }
        }
        
       
        return cell
    }
}

extension MenuCollectionTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
        //        let itemWidth = ConstantSize.getItemWidth(boundWidth: collectionView.bounds.size.width)
        //        return CGSize(width: itemWidth, height: itemWidth)
    }
}
