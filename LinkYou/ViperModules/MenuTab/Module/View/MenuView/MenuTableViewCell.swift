//
//  MenuTableViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 27.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class MenuTableViewCell: UITableViewCell {
    
    //var tapDelegate: CellSelectedType?
    var cellType: MenuTabViewController.TypeCell?
    var user: User!
    
    @IBOutlet weak var nameItem: UILabel!
    @IBOutlet weak var itemCount: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var centerConstraint: NSLayoutConstraint!
    @IBOutlet weak var separatorLineTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var guestModeCover: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        itemCount.layer.cornerRadius = 16
        itemCount.layer.masksToBounds = true
        
        guestModeCover.addTapGestureRecognizer {
            DeviceCurrent.current.showGuestModeAlert()
        }
        
    }
    
    override func prepareForReuse() {
        self.separatorView.isHidden = true
        self.nameItem.font = UIFont.systemFont(ofSize: 16)
        shiftCenter()
        nameItem.isEnabled = true
        guestModeCover.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func shiftCenter(isOn: Bool = false) {
        if isOn {
            centerConstraint.constant = separatorLineTopConstraint.constant
        } else {
            centerConstraint.constant = 0
        }
    }
    
    func setupBadge(_ badges: Badges?) {
        
        if let cellBadges = badges, let cellType = cellType {
            itemCount.isHidden = false
            switch cellType {
            case .myMessages where cellBadges.messages > 0:
               itemCount.text = String(cellBadges.messages)
            case .myLikes where cellBadges.likes > 0:
               itemCount.text = String(cellBadges.likes)
            
                
            default:
                itemCount.isHidden = true
                
            }
        }
    }
    
    func setGuestMode(_ status: Bool) {
        if status, let cellType = cellType {
            
            switch cellType { case .myMessages, .editForm, .myLikes, .myReviews, .invisableMode, .profile, .settings, .writeInBlog, .yourRecomendation, .favorite, .likeYou, .blogs:
                nameItem.isEnabled = false
                guestModeCover.isHidden = false
                
            default:
                nameItem.isEnabled = true
                guestModeCover.isHidden = true
            }
        }
    }
    
    
}
