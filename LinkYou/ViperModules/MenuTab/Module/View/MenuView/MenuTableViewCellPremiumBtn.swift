//
//  MenuTableViewCellPremiumBtn.swift
//  LinkYou
//
//  Created by Denis Tkachev on 28.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class MenuTableViewCellPremiumBtn: UITableViewCell {

    weak var tapDelegate: CellMenuAction!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 190, height: 39))
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.cerulean
        button.setTitle("Стать Premium", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        button.addTarget(self, action: #selector(pressPremium), for: .touchDown)
        self.addSubview(button)
        
        button.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        button.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 190).isActive = true
        button.heightAnchor.constraint(equalToConstant: 39).isActive = true
        button.layer.cornerRadius = 19.5
        button.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @objc func pressPremium() {
        tapDelegate.pressPayBtn(type: .premium)
    }
}
