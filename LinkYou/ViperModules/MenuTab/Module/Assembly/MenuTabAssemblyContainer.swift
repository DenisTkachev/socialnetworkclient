//
//  MenuTabMenuTabAssemblyContainer.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/07/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class MenuTabAssemblyContainer: Assembly {
    
	func assemble(container: Container) {
		container.register(MenuTabInteractor.self) { (r, presenter: MenuTabPresenter) in
			let interactor = MenuTabInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(MenuTabRouter.self) { (r, viewController: MenuTabViewController) in
			let router = MenuTabRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(MenuTabPresenter.self) { (r, viewController: MenuTabViewController) in
			let presenter = MenuTabPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(MenuTabInteractor.self, argument: presenter)
			presenter.router = r.resolve(MenuTabRouter.self, argument: viewController)
            presenter.superUser = SuperUser.shared.superUser
			return presenter
		}

		container.storyboardInitCompleted(MenuTabViewController.self) { r, viewController in
			viewController.output = r.resolve(MenuTabPresenter.self, argument: viewController)
		}
	}

}
