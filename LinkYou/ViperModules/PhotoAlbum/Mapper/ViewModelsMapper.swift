//
//  ViewModelsMapper.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18/12/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation

protocol ViewModelsMapper {
    func mapPhotoSection(photoArray: [UserPhotos], canAddNewPhoto: Bool) -> [PhotoSection]
}

class ViewModelsMapperImp: ViewModelsMapper {
    func mapPhotoSection(photoArray: [UserPhotos], canAddNewPhoto: Bool) -> [PhotoSection] {
        
        var photoSection = [PhotoSection]()
        if canAddNewPhoto {
            let photoPlaceholder = PhotoPlaceholderViewModel.init(placehoolder: "placeholderame")
            photoSection.append(PhotoSection.photoplaceHolder(photoPlaceholder))
        }
        
        let photos = photoArray.compactMap({ photo -> PhotoSection in
            let viewModel = PhotoViewModel.init(userPhoto: photo)
            return PhotoSection.photo(viewModel)
        })
        
        photoSection.append(contentsOf: photos)
        
        return photoSection
    }
}


enum PhotoSection {
    case photoplaceHolder(PhotoPlaceholderViewModel)
    case photo(PhotoViewModel)
}

//struct Photo: Codable {
//    let photoName: String
//}

struct PhotoPlaceholderViewModel {
    let placehoolder: String
}

struct PhotoViewModel {
    let userPhoto: UserPhotos
}
