//
//  PhotoAlbumPhotoAlbumViewOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

protocol PhotoAlbumViewOutput: class {
    /// - author: KlenMarket
    var userPhotos: [UserPhotos] { get set }
    var user: User! { get set }
    
    func sendClaimReport(_ data: ClaimReport)
    func sendUserLike(_ userID: Int)
    func sendPhotoLike(_ photoID: Int)
    func showMessageDialog(with user: User)
    func loadUserPhotos(userId: Int)
    func requestCountersGallery(_ idPhoto: Int)
    func deleteComment(_ commentId: Int, _ photoId: Int)
    func sendCommentForPhoto(id: Int, text: String)
    func uploadImage(image: UIImage)
    func deletePhoto(_ photoId: Int, _ indexPath: IndexPath)
    func setAvatar(_ id: Int)
    func removePhotosNow()
    func undoDeletePhoto(_ photoId: Int)
}
