//
//  PhotoAlbumPhotoAlbumViewInput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Foundation

protocol PhotoAlbumViewInput: class {
    /// - author: KlenMarket
    func reloadCollection()
    func showAlert(title: String, msg: String)
    func showClaimIdAlert(id: Int)
    func setCounters(likes: [PhotoLike], comments: [PhotoComment])
    func setUserPhoto()
    func isShowHUD(show: Bool)
    func hidePhoto(_ indexPath: IndexPath, _ flag: Bool)
}
