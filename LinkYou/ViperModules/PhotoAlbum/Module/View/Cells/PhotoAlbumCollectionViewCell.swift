//
//  PhotoAlbumCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 17/12/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoAlbumCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var editPencilBtn: UIButton!
    @IBOutlet weak var deletedView: UIView!
    
    weak var delegate: PhotoAlbumCellType!
    var photoId: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        deletedView.isHidden = true
    }
    
    @IBAction func pressEditPencilBtn(_ sender: UIButton) {
        delegate.pressEditBtn(photoId: photoId!, cell: self)
    
    }
    
    @IBAction func pressCancelBtn(_ sender: RoundButtonCustom) {
        delegate.pressCancelBtn(photoId: photoId!, cell: self)
    }
    
    func setup(_ photo: UserPhotos) {
        guard let photoSRC = photo.src?.default, let url = URL(string: photoSRC), let id = photo.id else { return }
        self.photoId = id
        photoView.sd_setImage(with: url) { (image, error, cache, url) in
            self.photoView.image = image
        }
        
        if photo.user_id == SuperUser.shared.superUser?.id {
            self.editPencilBtn.isHidden = false
        } else {
            self.editPencilBtn.isHidden = true
        }
    }
    
    func hidePhotoShowСap(isShow: Bool) {
        if isShow {
            print("показываем заглушку")
            deletedView.isHidden = false
        } else {
            print("скрываем заглушку")
            deletedView.isHidden = true
        }
    }
}
