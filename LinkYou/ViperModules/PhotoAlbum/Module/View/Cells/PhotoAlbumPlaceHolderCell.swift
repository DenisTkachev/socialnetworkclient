//
//  PhotoAlbumPlaceHolderCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18/12/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

class PhotoAlbumPlaceHolderCell: UICollectionViewCell {

    @IBOutlet weak var placeHolderImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
