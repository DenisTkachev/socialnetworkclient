//
//  PhotoAlbumPhotoAlbumViewController.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import UIKit
import PKHUD
import Photos

protocol PhotoAlbumCellType: class {
    func pressEditBtn(photoId: Int, cell: UICollectionViewCell)
    func pressCancelBtn(photoId: Int, cell: UICollectionViewCell)
}

final class PhotoAlbumViewController: UIViewController {

	// MARK: -
	// MARK: Properties
	var output: PhotoAlbumViewOutput!
    @IBOutlet weak var collectionView: UICollectionView!
    weak var photoGalleryController: PhotoGalleryViewController?
    
    private var mapper: ViewModelsMapper!
    var result: [PhotoSection]?
    var photoOwner = false // владелец фотографий
    
    // MARK: -
	// MARK: Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
        configureCollectionView()
        title = "Фотоальбом"
        mapper = ViewModelsMapperImp()
        reloadCollection()
	}
    
    override func viewWillDisappear(_ animated: Bool) {
        output.removePhotosNow()
        if let parentController = parent?.children.last as? PersonalPageViewController {
            parentController.output.viewIsReady() // refresh view
        }
    }
    
    func reloadCollection() {
        photoOwner = output.user.id == SuperUser.shared.superUser?.id
        result = mapper.mapPhotoSection(photoArray: output.userPhotos, canAddNewPhoto: photoOwner)
        collectionView.reloadData()
    }
    
    private struct Constants {
        static let photoCollectionViewCellReuseIdentifier = String(describing: PhotoAlbumCollectionViewCell.self)
        static let placeholderCollectionViewCellReuseIdentifier = String(describing: PhotoAlbumPlaceHolderCell.self)
        static let cellReuseIdentifiers = [String(describing: PhotoAlbumCollectionViewCell.self), String(describing: PhotoAlbumPlaceHolderCell.self)]
    }
    
    private func configureCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        for reuseId in Constants.cellReuseIdentifiers {
            registerCell(reuseId: reuseId, collectionView: collectionView)
        }
    }
    
    private func registerCell(reuseId: String, collectionView: UICollectionView) {
        let cellNib = UINib(nibName: reuseId, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: reuseId)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let widthView = view.bounds.width
            let itemWidth = (widthView / 2) - 21
            let itemHeight = CGFloat(110)
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
            layout.invalidateLayout()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath == [0,0], photoOwner {
            addPhoto()
        } else {
            let shift = (photoOwner) ? 1 : 0
            let photoGalleryController = PhotoGalleryViewController(userPhotos: output.userPhotos, selectedIndex: indexPath.row - shift, user: output.user)
            photoGalleryController.delegateParent = self
            self.present(photoGalleryController, animated: true)
        }
    }
    
    private func addPhoto() {
        showGalleryActionSheet()
    }
    
    private func showGalleryActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Камера", style: .default) { action in
            self.openCamera()
        }
        let photoGallery = UIAlertAction(title: "Галлерея", style: .default) { action in
            self.openPhotoLibrary()
        }
        actionSheet.addAction(photoGallery)
        actionSheet.addAction(camera)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func isShowHUD(show: Bool) {
        switch show {
        case true: HUD.show(.progress, onView: view)
        case false: HUD.hide()
        }
    }
}

// MARK: -
// MARK: PhotoAlbumViewInput
extension PhotoAlbumViewController: PhotoAlbumViewInput {

    func showAlert(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showClaimIdAlert(id: Int) {
        HUD.hide()
        showAlert(title: "", msg: "Номер Вашей жалобы \(id)")
    }

    func setCounters(likes: [PhotoLike], comments: [PhotoComment]) {
        photoGalleryController?.setCounters(likes: likes, comments: comments)
    }
    
    func setUserPhoto() {
        if let photoGallery = self.navigationController?.viewControllers.last?.presentedViewController as? PhotoGalleryViewController {
            photoGallery.photos = output.userPhotos
            photoGallery.updateCounters()
        }
    }
    
    private func showGalleryMoreSheet(photoId: Int, indexPath: IndexPath) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        if parent?.children.dropLast().last is PersonalPageViewController {
            let deletePhoto = UIAlertAction(title: "Удалить фотографию", style: .destructive) { action in
                self.output.deletePhoto(photoId, indexPath)
            }
            if photoOwner {
                actionSheet.addAction(deletePhoto)
            }
            let setAvatar = UIAlertAction(title: "Установить как фото профиля", style: .default) { action in
                self.output.setAvatar(photoId)
            }
            actionSheet.addAction(setAvatar)
            
        }
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }

    func hidePhoto(_ indexPath: IndexPath, _ flag: Bool) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? PhotoAlbumCollectionViewCell else { return }
        cell.hidePhotoShowСap(isShow: flag)
    }
    
}

extension PhotoAlbumViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return result?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isShowHUD(show: false)
        guard let result = result else { return UICollectionViewCell() }
        let model = result[indexPath.row]
        
        switch model {
        case .photoplaceHolder://(let placeholder):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.placeholderCollectionViewCellReuseIdentifier, for: indexPath) as! PhotoAlbumPlaceHolderCell
            
            return cell
            
        case .photo(let photo):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.photoCollectionViewCellReuseIdentifier, for: indexPath) as! PhotoAlbumCollectionViewCell
            cell.delegate = self
            cell.setup(photo.userPhoto)
            return cell
        }
    }
}

extension PhotoAlbumViewController: PhotoGalleryDelegateType {

    func updateUserPhotos(userId: Int) {
       output.loadUserPhotos(userId: userId)
    }

    func requestCountersGallery(_ idPhoto: Int) {
        return output.requestCountersGallery(idPhoto)
    }
    
    func pressButton(data: Any, actionType: UserpageViewController.ActionBtnType) {
        if let claimReport = data as? ClaimReport, actionType == .claimReport {
            claimReport.userId = output.user.id
            output.sendClaimReport(claimReport)
            HUD.show(.progress)
        } else if let user = data as? User, let userId = user.id, actionType == .sendLike {
            output.user = user //update local user data
            output.sendUserLike(userId)
        } else if let photoId = data as? Int, actionType == .sendPhotoLike {
            output.sendPhotoLike(photoId)
        } else if actionType == .sendMessage, let user = data as? User {
            output.showMessageDialog(with: user)
        }
    }
    
    func showPhotoComments(view: PhotoCommentView) {
//        not uses
    }
    
    
}

extension PhotoAlbumViewController: PhotoCommentType {
    func showUserPage(id: Int) {
        print("нет реализации метода")
    }
    
    func setLikePhoto(photoId: Int) {
        output.sendPhotoLike(photoId)
    }

    func deleteComment(commentId: Int, photoId: Int) {
        output.deleteComment(commentId, photoId)
    }

    func sendComment(id: Int, text: String) {
        output.sendCommentForPhoto(id: id, text: text)
    }
    
    
}

extension PhotoAlbumViewController: PhotoAlbumCellType {
    
    func pressCancelBtn(photoId: Int, cell: UICollectionViewCell) {
        guard let delCellIndexPath = collectionView.indexPath(for: cell) else { return }
        hidePhoto(delCellIndexPath, false)
        output.undoDeletePhoto(photoId)
    }
    
    func pressEditBtn(photoId: Int, cell: UICollectionViewCell) {
        guard let delCellIndexPath = collectionView.indexPath(for: cell) else { return }
        showGalleryMoreSheet(photoId: photoId, indexPath: delCellIndexPath)
    }
    
}





extension PhotoAlbumViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // TODO: Вынести работу с фото в отдельный сервис
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            imagePicker.navigationBar.isTranslucent = false
            imagePicker.navigationBar.barTintColor = .cerulean
            imagePicker.navigationBar.tintColor = .white
            imagePicker.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        checkPermission()
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        //        imagePicked.image = image
        // send image data to server and ger url, then send url and get id
        output.uploadImage(image: image)
        //let newPhoto = UserPhotos.init(id: 0, user_id: user.id!, deleted: 0, datetime: Date().toString(), description: "", src: nil, isLiked: false)
        //userPhotos.append(newPhoto)
        //tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        
        dismiss(animated:true, completion: nil)
        HUD.show(.progress)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
