//
//  PhotoAlbumPhotoAlbumModuleInput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

protocol PhotoAlbumModuleInput: class {
    func configure(_ photos: [UserPhotos], user: User)
    
}
