//
//  PhotoAlbumPhotoAlbumPresenter.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import UIKit

final class PhotoAlbumPresenter: PhotoAlbumViewOutput {

    // MARK: -
    // MARK: Properties

    weak var view: PhotoAlbumViewInput!
    var interactor: PhotoAlbumInteractorInput!
    var router: PhotoAlbumRouterInput!
    var userPhotos: [UserPhotos] = []
    var user: User!
    var garbagePhotos: [Int] = []
    
    // MARK: -
    // MARK: PhotoAlbumViewOutput

    func sendClaimReport(_ data: ClaimReport) {
        interactor.sendClaimReport(data)
    }
    
    func sendUserLike(_ userID: Int) {
        interactor.sendLike(userID)
    }
    
    func sendPhotoLike(_ photoID: Int) {
        interactor.addPhotoLike(photoID)
    }
    
    func showMessageDialog(with user: User) {
        router.presentDialogView(user)
    }
    
    func loadUserPhotos(userId: Int) {
        let countOfPhoto = user.photos_count ?? 0
        interactor.loadUserPhotos(userID: userId, photoLimit: countOfPhoto)
    }
    
    func requestCountersGallery(_ idPhoto: Int) {
        return interactor.loadCounterForPhoto(idPhoto)
    }
    
    func deleteComment(_ commentId: Int, _ photoId: Int) {
        interactor.deletePhotoComment(commentId, photoId)
    }
    
    func sendCommentForPhoto(id: Int, text: String) {
        interactor.sendCommentForPhoto(id: id, text: text)
    }
    
    func uploadImage(image: UIImage) {
        if let data = image.jpegData(compressionQuality: 0.5) {
            interactor.uploadImageData(imageData: data)
            view.isShowHUD(show: true)
        } else {
            // convert picture error
        }
    }
    
    func deletePhoto(_ photoId: Int, _ indexPath: IndexPath) {
        garbagePhotos.append(photoId)
        view.hidePhoto(indexPath, true)
    }
    
    func undoDeletePhoto(_ photoId: Int) {
        garbagePhotos.removeAll { (photo) -> Bool in
            photo == photoId
        }
    }
    
    func setAvatar(_ id: Int) {
//        let selectedPhoto = userPhotos.filter { (photos) -> Bool in
//            photos.id == id
//        }
//        guard let newAvatar = selectedPhoto.first, let newAvatarUrl = newAvatar.src?.origin else { return }
        view.isShowHUD(show: true)
        interactor.setNewAvatar(id)
    }
    
    func removePhotosNow() {
        DispatchQueue.main.async {
            for photoId in self.garbagePhotos {
                self.interactor.deletePhoto(photoId)
            }
        }
    }

}

// MARK: -
// MARK: PhotoAlbumInteractorOutput
extension PhotoAlbumPresenter: PhotoAlbumInteractorOutput {
    
    func photoWasDeleted(id: Int) {
       userPhotos = userPhotos.filter { (photo) -> Bool in
            photo.id != id
        }
    }
    
    func userPhotosUpload(_ userId: Int) {
        self.user = SuperUser.shared.superUser
        self.loadUserPhotos(userId: userId)
    }
    
    func claimIdGeted(id: Int) {
        view.showClaimIdAlert(id: id)
    }
    
    func showAlert(title: String, msg: String) {
        view.showAlert(title: title, msg: msg)
    }
    
    func likesAndCommentGeted(_ likes: [PhotoLike], _ comments: [PhotoComment]) {
        view.setCounters(likes: likes, comments: comments) // возврат установка на странице юзера лайков
    }
    
    func userPhotoUrlGeted(data: [UserPhotos]) {
        self.userPhotos = data
        self.view.setUserPhoto()
        self.view.reloadCollection()
        self.view.isShowHUD(show: false)
    }

    func avatarWasUpload() {
        view.isShowHUD(show: false)
    }
}

// MARK: -
// MARK: PhotoAlbumModuleInput
extension PhotoAlbumPresenter: PhotoAlbumModuleInput {
    func configure(_ photos: [UserPhotos], user: User) {
        self.userPhotos = photos
        self.user = user
    }
    
}
