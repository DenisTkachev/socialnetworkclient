//
//  PhotoAlbumPhotoAlbumRouter.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import LightRoute

final class PhotoAlbumRouter: PhotoAlbumRouterInput {

    enum StorybordsID: String {
        case dialog = "DialogsSB" // Возможно, это тут и не нужно, надо проверить весь функционал модуля ФотоАльбом на соотв. тех. требованиям.
    }
    
    
    
  weak var transitionHandler: TransitionHandler!

    private func useFactory(storyboardID to: StorybordsID) -> StoryboardFactoryProtocol {
        let storyboard = UIStoryboard(name: to.rawValue, bundle: Bundle.main)
        let moduleID = to
        
        let transitionModuleFactory: StoryboardFactoryProtocol = {
            let factory = StoryboardFactory(storyboard: storyboard, restorationId: moduleID.rawValue)
            return factory
        }()
        return transitionModuleFactory
    }
    
    func presentDialogView(_ user: User) {
//        try! transitionHandler
//            .forStoryboard(factory: self.useFactory(storyboardID: .dialog), to: DialogsModuleInput.self)
//            .to(preferred: TransitionStyle.navigation(style: .push))
//            .then {
//                moduleInput in
//                moduleInput.newConfigureDialog(user: user)
//        }
    }
    
}
