//
//  PhotoAlbumPhotoAlbumRouterInput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol PhotoAlbumRouterInput: class {
    func presentDialogView(_ user: User)
}
