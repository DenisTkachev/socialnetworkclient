//
//  PhotoAlbumPhotoAlbumInteractorOutput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol PhotoAlbumInteractorOutput: class {
    func showAlert(title: String, msg: String)
    func claimIdGeted(id: Int)
    func likesAndCommentGeted(_ likes: [PhotoLike], _ comments: [PhotoComment])
    func userPhotoUrlGeted(data: [UserPhotos])
    func userPhotosUpload(_ userId: Int)
    func photoWasDeleted(id: Int)
    func avatarWasUpload()
}
