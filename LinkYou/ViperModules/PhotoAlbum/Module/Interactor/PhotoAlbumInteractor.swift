//
//  PhotoAlbumPhotoAlbumInteractor.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//
import Moya

final class PhotoAlbumInteractor: PhotoAlbumInteractorInput {
    
    let dispatchGroup = DispatchGroup()
    weak var output: PhotoAlbumInteractorOutput!
    
    let debugPlagin = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
    let authPlagin = AccessTokenPlugin { () -> String in
        DeviceCurrent.current.sessionToken
    }
    
    func sendClaimReport(_ data: ClaimReport) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<ClaimTicketService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.sendTicket(id: data.userId, title: data.header, message: data.body, entity: data.ticketType.rawValue)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([String:Int].self, from: response.data)
                    guard let id = serverResponse["id"] else { return }
                    self.output.claimIdGeted(id: id)
                } catch let errorC {
                    if let errorLynkYou = errorC as? ErrorResponse {
                        do {
                            let bodyError = try JSONSerialization.data(withJSONObject: errorLynkYou, options: [])
                            print(bodyError)
                        } catch {
                            print(error)
                        }
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func sendLike(_ userId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<LikesService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.userLike(id: userId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    print(userFullData.first!)
                    //self.output.userDataGeted(data: userFullData)
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addPhotoLike(_ photoId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.userPhotoLike(id: photoId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    self.loadCounterForPhoto(photoId)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func loadUserPhotos(userID: Int, photoLimit: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let photosProvider = MoyaProvider<PhotosService>(plugins: [authPlagin])
        photosProvider.request(.userPhotos(id: userID, limit: photoLimit)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userPhotosUrl = try JSONDecoder().decode([UserPhotos].self, from: response.data)
                    self.output.userPhotoUrlGeted(data: userPhotosUrl)
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func deletePhotoComment(_ commentId: Int, _ photoId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.deletePhotoComment(commentId: commentId, photoId: photoId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    print(serverResponse)
                    self.loadCounterForPhoto(photoId)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func sendCommentForPhoto(id: Int, text: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin])
        usersProvider.request(.addComment(id: id, comment: text)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode(LikeServerRespone.self, from: response.data)
                    print(serverResponse)
                    self.loadCounterForPhoto(id)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    
    
    func loadCounterForPhoto(_ idPhoto: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        //request first
        var likes: [PhotoLike] = []
        var comments: [PhotoComment] = []
        dispatchGroup.enter()
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        
        usersProvider.request(.userPhotoComments(id: idPhoto)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([PhotoComment].self, from: response.data)
                    comments = serverResponse
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        // request second
        dispatchGroup.enter()
        usersProvider.request(.userPhotosLikes(id: idPhoto)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let serverResponse = try JSONDecoder().decode([PhotoLike].self, from: response.data)
                    likes = serverResponse
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
                
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        dispatchGroup.notify(queue: .main) {
            self.output.likesAndCommentGeted(likes, comments)
        }
    }
    
    // SEND image data and send image to user gallery
    func uploadImageData(imageData: Data) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let photosProvider = MoyaProvider<UploadPhotosService>(plugins: [debugPlagin, authPlagin])
        // send data image and get url
        photosProvider.request(.uploadUserPhoto(data: imageData)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userPhotos = try JSONDecoder().decode(ImageDataResponse.self, from: response.data)
                    if let photos = userPhotos.data {
                        for photo in photos {
                            if let url = photo.src {
                                self.addPhotoToGallery(url: url)
                            }
                        }
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func addPhotoToGallery(url: String) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        dispatchGroup.enter()
        let photosProvider = MoyaProvider<PhotosService>(plugins: [debugPlagin, authPlagin])
        photosProvider.request(.bindPhotoToGallery(url: url)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try JSONDecoder().decode([UserPhotos].self, from: response.data)
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        // updateUserInfo
        dispatchGroup.enter()
        guard let selfId = SuperUser.shared.superUser?.id else { return }
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        usersProvider.request(.userFullData(id: selfId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                    SuperUser.shared.save(user: userFullData)
                    self.dispatchGroup.leave()
                } catch {
                    print(error)
                    self.dispatchGroup.leave()
                }
            case let .failure(error):
                print(error)
                self.dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            guard let selfId = SuperUser.shared.superUser?.id else { return }
            self.output.userPhotosUpload(selfId)
        }
    }
    
    func deletePhoto(_ photoId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let usersProvider = MoyaProvider<PhotosService>(plugins: [authPlagin, debugPlagin])
        usersProvider.request(.deletePhoto(id: photoId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if response["done"] == true { // photo was deleted
//                        self.output.photoWasDeleted(id: photoId)
                        print("Фото была удалена \(photoId)")
                    }
                } catch {
                    print(error)
                    print("Ошибка удаления фотографии")
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func setNewAvatar(_ imageId: Int) {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        let photosProvider = MoyaProvider<UsersService>(plugins: [authPlagin, debugPlagin])
        
//        photosProvider.request(.convertPhotoToAvatar(url: avatarUrl)) { (result) in
//            switch result {
//            case .success(let response):
//                do {
//                    let specialAvatarUrl = try JSONDecoder().decode(ServerAvatarResponse.self, from: response.data)
//                    if specialAvatarUrl.error == nil {
//                        self.setAvatar(avatar: specialAvatarUrl)
//                    }
//                } catch {
//                    print(error)
//                    print("Ошибка установки аватарки")
//                }
//            case let .failure(error):
//                print(error)
//            }
//        }
        
        photosProvider.request(.setAvatar(photoId: imageId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let response = try JSONDecoder().decode([String:Bool].self, from: response.data)
                    if response["avatar"] == true {
                        self.updateUserData()
                        self.output.avatarWasUpload()
                    } else {
                        print("ошибка обновления аватара")
                        self.output.showAlert(title: "Ошибка", msg: "Не удалось обновить аватар")
                    }
                } catch {
                    print(error)
                    print("Ошибка установки аватарки")
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
//    private func setAvatar(avatar: ServerAvatarResponse) { // для установки аватара сразу передаём id картинки из галереи
//        guard let avatarUrl = avatar.data?.src else { return }
//
//        let photosProvider = MoyaProvider<UsersService>(plugins: [authPlagin, debugPlagin])
//
//        photosProvider.request(.setAvatar(avatarUrl: avatarUrl)) { (result) in
//            switch result {
//            case .success(let response):
//                do {
//                    let response = try JSONDecoder().decode(ServerStandartResponse.self, from: response.data)
//                    if response.key == true {
//                        self.updateUserData()
//                        self.output.avatarWasUpload()
//                    } else {
//                        self.output.showAlert(title: "Ошибка", msg: "Не удалось обновить аватар")
//                    }
//                } catch {
//                    print(error)
//                    print("Ошибка установки аватарки")
//                }
//            case let .failure(error):
//                print(error)
//            }
//        }
//    }
    
    func updateUserData() {
        if !Reachability.isConnectedToNetwork() {
            output.showAlert(title: "Ошибка сети", msg: "Отсутствует подключение к сети Интернет")
            return
        }
        guard let selfId = SuperUser.shared.superUser?.id else { return }
        let usersProvider = MoyaProvider<UsersService>(plugins: [authPlagin])
        usersProvider.request(.userFullData(id: selfId)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let userFullData = try JSONDecoder().decode(User.self, from: response.data)
                    SuperUser.shared.save(user: userFullData)
//                    self.output.updateHeader()
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
}
