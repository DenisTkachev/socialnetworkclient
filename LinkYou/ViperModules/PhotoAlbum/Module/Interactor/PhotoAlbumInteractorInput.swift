//
//  PhotoAlbumPhotoAlbumInteractorInput.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Foundation

protocol PhotoAlbumInteractorInput: class {
    func sendClaimReport(_ data: ClaimReport)
    func sendLike(_ userId: Int)
    func addPhotoLike(_ photoId: Int)
    func loadUserPhotos(userID: Int, photoLimit: Int)
    func loadCounterForPhoto(_ idPhoto: Int)
    func deletePhotoComment(_ commentId: Int, _ photoId: Int)
    func sendCommentForPhoto(id: Int, text: String)
    func uploadImageData(imageData: Data)
    func deletePhoto(_ photoId: Int)
    func setNewAvatar(_ imageId: Int)
    func updateUserData()
}
