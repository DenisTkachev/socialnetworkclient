//
//  PhotoAlbumPhotoAlbumAssemblyContainer.swift
//  LinkYou
//
//  Created by KlenMarket on 17/12/2018.
//  Copyright © 2018 KlenMarket. All rights reserved.
//

import Swinject
import SwinjectStoryboard

final class PhotoAlbumAssemblyContainer: Assembly {

	func assemble(container: Container) {
		container.register(PhotoAlbumInteractor.self) { (r, presenter: PhotoAlbumPresenter) in
			let interactor = PhotoAlbumInteractor()
			interactor.output = presenter

			return interactor
		}

		container.register(PhotoAlbumRouter.self) { (r, viewController: PhotoAlbumViewController) in
			let router = PhotoAlbumRouter()
			router.transitionHandler = viewController

			return router
		}

		container.register(PhotoAlbumPresenter.self) { (r, viewController: PhotoAlbumViewController) in
			let presenter = PhotoAlbumPresenter()
			presenter.view = viewController
			presenter.interactor = r.resolve(PhotoAlbumInteractor.self, argument: presenter)
			presenter.router = r.resolve(PhotoAlbumRouter.self, argument: viewController)

			return presenter
		}

		container.storyboardInitCompleted(PhotoAlbumViewController.self) { r, viewController in
			viewController.output = r.resolve(PhotoAlbumPresenter.self, argument: viewController)
		}
	}

}
