//
//  AppDelegate.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import Swinject
import WatchdogInspector
import Simplicity
import YandexMobileMetrica
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSSubscriptionObserver {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UserDefaults.standard.set("", forKey: Keys.sessionToken) // erase session
//              TWWatchdogInspector.start()
        
        Container.loggingFunction = nil // Simplest workaround. "Resolution failed" error, when it did not fail #328
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        switch DeviceCurrent.current.isActiveUser {
        case true:
            self.prepareStoryBoard(nameSB: "MainSB", viewWithIdentifier: "MainScreen")
        case false:
            self.prepareStoryBoard(nameSB: "RegistrationSB", viewWithIdentifier: "RegistrationSB")
        }
        
        self.window?.makeKeyAndVisible()
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = .white
        //        navigationBarAppearace.barTintColor = UIColor.cerulean
        //        navigationBarAppearace.backItem?.backBarButtonItem?.tintColor = .white
        
        YMMYandexMetrica.activate(with: YMMYandexMetricaConfiguration.init(apiKey: "2fefa4ae-1275-48e5-b617-827463f85910")!)
        DeviceCurrent.pushNotification.notificationCenter.delegate = DeviceCurrent.pushNotification // local push notification
        
        // OneSignalService
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "470abe54-5816-4ec8-a855-6d95ad57a3f2",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        // ***
        
        // Обзёрвер на удалённый пуш, метод делегат OSSubscriptionObserver
        OneSignal.add(self as OSSubscriptionObserver)
        
        
        return true
    }
    
    private func prepareStoryBoard(nameSB: String, viewWithIdentifier: String) {
        let storyboard = UIStoryboard(name: nameSB, bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: viewWithIdentifier)
        self.window?.rootViewController = initialViewController
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0 // удаляем все бейджи с иконки приложения
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:
        UserDefaults.standard.set("", forKey: Keys.sessionToken)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        return Simplicity.application(app, open: url, options: options)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return Simplicity.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    
    // OneSignal Delegate подписаться на протокол OSSubscriptionObserver
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            // get player ID
            stateChanges.to.userId
            
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
    }
    
    func getTopMostViewController() -> UIViewController? {
        return UIApplication.getTopMostViewController()
    }
    
}

