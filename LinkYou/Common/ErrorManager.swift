//
//  ErrorManager.swift
//  LinkYou
//
//  Created by Denis Tkachev on 23.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation

public let LIYNetworkingErrorDomain = "ru.LinkYou.KlenMarket"
public let MissingHTTPResponseError = 100
public let UnexpectedResponseError = 200
public let Unauthorized = 401
