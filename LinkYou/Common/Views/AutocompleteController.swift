//
//  AutocompleteController.swift
//  popUpView
//
//  Created by Denis Tkachev on 01/02/2019.
//  Copyright © 2019 Denis Tkachev. All rights reserved.
//

import UIKit
import Eureka

protocol AutocompleteControllerType: class {
    func returnSelectedValue(value: String, id: Int)
}

/// let popVC = AutocompleteController.init(mapObject: mapObject, rows: row, delegate: delegate)
/// self.present(popVC, animated: true, completion: nil)
class AutocompleteController: UIViewController {
    
    var popOverVC: UIPopoverPresentationController!
    
    var allRow: [AutocompleteModel]!
    var filteredRows: [AutocompleteModel]!
    
    weak var delegate: SaveFieldsType?
    weak var defaultDelegate: AutocompleteControllerType?
    var selectedRow: IndexPath?
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    init(mapObject: UIView, rows: [AutocompleteModel], delegate: SaveFieldsType?) {
        super.init(nibName: "AutocompleteView", bundle: nil)
        self.delegate = delegate
        self.view.backgroundColor = .white
        self.preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 32, height: 250)
        self.modalPresentationStyle = .popover
        self.filteredRows = rows
        self.allRow = rows

        if delegate == nil { // если не редактирование анкеты
            let any = AutocompleteModel(id: 0, title: "Все", highlighted: "", subtitle: "")
            self.allRow.insert(any, at: 0)
            self.filteredRows.insert(any, at: 0)
        }
        
        popOverVC = self.popoverPresentationController!
        popOverVC.delegate = self
        
        popOverVC.sourceView = mapObject
        popOverVC.sourceRect = mapObject.bounds
        
        tableView.dataSource = self
        tableView.delegate = self
        
        textField.delegate = self
        tableView.separatorStyle = .none
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        textField.becomeFirstResponder()
    }
    
}
extension AutocompleteController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

extension AutocompleteController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredRows.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        cell.textLabel!.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        cell.textLabel!.textColor = UIColor.charcoalGrey.withAlphaComponent(0.5)
        
        let row = filteredRows[indexPath.row]
        if let title = row.title, let subtitle = row.subtitle, subtitle.count > 0 {
            cell.textLabel!.text = "\(title), \(subtitle)"
        } else if let title = row.title {
            cell.textLabel!.text = "\(title)"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedRow = filteredRows[indexPath.row]
        
        if let cellObj = popOverVC.sourceView as? LabelTextFieldFormTableViewCell {
            if let title = selectedRow.title, let subtitle = selectedRow.subtitle {
                if subtitle.count > 0 {
                    cellObj.valueTextField.text = "\(title), \(subtitle)"
                } else {
                    cellObj.valueTextField.text = "\(title)"
                }
                cellObj.row.value = cellObj.valueTextField.text
                cellObj.id = filteredRows[indexPath.row].id?.toString()
            }
            cellObj.row.validate()
            if let rowTag = cellObj.baseRow.tag, let id = filteredRows[indexPath.row].id, delegate != nil {
                delegate!.preparePickerValueToSave(cellTag: rowTag, id: id)
            }
            self.dismiss(animated: true, completion: nil)
            
            
        } else if let parentView = popOverVC.sourceView as? AutocompleteControllerType {
            
            if let title = selectedRow.title, let subtitle = selectedRow.subtitle, let id = selectedRow.id {
                if subtitle.count > 0 {
                    parentView.returnSelectedValue(value: "\(title), \(subtitle)", id: id)
                } else {
                    parentView.returnSelectedValue(value: "\(title)", id: id)
                }
                self.dismiss(animated: true, completion: nil)
            }
        } else if defaultDelegate != nil, let title = selectedRow.title, let id = selectedRow.id {
            defaultDelegate?.returnSelectedValue(value: title, id: id)
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    private func updateTableView(predicate: String) {
        let filteredStrings = allRow.filter { (element) -> Bool in
            element.title!.localizedCaseInsensitiveContains(predicate)
        }
        filteredRows = filteredStrings
        tableView.reloadData()
    }
}

extension AutocompleteController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            updateTableView(predicate: String(text + string))
        }
        return true
    }
}
