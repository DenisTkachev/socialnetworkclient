//
//  InformationPopUpViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

protocol InformationPopUpType: class {
    func pressBtn()
}

class InformationPopUpViewController: UIViewController {

    weak var delegate: InformationPopUpType?
    enum InformationMessage {
        case emailConfirm
        case paymentSuccess
        case errorMessage
    }
    
    var type: InformationMessage?
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var mainTextLabel: UILabel!
    
    required init(type: InformationMessage, delegate: InformationPopUpType? = nil) {
        super.init(nibName: "InformationPopUpViewController", bundle: nil)
        self.modalPresentationStyle = .overCurrentContext
        let topView = AppDelegate().getTopMostViewController()
        topView!.present(self, animated: true, completion: nil)
        self.type = type
        self.delegate = delegate
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 3
        
    }

    override func viewWillAppear(_ animated: Bool) {
        guard let type = type else { return }
        self.setup(type)
    }
    
    @IBAction func pressOkBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.delegate?.pressBtn()
        })
    }

    private func setup(_ type: InformationMessage) {
        switch type {
        case .emailConfirm:
            pageTitleLabel.text = "Подтверждение почты"
            mainTextLabel.text = "Подтвердите регистрацию, нажав кнопку в письме, которое мы отправили вам на почту \n\(SuperUser.shared.superUser?.email ?? "")"
        
        case .paymentSuccess:
            pageTitleLabel.text = "Ты молодец!"
            mainTextLabel.text = "Теперь твои денеШки у нас ахах"
            
        case .errorMessage:
            pageTitleLabel.text = "УПС!"
            mainTextLabel.text = "Что-то пошло не так..."
        }
    }
    
}

extension InformationPopUpViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}
