//
//  StickersViewController.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/05/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

protocol StickersViewType: class {
    func returnSelectedSticker(sticker: UIImage) 
}

class StickersViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    private enum ViewTouchState {
        case began
        case moved
        case end, cancelled
    }
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstr: NSLayoutConstraint!
    
    weak var delegate: StickersViewType!
    var stickers = [UIImage]()
    
    var initialTouchPoint: CGPoint = CGPoint(x: 0, y: 0)
    
    init(delegate: StickersViewType) {
       super.init(nibName: "StickersViewController", bundle: nil)
        self.delegate = delegate
        self.modalPresentationStyle = .overCurrentContext

        let topView = AppDelegate().getTopMostViewController()
        topView!.present(self, animated: true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 10

        collectionView.register(UINib(nibName: "StickerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "StickerCollectionViewCell")

       let nameSticker = [
        "heart_smile-01",
        "heart_smile-02",
        "heart_smile-03",
        "heart_smile-04",
        "heart_smile-05",
        "heart_smile-06",
        "heart_smile-07",
        "heart_smile-08",
        "heart_smile-09",
        "heart_smile-10",
        "heart_smile-11",
        "heart_smile-12",
        "heart_smile-13",
        "heart_smile-14",
        "heart_smile-15",
        "heart_smile-16",
        "heart_smile-17",
        "heart_smile-18",
        "heart_smile-19",
        "heart_smile-20"]
        
        nameSticker.forEach { (picName) in
            guard let image = UIImage(named: picName) else { return }
            stickers.append(image)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        collectionViewHeightConstr.constant = 50.0 * (CGFloat(stickers.count / 4))
        
        return stickers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCollectionViewCell", for: indexPath) as! StickerCollectionViewCell
        cell.stickerImageView.image = stickers[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.returnSelectedSticker(sticker: stickers[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


extension StickersViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

extension StickersViewController: UIGestureRecognizerDelegate {
    
    private func touchHandler(state: ViewTouchState, touches: Set<UITouch>) {
        let touchPoint = touches.first?.location(in: self.view?.window)
        
        switch state {
        case .began:
            initialTouchPoint = touchPoint!
        case .moved:
            if touchPoint!.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint!.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        case .cancelled, .end:
            if touchPoint!.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchHandler(state: .began, touches: touches)
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchHandler(state: .moved, touches: touches)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchHandler(state: .end, touches: touches)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchHandler(state: .cancelled, touches: touches)
    }
    
}
