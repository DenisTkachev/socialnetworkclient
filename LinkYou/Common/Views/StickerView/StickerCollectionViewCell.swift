//
//  StickerCollectionViewCell.swift
//  LinkYou
//
//  Created by Denis Tkachev on 07/05/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class StickerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var stickerImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
