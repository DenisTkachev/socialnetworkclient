//
//  NoDataView.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/01/2019.
//  Copyright © 2019 klen. All rights reserved.
//

/*
private func setupNoDataView() {
    noDataView.isHidden = true
    noDataView.frame = self.view.frame
    noDataView.delegate = self
    noDataView.setText(title: "Тут пусто", body: "Пока ничиего нет для вас. Попробуйте заполнить больше полей анкеты.", buttonText: nil) // можно отправлять в редактор анкеты
    
    self.view.addSubview(noDataView)
}
 
 func showNoDataView(condition: Bool) {
    noDataView.setHidden(condition: condition)
    HUD.hide()
 }
 
*/

import UIKit

protocol NoDataViewDelegate: class {
    func buttonWasPressed()
}

class NoDataView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var mainTextLabel: UILabel!
    @IBOutlet weak private var mainButton: RoundButtonCustom!
    
    weak var delegate: NoDataViewDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("NoDataView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction private func pressBtn(_ sender: RoundButtonCustom) {
       delegate.buttonWasPressed()
    }
    
    public func setText(title: String, body: String, buttonText: String?) {
        mainTextLabel.text = nil
        if buttonText == nil {
            mainButton.isHidden = true
        } else {
            mainButton.isHidden = false
            mainButton.setTitle(buttonText!, for: .normal)
        }
        
        titleLabel.text = title
        let attributedString = NSMutableAttributedString(string: body)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        paragraphStyle.alignment = .center
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        mainTextLabel.attributedText = attributedString
        
    }
    
    public func setHidden(condition: Bool) {
        self.isHidden = condition
    }
}
