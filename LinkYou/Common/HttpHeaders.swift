//
//  HttpHeaders.swift
//  LinkYou
//
//  Created by Denis Tkachev on 25/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation

class HttpHeaders {
    static func prepareHeaders(_ headers: [AnyHashable:Any]) -> [XPaginationHeaders : Int] {
        var temp = [XPaginationHeaders:String]()
        
        temp[.XPaginationCurrent] = headers[XPaginationHeaders.XPaginationCurrent.rawValue] as? String
        temp[.XPaginationIsEnd] = headers[XPaginationHeaders.XPaginationIsEnd.rawValue] as? String
        temp[.XPaginationLimit] = headers[XPaginationHeaders.XPaginationLimit.rawValue] as? String
        temp[.XPaginationTotal] = headers[XPaginationHeaders.XPaginationTotal.rawValue] as? String
        
        var result = [XPaginationHeaders:Int]()
        for (key,value) in temp {
            result[key] = Int(value)
        }
        return result
    }
}
