//
//  ServicesAssembly.swift
//  LinkYou
//
//  Created by Denis Tkachev on 18.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

//import Swinject

// class ServicesAssembly: Assembly {
//    func assemble(container: Container) {
//        container.register(Remote.self) { _ in RemoteService() }
//        container.register(PersistentStack.self) { _ in AppDelegate.coredataStack! }
//        container.register(CameraServiceProtocol.self) { _ in CameraService() }
//        container.register(SteganographyServiceType.self) { _ in SteganographyService() }
//        container.register(LocationServiceType.self) { _ in AppDelegate.locationService }
//        container.register(UploadServiceType.self) { _ in AppDelegate.uploadService }
//        container.register(InternetChekerServiceType.self) { _ in InternetChekerService() }
//        container.register(VersionUpdateCheckType.self) { _ in VersionUpdateCheck() }
//        container.register(KeychainServiceType.self) { _ in KeychainService() }
//        container.register(UserDefaultsServiceType.self) { _ in UserDefaultsService() }
//        container.register(CommunicationServiceType.self) { _ in AppDelegate.communicationService }
//        container.register(SmsSenderType.self) { _ in SmsSender() }
//        container.register(CallTemplateServiceType.self) { _ in CallTemplateService() }
//    }
//}
