//
//  UIView+shake.swift
//  LinkYou
//
//  Created by Denis Tkachev on 26/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

public extension UIView {
    
    func shake(count : Float = 3, for duration : TimeInterval = 1.5, withTranslation translation : Float = 10) {
        
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = count
        animation.duration = duration/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: CGFloat(-translation), y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: CGFloat(translation), y: self.center.y))
        layer.add(animation, forKey: "shake")
        
    }
}

// yourView.shake(count: 5, for: 1.5, withTranslation: 10)
// yourView.shake()
