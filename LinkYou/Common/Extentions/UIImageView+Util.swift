//
//  UIImageView+Util.swift
//  LinkYou
//
//  Created by Denis Tkachev on 30.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func toRoundedImage() {
        guard let image = self.image else {
            return
        }
        DispatchQueue.main.async { [weak self]  in
            guard let strongSelf = self else { return }
            strongSelf.image = image
            strongSelf.roundedImage((self?.frame.width ?? 0.0) / 2 )
        }
    }

    func roundedImage(_ cornerRadius: CGFloat, withBorder: Bool = true) {
        self.layer.borderWidth = 0.0
        self.layer.masksToBounds = false
        self.layer.cornerRadius = cornerRadius
        if withBorder {
            self.layer.borderColor = UIColor.white.cgColor
        }
        clipsToBounds = true
    }
}
