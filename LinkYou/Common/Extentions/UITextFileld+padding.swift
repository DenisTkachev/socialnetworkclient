//
//  UITextFileld+padding.swift
//  LinkYou
//
//  Created by Denis Tkachev on 04/12/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UITextField {
    func setLeftPaddingPoints(_ amount: CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
//    func setTopPaddingPoints(_ amount: CGFloat) {
//        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
//        
//        self.rightView = paddingView
//        self.rightViewMode = .always
//    }
}

class PaddedTextField: UITextField {
    
    var paddingPlaceHolder = UIEdgeInsets(top: -37, left: 6, bottom: 11, right: 16)
    var padding = UIEdgeInsets(top: 6, left: 6, bottom: 11, right: 16)
    
//    func getPadding(plusExtraFor clearButtonMode: ViewMode) -> UIEdgeInsets {
//        var padding = UIEdgeInsets(top: 0, left: 16, bottom: 11, right: 16)
//
//        // Add additional padding on the right side when showing the clear button
//        if self.clearButtonMode == .always || self.clearButtonMode == clearButtonMode {
//            padding.right = 28
//        }
//
//        return padding
//    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
//        let padding = getPadding(plusExtraFor: .unlessEditing)
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        
        return bounds.inset(by: paddingPlaceHolder)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
//        let padding = getPadding(plusExtraFor: .whileEditing)
        return bounds.inset(by: padding)
    }
    
}
