//
//  TableView+completion.swift
//  LinkYou
//
//  Created by Denis Tkachev on 08.10.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}
