//
//  UITableViewCell+IndexPath.swift
//  LinkYou
//
//  Created by Denis Tkachev on 23/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UITableViewCell {
    func getIndexPath() -> IndexPath? {
        let superView = self.superview as? UITableView
        let index = superView?.indexPath(for: self)
        return index
    }
}

