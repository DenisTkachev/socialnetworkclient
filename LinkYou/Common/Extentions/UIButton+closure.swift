//
//  UIButton+closure.swift
//  LinkYou
//
//  Created by Denis Tkachev on 09/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

// в структуре нельзя к кнопке привязать @obc func обработку нажатия. Для отработки нажатия нужно замыкание
extension UIButton {
    
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var action :(() -> Void)?
        }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }
    
    @objc private func triggerActionHandleBlock() {
        self.actionHandleBlock()
    }
    
    func actionHandle(controlEvents control :UIControl.Event, ForAction action: @escaping () -> Void) {
        self.actionHandleBlock(action: action)
        self.addTarget(self, action: #selector(triggerActionHandleBlock), for: control)
    }
}
