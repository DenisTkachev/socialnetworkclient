//
//  Notification+name.swift
//  LinkYou
//
//  Created by Denis Tkachev on 12.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let savedBlogPost = Notification.Name("savedBlogPost")
    static let deletedBlogPost = Notification.Name("deletedBlogPost")
    static let photoDetailsWasUpdated = Notification.Name("photoDetailsWasUpdated")
    
    // Socket notification
    static let socketUserOnline = Notification.Name("socketUserOnline")
    static let socketMessageNew = Notification.Name("socketMessageNew")
    static let socketMessageIsRead = Notification.Name("socketMessageIsRead")
    
}
