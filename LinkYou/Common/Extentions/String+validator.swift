//
//  String+validator.swift
//  LinkYou
//
//  Created by Denis Tkachev on 21.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension String {
    
    func isValidEmailFormat() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.lowercased())
    }
    
}
