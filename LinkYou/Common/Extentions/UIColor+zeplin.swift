//
//  UIColor+zeplin.swift
//  LinkYou
//
//  Created by Denis Tkachev on 20.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
// Color palette

extension UIColor {
    
    @nonobjc class var cerulean: UIColor {
        return UIColor(red: 0.0, green: 123.0 / 255.0, blue: 223.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var paleGrey: UIColor {
        return UIColor(red: 233.0 / 255.0, green: 233.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var charcoalGrey: UIColor {
        return UIColor(red: 62.0 / 255.0, green: 63.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var steel: UIColor {
        return UIColor(red: 125.0 / 255.0, green: 125.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var veryLightBlue: UIColor {
        return UIColor(red: 236.0 / 255.0, green: 244.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var ratingGreen: UIColor {
        return UIColor(red: 0.0, green: 161.0 / 255.0, blue: 97.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var ratingLightBlue: UIColor {
        return UIColor(red: 42.0 / 255.0, green: 241.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var blueyGrey: UIColor {
        return UIColor(red: 183.0 / 255.0, green: 182.0 / 255.0, blue: 199.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var gunmetal: UIColor {
        return UIColor(red: 65.0 / 255.0, green: 66.0 / 255.0, blue: 87.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coral: UIColor {
        return UIColor(red: 254.0 / 255.0, green: 88.0 / 255.0, blue: 74.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var cloudyBlue: UIColor {
        return UIColor(red: 172.0 / 255.0, green: 167.0 / 255.0, blue: 201.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var paleGreyTwo: UIColor {
        return UIColor(red: 246.0 / 255.0, green: 248.0 / 255.0, blue: 251.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var steelTwo: UIColor {
        return UIColor(red: 120.0 / 255.0, green: 120.0 / 255.0, blue: 135.0 / 255.0, alpha: 0.5)
    }
    
    @nonobjc class var emerald: UIColor {
        return UIColor(red: 0.0, green: 161.0 / 255.0, blue: 97.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var tableBackGround: UIColor {
        return UIColor(red:0.96, green:0.97, blue:0.98, alpha:1.0)
    }
    
    @nonobjc class var brightSkyBlue: UIColor {
        return UIColor(red: 23.0 / 255.0, green: 184.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var ceruleanTwo: UIColor {
        return UIColor(red: 1.0 / 255.0, green: 124.0 / 255.0, blue: 224.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var palePurple: UIColor {
        return UIColor(red: 166.0 / 255.0, green: 161.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var azure: UIColor {
        return UIColor(red: 18.0 / 255.0, green: 169.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    }
    
}

// Sample text styles

extension UIFont {
    
    class var header: UIFont {
        return UIFont.systemFont(ofSize: 24.0, weight: .bold)
    }
    
}
