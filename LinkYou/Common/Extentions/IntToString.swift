//
//  IntToString.swift
//  LinkYou
//
//  Created by Denis Tkachev on 28.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation

extension Int {
    func toString() -> String {
        return String(self)
    }
}
