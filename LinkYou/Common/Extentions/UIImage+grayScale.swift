//
//  UIImage+grayScale.swift
//  LinkYou
//
//  Created by Denis Tkachev on 21/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
    // "покрасить" в серый
extension UIImage {

    var toGrayTone: UIImage? {
        let context = CIContext(options: nil)
        guard let currentFilter = CIFilter(name: "CIPhotoEffectNoir") else { return nil }
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        if let output = currentFilter.outputImage,
            let cgImage = context.createCGImage(output, from: output.extent) {
            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        }
        return nil
    }
}
