//
//  String+Weight.swift
//  LinkYou
//
//  Created by Denis Tkachev on 02.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//
import UIKit

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 17, weight: .bold)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}

//let formattedString = NSMutableAttributedString()
//formattedString
//    .bold("Bold Text")
//    .normal(" Normal Text ")
//    .bold("Bold Text")
//
//let lbl = UILabel()
//lbl.attributedText = formattedString
