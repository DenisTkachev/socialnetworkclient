//
//  UIView+gradient.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

@IBDesignable
class LineGradientView: UIView {

    @IBInspectable var InsideColor: UIColor = UIColor.clear
    @IBInspectable var OutsideColor: UIColor = UIColor.clear

    override func draw(_ rect: CGRect) {
        let start = CGPoint(x: self.bounds.maxX / 2, y: self.bounds.minY)
        let end = CGPoint(x: self.bounds.maxX / 2, y: self.bounds.maxY)
        let colors = [InsideColor.cgColor, OutsideColor.cgColor] as CFArray
        let gradient = CGGradient(colorsSpace: nil, colors: colors, locations: nil)
        UIGraphicsGetCurrentContext()!.drawLinearGradient(gradient!, start: start, end: end, options: CGGradientDrawingOptions.drawsBeforeStartLocation)
    }
}
