//
//  UIImageView+rotation.swift
//  LinkYou
//
//  Created by Denis Tkachev on 24/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func flip(open: Bool) {
        if open {
            self.transform = CGAffineTransform(rotationAngle: .pi)
        } else {
            self.transform = CGAffineTransform.identity
        }
    }
    
    func rotate180WithAnimation(open: Bool) {
        if open {
            UIView.animate(withDuration: 0.3) {
                self.transform = CGAffineTransform(rotationAngle: .pi)
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.transform = CGAffineTransform.identity
            }
        }
    }
    
}
