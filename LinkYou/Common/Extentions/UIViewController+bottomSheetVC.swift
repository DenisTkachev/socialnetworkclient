//
//  UIViewController+bottomSheetVC.swift
//  LinkYou
//
//  Created by Denis Tkachev on 12/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func addBottomSheetView(asChildViewController viewController: UIViewController) {
        // 1- Init bottomSheetVC
        
        
        // 2- Add bottomSheetVC as a child view
        self.addChild(viewController)
        self.view.addSubview(viewController.view)
        viewController.didMove(toParent: self)
        
        // 3- Adjust bottomSheet frame and initial position.
        let height = view.frame.height
        let width  = view.frame.width
        viewController.view.frame = CGRect(x: 0,y: self.view.frame.maxY,width: width,height: height)
    }
}
