//
//  UIImageView+pulse.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/12/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UIImageView {

    func scaleMe() {
        self.clipsToBounds = true
        var startPoint: CGPoint?
        
        startPoint = self.center
        let minSize = CGSize(width: 0, height: 0)
        
        UIView.animate(withDuration: 1.5, delay: 0, options: [.autoreverse, .repeat], animations: {
            self.frame = CGRect(origin: startPoint!, size: minSize)
            self.layer.cornerRadius = self.bounds.width / 2
        }, completion: nil)
    }
}




