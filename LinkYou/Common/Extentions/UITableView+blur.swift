//
//  UITableView+blur.swift
//  LinkYou
//
//  Created by Denis Tkachev on 11/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation
import UIKit

//extension UITableView {
//    func addBlurEffect() {
//        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = self.bounds
//        
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
//        self.addSubview(blurEffectView)
//
//    }
//    
//    /// Remove UIBlurEffect from UIView
//    func removeBlurEffect() {
//        let blurredEffectViews = self.subviews.filter{ $0 is UIVisualEffectView }
//        blurredEffectViews.forEach{ blurView in
//            blurView.removeFromSuperview()
//        }
//    }
//}
