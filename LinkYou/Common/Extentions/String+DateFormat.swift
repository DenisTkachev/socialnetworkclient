//
//  String+DateFormat.swift
//  LinkYou
//
//  Created by Denis Tkachev on 08/10/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation
extension String {
    
    func toDateHHmm() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let myString = formatter.date(from: self) else { return "date error" }
        formatter.dateFormat = "HH:mm"
        let myStringFormatter = formatter.string(from: myString)
        
        return myStringFormatter
    }

    func toyyyyMMdd() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let myDate = formatter.date(from: self) else { return "date error" }
        formatter.dateFormat = "d MMMM, yyyy"
        let myStringFormatter = formatter.string(from: myDate)
        return myStringFormatter
    }
    
    func toyyyy() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let myDate = formatter.date(from: self) else { return "date error" }
        formatter.dateFormat = "yyyy"
        let myStringFormatter = formatter.string(from: myDate)
        return myStringFormatter
    }
    
    func currentYear() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.dateFormat = "yyyy"
        let myStringFormatter = formatter.string(from: Date())
        return myStringFormatter
    }
    
    func todMMMM() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let myDate = formatter.date(from: self) else { return "date error" }
        formatter.dateFormat = "d MMMM"
        let myStringFormatter = formatter.string(from: myDate)
        return myStringFormatter
    }
    
    func tohhddYYYY() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let myDate = formatter.date(from: self) else { return "date error" }
        formatter.dateFormat = "HH:mm dd-YYYY"
        let myStringFormatter = formatter.string(from: myDate)
        return myStringFormatter
    }
    /// format: yyyy-MM-dd
    func toBirthday() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let myDate = formatter.date(from: self) else { return "date error" }
        formatter.dateFormat = "d MMMM yyyy"
        let myStringFormatter = formatter.string(from: myDate)
        return myStringFormatter
    }
    
}
