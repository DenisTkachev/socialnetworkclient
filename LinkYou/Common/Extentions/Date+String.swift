//
//  Date+String.swift
//  LinkYou
//
//  Created by Denis Tkachev on 19.08.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation
import SwiftDate

extension Date {
    func toString() -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: self + 3.hours)
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
//        formatter.dateFormat = "dd-MMM-yyyy"
        formatter.dateFormat = "HH:mm"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        return myStringafd
    }
    
    func toBirthdayString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: self + 3.hours)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "d MMMM yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        return myStringafd
    }
}

extension DateFormatter {
    static let iso8601Full: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // 2018-07-15 10:06:20
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 10800)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}
