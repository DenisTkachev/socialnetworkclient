//
//  MyAvatarImageView.swift
//  avatarImage
//
//  Created by Denis Tkachev on 15.09.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit
import SDWebImage

class MyAvatarView: UIView {
    
    let statusOnLine = UIImageView.init(image: #imageLiteral(resourceName: "avatarOnlineIndicator"))
    let favImage = UIImageView.init(image: #imageLiteral(resourceName: "avatarFavIndicator"))
    let goldRing = UIImageView.init(image: #imageLiteral(resourceName: "avatarGoldRing"))
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var userAvatar: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("MyAvatarView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.contentView.backgroundColor = UIColor.clear
        
    }
    
    func addTunning() {
        
        goldRing.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        self.addSubview(goldRing)
        
        favImage.frame = CGRect(x: 2, y: 6, width: 15, height: 15)
        favImage.isUserInteractionEnabled = true
        self.addSubview(favImage)
        
        statusOnLine.frame = CGRect(x: 2, y: 58, width: 15, height: 15)
        self.addSubview(statusOnLine)
        
    }
    
    func setStartSettings(hidden: Bool) {
        self.goldRing.isHidden = hidden
        self.favImage.isHidden = hidden
        self.statusOnLine.isHidden = hidden
        self.userAvatar.image = nil
    }
    
    func enableOnlineStatus(on: Bool) {
        self.statusOnLine.isHidden = !on
    }
    
    func enablePremiumStatus(on: Bool) {
        self.favImage.isHidden = !on
    }
    
    func enableGoldRing(on: Bool) {
        self.goldRing.isHidden = !on
    }
    
    func setAvatar(userData: User?) {
        guard let userData = userData else { return }
        if let urlPath = userData.avatar {
            guard let fullUrlPath = urlPath.src?.origin else { return }
            self.userAvatar.sd_setImage(with: URL(string: fullUrlPath)) { (image, error, cache, url) in
                self.userAvatar.toRoundedImage()
                self.addTunning()
            }
        } else {
            self.userAvatar.image = #imageLiteral(resourceName: "no-photo-user")
        }
        
        if let premium = userData.is_premium {
            self.enablePremiumStatus(on: premium)
        }
        
        if let online = userData.is_online {
            self.enableOnlineStatus(on: online)
        }
        
        if let vip = userData.is_vip {
            self.enableGoldRing(on: vip)
        }
    }
}


