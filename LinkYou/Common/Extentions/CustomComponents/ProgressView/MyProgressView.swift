//
//  MyProgressView.swift
//  ProgressIndicator
//
//  Created by Denis Tkachev on 27/12/2018.
//  Copyright © 2018 Denis Tkachev. All rights reserved.
//

import UIKit

class MyProgressView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var progressDelimiter: ProgressDelimiterView!
    @IBOutlet weak var progressLine: GradientView!
    @IBOutlet weak var progressLineWidthConstr: NSLayoutConstraint!
    
    @IBOutlet weak var delimiterPointConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("MyProgressView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        progressLine.gradientLayer.colors = [UIColor.brightSkyBlue.cgColor, UIColor.ceruleanTwo.cgColor]
        progressLine.gradientLayer.gradient = GradientPoint.leftRight.draw()
        progressLine.clipsToBounds = false
        progressLine.layer.cornerRadius = 3
        
    }
    
    func setDelimiter() {
        let range = contentView.bounds.width
        let position = (76 / 100) * range // 76 проходной балл
        self.delimiterPointConstraint.constant = position
        self.progressDelimiter.isHidden = false
    }
    
    func setProgress(value: CGFloat) {
        let range = contentView.bounds.width
        let position = (value / 100) * range
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
                self.progressLineWidthConstr.constant = position
            }, completion: nil)
        }
    }
    
}


@IBDesignable
class ProgressDelimiterView: UIImageView {
    
    let point = UIView()
    
    override init(image: UIImage?) {
        super.init(image: image)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func setup() {
        self.clipsToBounds = true
        self.backgroundColor = .white
        
        point.frame = CGRect(x: self.bounds.width / 3, y: self.bounds.origin.y, width: self.bounds.width / 3, height: self.bounds.height)
        point.backgroundColor = .paleGrey
        point.clipsToBounds = true
        point.layer.cornerRadius = 2
        
        self.addSubview(point)
        layoutIfNeeded()

    }
}

