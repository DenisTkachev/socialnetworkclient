//
//  UIImage+Alpha.swift
//  LinkYou
//
//  Created by Denis Tkachev on 02/12/2018.
//  Copyright © 2018 klen. All rights reserved.
//

import UIKit

extension UIImage {
    
    func alpha(_ value: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
