//
//  Constants.swift
//  LinkYou
//
//  Created by Denis Tkachev on 20.07.2018.
//  Copyright © 2018 klen. All rights reserved.
//

import Foundation

struct Keys {
    static var isActiveUser = "isActvarUser"
    static var sessionToken = "sessionToken"
}

struct Constants {
    static var photosFolder = "Photos"
    static var syncTimeInterval = 5.0
    
    struct ViewControllers {
        static var dashboard = "DashboardBarController"
        static var registration = "RegistrationViewController"
    }
}
