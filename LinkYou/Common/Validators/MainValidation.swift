//
//  MainValidation.swift
//  LinkYou
//
//  Created by Denis Tkachev on 13/11/2018.
//  Copyright © 2018 klen. All rights reserved.
//

//import UIKit

//let emailRegularExpression = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
//
//enum TypeOfMasks: String {
//    case amount = "*****"
//    case phone = "+7(***)***-**-**"
////    case passportSeries = "** **"
////    case passportNumber = "******"
//    case income = "*******"
////    case productNumber = "****"
//    case bdaydate = "**.**.****"
////    case bankCard = "**** **** **** ****"
////    case monthYear = "**/**"
////    case passportIssueWhere = "***-***"
//}
//
//enum ValidationRule {
//    case digital
//    case characters
//}

//extension String {
//    func validate(mask: TypeOfMasks) -> String {
//        let output = "" as NSMutableString
//        let mask = mask.rawValue.flatMap({String($0)})
//        let input = self.flatMap{(String($0))}
//
//        var inputIndex = 0
//        var maskIndex = 0
//
//        while inputIndex < input.count && maskIndex < mask.count {
//            switch mask[maskIndex] {
//            case "*":
//                let decimal = input[inputIndex]
//                output.append(String(decimal))
//                maskIndex += 1
//                inputIndex += 1
//            default:
//                output.append(String(mask[maskIndex]))
//                if input[inputIndex] == mask[maskIndex] {
//                    inputIndex += 1
//                }
//                maskIndex += 1
//            }
//        }
//        return output as String
//    }
//
//    func isSymbolsAllowedWithCirillyc() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "абвгдежзийклмнопрстуфхцчшщьъыэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЪЫЭУЮЯ- ").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isSymbolsAllowedWithCirillycIssuedBy() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "абвгдежзийклмнопрстуфхцчшщьъыэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЪЫЭУЮЯ- 1234567890").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isSymbolsAllowedWithCirillycForDData() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "абвгдежзийклмнопрстуфхцчшщьъыэюяАБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЬЪЫЭУЮЯ- ()1234567890.").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isCodeWordValid() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "абвгдежзийклмнопрстуфхцчшщьъыэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЪЫЭУЮЯ-1234567890").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isStreetValid() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "абвгдежзийклмнопрстуфхцчшщьъыэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЪЫЭУЮЯ-1234567890 ").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isSymbolsAllowedInEmail() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@.0123456789-_").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isSymbolsAllowedWithDecimal() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "0123456789").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isSymbolsAllowedWithDecimalForPassport() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "-0123456789").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isAllowedPhoneString() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "+() -0123456789").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isSymbolsAllowedWithPassportSeries() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: " 0123456789").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isSymbolsAllowedWithPassportIssueWhere() -> Bool {
//        let allowedSet = NSCharacterSet(charactersIn: "-0123456789").inverted
//        return self.rangeOfCharacter(from: allowedSet) == nil
//    }
//
//    func isEmailValid() -> Bool {
//        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegularExpression)
//        return emailPredicate.evaluate(with: self.lowercased())
//    }
//
//    func isValidEmailFormat(_ testStr: String) -> Bool {
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//        return emailTest.evaluate(with: testStr)
//    }

//    func check(_ rule: ValidationRule, maxLenght max: Int) -> Bool {
//        switch rule {
//        case .digital:
//            let notDigitalSet = CharacterSet.decimalDigits.inverted
//            let stringComponents = self.components(separatedBy: notDigitalSet)
//
//            if stringComponents.count > 1 {
//                return false
//            }
//        default:
//            return false
//        }
//
//        return true
//    }
//}

